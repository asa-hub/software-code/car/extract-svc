#!/bin/sh

cat /srv/asac/app/hubasac/extract-svc/scripts/cronjob
#1,11,21,31,41,51 * * * * sh /srv/hubasac/app/extract/scripts/runExtractor.sh

chmod +x /srv/asac/app/hubasac/extract-svc/scripts/runExtractor.sh
chmod +x /srv/asac/app/hubasac/extract-svc/scripts/cronjob    
#/etc/init.d/crond start  #redhat based servers like centos
#/etc/init.d/cron  start  #debian based servers like ubuntu
#rc-service crond start
exec crond -f  #for alpine linux

crontab /srv/asac/app/hubasac/extract-svc/scripts/cronjob