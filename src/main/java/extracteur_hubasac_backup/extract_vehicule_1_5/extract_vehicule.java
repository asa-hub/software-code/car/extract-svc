// ============================================================================
//
// Copyright (c) 2006-2015, Talend SA
//
// Ce code source a été automatiquement généré par_Talend Open Studio for ESB
// / Soumis à la Licence Apache, Version 2.0 (la "Licence") ;
// votre utilisation de ce fichier doit respecter les termes de la Licence.
// Vous pouvez obtenir une copie de la Licence sur
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Sauf lorsqu'explicitement prévu par la loi en vigueur ou accepté par écrit, le logiciel
// distribué sous la Licence est distribué "TEL QUEL",
// SANS GARANTIE OU CONDITION D'AUCUNE SORTE, expresse ou implicite.
// Consultez la Licence pour connaître la terminologie spécifique régissant les autorisations et
// les limites prévues par la Licence.


package extracteur_hubasac_backup.extract_vehicule_1_5;

import routines.Numeric;
import routines.DataOperation;
import routines.TalendDataGenerator;
import routines.TalendStringUtil;
import routines.TalendString;
import routines.StringHandling;
import routines.Relational;
import routines.TalendDate;
import routines.Mathematical;
import routines.system.*;
import routines.system.api.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.math.BigDecimal;
import java.io.ByteArrayOutputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.IOException;
import java.util.Comparator;
 





@SuppressWarnings("unused")

/**
 * Job: extract_vehicule Purpose: <br>
 * Description:  <br>
 * @author user@talend.com
 * @version 8.0.1.20211109_1610
 * @status DEV
 */
public class extract_vehicule implements TalendJob {

protected static void logIgnoredError(String message, Throwable cause) {
       System.err.println(message);
       if (cause != null) {
               cause.printStackTrace();
       }

}


	public final Object obj = new Object();

	// for transmiting parameters purpose
	private Object valueObject = null;

	public Object getValueObject() {
		return this.valueObject;
	}

	public void setValueObject(Object valueObject) {
		this.valueObject = valueObject;
	}
	
	private final static String defaultCharset = java.nio.charset.Charset.defaultCharset().name();

	
	private final static String utf8Charset = "UTF-8";
	//contains type for every context property
	public class PropertiesWithType extends java.util.Properties {
		private static final long serialVersionUID = 1L;
		private java.util.Map<String,String> propertyTypes = new java.util.HashMap<>();
		
		public PropertiesWithType(java.util.Properties properties){
			super(properties);
		}
		public PropertiesWithType(){
			super();
		}
		
		public void setContextType(String key, String type) {
			propertyTypes.put(key,type);
		}
	
		public String getContextType(String key) {
			return propertyTypes.get(key);
		}
	}
	
	// create and load default properties
	private java.util.Properties defaultProps = new java.util.Properties();
	// create application properties with default
	public class ContextProperties extends PropertiesWithType {

		private static final long serialVersionUID = 1L;

		public ContextProperties(java.util.Properties properties){
			super(properties);
		}
		public ContextProperties(){
			super();
		}

		public void synchronizeContext(){
			
			if(vehicule_RowSeparator != null){
				
					this.setProperty("vehicule_RowSeparator", vehicule_RowSeparator.toString());
				
			}
			
			if(vehicule_Header != null){
				
					this.setProperty("vehicule_Header", vehicule_Header.toString());
				
			}
			
			if(vehicule_File != null){
				
					this.setProperty("vehicule_File", vehicule_File.toString());
				
			}
			
			if(vehicule_FieldSeparator != null){
				
					this.setProperty("vehicule_FieldSeparator", vehicule_FieldSeparator.toString());
				
			}
			
			if(vehicule_Encoding != null){
				
					this.setProperty("vehicule_Encoding", vehicule_Encoding.toString());
				
			}
			
			if(pg_connexion_Port != null){
				
					this.setProperty("pg_connexion_Port", pg_connexion_Port.toString());
				
			}
			
			if(pg_connexion_Login != null){
				
					this.setProperty("pg_connexion_Login", pg_connexion_Login.toString());
				
			}
			
			if(pg_connexion_AdditionalParams != null){
				
					this.setProperty("pg_connexion_AdditionalParams", pg_connexion_AdditionalParams.toString());
				
			}
			
			if(pg_connexion_Schema != null){
				
					this.setProperty("pg_connexion_Schema", pg_connexion_Schema.toString());
				
			}
			
			if(pg_connexion_Server != null){
				
					this.setProperty("pg_connexion_Server", pg_connexion_Server.toString());
				
			}
			
			if(pg_connexion_Password != null){
				
					this.setProperty("pg_connexion_Password", pg_connexion_Password.toString());
				
			}
			
			if(pg_connexion_Database != null){
				
					this.setProperty("pg_connexion_Database", pg_connexion_Database.toString());
				
			}
			
		}
		
		//if the stored or passed value is "<TALEND_NULL>" string, it mean null
		public String getStringValue(String key) {
			String origin_value = this.getProperty(key);
			if(NULL_VALUE_EXPRESSION_IN_COMMAND_STRING_FOR_CHILD_JOB_ONLY.equals(origin_value)) {
				return null;
			}
			return origin_value;
		}

public String vehicule_RowSeparator;
public String getVehicule_RowSeparator(){
	return this.vehicule_RowSeparator;
}
public Integer vehicule_Header;
public Integer getVehicule_Header(){
	return this.vehicule_Header;
}
		public String vehicule_File;
		public String getVehicule_File(){
			return this.vehicule_File;
		}
		
public String vehicule_FieldSeparator;
public String getVehicule_FieldSeparator(){
	return this.vehicule_FieldSeparator;
}
public String vehicule_Encoding;
public String getVehicule_Encoding(){
	return this.vehicule_Encoding;
}
public String pg_connexion_Port;
public String getPg_connexion_Port(){
	return this.pg_connexion_Port;
}
public String pg_connexion_Login;
public String getPg_connexion_Login(){
	return this.pg_connexion_Login;
}
public String pg_connexion_AdditionalParams;
public String getPg_connexion_AdditionalParams(){
	return this.pg_connexion_AdditionalParams;
}
public String pg_connexion_Schema;
public String getPg_connexion_Schema(){
	return this.pg_connexion_Schema;
}
public String pg_connexion_Server;
public String getPg_connexion_Server(){
	return this.pg_connexion_Server;
}
public java.lang.String pg_connexion_Password;
public java.lang.String getPg_connexion_Password(){
	return this.pg_connexion_Password;
}
public String pg_connexion_Database;
public String getPg_connexion_Database(){
	return this.pg_connexion_Database;
}
	}
	protected ContextProperties context = new ContextProperties(); // will be instanciated by MS.
	public ContextProperties getContext() {
		return this.context;
	}
	private final String jobVersion = "1.5";
	private final String jobName = "extract_vehicule";
	private final String projectName = "EXTRACTEUR_HUBASAC_BACKUP";
	public Integer errorCode = null;
	private String currentComponent = "";
	
		private final java.util.Map<String, Object> globalMap = new java.util.HashMap<String, Object>();
        private final static java.util.Map<String, Object> junitGlobalMap = new java.util.HashMap<String, Object>();
	
		private final java.util.Map<String, Long> start_Hash = new java.util.HashMap<String, Long>();
		private final java.util.Map<String, Long> end_Hash = new java.util.HashMap<String, Long>();
		private final java.util.Map<String, Boolean> ok_Hash = new java.util.HashMap<String, Boolean>();
		public  final java.util.List<String[]> globalBuffer = new java.util.ArrayList<String[]>();
	

private RunStat runStat = new RunStat();

	// OSGi DataSource
	private final static String KEY_DB_DATASOURCES = "KEY_DB_DATASOURCES";
	
	private final static String KEY_DB_DATASOURCES_RAW = "KEY_DB_DATASOURCES_RAW";

	public void setDataSources(java.util.Map<String, javax.sql.DataSource> dataSources) {
		java.util.Map<String, routines.system.TalendDataSource> talendDataSources = new java.util.HashMap<String, routines.system.TalendDataSource>();
		for (java.util.Map.Entry<String, javax.sql.DataSource> dataSourceEntry : dataSources.entrySet()) {
			talendDataSources.put(dataSourceEntry.getKey(), new routines.system.TalendDataSource(dataSourceEntry.getValue()));
		}
		globalMap.put(KEY_DB_DATASOURCES, talendDataSources);
		globalMap.put(KEY_DB_DATASOURCES_RAW, new java.util.HashMap<String, javax.sql.DataSource>(dataSources));
	}
	
	public void setDataSourceReferences(List serviceReferences) throws Exception{
		
		java.util.Map<String, routines.system.TalendDataSource> talendDataSources = new java.util.HashMap<String, routines.system.TalendDataSource>();
		java.util.Map<String, javax.sql.DataSource> dataSources = new java.util.HashMap<String, javax.sql.DataSource>();
		
		for (java.util.Map.Entry<String, javax.sql.DataSource> entry : BundleUtils.getServices(serviceReferences,  javax.sql.DataSource.class).entrySet()) {
                    dataSources.put(entry.getKey(), entry.getValue());
                    talendDataSources.put(entry.getKey(), new routines.system.TalendDataSource(entry.getValue()));
		}

		globalMap.put(KEY_DB_DATASOURCES, talendDataSources);
		globalMap.put(KEY_DB_DATASOURCES_RAW, new java.util.HashMap<String, javax.sql.DataSource>(dataSources));
	}


private final java.io.ByteArrayOutputStream baos = new java.io.ByteArrayOutputStream();
private final java.io.PrintStream errorMessagePS = new java.io.PrintStream(new java.io.BufferedOutputStream(baos));

public String getExceptionStackTrace() {
	if ("failure".equals(this.getStatus())) {
		errorMessagePS.flush();
		return baos.toString();
	}
	return null;
}

private Exception exception;

public Exception getException() {
	if ("failure".equals(this.getStatus())) {
		return this.exception;
	}
	return null;
}

private class TalendException extends Exception {

	private static final long serialVersionUID = 1L;

	private java.util.Map<String, Object> globalMap = null;
	private Exception e = null;
	private String currentComponent = null;
	private String virtualComponentName = null;
	
	public void setVirtualComponentName (String virtualComponentName){
		this.virtualComponentName = virtualComponentName;
	}

	private TalendException(Exception e, String errorComponent, final java.util.Map<String, Object> globalMap) {
		this.currentComponent= errorComponent;
		this.globalMap = globalMap;
		this.e = e;
	}

	public Exception getException() {
		return this.e;
	}

	public String getCurrentComponent() {
		return this.currentComponent;
	}

	
    public String getExceptionCauseMessage(Exception e){
        Throwable cause = e;
        String message = null;
        int i = 10;
        while (null != cause && 0 < i--) {
            message = cause.getMessage();
            if (null == message) {
                cause = cause.getCause();
            } else {
                break;          
            }
        }
        if (null == message) {
            message = e.getClass().getName();
        }   
        return message;
    }

	@Override
	public void printStackTrace() {
		if (!(e instanceof TalendException || e instanceof TDieException)) {
			if(virtualComponentName!=null && currentComponent.indexOf(virtualComponentName+"_")==0){
				globalMap.put(virtualComponentName+"_ERROR_MESSAGE",getExceptionCauseMessage(e));
			}
			globalMap.put(currentComponent+"_ERROR_MESSAGE",getExceptionCauseMessage(e));
			System.err.println("Exception in component " + currentComponent + " (" + jobName + ")");
		}
		if (!(e instanceof TDieException)) {
			if(e instanceof TalendException){
				e.printStackTrace();
			} else {
				e.printStackTrace();
				e.printStackTrace(errorMessagePS);
				extract_vehicule.this.exception = e;
			}
		}
		if (!(e instanceof TalendException)) {
		try {
			for (java.lang.reflect.Method m : this.getClass().getEnclosingClass().getMethods()) {
				if (m.getName().compareTo(currentComponent + "_error") == 0) {
					m.invoke(extract_vehicule.this, new Object[] { e , currentComponent, globalMap});
					break;
				}
			}

			if(!(e instanceof TDieException)){
			}
		} catch (Exception e) {
			this.e.printStackTrace();
		}
		}
	}
}

			public void tFileList_1_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tFileInputDelimited_1_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tSchemaComplianceCheck_1_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tLogRow_2_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tMap_1_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tLogRow_4_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tDBOutput_1_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tFileCopy_1_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileCopy_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tLogRow_5_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tDBOutput_2_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tFileOutputDelimited_3_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tLogRow_3_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tMap_3_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tDBOutput_3_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tFileOutputDelimited_2_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tLogRow_1_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tMap_2_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tDBOutput_4_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tFileOutputDelimited_1_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tFileList_1_onSubJobError(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {

resumeUtil.addLog("SYSTEM_LOG", "NODE:"+ errorComponent, "", Thread.currentThread().getId()+ "", "FATAL", "", exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception),"");

			}
			public void tFileCopy_1_onSubJobError(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {

resumeUtil.addLog("SYSTEM_LOG", "NODE:"+ errorComponent, "", Thread.currentThread().getId()+ "", "FATAL", "", exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception),"");

			}
	

	public static class ContextBean {
		static String evaluate(String context, String contextExpression)
				throws IOException, javax.script.ScriptException {
			boolean isExpression = contextExpression.contains("+") || contextExpression.contains("(");
			final String prefix = isExpression ? "\"" : "";
			java.util.Properties defaultProps = new java.util.Properties();
			java.io.InputStream inContext = extract_vehicule.class.getClassLoader()
					.getResourceAsStream("extracteur_hubasac_backup/extract_vehicule_1_5/contexts/" + context + ".properties");
			if (inContext == null) {
				inContext = extract_vehicule.class.getClassLoader()
						.getResourceAsStream("config/contexts/" + context + ".properties");
			}
			try {
			    defaultProps.load(inContext);
			} finally {
			    inContext.close();
			}
			java.util.regex.Pattern pattern = java.util.regex.Pattern.compile("context.([\\w]+)");
			java.util.regex.Matcher matcher = pattern.matcher(contextExpression);

			while (matcher.find()) {
				contextExpression = contextExpression.replaceAll(matcher.group(0),
						prefix + defaultProps.getProperty(matcher.group(1)) + prefix);
			}
			if (contextExpression.startsWith("/services")) {
				contextExpression = contextExpression.replaceFirst("/services","");
            }
			return isExpression ? evaluateContextExpression(contextExpression) : contextExpression;
		}

		public static String evaluateContextExpression(String expression) throws javax.script.ScriptException {
			javax.script.ScriptEngineManager manager = new javax.script.ScriptEngineManager();
			javax.script.ScriptEngine engine = manager.getEngineByName("nashorn");
			// Add some import for Java
			expression = expression.replaceAll("System.getProperty", "java.lang.System.getProperty");
			return engine.eval(expression).toString();
		}

        public static String getContext(String context, String contextName, String jobName) throws Exception {

            String currentContext = null;
            String jobNameStripped = jobName.substring(jobName.lastIndexOf(".") + 1);

            boolean inOSGi = routines.system.BundleUtils.inOSGi();

            if (inOSGi) {
                java.util.Dictionary<String, Object> jobProperties = routines.system.BundleUtils.getJobProperties(jobNameStripped);

                if (jobProperties != null) {
                    currentContext = (String)jobProperties.get("context");
                }
            }

            return contextName.contains("context.") ? evaluate(currentContext == null ? context : currentContext, contextName) : contextName;
        }
    }








public static class row3Struct implements routines.system.IPersistableRow<row3Struct> {
    final static byte[] commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[0];
    static byte[] commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[0];
	protected static final int DEFAULT_HASHCODE = 1;
    protected static final int PRIME = 31;
    protected int hashCode = DEFAULT_HASHCODE;
    public boolean hashCodeDirty = true;

    public String loopKey;



	
			    public String code_assure;

				public String getCode_assure () {
					return this.code_assure;
				}
				
			    public String code_assureur;

				public String getCode_assureur () {
					return this.code_assureur;
				}
				
			    public String marque;

				public String getMarque () {
					return this.marque;
				}
				
			    public String modele;

				public String getModele () {
					return this.modele;
				}
				
			    public java.util.Date date_premiere_mise_circulation;

				public java.util.Date getDate_premiere_mise_circulation () {
					return this.date_premiere_mise_circulation;
				}
				
			    public String immatriculation;

				public String getImmatriculation () {
					return this.immatriculation;
				}
				
			    public String chassis;

				public String getChassis () {
					return this.chassis;
				}
				
			    public String usage;

				public String getUsage () {
					return this.usage;
				}
				
			    public String charge_utile;

				public String getCharge_utile () {
					return this.charge_utile;
				}
				
			    public int puissance_fiscale;

				public int getPuissance_fiscale () {
					return this.puissance_fiscale;
				}
				
			    public String remorque;

				public String getRemorque () {
					return this.remorque;
				}
				
			    public Integer nombre_portes;

				public Integer getNombre_portes () {
					return this.nombre_portes;
				}
				
			    public String immatriculation_remorque;

				public String getImmatriculation_remorque () {
					return this.immatriculation_remorque;
				}
				
			    public String source_energie;

				public String getSource_energie () {
					return this.source_energie;
				}
				
			    public Integer nombre_de_places;

				public Integer getNombre_de_places () {
					return this.nombre_de_places;
				}
				
			    public Integer cylindree;

				public Integer getCylindree () {
					return this.cylindree;
				}
				
			    public Integer double_commande;

				public Integer getDouble_commande () {
					return this.double_commande;
				}
				
			    public Integer responsabilite_civile;

				public Integer getResponsabilite_civile () {
					return this.responsabilite_civile;
				}
				
			    public Integer utilitaire;

				public Integer getUtilitaire () {
					return this.utilitaire;
				}
				
			    public Integer type_engin;

				public Integer getType_engin () {
					return this.type_engin;
				}
				
			    public Float poids_total_autorise_en_charge;

				public Float getPoids_total_autorise_en_charge () {
					return this.poids_total_autorise_en_charge;
				}
				
			    public java.util.Date date_extraction;

				public java.util.Date getDate_extraction () {
					return this.date_extraction;
				}
				
			    public java.util.Date date_depot;

				public java.util.Date getDate_depot () {
					return this.date_depot;
				}
				
			    public String Error_Message;

				public String getError_Message () {
					return this.Error_Message;
				}
				


	@Override
	public int hashCode() {
		if (this.hashCodeDirty) {
			final int prime = PRIME;
			int result = DEFAULT_HASHCODE;
	
						result = prime * result + ((this.immatriculation == null) ? 0 : this.immatriculation.hashCode());
					
    		this.hashCode = result;
    		this.hashCodeDirty = false;
		}
		return this.hashCode;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		final row3Struct other = (row3Struct) obj;
		
						if (this.immatriculation == null) {
							if (other.immatriculation != null)
								return false;
						
						} else if (!this.immatriculation.equals(other.immatriculation))
						
							return false;
					

		return true;
    }

	public void copyDataTo(row3Struct other) {

		other.code_assure = this.code_assure;
	            other.code_assureur = this.code_assureur;
	            other.marque = this.marque;
	            other.modele = this.modele;
	            other.date_premiere_mise_circulation = this.date_premiere_mise_circulation;
	            other.immatriculation = this.immatriculation;
	            other.chassis = this.chassis;
	            other.usage = this.usage;
	            other.charge_utile = this.charge_utile;
	            other.puissance_fiscale = this.puissance_fiscale;
	            other.remorque = this.remorque;
	            other.nombre_portes = this.nombre_portes;
	            other.immatriculation_remorque = this.immatriculation_remorque;
	            other.source_energie = this.source_energie;
	            other.nombre_de_places = this.nombre_de_places;
	            other.cylindree = this.cylindree;
	            other.double_commande = this.double_commande;
	            other.responsabilite_civile = this.responsabilite_civile;
	            other.utilitaire = this.utilitaire;
	            other.type_engin = this.type_engin;
	            other.poids_total_autorise_en_charge = this.poids_total_autorise_en_charge;
	            other.date_extraction = this.date_extraction;
	            other.date_depot = this.date_depot;
	            other.Error_Message = this.Error_Message;
	            
	}

	public void copyKeysDataTo(row3Struct other) {

		other.immatriculation = this.immatriculation;
	            	
	}




	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length, utf8Charset);
		}
		return strReturn;
	}
	
	private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		String strReturn = null;
		int length = 0;
        length = unmarshaller.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[2 * length];
   				}
			}
			unmarshaller.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
    
    private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(str == null) {
			marshaller.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
            marshaller.writeInt(byteArray.length);
            marshaller.write(byteArray);
    	}
    }

	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}
	
	private java.util.Date readDate(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = unmarshaller.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(unmarshaller.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }
    
    private void writeDate(java.util.Date date1, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(date1 == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeLong(date1.getTime());
    	}
    }
	private Integer readInteger(ObjectInputStream dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}
	
	private Integer readInteger(org.jboss.marshalling.Unmarshaller dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}

	private void writeInteger(Integer intNum, ObjectOutputStream dos) throws IOException{
		if(intNum == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeInt(intNum);
    	}
	}
	
	private void writeInteger(Integer intNum, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(intNum == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeInt(intNum);
    	}
	}

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.marque = readString(dis);
					
					this.modele = readString(dis);
					
					this.date_premiere_mise_circulation = readDate(dis);
					
					this.immatriculation = readString(dis);
					
					this.chassis = readString(dis);
					
					this.usage = readString(dis);
					
					this.charge_utile = readString(dis);
					
			        this.puissance_fiscale = dis.readInt();
					
					this.remorque = readString(dis);
					
						this.nombre_portes = readInteger(dis);
					
					this.immatriculation_remorque = readString(dis);
					
					this.source_energie = readString(dis);
					
						this.nombre_de_places = readInteger(dis);
					
						this.cylindree = readInteger(dis);
					
						this.double_commande = readInteger(dis);
					
						this.responsabilite_civile = readInteger(dis);
					
						this.utilitaire = readInteger(dis);
					
						this.type_engin = readInteger(dis);
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.poids_total_autorise_en_charge = null;
           				} else {
           			    	this.poids_total_autorise_en_charge = dis.readFloat();
           				}
					
					this.date_extraction = readDate(dis);
					
					this.date_depot = readDate(dis);
					
					this.Error_Message = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }
    
    public void readData(org.jboss.marshalling.Unmarshaller dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.marque = readString(dis);
					
					this.modele = readString(dis);
					
					this.date_premiere_mise_circulation = readDate(dis);
					
					this.immatriculation = readString(dis);
					
					this.chassis = readString(dis);
					
					this.usage = readString(dis);
					
					this.charge_utile = readString(dis);
					
			        this.puissance_fiscale = dis.readInt();
					
					this.remorque = readString(dis);
					
						this.nombre_portes = readInteger(dis);
					
					this.immatriculation_remorque = readString(dis);
					
					this.source_energie = readString(dis);
					
						this.nombre_de_places = readInteger(dis);
					
						this.cylindree = readInteger(dis);
					
						this.double_commande = readInteger(dis);
					
						this.responsabilite_civile = readInteger(dis);
					
						this.utilitaire = readInteger(dis);
					
						this.type_engin = readInteger(dis);
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.poids_total_autorise_en_charge = null;
           				} else {
           			    	this.poids_total_autorise_en_charge = dis.readFloat();
           				}
					
					this.date_extraction = readDate(dis);
					
					this.date_depot = readDate(dis);
					
					this.Error_Message = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.marque,dos);
					
					// String
				
						writeString(this.modele,dos);
					
					// java.util.Date
				
						writeDate(this.date_premiere_mise_circulation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.chassis,dos);
					
					// String
				
						writeString(this.usage,dos);
					
					// String
				
						writeString(this.charge_utile,dos);
					
					// int
				
		            	dos.writeInt(this.puissance_fiscale);
					
					// String
				
						writeString(this.remorque,dos);
					
					// Integer
				
						writeInteger(this.nombre_portes,dos);
					
					// String
				
						writeString(this.immatriculation_remorque,dos);
					
					// String
				
						writeString(this.source_energie,dos);
					
					// Integer
				
						writeInteger(this.nombre_de_places,dos);
					
					// Integer
				
						writeInteger(this.cylindree,dos);
					
					// Integer
				
						writeInteger(this.double_commande,dos);
					
					// Integer
				
						writeInteger(this.responsabilite_civile,dos);
					
					// Integer
				
						writeInteger(this.utilitaire,dos);
					
					// Integer
				
						writeInteger(this.type_engin,dos);
					
					// Float
				
						if(this.poids_total_autorise_en_charge == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeFloat(this.poids_total_autorise_en_charge);
		            	}
					
					// java.util.Date
				
						writeDate(this.date_extraction,dos);
					
					// java.util.Date
				
						writeDate(this.date_depot,dos);
					
					// String
				
						writeString(this.Error_Message,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }
    
    public void writeData(org.jboss.marshalling.Marshaller dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.marque,dos);
					
					// String
				
						writeString(this.modele,dos);
					
					// java.util.Date
				
						writeDate(this.date_premiere_mise_circulation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.chassis,dos);
					
					// String
				
						writeString(this.usage,dos);
					
					// String
				
						writeString(this.charge_utile,dos);
					
					// int
				
		            	dos.writeInt(this.puissance_fiscale);
					
					// String
				
						writeString(this.remorque,dos);
					
					// Integer
				
						writeInteger(this.nombre_portes,dos);
					
					// String
				
						writeString(this.immatriculation_remorque,dos);
					
					// String
				
						writeString(this.source_energie,dos);
					
					// Integer
				
						writeInteger(this.nombre_de_places,dos);
					
					// Integer
				
						writeInteger(this.cylindree,dos);
					
					// Integer
				
						writeInteger(this.double_commande,dos);
					
					// Integer
				
						writeInteger(this.responsabilite_civile,dos);
					
					// Integer
				
						writeInteger(this.utilitaire,dos);
					
					// Integer
				
						writeInteger(this.type_engin,dos);
					
					// Float
				
						if(this.poids_total_autorise_en_charge == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeFloat(this.poids_total_autorise_en_charge);
		            	}
					
					// java.util.Date
				
						writeDate(this.date_extraction,dos);
					
					// java.util.Date
				
						writeDate(this.date_depot,dos);
					
					// String
				
						writeString(this.Error_Message,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("code_assure="+code_assure);
		sb.append(",code_assureur="+code_assureur);
		sb.append(",marque="+marque);
		sb.append(",modele="+modele);
		sb.append(",date_premiere_mise_circulation="+String.valueOf(date_premiere_mise_circulation));
		sb.append(",immatriculation="+immatriculation);
		sb.append(",chassis="+chassis);
		sb.append(",usage="+usage);
		sb.append(",charge_utile="+charge_utile);
		sb.append(",puissance_fiscale="+String.valueOf(puissance_fiscale));
		sb.append(",remorque="+remorque);
		sb.append(",nombre_portes="+String.valueOf(nombre_portes));
		sb.append(",immatriculation_remorque="+immatriculation_remorque);
		sb.append(",source_energie="+source_energie);
		sb.append(",nombre_de_places="+String.valueOf(nombre_de_places));
		sb.append(",cylindree="+String.valueOf(cylindree));
		sb.append(",double_commande="+String.valueOf(double_commande));
		sb.append(",responsabilite_civile="+String.valueOf(responsabilite_civile));
		sb.append(",utilitaire="+String.valueOf(utilitaire));
		sb.append(",type_engin="+String.valueOf(type_engin));
		sb.append(",poids_total_autorise_en_charge="+String.valueOf(poids_total_autorise_en_charge));
		sb.append(",date_extraction="+String.valueOf(date_extraction));
		sb.append(",date_depot="+String.valueOf(date_depot));
		sb.append(",Error_Message="+Error_Message);
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row3Struct other) {

		int returnValue = -1;
		
						returnValue = checkNullsAndCompare(this.immatriculation, other.immatriculation);
						if(returnValue != 0) {
							return returnValue;
						}

					
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class row16Struct implements routines.system.IPersistableRow<row16Struct> {
    final static byte[] commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[0];
    static byte[] commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[0];

	
			    public String code_assure;

				public String getCode_assure () {
					return this.code_assure;
				}
				
			    public String code_assureur;

				public String getCode_assureur () {
					return this.code_assureur;
				}
				
			    public String marque;

				public String getMarque () {
					return this.marque;
				}
				
			    public String modele;

				public String getModele () {
					return this.modele;
				}
				
			    public java.util.Date date_premiere_mise_circulation;

				public java.util.Date getDate_premiere_mise_circulation () {
					return this.date_premiere_mise_circulation;
				}
				
			    public String immatriculation;

				public String getImmatriculation () {
					return this.immatriculation;
				}
				
			    public String chassis;

				public String getChassis () {
					return this.chassis;
				}
				
			    public String usage;

				public String getUsage () {
					return this.usage;
				}
				
			    public String charge_utile;

				public String getCharge_utile () {
					return this.charge_utile;
				}
				
			    public int puissance_fiscale;

				public int getPuissance_fiscale () {
					return this.puissance_fiscale;
				}
				
			    public String remorque;

				public String getRemorque () {
					return this.remorque;
				}
				
			    public Integer nombre_portes;

				public Integer getNombre_portes () {
					return this.nombre_portes;
				}
				
			    public String immatriculation_remorque;

				public String getImmatriculation_remorque () {
					return this.immatriculation_remorque;
				}
				
			    public String source_energie;

				public String getSource_energie () {
					return this.source_energie;
				}
				
			    public Integer nombre_de_places;

				public Integer getNombre_de_places () {
					return this.nombre_de_places;
				}
				
			    public Integer cylindree;

				public Integer getCylindree () {
					return this.cylindree;
				}
				
			    public Integer double_commande;

				public Integer getDouble_commande () {
					return this.double_commande;
				}
				
			    public Integer responsabilite_civile;

				public Integer getResponsabilite_civile () {
					return this.responsabilite_civile;
				}
				
			    public Integer utilitaire;

				public Integer getUtilitaire () {
					return this.utilitaire;
				}
				
			    public Integer type_engin;

				public Integer getType_engin () {
					return this.type_engin;
				}
				
			    public Float poids_total_autorise_en_charge;

				public Float getPoids_total_autorise_en_charge () {
					return this.poids_total_autorise_en_charge;
				}
				
			    public java.util.Date date_extraction;

				public java.util.Date getDate_extraction () {
					return this.date_extraction;
				}
				
			    public java.util.Date date_depot;

				public java.util.Date getDate_depot () {
					return this.date_depot;
				}
				
			    public String Error_Message;

				public String getError_Message () {
					return this.Error_Message;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length, utf8Charset);
		}
		return strReturn;
	}
	
	private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		String strReturn = null;
		int length = 0;
        length = unmarshaller.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[2 * length];
   				}
			}
			unmarshaller.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
    
    private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(str == null) {
			marshaller.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
            marshaller.writeInt(byteArray.length);
            marshaller.write(byteArray);
    	}
    }

	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}
	
	private java.util.Date readDate(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = unmarshaller.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(unmarshaller.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }
    
    private void writeDate(java.util.Date date1, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(date1 == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeLong(date1.getTime());
    	}
    }
	private Integer readInteger(ObjectInputStream dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}
	
	private Integer readInteger(org.jboss.marshalling.Unmarshaller dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}

	private void writeInteger(Integer intNum, ObjectOutputStream dos) throws IOException{
		if(intNum == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeInt(intNum);
    	}
	}
	
	private void writeInteger(Integer intNum, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(intNum == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeInt(intNum);
    	}
	}

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.marque = readString(dis);
					
					this.modele = readString(dis);
					
					this.date_premiere_mise_circulation = readDate(dis);
					
					this.immatriculation = readString(dis);
					
					this.chassis = readString(dis);
					
					this.usage = readString(dis);
					
					this.charge_utile = readString(dis);
					
			        this.puissance_fiscale = dis.readInt();
					
					this.remorque = readString(dis);
					
						this.nombre_portes = readInteger(dis);
					
					this.immatriculation_remorque = readString(dis);
					
					this.source_energie = readString(dis);
					
						this.nombre_de_places = readInteger(dis);
					
						this.cylindree = readInteger(dis);
					
						this.double_commande = readInteger(dis);
					
						this.responsabilite_civile = readInteger(dis);
					
						this.utilitaire = readInteger(dis);
					
						this.type_engin = readInteger(dis);
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.poids_total_autorise_en_charge = null;
           				} else {
           			    	this.poids_total_autorise_en_charge = dis.readFloat();
           				}
					
					this.date_extraction = readDate(dis);
					
					this.date_depot = readDate(dis);
					
					this.Error_Message = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }
    
    public void readData(org.jboss.marshalling.Unmarshaller dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.marque = readString(dis);
					
					this.modele = readString(dis);
					
					this.date_premiere_mise_circulation = readDate(dis);
					
					this.immatriculation = readString(dis);
					
					this.chassis = readString(dis);
					
					this.usage = readString(dis);
					
					this.charge_utile = readString(dis);
					
			        this.puissance_fiscale = dis.readInt();
					
					this.remorque = readString(dis);
					
						this.nombre_portes = readInteger(dis);
					
					this.immatriculation_remorque = readString(dis);
					
					this.source_energie = readString(dis);
					
						this.nombre_de_places = readInteger(dis);
					
						this.cylindree = readInteger(dis);
					
						this.double_commande = readInteger(dis);
					
						this.responsabilite_civile = readInteger(dis);
					
						this.utilitaire = readInteger(dis);
					
						this.type_engin = readInteger(dis);
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.poids_total_autorise_en_charge = null;
           				} else {
           			    	this.poids_total_autorise_en_charge = dis.readFloat();
           				}
					
					this.date_extraction = readDate(dis);
					
					this.date_depot = readDate(dis);
					
					this.Error_Message = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.marque,dos);
					
					// String
				
						writeString(this.modele,dos);
					
					// java.util.Date
				
						writeDate(this.date_premiere_mise_circulation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.chassis,dos);
					
					// String
				
						writeString(this.usage,dos);
					
					// String
				
						writeString(this.charge_utile,dos);
					
					// int
				
		            	dos.writeInt(this.puissance_fiscale);
					
					// String
				
						writeString(this.remorque,dos);
					
					// Integer
				
						writeInteger(this.nombre_portes,dos);
					
					// String
				
						writeString(this.immatriculation_remorque,dos);
					
					// String
				
						writeString(this.source_energie,dos);
					
					// Integer
				
						writeInteger(this.nombre_de_places,dos);
					
					// Integer
				
						writeInteger(this.cylindree,dos);
					
					// Integer
				
						writeInteger(this.double_commande,dos);
					
					// Integer
				
						writeInteger(this.responsabilite_civile,dos);
					
					// Integer
				
						writeInteger(this.utilitaire,dos);
					
					// Integer
				
						writeInteger(this.type_engin,dos);
					
					// Float
				
						if(this.poids_total_autorise_en_charge == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeFloat(this.poids_total_autorise_en_charge);
		            	}
					
					// java.util.Date
				
						writeDate(this.date_extraction,dos);
					
					// java.util.Date
				
						writeDate(this.date_depot,dos);
					
					// String
				
						writeString(this.Error_Message,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }
    
    public void writeData(org.jboss.marshalling.Marshaller dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.marque,dos);
					
					// String
				
						writeString(this.modele,dos);
					
					// java.util.Date
				
						writeDate(this.date_premiere_mise_circulation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.chassis,dos);
					
					// String
				
						writeString(this.usage,dos);
					
					// String
				
						writeString(this.charge_utile,dos);
					
					// int
				
		            	dos.writeInt(this.puissance_fiscale);
					
					// String
				
						writeString(this.remorque,dos);
					
					// Integer
				
						writeInteger(this.nombre_portes,dos);
					
					// String
				
						writeString(this.immatriculation_remorque,dos);
					
					// String
				
						writeString(this.source_energie,dos);
					
					// Integer
				
						writeInteger(this.nombre_de_places,dos);
					
					// Integer
				
						writeInteger(this.cylindree,dos);
					
					// Integer
				
						writeInteger(this.double_commande,dos);
					
					// Integer
				
						writeInteger(this.responsabilite_civile,dos);
					
					// Integer
				
						writeInteger(this.utilitaire,dos);
					
					// Integer
				
						writeInteger(this.type_engin,dos);
					
					// Float
				
						if(this.poids_total_autorise_en_charge == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeFloat(this.poids_total_autorise_en_charge);
		            	}
					
					// java.util.Date
				
						writeDate(this.date_extraction,dos);
					
					// java.util.Date
				
						writeDate(this.date_depot,dos);
					
					// String
				
						writeString(this.Error_Message,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("code_assure="+code_assure);
		sb.append(",code_assureur="+code_assureur);
		sb.append(",marque="+marque);
		sb.append(",modele="+modele);
		sb.append(",date_premiere_mise_circulation="+String.valueOf(date_premiere_mise_circulation));
		sb.append(",immatriculation="+immatriculation);
		sb.append(",chassis="+chassis);
		sb.append(",usage="+usage);
		sb.append(",charge_utile="+charge_utile);
		sb.append(",puissance_fiscale="+String.valueOf(puissance_fiscale));
		sb.append(",remorque="+remorque);
		sb.append(",nombre_portes="+String.valueOf(nombre_portes));
		sb.append(",immatriculation_remorque="+immatriculation_remorque);
		sb.append(",source_energie="+source_energie);
		sb.append(",nombre_de_places="+String.valueOf(nombre_de_places));
		sb.append(",cylindree="+String.valueOf(cylindree));
		sb.append(",double_commande="+String.valueOf(double_commande));
		sb.append(",responsabilite_civile="+String.valueOf(responsabilite_civile));
		sb.append(",utilitaire="+String.valueOf(utilitaire));
		sb.append(",type_engin="+String.valueOf(type_engin));
		sb.append(",poids_total_autorise_en_charge="+String.valueOf(poids_total_autorise_en_charge));
		sb.append(",date_extraction="+String.valueOf(date_extraction));
		sb.append(",date_depot="+String.valueOf(date_depot));
		sb.append(",Error_Message="+Error_Message);
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row16Struct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class row15Struct implements routines.system.IPersistableRow<row15Struct> {
    final static byte[] commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[0];
    static byte[] commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[0];

	
			    public String code_assure;

				public String getCode_assure () {
					return this.code_assure;
				}
				
			    public String code_assureur;

				public String getCode_assureur () {
					return this.code_assureur;
				}
				
			    public String marque;

				public String getMarque () {
					return this.marque;
				}
				
			    public String modele;

				public String getModele () {
					return this.modele;
				}
				
			    public java.util.Date date_premiere_mise_circulation;

				public java.util.Date getDate_premiere_mise_circulation () {
					return this.date_premiere_mise_circulation;
				}
				
			    public String immatriculation;

				public String getImmatriculation () {
					return this.immatriculation;
				}
				
			    public String chassis;

				public String getChassis () {
					return this.chassis;
				}
				
			    public String usage;

				public String getUsage () {
					return this.usage;
				}
				
			    public String charge_utile;

				public String getCharge_utile () {
					return this.charge_utile;
				}
				
			    public int puissance_fiscale;

				public int getPuissance_fiscale () {
					return this.puissance_fiscale;
				}
				
			    public String remorque;

				public String getRemorque () {
					return this.remorque;
				}
				
			    public Integer nombre_portes;

				public Integer getNombre_portes () {
					return this.nombre_portes;
				}
				
			    public String immatriculation_remorque;

				public String getImmatriculation_remorque () {
					return this.immatriculation_remorque;
				}
				
			    public String source_energie;

				public String getSource_energie () {
					return this.source_energie;
				}
				
			    public Integer nombre_de_places;

				public Integer getNombre_de_places () {
					return this.nombre_de_places;
				}
				
			    public Integer cylindree;

				public Integer getCylindree () {
					return this.cylindree;
				}
				
			    public Integer double_commande;

				public Integer getDouble_commande () {
					return this.double_commande;
				}
				
			    public Integer responsabilite_civile;

				public Integer getResponsabilite_civile () {
					return this.responsabilite_civile;
				}
				
			    public Integer utilitaire;

				public Integer getUtilitaire () {
					return this.utilitaire;
				}
				
			    public Integer type_engin;

				public Integer getType_engin () {
					return this.type_engin;
				}
				
			    public Float poids_total_autorise_en_charge;

				public Float getPoids_total_autorise_en_charge () {
					return this.poids_total_autorise_en_charge;
				}
				
			    public int c_status;

				public int getC_status () {
					return this.c_status;
				}
				
			    public java.util.Date c_date_mis_a_jour;

				public java.util.Date getC_date_mis_a_jour () {
					return this.c_date_mis_a_jour;
				}
				
			    public java.util.Date c_date_transfer;

				public java.util.Date getC_date_transfer () {
					return this.c_date_transfer;
				}
				
			    public String commentaires;

				public String getCommentaires () {
					return this.commentaires;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length, utf8Charset);
		}
		return strReturn;
	}
	
	private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		String strReturn = null;
		int length = 0;
        length = unmarshaller.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[2 * length];
   				}
			}
			unmarshaller.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
    
    private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(str == null) {
			marshaller.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
            marshaller.writeInt(byteArray.length);
            marshaller.write(byteArray);
    	}
    }

	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}
	
	private java.util.Date readDate(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = unmarshaller.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(unmarshaller.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }
    
    private void writeDate(java.util.Date date1, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(date1 == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeLong(date1.getTime());
    	}
    }
	private Integer readInteger(ObjectInputStream dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}
	
	private Integer readInteger(org.jboss.marshalling.Unmarshaller dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}

	private void writeInteger(Integer intNum, ObjectOutputStream dos) throws IOException{
		if(intNum == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeInt(intNum);
    	}
	}
	
	private void writeInteger(Integer intNum, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(intNum == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeInt(intNum);
    	}
	}

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.marque = readString(dis);
					
					this.modele = readString(dis);
					
					this.date_premiere_mise_circulation = readDate(dis);
					
					this.immatriculation = readString(dis);
					
					this.chassis = readString(dis);
					
					this.usage = readString(dis);
					
					this.charge_utile = readString(dis);
					
			        this.puissance_fiscale = dis.readInt();
					
					this.remorque = readString(dis);
					
						this.nombre_portes = readInteger(dis);
					
					this.immatriculation_remorque = readString(dis);
					
					this.source_energie = readString(dis);
					
						this.nombre_de_places = readInteger(dis);
					
						this.cylindree = readInteger(dis);
					
						this.double_commande = readInteger(dis);
					
						this.responsabilite_civile = readInteger(dis);
					
						this.utilitaire = readInteger(dis);
					
						this.type_engin = readInteger(dis);
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.poids_total_autorise_en_charge = null;
           				} else {
           			    	this.poids_total_autorise_en_charge = dis.readFloat();
           				}
					
			        this.c_status = dis.readInt();
					
					this.c_date_mis_a_jour = readDate(dis);
					
					this.c_date_transfer = readDate(dis);
					
					this.commentaires = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }
    
    public void readData(org.jboss.marshalling.Unmarshaller dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.marque = readString(dis);
					
					this.modele = readString(dis);
					
					this.date_premiere_mise_circulation = readDate(dis);
					
					this.immatriculation = readString(dis);
					
					this.chassis = readString(dis);
					
					this.usage = readString(dis);
					
					this.charge_utile = readString(dis);
					
			        this.puissance_fiscale = dis.readInt();
					
					this.remorque = readString(dis);
					
						this.nombre_portes = readInteger(dis);
					
					this.immatriculation_remorque = readString(dis);
					
					this.source_energie = readString(dis);
					
						this.nombre_de_places = readInteger(dis);
					
						this.cylindree = readInteger(dis);
					
						this.double_commande = readInteger(dis);
					
						this.responsabilite_civile = readInteger(dis);
					
						this.utilitaire = readInteger(dis);
					
						this.type_engin = readInteger(dis);
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.poids_total_autorise_en_charge = null;
           				} else {
           			    	this.poids_total_autorise_en_charge = dis.readFloat();
           				}
					
			        this.c_status = dis.readInt();
					
					this.c_date_mis_a_jour = readDate(dis);
					
					this.c_date_transfer = readDate(dis);
					
					this.commentaires = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.marque,dos);
					
					// String
				
						writeString(this.modele,dos);
					
					// java.util.Date
				
						writeDate(this.date_premiere_mise_circulation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.chassis,dos);
					
					// String
				
						writeString(this.usage,dos);
					
					// String
				
						writeString(this.charge_utile,dos);
					
					// int
				
		            	dos.writeInt(this.puissance_fiscale);
					
					// String
				
						writeString(this.remorque,dos);
					
					// Integer
				
						writeInteger(this.nombre_portes,dos);
					
					// String
				
						writeString(this.immatriculation_remorque,dos);
					
					// String
				
						writeString(this.source_energie,dos);
					
					// Integer
				
						writeInteger(this.nombre_de_places,dos);
					
					// Integer
				
						writeInteger(this.cylindree,dos);
					
					// Integer
				
						writeInteger(this.double_commande,dos);
					
					// Integer
				
						writeInteger(this.responsabilite_civile,dos);
					
					// Integer
				
						writeInteger(this.utilitaire,dos);
					
					// Integer
				
						writeInteger(this.type_engin,dos);
					
					// Float
				
						if(this.poids_total_autorise_en_charge == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeFloat(this.poids_total_autorise_en_charge);
		            	}
					
					// int
				
		            	dos.writeInt(this.c_status);
					
					// java.util.Date
				
						writeDate(this.c_date_mis_a_jour,dos);
					
					// java.util.Date
				
						writeDate(this.c_date_transfer,dos);
					
					// String
				
						writeString(this.commentaires,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }
    
    public void writeData(org.jboss.marshalling.Marshaller dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.marque,dos);
					
					// String
				
						writeString(this.modele,dos);
					
					// java.util.Date
				
						writeDate(this.date_premiere_mise_circulation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.chassis,dos);
					
					// String
				
						writeString(this.usage,dos);
					
					// String
				
						writeString(this.charge_utile,dos);
					
					// int
				
		            	dos.writeInt(this.puissance_fiscale);
					
					// String
				
						writeString(this.remorque,dos);
					
					// Integer
				
						writeInteger(this.nombre_portes,dos);
					
					// String
				
						writeString(this.immatriculation_remorque,dos);
					
					// String
				
						writeString(this.source_energie,dos);
					
					// Integer
				
						writeInteger(this.nombre_de_places,dos);
					
					// Integer
				
						writeInteger(this.cylindree,dos);
					
					// Integer
				
						writeInteger(this.double_commande,dos);
					
					// Integer
				
						writeInteger(this.responsabilite_civile,dos);
					
					// Integer
				
						writeInteger(this.utilitaire,dos);
					
					// Integer
				
						writeInteger(this.type_engin,dos);
					
					// Float
				
						if(this.poids_total_autorise_en_charge == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeFloat(this.poids_total_autorise_en_charge);
		            	}
					
					// int
				
		            	dos.writeInt(this.c_status);
					
					// java.util.Date
				
						writeDate(this.c_date_mis_a_jour,dos);
					
					// java.util.Date
				
						writeDate(this.c_date_transfer,dos);
					
					// String
				
						writeString(this.commentaires,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("code_assure="+code_assure);
		sb.append(",code_assureur="+code_assureur);
		sb.append(",marque="+marque);
		sb.append(",modele="+modele);
		sb.append(",date_premiere_mise_circulation="+String.valueOf(date_premiere_mise_circulation));
		sb.append(",immatriculation="+immatriculation);
		sb.append(",chassis="+chassis);
		sb.append(",usage="+usage);
		sb.append(",charge_utile="+charge_utile);
		sb.append(",puissance_fiscale="+String.valueOf(puissance_fiscale));
		sb.append(",remorque="+remorque);
		sb.append(",nombre_portes="+String.valueOf(nombre_portes));
		sb.append(",immatriculation_remorque="+immatriculation_remorque);
		sb.append(",source_energie="+source_energie);
		sb.append(",nombre_de_places="+String.valueOf(nombre_de_places));
		sb.append(",cylindree="+String.valueOf(cylindree));
		sb.append(",double_commande="+String.valueOf(double_commande));
		sb.append(",responsabilite_civile="+String.valueOf(responsabilite_civile));
		sb.append(",utilitaire="+String.valueOf(utilitaire));
		sb.append(",type_engin="+String.valueOf(type_engin));
		sb.append(",poids_total_autorise_en_charge="+String.valueOf(poids_total_autorise_en_charge));
		sb.append(",c_status="+String.valueOf(c_status));
		sb.append(",c_date_mis_a_jour="+String.valueOf(c_date_mis_a_jour));
		sb.append(",c_date_transfer="+String.valueOf(c_date_transfer));
		sb.append(",commentaires="+commentaires);
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row15Struct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class vehicule_valideStruct implements routines.system.IPersistableRow<vehicule_valideStruct> {
    final static byte[] commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[0];
    static byte[] commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[0];

	
			    public String code_assure;

				public String getCode_assure () {
					return this.code_assure;
				}
				
			    public String code_assureur;

				public String getCode_assureur () {
					return this.code_assureur;
				}
				
			    public String marque;

				public String getMarque () {
					return this.marque;
				}
				
			    public String modele;

				public String getModele () {
					return this.modele;
				}
				
			    public java.util.Date date_premiere_mise_circulation;

				public java.util.Date getDate_premiere_mise_circulation () {
					return this.date_premiere_mise_circulation;
				}
				
			    public String immatriculation;

				public String getImmatriculation () {
					return this.immatriculation;
				}
				
			    public String chassis;

				public String getChassis () {
					return this.chassis;
				}
				
			    public String usage;

				public String getUsage () {
					return this.usage;
				}
				
			    public String charge_utile;

				public String getCharge_utile () {
					return this.charge_utile;
				}
				
			    public int puissance_fiscale;

				public int getPuissance_fiscale () {
					return this.puissance_fiscale;
				}
				
			    public String remorque;

				public String getRemorque () {
					return this.remorque;
				}
				
			    public Integer nombre_portes;

				public Integer getNombre_portes () {
					return this.nombre_portes;
				}
				
			    public String immatriculation_remorque;

				public String getImmatriculation_remorque () {
					return this.immatriculation_remorque;
				}
				
			    public String source_energie;

				public String getSource_energie () {
					return this.source_energie;
				}
				
			    public Integer nombre_de_places;

				public Integer getNombre_de_places () {
					return this.nombre_de_places;
				}
				
			    public Integer cylindree;

				public Integer getCylindree () {
					return this.cylindree;
				}
				
			    public Integer double_commande;

				public Integer getDouble_commande () {
					return this.double_commande;
				}
				
			    public Integer responsabilite_civile;

				public Integer getResponsabilite_civile () {
					return this.responsabilite_civile;
				}
				
			    public Integer utilitaire;

				public Integer getUtilitaire () {
					return this.utilitaire;
				}
				
			    public Integer type_engin;

				public Integer getType_engin () {
					return this.type_engin;
				}
				
			    public Float poids_total_autorise_en_charge;

				public Float getPoids_total_autorise_en_charge () {
					return this.poids_total_autorise_en_charge;
				}
				
			    public int c_status;

				public int getC_status () {
					return this.c_status;
				}
				
			    public java.util.Date c_date_mis_a_jour;

				public java.util.Date getC_date_mis_a_jour () {
					return this.c_date_mis_a_jour;
				}
				
			    public java.util.Date c_date_transfer;

				public java.util.Date getC_date_transfer () {
					return this.c_date_transfer;
				}
				
			    public String commentaires;

				public String getCommentaires () {
					return this.commentaires;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length, utf8Charset);
		}
		return strReturn;
	}
	
	private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		String strReturn = null;
		int length = 0;
        length = unmarshaller.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[2 * length];
   				}
			}
			unmarshaller.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
    
    private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(str == null) {
			marshaller.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
            marshaller.writeInt(byteArray.length);
            marshaller.write(byteArray);
    	}
    }

	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}
	
	private java.util.Date readDate(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = unmarshaller.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(unmarshaller.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }
    
    private void writeDate(java.util.Date date1, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(date1 == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeLong(date1.getTime());
    	}
    }
	private Integer readInteger(ObjectInputStream dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}
	
	private Integer readInteger(org.jboss.marshalling.Unmarshaller dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}

	private void writeInteger(Integer intNum, ObjectOutputStream dos) throws IOException{
		if(intNum == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeInt(intNum);
    	}
	}
	
	private void writeInteger(Integer intNum, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(intNum == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeInt(intNum);
    	}
	}

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.marque = readString(dis);
					
					this.modele = readString(dis);
					
					this.date_premiere_mise_circulation = readDate(dis);
					
					this.immatriculation = readString(dis);
					
					this.chassis = readString(dis);
					
					this.usage = readString(dis);
					
					this.charge_utile = readString(dis);
					
			        this.puissance_fiscale = dis.readInt();
					
					this.remorque = readString(dis);
					
						this.nombre_portes = readInteger(dis);
					
					this.immatriculation_remorque = readString(dis);
					
					this.source_energie = readString(dis);
					
						this.nombre_de_places = readInteger(dis);
					
						this.cylindree = readInteger(dis);
					
						this.double_commande = readInteger(dis);
					
						this.responsabilite_civile = readInteger(dis);
					
						this.utilitaire = readInteger(dis);
					
						this.type_engin = readInteger(dis);
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.poids_total_autorise_en_charge = null;
           				} else {
           			    	this.poids_total_autorise_en_charge = dis.readFloat();
           				}
					
			        this.c_status = dis.readInt();
					
					this.c_date_mis_a_jour = readDate(dis);
					
					this.c_date_transfer = readDate(dis);
					
					this.commentaires = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }
    
    public void readData(org.jboss.marshalling.Unmarshaller dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.marque = readString(dis);
					
					this.modele = readString(dis);
					
					this.date_premiere_mise_circulation = readDate(dis);
					
					this.immatriculation = readString(dis);
					
					this.chassis = readString(dis);
					
					this.usage = readString(dis);
					
					this.charge_utile = readString(dis);
					
			        this.puissance_fiscale = dis.readInt();
					
					this.remorque = readString(dis);
					
						this.nombre_portes = readInteger(dis);
					
					this.immatriculation_remorque = readString(dis);
					
					this.source_energie = readString(dis);
					
						this.nombre_de_places = readInteger(dis);
					
						this.cylindree = readInteger(dis);
					
						this.double_commande = readInteger(dis);
					
						this.responsabilite_civile = readInteger(dis);
					
						this.utilitaire = readInteger(dis);
					
						this.type_engin = readInteger(dis);
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.poids_total_autorise_en_charge = null;
           				} else {
           			    	this.poids_total_autorise_en_charge = dis.readFloat();
           				}
					
			        this.c_status = dis.readInt();
					
					this.c_date_mis_a_jour = readDate(dis);
					
					this.c_date_transfer = readDate(dis);
					
					this.commentaires = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.marque,dos);
					
					// String
				
						writeString(this.modele,dos);
					
					// java.util.Date
				
						writeDate(this.date_premiere_mise_circulation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.chassis,dos);
					
					// String
				
						writeString(this.usage,dos);
					
					// String
				
						writeString(this.charge_utile,dos);
					
					// int
				
		            	dos.writeInt(this.puissance_fiscale);
					
					// String
				
						writeString(this.remorque,dos);
					
					// Integer
				
						writeInteger(this.nombre_portes,dos);
					
					// String
				
						writeString(this.immatriculation_remorque,dos);
					
					// String
				
						writeString(this.source_energie,dos);
					
					// Integer
				
						writeInteger(this.nombre_de_places,dos);
					
					// Integer
				
						writeInteger(this.cylindree,dos);
					
					// Integer
				
						writeInteger(this.double_commande,dos);
					
					// Integer
				
						writeInteger(this.responsabilite_civile,dos);
					
					// Integer
				
						writeInteger(this.utilitaire,dos);
					
					// Integer
				
						writeInteger(this.type_engin,dos);
					
					// Float
				
						if(this.poids_total_autorise_en_charge == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeFloat(this.poids_total_autorise_en_charge);
		            	}
					
					// int
				
		            	dos.writeInt(this.c_status);
					
					// java.util.Date
				
						writeDate(this.c_date_mis_a_jour,dos);
					
					// java.util.Date
				
						writeDate(this.c_date_transfer,dos);
					
					// String
				
						writeString(this.commentaires,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }
    
    public void writeData(org.jboss.marshalling.Marshaller dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.marque,dos);
					
					// String
				
						writeString(this.modele,dos);
					
					// java.util.Date
				
						writeDate(this.date_premiere_mise_circulation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.chassis,dos);
					
					// String
				
						writeString(this.usage,dos);
					
					// String
				
						writeString(this.charge_utile,dos);
					
					// int
				
		            	dos.writeInt(this.puissance_fiscale);
					
					// String
				
						writeString(this.remorque,dos);
					
					// Integer
				
						writeInteger(this.nombre_portes,dos);
					
					// String
				
						writeString(this.immatriculation_remorque,dos);
					
					// String
				
						writeString(this.source_energie,dos);
					
					// Integer
				
						writeInteger(this.nombre_de_places,dos);
					
					// Integer
				
						writeInteger(this.cylindree,dos);
					
					// Integer
				
						writeInteger(this.double_commande,dos);
					
					// Integer
				
						writeInteger(this.responsabilite_civile,dos);
					
					// Integer
				
						writeInteger(this.utilitaire,dos);
					
					// Integer
				
						writeInteger(this.type_engin,dos);
					
					// Float
				
						if(this.poids_total_autorise_en_charge == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeFloat(this.poids_total_autorise_en_charge);
		            	}
					
					// int
				
		            	dos.writeInt(this.c_status);
					
					// java.util.Date
				
						writeDate(this.c_date_mis_a_jour,dos);
					
					// java.util.Date
				
						writeDate(this.c_date_transfer,dos);
					
					// String
				
						writeString(this.commentaires,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("code_assure="+code_assure);
		sb.append(",code_assureur="+code_assureur);
		sb.append(",marque="+marque);
		sb.append(",modele="+modele);
		sb.append(",date_premiere_mise_circulation="+String.valueOf(date_premiere_mise_circulation));
		sb.append(",immatriculation="+immatriculation);
		sb.append(",chassis="+chassis);
		sb.append(",usage="+usage);
		sb.append(",charge_utile="+charge_utile);
		sb.append(",puissance_fiscale="+String.valueOf(puissance_fiscale));
		sb.append(",remorque="+remorque);
		sb.append(",nombre_portes="+String.valueOf(nombre_portes));
		sb.append(",immatriculation_remorque="+immatriculation_remorque);
		sb.append(",source_energie="+source_energie);
		sb.append(",nombre_de_places="+String.valueOf(nombre_de_places));
		sb.append(",cylindree="+String.valueOf(cylindree));
		sb.append(",double_commande="+String.valueOf(double_commande));
		sb.append(",responsabilite_civile="+String.valueOf(responsabilite_civile));
		sb.append(",utilitaire="+String.valueOf(utilitaire));
		sb.append(",type_engin="+String.valueOf(type_engin));
		sb.append(",poids_total_autorise_en_charge="+String.valueOf(poids_total_autorise_en_charge));
		sb.append(",c_status="+String.valueOf(c_status));
		sb.append(",c_date_mis_a_jour="+String.valueOf(c_date_mis_a_jour));
		sb.append(",c_date_transfer="+String.valueOf(c_date_transfer));
		sb.append(",commentaires="+commentaires);
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(vehicule_valideStruct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class vehicule_rejetStruct implements routines.system.IPersistableRow<vehicule_rejetStruct> {
    final static byte[] commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[0];
    static byte[] commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[0];

	
			    public String code_assure;

				public String getCode_assure () {
					return this.code_assure;
				}
				
			    public String code_assureur;

				public String getCode_assureur () {
					return this.code_assureur;
				}
				
			    public String marque;

				public String getMarque () {
					return this.marque;
				}
				
			    public String modele;

				public String getModele () {
					return this.modele;
				}
				
			    public java.util.Date date_premiere_mise_circulation;

				public java.util.Date getDate_premiere_mise_circulation () {
					return this.date_premiere_mise_circulation;
				}
				
			    public String immatriculation;

				public String getImmatriculation () {
					return this.immatriculation;
				}
				
			    public String chassis;

				public String getChassis () {
					return this.chassis;
				}
				
			    public String usage;

				public String getUsage () {
					return this.usage;
				}
				
			    public String charge_utile;

				public String getCharge_utile () {
					return this.charge_utile;
				}
				
			    public int puissance_fiscale;

				public int getPuissance_fiscale () {
					return this.puissance_fiscale;
				}
				
			    public String remorque;

				public String getRemorque () {
					return this.remorque;
				}
				
			    public Integer nombre_portes;

				public Integer getNombre_portes () {
					return this.nombre_portes;
				}
				
			    public String immatriculation_remorque;

				public String getImmatriculation_remorque () {
					return this.immatriculation_remorque;
				}
				
			    public String source_energie;

				public String getSource_energie () {
					return this.source_energie;
				}
				
			    public Integer nombre_de_places;

				public Integer getNombre_de_places () {
					return this.nombre_de_places;
				}
				
			    public Integer cylindree;

				public Integer getCylindree () {
					return this.cylindree;
				}
				
			    public Integer double_commande;

				public Integer getDouble_commande () {
					return this.double_commande;
				}
				
			    public Integer responsabilite_civile;

				public Integer getResponsabilite_civile () {
					return this.responsabilite_civile;
				}
				
			    public Integer utilitaire;

				public Integer getUtilitaire () {
					return this.utilitaire;
				}
				
			    public Integer type_engin;

				public Integer getType_engin () {
					return this.type_engin;
				}
				
			    public Float poids_total_autorise_en_charge;

				public Float getPoids_total_autorise_en_charge () {
					return this.poids_total_autorise_en_charge;
				}
				
			    public java.util.Date date_extraction;

				public java.util.Date getDate_extraction () {
					return this.date_extraction;
				}
				
			    public java.util.Date date_depot;

				public java.util.Date getDate_depot () {
					return this.date_depot;
				}
				
			    public String Error_Message;

				public String getError_Message () {
					return this.Error_Message;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length, utf8Charset);
		}
		return strReturn;
	}
	
	private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		String strReturn = null;
		int length = 0;
        length = unmarshaller.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[2 * length];
   				}
			}
			unmarshaller.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
    
    private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(str == null) {
			marshaller.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
            marshaller.writeInt(byteArray.length);
            marshaller.write(byteArray);
    	}
    }

	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}
	
	private java.util.Date readDate(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = unmarshaller.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(unmarshaller.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }
    
    private void writeDate(java.util.Date date1, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(date1 == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeLong(date1.getTime());
    	}
    }
	private Integer readInteger(ObjectInputStream dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}
	
	private Integer readInteger(org.jboss.marshalling.Unmarshaller dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}

	private void writeInteger(Integer intNum, ObjectOutputStream dos) throws IOException{
		if(intNum == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeInt(intNum);
    	}
	}
	
	private void writeInteger(Integer intNum, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(intNum == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeInt(intNum);
    	}
	}

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.marque = readString(dis);
					
					this.modele = readString(dis);
					
					this.date_premiere_mise_circulation = readDate(dis);
					
					this.immatriculation = readString(dis);
					
					this.chassis = readString(dis);
					
					this.usage = readString(dis);
					
					this.charge_utile = readString(dis);
					
			        this.puissance_fiscale = dis.readInt();
					
					this.remorque = readString(dis);
					
						this.nombre_portes = readInteger(dis);
					
					this.immatriculation_remorque = readString(dis);
					
					this.source_energie = readString(dis);
					
						this.nombre_de_places = readInteger(dis);
					
						this.cylindree = readInteger(dis);
					
						this.double_commande = readInteger(dis);
					
						this.responsabilite_civile = readInteger(dis);
					
						this.utilitaire = readInteger(dis);
					
						this.type_engin = readInteger(dis);
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.poids_total_autorise_en_charge = null;
           				} else {
           			    	this.poids_total_autorise_en_charge = dis.readFloat();
           				}
					
					this.date_extraction = readDate(dis);
					
					this.date_depot = readDate(dis);
					
					this.Error_Message = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }
    
    public void readData(org.jboss.marshalling.Unmarshaller dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.marque = readString(dis);
					
					this.modele = readString(dis);
					
					this.date_premiere_mise_circulation = readDate(dis);
					
					this.immatriculation = readString(dis);
					
					this.chassis = readString(dis);
					
					this.usage = readString(dis);
					
					this.charge_utile = readString(dis);
					
			        this.puissance_fiscale = dis.readInt();
					
					this.remorque = readString(dis);
					
						this.nombre_portes = readInteger(dis);
					
					this.immatriculation_remorque = readString(dis);
					
					this.source_energie = readString(dis);
					
						this.nombre_de_places = readInteger(dis);
					
						this.cylindree = readInteger(dis);
					
						this.double_commande = readInteger(dis);
					
						this.responsabilite_civile = readInteger(dis);
					
						this.utilitaire = readInteger(dis);
					
						this.type_engin = readInteger(dis);
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.poids_total_autorise_en_charge = null;
           				} else {
           			    	this.poids_total_autorise_en_charge = dis.readFloat();
           				}
					
					this.date_extraction = readDate(dis);
					
					this.date_depot = readDate(dis);
					
					this.Error_Message = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.marque,dos);
					
					// String
				
						writeString(this.modele,dos);
					
					// java.util.Date
				
						writeDate(this.date_premiere_mise_circulation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.chassis,dos);
					
					// String
				
						writeString(this.usage,dos);
					
					// String
				
						writeString(this.charge_utile,dos);
					
					// int
				
		            	dos.writeInt(this.puissance_fiscale);
					
					// String
				
						writeString(this.remorque,dos);
					
					// Integer
				
						writeInteger(this.nombre_portes,dos);
					
					// String
				
						writeString(this.immatriculation_remorque,dos);
					
					// String
				
						writeString(this.source_energie,dos);
					
					// Integer
				
						writeInteger(this.nombre_de_places,dos);
					
					// Integer
				
						writeInteger(this.cylindree,dos);
					
					// Integer
				
						writeInteger(this.double_commande,dos);
					
					// Integer
				
						writeInteger(this.responsabilite_civile,dos);
					
					// Integer
				
						writeInteger(this.utilitaire,dos);
					
					// Integer
				
						writeInteger(this.type_engin,dos);
					
					// Float
				
						if(this.poids_total_autorise_en_charge == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeFloat(this.poids_total_autorise_en_charge);
		            	}
					
					// java.util.Date
				
						writeDate(this.date_extraction,dos);
					
					// java.util.Date
				
						writeDate(this.date_depot,dos);
					
					// String
				
						writeString(this.Error_Message,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }
    
    public void writeData(org.jboss.marshalling.Marshaller dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.marque,dos);
					
					// String
				
						writeString(this.modele,dos);
					
					// java.util.Date
				
						writeDate(this.date_premiere_mise_circulation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.chassis,dos);
					
					// String
				
						writeString(this.usage,dos);
					
					// String
				
						writeString(this.charge_utile,dos);
					
					// int
				
		            	dos.writeInt(this.puissance_fiscale);
					
					// String
				
						writeString(this.remorque,dos);
					
					// Integer
				
						writeInteger(this.nombre_portes,dos);
					
					// String
				
						writeString(this.immatriculation_remorque,dos);
					
					// String
				
						writeString(this.source_energie,dos);
					
					// Integer
				
						writeInteger(this.nombre_de_places,dos);
					
					// Integer
				
						writeInteger(this.cylindree,dos);
					
					// Integer
				
						writeInteger(this.double_commande,dos);
					
					// Integer
				
						writeInteger(this.responsabilite_civile,dos);
					
					// Integer
				
						writeInteger(this.utilitaire,dos);
					
					// Integer
				
						writeInteger(this.type_engin,dos);
					
					// Float
				
						if(this.poids_total_autorise_en_charge == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeFloat(this.poids_total_autorise_en_charge);
		            	}
					
					// java.util.Date
				
						writeDate(this.date_extraction,dos);
					
					// java.util.Date
				
						writeDate(this.date_depot,dos);
					
					// String
				
						writeString(this.Error_Message,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("code_assure="+code_assure);
		sb.append(",code_assureur="+code_assureur);
		sb.append(",marque="+marque);
		sb.append(",modele="+modele);
		sb.append(",date_premiere_mise_circulation="+String.valueOf(date_premiere_mise_circulation));
		sb.append(",immatriculation="+immatriculation);
		sb.append(",chassis="+chassis);
		sb.append(",usage="+usage);
		sb.append(",charge_utile="+charge_utile);
		sb.append(",puissance_fiscale="+String.valueOf(puissance_fiscale));
		sb.append(",remorque="+remorque);
		sb.append(",nombre_portes="+String.valueOf(nombre_portes));
		sb.append(",immatriculation_remorque="+immatriculation_remorque);
		sb.append(",source_energie="+source_energie);
		sb.append(",nombre_de_places="+String.valueOf(nombre_de_places));
		sb.append(",cylindree="+String.valueOf(cylindree));
		sb.append(",double_commande="+String.valueOf(double_commande));
		sb.append(",responsabilite_civile="+String.valueOf(responsabilite_civile));
		sb.append(",utilitaire="+String.valueOf(utilitaire));
		sb.append(",type_engin="+String.valueOf(type_engin));
		sb.append(",poids_total_autorise_en_charge="+String.valueOf(poids_total_autorise_en_charge));
		sb.append(",date_extraction="+String.valueOf(date_extraction));
		sb.append(",date_depot="+String.valueOf(date_depot));
		sb.append(",Error_Message="+Error_Message);
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(vehicule_rejetStruct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class row14Struct implements routines.system.IPersistableRow<row14Struct> {
    final static byte[] commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[0];
    static byte[] commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[0];

	
			    public String code_assure;

				public String getCode_assure () {
					return this.code_assure;
				}
				
			    public String code_assureur;

				public String getCode_assureur () {
					return this.code_assureur;
				}
				
			    public String marque;

				public String getMarque () {
					return this.marque;
				}
				
			    public String modele;

				public String getModele () {
					return this.modele;
				}
				
			    public java.util.Date date_premiere_mise_circulation;

				public java.util.Date getDate_premiere_mise_circulation () {
					return this.date_premiere_mise_circulation;
				}
				
			    public String immatriculation;

				public String getImmatriculation () {
					return this.immatriculation;
				}
				
			    public String chassis;

				public String getChassis () {
					return this.chassis;
				}
				
			    public String usage;

				public String getUsage () {
					return this.usage;
				}
				
			    public String charge_utile;

				public String getCharge_utile () {
					return this.charge_utile;
				}
				
			    public int puissance_fiscale;

				public int getPuissance_fiscale () {
					return this.puissance_fiscale;
				}
				
			    public String remorque;

				public String getRemorque () {
					return this.remorque;
				}
				
			    public Integer nombre_portes;

				public Integer getNombre_portes () {
					return this.nombre_portes;
				}
				
			    public String immatriculation_remorque;

				public String getImmatriculation_remorque () {
					return this.immatriculation_remorque;
				}
				
			    public String source_energie;

				public String getSource_energie () {
					return this.source_energie;
				}
				
			    public Integer nombre_de_places;

				public Integer getNombre_de_places () {
					return this.nombre_de_places;
				}
				
			    public Integer cylindree;

				public Integer getCylindree () {
					return this.cylindree;
				}
				
			    public Integer double_commande;

				public Integer getDouble_commande () {
					return this.double_commande;
				}
				
			    public Integer responsabilite_civile;

				public Integer getResponsabilite_civile () {
					return this.responsabilite_civile;
				}
				
			    public Integer utilitaire;

				public Integer getUtilitaire () {
					return this.utilitaire;
				}
				
			    public Integer type_engin;

				public Integer getType_engin () {
					return this.type_engin;
				}
				
			    public Float poids_total_autorise_en_charge;

				public Float getPoids_total_autorise_en_charge () {
					return this.poids_total_autorise_en_charge;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length, utf8Charset);
		}
		return strReturn;
	}
	
	private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		String strReturn = null;
		int length = 0;
        length = unmarshaller.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[2 * length];
   				}
			}
			unmarshaller.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
    
    private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(str == null) {
			marshaller.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
            marshaller.writeInt(byteArray.length);
            marshaller.write(byteArray);
    	}
    }

	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}
	
	private java.util.Date readDate(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = unmarshaller.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(unmarshaller.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }
    
    private void writeDate(java.util.Date date1, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(date1 == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeLong(date1.getTime());
    	}
    }
	private Integer readInteger(ObjectInputStream dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}
	
	private Integer readInteger(org.jboss.marshalling.Unmarshaller dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}

	private void writeInteger(Integer intNum, ObjectOutputStream dos) throws IOException{
		if(intNum == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeInt(intNum);
    	}
	}
	
	private void writeInteger(Integer intNum, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(intNum == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeInt(intNum);
    	}
	}

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.marque = readString(dis);
					
					this.modele = readString(dis);
					
					this.date_premiere_mise_circulation = readDate(dis);
					
					this.immatriculation = readString(dis);
					
					this.chassis = readString(dis);
					
					this.usage = readString(dis);
					
					this.charge_utile = readString(dis);
					
			        this.puissance_fiscale = dis.readInt();
					
					this.remorque = readString(dis);
					
						this.nombre_portes = readInteger(dis);
					
					this.immatriculation_remorque = readString(dis);
					
					this.source_energie = readString(dis);
					
						this.nombre_de_places = readInteger(dis);
					
						this.cylindree = readInteger(dis);
					
						this.double_commande = readInteger(dis);
					
						this.responsabilite_civile = readInteger(dis);
					
						this.utilitaire = readInteger(dis);
					
						this.type_engin = readInteger(dis);
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.poids_total_autorise_en_charge = null;
           				} else {
           			    	this.poids_total_autorise_en_charge = dis.readFloat();
           				}
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }
    
    public void readData(org.jboss.marshalling.Unmarshaller dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.marque = readString(dis);
					
					this.modele = readString(dis);
					
					this.date_premiere_mise_circulation = readDate(dis);
					
					this.immatriculation = readString(dis);
					
					this.chassis = readString(dis);
					
					this.usage = readString(dis);
					
					this.charge_utile = readString(dis);
					
			        this.puissance_fiscale = dis.readInt();
					
					this.remorque = readString(dis);
					
						this.nombre_portes = readInteger(dis);
					
					this.immatriculation_remorque = readString(dis);
					
					this.source_energie = readString(dis);
					
						this.nombre_de_places = readInteger(dis);
					
						this.cylindree = readInteger(dis);
					
						this.double_commande = readInteger(dis);
					
						this.responsabilite_civile = readInteger(dis);
					
						this.utilitaire = readInteger(dis);
					
						this.type_engin = readInteger(dis);
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.poids_total_autorise_en_charge = null;
           				} else {
           			    	this.poids_total_autorise_en_charge = dis.readFloat();
           				}
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.marque,dos);
					
					// String
				
						writeString(this.modele,dos);
					
					// java.util.Date
				
						writeDate(this.date_premiere_mise_circulation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.chassis,dos);
					
					// String
				
						writeString(this.usage,dos);
					
					// String
				
						writeString(this.charge_utile,dos);
					
					// int
				
		            	dos.writeInt(this.puissance_fiscale);
					
					// String
				
						writeString(this.remorque,dos);
					
					// Integer
				
						writeInteger(this.nombre_portes,dos);
					
					// String
				
						writeString(this.immatriculation_remorque,dos);
					
					// String
				
						writeString(this.source_energie,dos);
					
					// Integer
				
						writeInteger(this.nombre_de_places,dos);
					
					// Integer
				
						writeInteger(this.cylindree,dos);
					
					// Integer
				
						writeInteger(this.double_commande,dos);
					
					// Integer
				
						writeInteger(this.responsabilite_civile,dos);
					
					// Integer
				
						writeInteger(this.utilitaire,dos);
					
					// Integer
				
						writeInteger(this.type_engin,dos);
					
					// Float
				
						if(this.poids_total_autorise_en_charge == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeFloat(this.poids_total_autorise_en_charge);
		            	}
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }
    
    public void writeData(org.jboss.marshalling.Marshaller dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.marque,dos);
					
					// String
				
						writeString(this.modele,dos);
					
					// java.util.Date
				
						writeDate(this.date_premiere_mise_circulation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.chassis,dos);
					
					// String
				
						writeString(this.usage,dos);
					
					// String
				
						writeString(this.charge_utile,dos);
					
					// int
				
		            	dos.writeInt(this.puissance_fiscale);
					
					// String
				
						writeString(this.remorque,dos);
					
					// Integer
				
						writeInteger(this.nombre_portes,dos);
					
					// String
				
						writeString(this.immatriculation_remorque,dos);
					
					// String
				
						writeString(this.source_energie,dos);
					
					// Integer
				
						writeInteger(this.nombre_de_places,dos);
					
					// Integer
				
						writeInteger(this.cylindree,dos);
					
					// Integer
				
						writeInteger(this.double_commande,dos);
					
					// Integer
				
						writeInteger(this.responsabilite_civile,dos);
					
					// Integer
				
						writeInteger(this.utilitaire,dos);
					
					// Integer
				
						writeInteger(this.type_engin,dos);
					
					// Float
				
						if(this.poids_total_autorise_en_charge == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeFloat(this.poids_total_autorise_en_charge);
		            	}
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("code_assure="+code_assure);
		sb.append(",code_assureur="+code_assureur);
		sb.append(",marque="+marque);
		sb.append(",modele="+modele);
		sb.append(",date_premiere_mise_circulation="+String.valueOf(date_premiere_mise_circulation));
		sb.append(",immatriculation="+immatriculation);
		sb.append(",chassis="+chassis);
		sb.append(",usage="+usage);
		sb.append(",charge_utile="+charge_utile);
		sb.append(",puissance_fiscale="+String.valueOf(puissance_fiscale));
		sb.append(",remorque="+remorque);
		sb.append(",nombre_portes="+String.valueOf(nombre_portes));
		sb.append(",immatriculation_remorque="+immatriculation_remorque);
		sb.append(",source_energie="+source_energie);
		sb.append(",nombre_de_places="+String.valueOf(nombre_de_places));
		sb.append(",cylindree="+String.valueOf(cylindree));
		sb.append(",double_commande="+String.valueOf(double_commande));
		sb.append(",responsabilite_civile="+String.valueOf(responsabilite_civile));
		sb.append(",utilitaire="+String.valueOf(utilitaire));
		sb.append(",type_engin="+String.valueOf(type_engin));
		sb.append(",poids_total_autorise_en_charge="+String.valueOf(poids_total_autorise_en_charge));
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row14Struct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class row2Struct implements routines.system.IPersistableRow<row2Struct> {
    final static byte[] commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[0];
    static byte[] commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[0];

	
			    public String code_assure;

				public String getCode_assure () {
					return this.code_assure;
				}
				
			    public String code_assureur;

				public String getCode_assureur () {
					return this.code_assureur;
				}
				
			    public String marque;

				public String getMarque () {
					return this.marque;
				}
				
			    public String modele;

				public String getModele () {
					return this.modele;
				}
				
			    public java.util.Date date_premiere_mise_circulation;

				public java.util.Date getDate_premiere_mise_circulation () {
					return this.date_premiere_mise_circulation;
				}
				
			    public String immatriculation;

				public String getImmatriculation () {
					return this.immatriculation;
				}
				
			    public String chassis;

				public String getChassis () {
					return this.chassis;
				}
				
			    public String usage;

				public String getUsage () {
					return this.usage;
				}
				
			    public String charge_utile;

				public String getCharge_utile () {
					return this.charge_utile;
				}
				
			    public Integer puissance_fiscale;

				public Integer getPuissance_fiscale () {
					return this.puissance_fiscale;
				}
				
			    public String remorque;

				public String getRemorque () {
					return this.remorque;
				}
				
			    public Integer nombre_portes;

				public Integer getNombre_portes () {
					return this.nombre_portes;
				}
				
			    public String immatriculation_remorque;

				public String getImmatriculation_remorque () {
					return this.immatriculation_remorque;
				}
				
			    public String source_energie;

				public String getSource_energie () {
					return this.source_energie;
				}
				
			    public Integer nombre_de_places;

				public Integer getNombre_de_places () {
					return this.nombre_de_places;
				}
				
			    public Integer cylindree;

				public Integer getCylindree () {
					return this.cylindree;
				}
				
			    public Integer double_commande;

				public Integer getDouble_commande () {
					return this.double_commande;
				}
				
			    public Integer responsabilite_civile;

				public Integer getResponsabilite_civile () {
					return this.responsabilite_civile;
				}
				
			    public Integer utilitaire;

				public Integer getUtilitaire () {
					return this.utilitaire;
				}
				
			    public Integer type_engin;

				public Integer getType_engin () {
					return this.type_engin;
				}
				
			    public Float poids_total_autorise_en_charge;

				public Float getPoids_total_autorise_en_charge () {
					return this.poids_total_autorise_en_charge;
				}
				
			    public java.util.Date date_extraction;

				public java.util.Date getDate_extraction () {
					return this.date_extraction;
				}
				
			    public java.util.Date date_depot;

				public java.util.Date getDate_depot () {
					return this.date_depot;
				}
				
			    public String errorCode;

				public String getErrorCode () {
					return this.errorCode;
				}
				
			    public String errorMessage;

				public String getErrorMessage () {
					return this.errorMessage;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length, utf8Charset);
		}
		return strReturn;
	}
	
	private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		String strReturn = null;
		int length = 0;
        length = unmarshaller.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[2 * length];
   				}
			}
			unmarshaller.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
    
    private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(str == null) {
			marshaller.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
            marshaller.writeInt(byteArray.length);
            marshaller.write(byteArray);
    	}
    }

	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}
	
	private java.util.Date readDate(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = unmarshaller.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(unmarshaller.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }
    
    private void writeDate(java.util.Date date1, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(date1 == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeLong(date1.getTime());
    	}
    }
	private Integer readInteger(ObjectInputStream dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}
	
	private Integer readInteger(org.jboss.marshalling.Unmarshaller dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}

	private void writeInteger(Integer intNum, ObjectOutputStream dos) throws IOException{
		if(intNum == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeInt(intNum);
    	}
	}
	
	private void writeInteger(Integer intNum, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(intNum == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeInt(intNum);
    	}
	}

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.marque = readString(dis);
					
					this.modele = readString(dis);
					
					this.date_premiere_mise_circulation = readDate(dis);
					
					this.immatriculation = readString(dis);
					
					this.chassis = readString(dis);
					
					this.usage = readString(dis);
					
					this.charge_utile = readString(dis);
					
						this.puissance_fiscale = readInteger(dis);
					
					this.remorque = readString(dis);
					
						this.nombre_portes = readInteger(dis);
					
					this.immatriculation_remorque = readString(dis);
					
					this.source_energie = readString(dis);
					
						this.nombre_de_places = readInteger(dis);
					
						this.cylindree = readInteger(dis);
					
						this.double_commande = readInteger(dis);
					
						this.responsabilite_civile = readInteger(dis);
					
						this.utilitaire = readInteger(dis);
					
						this.type_engin = readInteger(dis);
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.poids_total_autorise_en_charge = null;
           				} else {
           			    	this.poids_total_autorise_en_charge = dis.readFloat();
           				}
					
					this.date_extraction = readDate(dis);
					
					this.date_depot = readDate(dis);
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }
    
    public void readData(org.jboss.marshalling.Unmarshaller dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.marque = readString(dis);
					
					this.modele = readString(dis);
					
					this.date_premiere_mise_circulation = readDate(dis);
					
					this.immatriculation = readString(dis);
					
					this.chassis = readString(dis);
					
					this.usage = readString(dis);
					
					this.charge_utile = readString(dis);
					
						this.puissance_fiscale = readInteger(dis);
					
					this.remorque = readString(dis);
					
						this.nombre_portes = readInteger(dis);
					
					this.immatriculation_remorque = readString(dis);
					
					this.source_energie = readString(dis);
					
						this.nombre_de_places = readInteger(dis);
					
						this.cylindree = readInteger(dis);
					
						this.double_commande = readInteger(dis);
					
						this.responsabilite_civile = readInteger(dis);
					
						this.utilitaire = readInteger(dis);
					
						this.type_engin = readInteger(dis);
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.poids_total_autorise_en_charge = null;
           				} else {
           			    	this.poids_total_autorise_en_charge = dis.readFloat();
           				}
					
					this.date_extraction = readDate(dis);
					
					this.date_depot = readDate(dis);
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.marque,dos);
					
					// String
				
						writeString(this.modele,dos);
					
					// java.util.Date
				
						writeDate(this.date_premiere_mise_circulation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.chassis,dos);
					
					// String
				
						writeString(this.usage,dos);
					
					// String
				
						writeString(this.charge_utile,dos);
					
					// Integer
				
						writeInteger(this.puissance_fiscale,dos);
					
					// String
				
						writeString(this.remorque,dos);
					
					// Integer
				
						writeInteger(this.nombre_portes,dos);
					
					// String
				
						writeString(this.immatriculation_remorque,dos);
					
					// String
				
						writeString(this.source_energie,dos);
					
					// Integer
				
						writeInteger(this.nombre_de_places,dos);
					
					// Integer
				
						writeInteger(this.cylindree,dos);
					
					// Integer
				
						writeInteger(this.double_commande,dos);
					
					// Integer
				
						writeInteger(this.responsabilite_civile,dos);
					
					// Integer
				
						writeInteger(this.utilitaire,dos);
					
					// Integer
				
						writeInteger(this.type_engin,dos);
					
					// Float
				
						if(this.poids_total_autorise_en_charge == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeFloat(this.poids_total_autorise_en_charge);
		            	}
					
					// java.util.Date
				
						writeDate(this.date_extraction,dos);
					
					// java.util.Date
				
						writeDate(this.date_depot,dos);
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }
    
    public void writeData(org.jboss.marshalling.Marshaller dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.marque,dos);
					
					// String
				
						writeString(this.modele,dos);
					
					// java.util.Date
				
						writeDate(this.date_premiere_mise_circulation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.chassis,dos);
					
					// String
				
						writeString(this.usage,dos);
					
					// String
				
						writeString(this.charge_utile,dos);
					
					// Integer
				
						writeInteger(this.puissance_fiscale,dos);
					
					// String
				
						writeString(this.remorque,dos);
					
					// Integer
				
						writeInteger(this.nombre_portes,dos);
					
					// String
				
						writeString(this.immatriculation_remorque,dos);
					
					// String
				
						writeString(this.source_energie,dos);
					
					// Integer
				
						writeInteger(this.nombre_de_places,dos);
					
					// Integer
				
						writeInteger(this.cylindree,dos);
					
					// Integer
				
						writeInteger(this.double_commande,dos);
					
					// Integer
				
						writeInteger(this.responsabilite_civile,dos);
					
					// Integer
				
						writeInteger(this.utilitaire,dos);
					
					// Integer
				
						writeInteger(this.type_engin,dos);
					
					// Float
				
						if(this.poids_total_autorise_en_charge == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeFloat(this.poids_total_autorise_en_charge);
		            	}
					
					// java.util.Date
				
						writeDate(this.date_extraction,dos);
					
					// java.util.Date
				
						writeDate(this.date_depot,dos);
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("code_assure="+code_assure);
		sb.append(",code_assureur="+code_assureur);
		sb.append(",marque="+marque);
		sb.append(",modele="+modele);
		sb.append(",date_premiere_mise_circulation="+String.valueOf(date_premiere_mise_circulation));
		sb.append(",immatriculation="+immatriculation);
		sb.append(",chassis="+chassis);
		sb.append(",usage="+usage);
		sb.append(",charge_utile="+charge_utile);
		sb.append(",puissance_fiscale="+String.valueOf(puissance_fiscale));
		sb.append(",remorque="+remorque);
		sb.append(",nombre_portes="+String.valueOf(nombre_portes));
		sb.append(",immatriculation_remorque="+immatriculation_remorque);
		sb.append(",source_energie="+source_energie);
		sb.append(",nombre_de_places="+String.valueOf(nombre_de_places));
		sb.append(",cylindree="+String.valueOf(cylindree));
		sb.append(",double_commande="+String.valueOf(double_commande));
		sb.append(",responsabilite_civile="+String.valueOf(responsabilite_civile));
		sb.append(",utilitaire="+String.valueOf(utilitaire));
		sb.append(",type_engin="+String.valueOf(type_engin));
		sb.append(",poids_total_autorise_en_charge="+String.valueOf(poids_total_autorise_en_charge));
		sb.append(",date_extraction="+String.valueOf(date_extraction));
		sb.append(",date_depot="+String.valueOf(date_depot));
		sb.append(",errorCode="+errorCode);
		sb.append(",errorMessage="+errorMessage);
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row2Struct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class vehicule_rejet_SchemaStruct implements routines.system.IPersistableRow<vehicule_rejet_SchemaStruct> {
    final static byte[] commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[0];
    static byte[] commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[0];

	
			    public String code_assure;

				public String getCode_assure () {
					return this.code_assure;
				}
				
			    public String code_assureur;

				public String getCode_assureur () {
					return this.code_assureur;
				}
				
			    public String marque;

				public String getMarque () {
					return this.marque;
				}
				
			    public String modele;

				public String getModele () {
					return this.modele;
				}
				
			    public java.util.Date date_premiere_mise_circulation;

				public java.util.Date getDate_premiere_mise_circulation () {
					return this.date_premiere_mise_circulation;
				}
				
			    public String immatriculation;

				public String getImmatriculation () {
					return this.immatriculation;
				}
				
			    public String chassis;

				public String getChassis () {
					return this.chassis;
				}
				
			    public String usage;

				public String getUsage () {
					return this.usage;
				}
				
			    public String charge_utile;

				public String getCharge_utile () {
					return this.charge_utile;
				}
				
			    public Integer puissance_fiscale;

				public Integer getPuissance_fiscale () {
					return this.puissance_fiscale;
				}
				
			    public String remorque;

				public String getRemorque () {
					return this.remorque;
				}
				
			    public Integer nombre_portes;

				public Integer getNombre_portes () {
					return this.nombre_portes;
				}
				
			    public String immatriculation_remorque;

				public String getImmatriculation_remorque () {
					return this.immatriculation_remorque;
				}
				
			    public String source_energie;

				public String getSource_energie () {
					return this.source_energie;
				}
				
			    public Integer nombre_de_places;

				public Integer getNombre_de_places () {
					return this.nombre_de_places;
				}
				
			    public Integer cylindree;

				public Integer getCylindree () {
					return this.cylindree;
				}
				
			    public Integer double_commande;

				public Integer getDouble_commande () {
					return this.double_commande;
				}
				
			    public Integer responsabilite_civile;

				public Integer getResponsabilite_civile () {
					return this.responsabilite_civile;
				}
				
			    public Integer utilitaire;

				public Integer getUtilitaire () {
					return this.utilitaire;
				}
				
			    public Integer type_engin;

				public Integer getType_engin () {
					return this.type_engin;
				}
				
			    public Float poids_total_autorise_en_charge;

				public Float getPoids_total_autorise_en_charge () {
					return this.poids_total_autorise_en_charge;
				}
				
			    public java.util.Date date_extraction;

				public java.util.Date getDate_extraction () {
					return this.date_extraction;
				}
				
			    public java.util.Date date_depot;

				public java.util.Date getDate_depot () {
					return this.date_depot;
				}
				
			    public String errorCode;

				public String getErrorCode () {
					return this.errorCode;
				}
				
			    public String errorMessage;

				public String getErrorMessage () {
					return this.errorMessage;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length, utf8Charset);
		}
		return strReturn;
	}
	
	private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		String strReturn = null;
		int length = 0;
        length = unmarshaller.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[2 * length];
   				}
			}
			unmarshaller.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
    
    private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(str == null) {
			marshaller.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
            marshaller.writeInt(byteArray.length);
            marshaller.write(byteArray);
    	}
    }

	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}
	
	private java.util.Date readDate(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = unmarshaller.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(unmarshaller.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }
    
    private void writeDate(java.util.Date date1, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(date1 == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeLong(date1.getTime());
    	}
    }
	private Integer readInteger(ObjectInputStream dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}
	
	private Integer readInteger(org.jboss.marshalling.Unmarshaller dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}

	private void writeInteger(Integer intNum, ObjectOutputStream dos) throws IOException{
		if(intNum == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeInt(intNum);
    	}
	}
	
	private void writeInteger(Integer intNum, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(intNum == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeInt(intNum);
    	}
	}

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.marque = readString(dis);
					
					this.modele = readString(dis);
					
					this.date_premiere_mise_circulation = readDate(dis);
					
					this.immatriculation = readString(dis);
					
					this.chassis = readString(dis);
					
					this.usage = readString(dis);
					
					this.charge_utile = readString(dis);
					
						this.puissance_fiscale = readInteger(dis);
					
					this.remorque = readString(dis);
					
						this.nombre_portes = readInteger(dis);
					
					this.immatriculation_remorque = readString(dis);
					
					this.source_energie = readString(dis);
					
						this.nombre_de_places = readInteger(dis);
					
						this.cylindree = readInteger(dis);
					
						this.double_commande = readInteger(dis);
					
						this.responsabilite_civile = readInteger(dis);
					
						this.utilitaire = readInteger(dis);
					
						this.type_engin = readInteger(dis);
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.poids_total_autorise_en_charge = null;
           				} else {
           			    	this.poids_total_autorise_en_charge = dis.readFloat();
           				}
					
					this.date_extraction = readDate(dis);
					
					this.date_depot = readDate(dis);
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }
    
    public void readData(org.jboss.marshalling.Unmarshaller dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.marque = readString(dis);
					
					this.modele = readString(dis);
					
					this.date_premiere_mise_circulation = readDate(dis);
					
					this.immatriculation = readString(dis);
					
					this.chassis = readString(dis);
					
					this.usage = readString(dis);
					
					this.charge_utile = readString(dis);
					
						this.puissance_fiscale = readInteger(dis);
					
					this.remorque = readString(dis);
					
						this.nombre_portes = readInteger(dis);
					
					this.immatriculation_remorque = readString(dis);
					
					this.source_energie = readString(dis);
					
						this.nombre_de_places = readInteger(dis);
					
						this.cylindree = readInteger(dis);
					
						this.double_commande = readInteger(dis);
					
						this.responsabilite_civile = readInteger(dis);
					
						this.utilitaire = readInteger(dis);
					
						this.type_engin = readInteger(dis);
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.poids_total_autorise_en_charge = null;
           				} else {
           			    	this.poids_total_autorise_en_charge = dis.readFloat();
           				}
					
					this.date_extraction = readDate(dis);
					
					this.date_depot = readDate(dis);
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.marque,dos);
					
					// String
				
						writeString(this.modele,dos);
					
					// java.util.Date
				
						writeDate(this.date_premiere_mise_circulation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.chassis,dos);
					
					// String
				
						writeString(this.usage,dos);
					
					// String
				
						writeString(this.charge_utile,dos);
					
					// Integer
				
						writeInteger(this.puissance_fiscale,dos);
					
					// String
				
						writeString(this.remorque,dos);
					
					// Integer
				
						writeInteger(this.nombre_portes,dos);
					
					// String
				
						writeString(this.immatriculation_remorque,dos);
					
					// String
				
						writeString(this.source_energie,dos);
					
					// Integer
				
						writeInteger(this.nombre_de_places,dos);
					
					// Integer
				
						writeInteger(this.cylindree,dos);
					
					// Integer
				
						writeInteger(this.double_commande,dos);
					
					// Integer
				
						writeInteger(this.responsabilite_civile,dos);
					
					// Integer
				
						writeInteger(this.utilitaire,dos);
					
					// Integer
				
						writeInteger(this.type_engin,dos);
					
					// Float
				
						if(this.poids_total_autorise_en_charge == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeFloat(this.poids_total_autorise_en_charge);
		            	}
					
					// java.util.Date
				
						writeDate(this.date_extraction,dos);
					
					// java.util.Date
				
						writeDate(this.date_depot,dos);
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }
    
    public void writeData(org.jboss.marshalling.Marshaller dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.marque,dos);
					
					// String
				
						writeString(this.modele,dos);
					
					// java.util.Date
				
						writeDate(this.date_premiere_mise_circulation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.chassis,dos);
					
					// String
				
						writeString(this.usage,dos);
					
					// String
				
						writeString(this.charge_utile,dos);
					
					// Integer
				
						writeInteger(this.puissance_fiscale,dos);
					
					// String
				
						writeString(this.remorque,dos);
					
					// Integer
				
						writeInteger(this.nombre_portes,dos);
					
					// String
				
						writeString(this.immatriculation_remorque,dos);
					
					// String
				
						writeString(this.source_energie,dos);
					
					// Integer
				
						writeInteger(this.nombre_de_places,dos);
					
					// Integer
				
						writeInteger(this.cylindree,dos);
					
					// Integer
				
						writeInteger(this.double_commande,dos);
					
					// Integer
				
						writeInteger(this.responsabilite_civile,dos);
					
					// Integer
				
						writeInteger(this.utilitaire,dos);
					
					// Integer
				
						writeInteger(this.type_engin,dos);
					
					// Float
				
						if(this.poids_total_autorise_en_charge == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeFloat(this.poids_total_autorise_en_charge);
		            	}
					
					// java.util.Date
				
						writeDate(this.date_extraction,dos);
					
					// java.util.Date
				
						writeDate(this.date_depot,dos);
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("code_assure="+code_assure);
		sb.append(",code_assureur="+code_assureur);
		sb.append(",marque="+marque);
		sb.append(",modele="+modele);
		sb.append(",date_premiere_mise_circulation="+String.valueOf(date_premiere_mise_circulation));
		sb.append(",immatriculation="+immatriculation);
		sb.append(",chassis="+chassis);
		sb.append(",usage="+usage);
		sb.append(",charge_utile="+charge_utile);
		sb.append(",puissance_fiscale="+String.valueOf(puissance_fiscale));
		sb.append(",remorque="+remorque);
		sb.append(",nombre_portes="+String.valueOf(nombre_portes));
		sb.append(",immatriculation_remorque="+immatriculation_remorque);
		sb.append(",source_energie="+source_energie);
		sb.append(",nombre_de_places="+String.valueOf(nombre_de_places));
		sb.append(",cylindree="+String.valueOf(cylindree));
		sb.append(",double_commande="+String.valueOf(double_commande));
		sb.append(",responsabilite_civile="+String.valueOf(responsabilite_civile));
		sb.append(",utilitaire="+String.valueOf(utilitaire));
		sb.append(",type_engin="+String.valueOf(type_engin));
		sb.append(",poids_total_autorise_en_charge="+String.valueOf(poids_total_autorise_en_charge));
		sb.append(",date_extraction="+String.valueOf(date_extraction));
		sb.append(",date_depot="+String.valueOf(date_depot));
		sb.append(",errorCode="+errorCode);
		sb.append(",errorMessage="+errorMessage);
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(vehicule_rejet_SchemaStruct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class row17Struct implements routines.system.IPersistableRow<row17Struct> {
    final static byte[] commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[0];
    static byte[] commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[0];

	
			    public String code_assure;

				public String getCode_assure () {
					return this.code_assure;
				}
				
			    public String code_assureur;

				public String getCode_assureur () {
					return this.code_assureur;
				}
				
			    public String marque;

				public String getMarque () {
					return this.marque;
				}
				
			    public String modele;

				public String getModele () {
					return this.modele;
				}
				
			    public java.util.Date date_premiere_mise_circulation;

				public java.util.Date getDate_premiere_mise_circulation () {
					return this.date_premiere_mise_circulation;
				}
				
			    public String immatriculation;

				public String getImmatriculation () {
					return this.immatriculation;
				}
				
			    public String chassis;

				public String getChassis () {
					return this.chassis;
				}
				
			    public String usage;

				public String getUsage () {
					return this.usage;
				}
				
			    public String charge_utile;

				public String getCharge_utile () {
					return this.charge_utile;
				}
				
			    public int puissance_fiscale;

				public int getPuissance_fiscale () {
					return this.puissance_fiscale;
				}
				
			    public String remorque;

				public String getRemorque () {
					return this.remorque;
				}
				
			    public Integer nombre_portes;

				public Integer getNombre_portes () {
					return this.nombre_portes;
				}
				
			    public String immatriculation_remorque;

				public String getImmatriculation_remorque () {
					return this.immatriculation_remorque;
				}
				
			    public String source_energie;

				public String getSource_energie () {
					return this.source_energie;
				}
				
			    public Integer nombre_de_places;

				public Integer getNombre_de_places () {
					return this.nombre_de_places;
				}
				
			    public Integer cylindree;

				public Integer getCylindree () {
					return this.cylindree;
				}
				
			    public Integer double_commande;

				public Integer getDouble_commande () {
					return this.double_commande;
				}
				
			    public Integer responsabilite_civile;

				public Integer getResponsabilite_civile () {
					return this.responsabilite_civile;
				}
				
			    public Integer utilitaire;

				public Integer getUtilitaire () {
					return this.utilitaire;
				}
				
			    public Integer type_engin;

				public Integer getType_engin () {
					return this.type_engin;
				}
				
			    public Float poids_total_autorise_en_charge;

				public Float getPoids_total_autorise_en_charge () {
					return this.poids_total_autorise_en_charge;
				}
				
			    public String errorCode;

				public String getErrorCode () {
					return this.errorCode;
				}
				
			    public String errorMessage;

				public String getErrorMessage () {
					return this.errorMessage;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length, utf8Charset);
		}
		return strReturn;
	}
	
	private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		String strReturn = null;
		int length = 0;
        length = unmarshaller.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[2 * length];
   				}
			}
			unmarshaller.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
    
    private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(str == null) {
			marshaller.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
            marshaller.writeInt(byteArray.length);
            marshaller.write(byteArray);
    	}
    }

	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}
	
	private java.util.Date readDate(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = unmarshaller.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(unmarshaller.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }
    
    private void writeDate(java.util.Date date1, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(date1 == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeLong(date1.getTime());
    	}
    }
	private Integer readInteger(ObjectInputStream dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}
	
	private Integer readInteger(org.jboss.marshalling.Unmarshaller dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}

	private void writeInteger(Integer intNum, ObjectOutputStream dos) throws IOException{
		if(intNum == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeInt(intNum);
    	}
	}
	
	private void writeInteger(Integer intNum, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(intNum == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeInt(intNum);
    	}
	}

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.marque = readString(dis);
					
					this.modele = readString(dis);
					
					this.date_premiere_mise_circulation = readDate(dis);
					
					this.immatriculation = readString(dis);
					
					this.chassis = readString(dis);
					
					this.usage = readString(dis);
					
					this.charge_utile = readString(dis);
					
			        this.puissance_fiscale = dis.readInt();
					
					this.remorque = readString(dis);
					
						this.nombre_portes = readInteger(dis);
					
					this.immatriculation_remorque = readString(dis);
					
					this.source_energie = readString(dis);
					
						this.nombre_de_places = readInteger(dis);
					
						this.cylindree = readInteger(dis);
					
						this.double_commande = readInteger(dis);
					
						this.responsabilite_civile = readInteger(dis);
					
						this.utilitaire = readInteger(dis);
					
						this.type_engin = readInteger(dis);
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.poids_total_autorise_en_charge = null;
           				} else {
           			    	this.poids_total_autorise_en_charge = dis.readFloat();
           				}
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }
    
    public void readData(org.jboss.marshalling.Unmarshaller dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.marque = readString(dis);
					
					this.modele = readString(dis);
					
					this.date_premiere_mise_circulation = readDate(dis);
					
					this.immatriculation = readString(dis);
					
					this.chassis = readString(dis);
					
					this.usage = readString(dis);
					
					this.charge_utile = readString(dis);
					
			        this.puissance_fiscale = dis.readInt();
					
					this.remorque = readString(dis);
					
						this.nombre_portes = readInteger(dis);
					
					this.immatriculation_remorque = readString(dis);
					
					this.source_energie = readString(dis);
					
						this.nombre_de_places = readInteger(dis);
					
						this.cylindree = readInteger(dis);
					
						this.double_commande = readInteger(dis);
					
						this.responsabilite_civile = readInteger(dis);
					
						this.utilitaire = readInteger(dis);
					
						this.type_engin = readInteger(dis);
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.poids_total_autorise_en_charge = null;
           				} else {
           			    	this.poids_total_autorise_en_charge = dis.readFloat();
           				}
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.marque,dos);
					
					// String
				
						writeString(this.modele,dos);
					
					// java.util.Date
				
						writeDate(this.date_premiere_mise_circulation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.chassis,dos);
					
					// String
				
						writeString(this.usage,dos);
					
					// String
				
						writeString(this.charge_utile,dos);
					
					// int
				
		            	dos.writeInt(this.puissance_fiscale);
					
					// String
				
						writeString(this.remorque,dos);
					
					// Integer
				
						writeInteger(this.nombre_portes,dos);
					
					// String
				
						writeString(this.immatriculation_remorque,dos);
					
					// String
				
						writeString(this.source_energie,dos);
					
					// Integer
				
						writeInteger(this.nombre_de_places,dos);
					
					// Integer
				
						writeInteger(this.cylindree,dos);
					
					// Integer
				
						writeInteger(this.double_commande,dos);
					
					// Integer
				
						writeInteger(this.responsabilite_civile,dos);
					
					// Integer
				
						writeInteger(this.utilitaire,dos);
					
					// Integer
				
						writeInteger(this.type_engin,dos);
					
					// Float
				
						if(this.poids_total_autorise_en_charge == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeFloat(this.poids_total_autorise_en_charge);
		            	}
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }
    
    public void writeData(org.jboss.marshalling.Marshaller dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.marque,dos);
					
					// String
				
						writeString(this.modele,dos);
					
					// java.util.Date
				
						writeDate(this.date_premiere_mise_circulation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.chassis,dos);
					
					// String
				
						writeString(this.usage,dos);
					
					// String
				
						writeString(this.charge_utile,dos);
					
					// int
				
		            	dos.writeInt(this.puissance_fiscale);
					
					// String
				
						writeString(this.remorque,dos);
					
					// Integer
				
						writeInteger(this.nombre_portes,dos);
					
					// String
				
						writeString(this.immatriculation_remorque,dos);
					
					// String
				
						writeString(this.source_energie,dos);
					
					// Integer
				
						writeInteger(this.nombre_de_places,dos);
					
					// Integer
				
						writeInteger(this.cylindree,dos);
					
					// Integer
				
						writeInteger(this.double_commande,dos);
					
					// Integer
				
						writeInteger(this.responsabilite_civile,dos);
					
					// Integer
				
						writeInteger(this.utilitaire,dos);
					
					// Integer
				
						writeInteger(this.type_engin,dos);
					
					// Float
				
						if(this.poids_total_autorise_en_charge == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeFloat(this.poids_total_autorise_en_charge);
		            	}
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("code_assure="+code_assure);
		sb.append(",code_assureur="+code_assureur);
		sb.append(",marque="+marque);
		sb.append(",modele="+modele);
		sb.append(",date_premiere_mise_circulation="+String.valueOf(date_premiere_mise_circulation));
		sb.append(",immatriculation="+immatriculation);
		sb.append(",chassis="+chassis);
		sb.append(",usage="+usage);
		sb.append(",charge_utile="+charge_utile);
		sb.append(",puissance_fiscale="+String.valueOf(puissance_fiscale));
		sb.append(",remorque="+remorque);
		sb.append(",nombre_portes="+String.valueOf(nombre_portes));
		sb.append(",immatriculation_remorque="+immatriculation_remorque);
		sb.append(",source_energie="+source_energie);
		sb.append(",nombre_de_places="+String.valueOf(nombre_de_places));
		sb.append(",cylindree="+String.valueOf(cylindree));
		sb.append(",double_commande="+String.valueOf(double_commande));
		sb.append(",responsabilite_civile="+String.valueOf(responsabilite_civile));
		sb.append(",utilitaire="+String.valueOf(utilitaire));
		sb.append(",type_engin="+String.valueOf(type_engin));
		sb.append(",poids_total_autorise_en_charge="+String.valueOf(poids_total_autorise_en_charge));
		sb.append(",errorCode="+errorCode);
		sb.append(",errorMessage="+errorMessage);
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row17Struct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class row12Struct implements routines.system.IPersistableRow<row12Struct> {
    final static byte[] commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[0];
    static byte[] commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[0];

	
			    public String code_assure;

				public String getCode_assure () {
					return this.code_assure;
				}
				
			    public String code_assureur;

				public String getCode_assureur () {
					return this.code_assureur;
				}
				
			    public String marque;

				public String getMarque () {
					return this.marque;
				}
				
			    public String modele;

				public String getModele () {
					return this.modele;
				}
				
			    public java.util.Date date_premiere_mise_circulation;

				public java.util.Date getDate_premiere_mise_circulation () {
					return this.date_premiere_mise_circulation;
				}
				
			    public String immatriculation;

				public String getImmatriculation () {
					return this.immatriculation;
				}
				
			    public String chassis;

				public String getChassis () {
					return this.chassis;
				}
				
			    public String usage;

				public String getUsage () {
					return this.usage;
				}
				
			    public String charge_utile;

				public String getCharge_utile () {
					return this.charge_utile;
				}
				
			    public int puissance_fiscale;

				public int getPuissance_fiscale () {
					return this.puissance_fiscale;
				}
				
			    public String remorque;

				public String getRemorque () {
					return this.remorque;
				}
				
			    public Integer nombre_portes;

				public Integer getNombre_portes () {
					return this.nombre_portes;
				}
				
			    public String immatriculation_remorque;

				public String getImmatriculation_remorque () {
					return this.immatriculation_remorque;
				}
				
			    public String source_energie;

				public String getSource_energie () {
					return this.source_energie;
				}
				
			    public Integer nombre_de_places;

				public Integer getNombre_de_places () {
					return this.nombre_de_places;
				}
				
			    public Integer cylindree;

				public Integer getCylindree () {
					return this.cylindree;
				}
				
			    public Integer double_commande;

				public Integer getDouble_commande () {
					return this.double_commande;
				}
				
			    public Integer responsabilite_civile;

				public Integer getResponsabilite_civile () {
					return this.responsabilite_civile;
				}
				
			    public Integer utilitaire;

				public Integer getUtilitaire () {
					return this.utilitaire;
				}
				
			    public Integer type_engin;

				public Integer getType_engin () {
					return this.type_engin;
				}
				
			    public Float poids_total_autorise_en_charge;

				public Float getPoids_total_autorise_en_charge () {
					return this.poids_total_autorise_en_charge;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length, utf8Charset);
		}
		return strReturn;
	}
	
	private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		String strReturn = null;
		int length = 0;
        length = unmarshaller.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[2 * length];
   				}
			}
			unmarshaller.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
    
    private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(str == null) {
			marshaller.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
            marshaller.writeInt(byteArray.length);
            marshaller.write(byteArray);
    	}
    }

	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}
	
	private java.util.Date readDate(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = unmarshaller.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(unmarshaller.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }
    
    private void writeDate(java.util.Date date1, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(date1 == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeLong(date1.getTime());
    	}
    }
	private Integer readInteger(ObjectInputStream dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}
	
	private Integer readInteger(org.jboss.marshalling.Unmarshaller dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}

	private void writeInteger(Integer intNum, ObjectOutputStream dos) throws IOException{
		if(intNum == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeInt(intNum);
    	}
	}
	
	private void writeInteger(Integer intNum, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(intNum == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeInt(intNum);
    	}
	}

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.marque = readString(dis);
					
					this.modele = readString(dis);
					
					this.date_premiere_mise_circulation = readDate(dis);
					
					this.immatriculation = readString(dis);
					
					this.chassis = readString(dis);
					
					this.usage = readString(dis);
					
					this.charge_utile = readString(dis);
					
			        this.puissance_fiscale = dis.readInt();
					
					this.remorque = readString(dis);
					
						this.nombre_portes = readInteger(dis);
					
					this.immatriculation_remorque = readString(dis);
					
					this.source_energie = readString(dis);
					
						this.nombre_de_places = readInteger(dis);
					
						this.cylindree = readInteger(dis);
					
						this.double_commande = readInteger(dis);
					
						this.responsabilite_civile = readInteger(dis);
					
						this.utilitaire = readInteger(dis);
					
						this.type_engin = readInteger(dis);
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.poids_total_autorise_en_charge = null;
           				} else {
           			    	this.poids_total_autorise_en_charge = dis.readFloat();
           				}
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }
    
    public void readData(org.jboss.marshalling.Unmarshaller dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.marque = readString(dis);
					
					this.modele = readString(dis);
					
					this.date_premiere_mise_circulation = readDate(dis);
					
					this.immatriculation = readString(dis);
					
					this.chassis = readString(dis);
					
					this.usage = readString(dis);
					
					this.charge_utile = readString(dis);
					
			        this.puissance_fiscale = dis.readInt();
					
					this.remorque = readString(dis);
					
						this.nombre_portes = readInteger(dis);
					
					this.immatriculation_remorque = readString(dis);
					
					this.source_energie = readString(dis);
					
						this.nombre_de_places = readInteger(dis);
					
						this.cylindree = readInteger(dis);
					
						this.double_commande = readInteger(dis);
					
						this.responsabilite_civile = readInteger(dis);
					
						this.utilitaire = readInteger(dis);
					
						this.type_engin = readInteger(dis);
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.poids_total_autorise_en_charge = null;
           				} else {
           			    	this.poids_total_autorise_en_charge = dis.readFloat();
           				}
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.marque,dos);
					
					// String
				
						writeString(this.modele,dos);
					
					// java.util.Date
				
						writeDate(this.date_premiere_mise_circulation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.chassis,dos);
					
					// String
				
						writeString(this.usage,dos);
					
					// String
				
						writeString(this.charge_utile,dos);
					
					// int
				
		            	dos.writeInt(this.puissance_fiscale);
					
					// String
				
						writeString(this.remorque,dos);
					
					// Integer
				
						writeInteger(this.nombre_portes,dos);
					
					// String
				
						writeString(this.immatriculation_remorque,dos);
					
					// String
				
						writeString(this.source_energie,dos);
					
					// Integer
				
						writeInteger(this.nombre_de_places,dos);
					
					// Integer
				
						writeInteger(this.cylindree,dos);
					
					// Integer
				
						writeInteger(this.double_commande,dos);
					
					// Integer
				
						writeInteger(this.responsabilite_civile,dos);
					
					// Integer
				
						writeInteger(this.utilitaire,dos);
					
					// Integer
				
						writeInteger(this.type_engin,dos);
					
					// Float
				
						if(this.poids_total_autorise_en_charge == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeFloat(this.poids_total_autorise_en_charge);
		            	}
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }
    
    public void writeData(org.jboss.marshalling.Marshaller dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.marque,dos);
					
					// String
				
						writeString(this.modele,dos);
					
					// java.util.Date
				
						writeDate(this.date_premiere_mise_circulation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.chassis,dos);
					
					// String
				
						writeString(this.usage,dos);
					
					// String
				
						writeString(this.charge_utile,dos);
					
					// int
				
		            	dos.writeInt(this.puissance_fiscale);
					
					// String
				
						writeString(this.remorque,dos);
					
					// Integer
				
						writeInteger(this.nombre_portes,dos);
					
					// String
				
						writeString(this.immatriculation_remorque,dos);
					
					// String
				
						writeString(this.source_energie,dos);
					
					// Integer
				
						writeInteger(this.nombre_de_places,dos);
					
					// Integer
				
						writeInteger(this.cylindree,dos);
					
					// Integer
				
						writeInteger(this.double_commande,dos);
					
					// Integer
				
						writeInteger(this.responsabilite_civile,dos);
					
					// Integer
				
						writeInteger(this.utilitaire,dos);
					
					// Integer
				
						writeInteger(this.type_engin,dos);
					
					// Float
				
						if(this.poids_total_autorise_en_charge == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeFloat(this.poids_total_autorise_en_charge);
		            	}
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("code_assure="+code_assure);
		sb.append(",code_assureur="+code_assureur);
		sb.append(",marque="+marque);
		sb.append(",modele="+modele);
		sb.append(",date_premiere_mise_circulation="+String.valueOf(date_premiere_mise_circulation));
		sb.append(",immatriculation="+immatriculation);
		sb.append(",chassis="+chassis);
		sb.append(",usage="+usage);
		sb.append(",charge_utile="+charge_utile);
		sb.append(",puissance_fiscale="+String.valueOf(puissance_fiscale));
		sb.append(",remorque="+remorque);
		sb.append(",nombre_portes="+String.valueOf(nombre_portes));
		sb.append(",immatriculation_remorque="+immatriculation_remorque);
		sb.append(",source_energie="+source_energie);
		sb.append(",nombre_de_places="+String.valueOf(nombre_de_places));
		sb.append(",cylindree="+String.valueOf(cylindree));
		sb.append(",double_commande="+String.valueOf(double_commande));
		sb.append(",responsabilite_civile="+String.valueOf(responsabilite_civile));
		sb.append(",utilitaire="+String.valueOf(utilitaire));
		sb.append(",type_engin="+String.valueOf(type_engin));
		sb.append(",poids_total_autorise_en_charge="+String.valueOf(poids_total_autorise_en_charge));
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row12Struct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class row13Struct implements routines.system.IPersistableRow<row13Struct> {
    final static byte[] commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[0];
    static byte[] commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[0];

	
			    public String code_assure;

				public String getCode_assure () {
					return this.code_assure;
				}
				
			    public String code_assureur;

				public String getCode_assureur () {
					return this.code_assureur;
				}
				
			    public String marque;

				public String getMarque () {
					return this.marque;
				}
				
			    public String modele;

				public String getModele () {
					return this.modele;
				}
				
			    public java.util.Date date_premiere_mise_circulation;

				public java.util.Date getDate_premiere_mise_circulation () {
					return this.date_premiere_mise_circulation;
				}
				
			    public String immatriculation;

				public String getImmatriculation () {
					return this.immatriculation;
				}
				
			    public String chassis;

				public String getChassis () {
					return this.chassis;
				}
				
			    public String usage;

				public String getUsage () {
					return this.usage;
				}
				
			    public String charge_utile;

				public String getCharge_utile () {
					return this.charge_utile;
				}
				
			    public int puissance_fiscale;

				public int getPuissance_fiscale () {
					return this.puissance_fiscale;
				}
				
			    public String remorque;

				public String getRemorque () {
					return this.remorque;
				}
				
			    public Integer nombre_portes;

				public Integer getNombre_portes () {
					return this.nombre_portes;
				}
				
			    public String immatriculation_remorque;

				public String getImmatriculation_remorque () {
					return this.immatriculation_remorque;
				}
				
			    public String source_energie;

				public String getSource_energie () {
					return this.source_energie;
				}
				
			    public Integer nombre_de_places;

				public Integer getNombre_de_places () {
					return this.nombre_de_places;
				}
				
			    public Integer cylindree;

				public Integer getCylindree () {
					return this.cylindree;
				}
				
			    public Integer double_commande;

				public Integer getDouble_commande () {
					return this.double_commande;
				}
				
			    public Integer responsabilite_civile;

				public Integer getResponsabilite_civile () {
					return this.responsabilite_civile;
				}
				
			    public Integer utilitaire;

				public Integer getUtilitaire () {
					return this.utilitaire;
				}
				
			    public Integer type_engin;

				public Integer getType_engin () {
					return this.type_engin;
				}
				
			    public Float poids_total_autorise_en_charge;

				public Float getPoids_total_autorise_en_charge () {
					return this.poids_total_autorise_en_charge;
				}
				
			    public String errorCode;

				public String getErrorCode () {
					return this.errorCode;
				}
				
			    public String errorMessage;

				public String getErrorMessage () {
					return this.errorMessage;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length, utf8Charset);
		}
		return strReturn;
	}
	
	private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		String strReturn = null;
		int length = 0;
        length = unmarshaller.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[2 * length];
   				}
			}
			unmarshaller.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
    
    private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(str == null) {
			marshaller.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
            marshaller.writeInt(byteArray.length);
            marshaller.write(byteArray);
    	}
    }

	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}
	
	private java.util.Date readDate(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = unmarshaller.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(unmarshaller.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }
    
    private void writeDate(java.util.Date date1, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(date1 == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeLong(date1.getTime());
    	}
    }
	private Integer readInteger(ObjectInputStream dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}
	
	private Integer readInteger(org.jboss.marshalling.Unmarshaller dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}

	private void writeInteger(Integer intNum, ObjectOutputStream dos) throws IOException{
		if(intNum == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeInt(intNum);
    	}
	}
	
	private void writeInteger(Integer intNum, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(intNum == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeInt(intNum);
    	}
	}

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.marque = readString(dis);
					
					this.modele = readString(dis);
					
					this.date_premiere_mise_circulation = readDate(dis);
					
					this.immatriculation = readString(dis);
					
					this.chassis = readString(dis);
					
					this.usage = readString(dis);
					
					this.charge_utile = readString(dis);
					
			        this.puissance_fiscale = dis.readInt();
					
					this.remorque = readString(dis);
					
						this.nombre_portes = readInteger(dis);
					
					this.immatriculation_remorque = readString(dis);
					
					this.source_energie = readString(dis);
					
						this.nombre_de_places = readInteger(dis);
					
						this.cylindree = readInteger(dis);
					
						this.double_commande = readInteger(dis);
					
						this.responsabilite_civile = readInteger(dis);
					
						this.utilitaire = readInteger(dis);
					
						this.type_engin = readInteger(dis);
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.poids_total_autorise_en_charge = null;
           				} else {
           			    	this.poids_total_autorise_en_charge = dis.readFloat();
           				}
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }
    
    public void readData(org.jboss.marshalling.Unmarshaller dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.marque = readString(dis);
					
					this.modele = readString(dis);
					
					this.date_premiere_mise_circulation = readDate(dis);
					
					this.immatriculation = readString(dis);
					
					this.chassis = readString(dis);
					
					this.usage = readString(dis);
					
					this.charge_utile = readString(dis);
					
			        this.puissance_fiscale = dis.readInt();
					
					this.remorque = readString(dis);
					
						this.nombre_portes = readInteger(dis);
					
					this.immatriculation_remorque = readString(dis);
					
					this.source_energie = readString(dis);
					
						this.nombre_de_places = readInteger(dis);
					
						this.cylindree = readInteger(dis);
					
						this.double_commande = readInteger(dis);
					
						this.responsabilite_civile = readInteger(dis);
					
						this.utilitaire = readInteger(dis);
					
						this.type_engin = readInteger(dis);
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.poids_total_autorise_en_charge = null;
           				} else {
           			    	this.poids_total_autorise_en_charge = dis.readFloat();
           				}
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.marque,dos);
					
					// String
				
						writeString(this.modele,dos);
					
					// java.util.Date
				
						writeDate(this.date_premiere_mise_circulation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.chassis,dos);
					
					// String
				
						writeString(this.usage,dos);
					
					// String
				
						writeString(this.charge_utile,dos);
					
					// int
				
		            	dos.writeInt(this.puissance_fiscale);
					
					// String
				
						writeString(this.remorque,dos);
					
					// Integer
				
						writeInteger(this.nombre_portes,dos);
					
					// String
				
						writeString(this.immatriculation_remorque,dos);
					
					// String
				
						writeString(this.source_energie,dos);
					
					// Integer
				
						writeInteger(this.nombre_de_places,dos);
					
					// Integer
				
						writeInteger(this.cylindree,dos);
					
					// Integer
				
						writeInteger(this.double_commande,dos);
					
					// Integer
				
						writeInteger(this.responsabilite_civile,dos);
					
					// Integer
				
						writeInteger(this.utilitaire,dos);
					
					// Integer
				
						writeInteger(this.type_engin,dos);
					
					// Float
				
						if(this.poids_total_autorise_en_charge == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeFloat(this.poids_total_autorise_en_charge);
		            	}
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }
    
    public void writeData(org.jboss.marshalling.Marshaller dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.marque,dos);
					
					// String
				
						writeString(this.modele,dos);
					
					// java.util.Date
				
						writeDate(this.date_premiere_mise_circulation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.chassis,dos);
					
					// String
				
						writeString(this.usage,dos);
					
					// String
				
						writeString(this.charge_utile,dos);
					
					// int
				
		            	dos.writeInt(this.puissance_fiscale);
					
					// String
				
						writeString(this.remorque,dos);
					
					// Integer
				
						writeInteger(this.nombre_portes,dos);
					
					// String
				
						writeString(this.immatriculation_remorque,dos);
					
					// String
				
						writeString(this.source_energie,dos);
					
					// Integer
				
						writeInteger(this.nombre_de_places,dos);
					
					// Integer
				
						writeInteger(this.cylindree,dos);
					
					// Integer
				
						writeInteger(this.double_commande,dos);
					
					// Integer
				
						writeInteger(this.responsabilite_civile,dos);
					
					// Integer
				
						writeInteger(this.utilitaire,dos);
					
					// Integer
				
						writeInteger(this.type_engin,dos);
					
					// Float
				
						if(this.poids_total_autorise_en_charge == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeFloat(this.poids_total_autorise_en_charge);
		            	}
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("code_assure="+code_assure);
		sb.append(",code_assureur="+code_assureur);
		sb.append(",marque="+marque);
		sb.append(",modele="+modele);
		sb.append(",date_premiere_mise_circulation="+String.valueOf(date_premiere_mise_circulation));
		sb.append(",immatriculation="+immatriculation);
		sb.append(",chassis="+chassis);
		sb.append(",usage="+usage);
		sb.append(",charge_utile="+charge_utile);
		sb.append(",puissance_fiscale="+String.valueOf(puissance_fiscale));
		sb.append(",remorque="+remorque);
		sb.append(",nombre_portes="+String.valueOf(nombre_portes));
		sb.append(",immatriculation_remorque="+immatriculation_remorque);
		sb.append(",source_energie="+source_energie);
		sb.append(",nombre_de_places="+String.valueOf(nombre_de_places));
		sb.append(",cylindree="+String.valueOf(cylindree));
		sb.append(",double_commande="+String.valueOf(double_commande));
		sb.append(",responsabilite_civile="+String.valueOf(responsabilite_civile));
		sb.append(",utilitaire="+String.valueOf(utilitaire));
		sb.append(",type_engin="+String.valueOf(type_engin));
		sb.append(",poids_total_autorise_en_charge="+String.valueOf(poids_total_autorise_en_charge));
		sb.append(",errorCode="+errorCode);
		sb.append(",errorMessage="+errorMessage);
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row13Struct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class row1Struct implements routines.system.IPersistableRow<row1Struct> {
    final static byte[] commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[0];
    static byte[] commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[0];

	
			    public String code_assure;

				public String getCode_assure () {
					return this.code_assure;
				}
				
			    public String code_assureur;

				public String getCode_assureur () {
					return this.code_assureur;
				}
				
			    public String marque;

				public String getMarque () {
					return this.marque;
				}
				
			    public String modele;

				public String getModele () {
					return this.modele;
				}
				
			    public java.util.Date date_premiere_mise_circulation;

				public java.util.Date getDate_premiere_mise_circulation () {
					return this.date_premiere_mise_circulation;
				}
				
			    public String immatriculation;

				public String getImmatriculation () {
					return this.immatriculation;
				}
				
			    public String chassis;

				public String getChassis () {
					return this.chassis;
				}
				
			    public String usage;

				public String getUsage () {
					return this.usage;
				}
				
			    public String charge_utile;

				public String getCharge_utile () {
					return this.charge_utile;
				}
				
			    public Integer puissance_fiscale;

				public Integer getPuissance_fiscale () {
					return this.puissance_fiscale;
				}
				
			    public String remorque;

				public String getRemorque () {
					return this.remorque;
				}
				
			    public Integer nombre_portes;

				public Integer getNombre_portes () {
					return this.nombre_portes;
				}
				
			    public String immatriculation_remorque;

				public String getImmatriculation_remorque () {
					return this.immatriculation_remorque;
				}
				
			    public String source_energie;

				public String getSource_energie () {
					return this.source_energie;
				}
				
			    public String nombre_de_places;

				public String getNombre_de_places () {
					return this.nombre_de_places;
				}
				
			    public Integer cylindree;

				public Integer getCylindree () {
					return this.cylindree;
				}
				
			    public Integer double_commande;

				public Integer getDouble_commande () {
					return this.double_commande;
				}
				
			    public Integer responsabilite_civile;

				public Integer getResponsabilite_civile () {
					return this.responsabilite_civile;
				}
				
			    public Integer utilitaire;

				public Integer getUtilitaire () {
					return this.utilitaire;
				}
				
			    public Integer type_engin;

				public Integer getType_engin () {
					return this.type_engin;
				}
				
			    public Float poids_total_autorise_en_charge;

				public Float getPoids_total_autorise_en_charge () {
					return this.poids_total_autorise_en_charge;
				}
				
			    public java.util.Date date_extraction;

				public java.util.Date getDate_extraction () {
					return this.date_extraction;
				}
				
			    public java.util.Date date_depot;

				public java.util.Date getDate_depot () {
					return this.date_depot;
				}
				
			    public String errorCode;

				public String getErrorCode () {
					return this.errorCode;
				}
				
			    public String errorMessage;

				public String getErrorMessage () {
					return this.errorMessage;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length, utf8Charset);
		}
		return strReturn;
	}
	
	private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		String strReturn = null;
		int length = 0;
        length = unmarshaller.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[2 * length];
   				}
			}
			unmarshaller.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
    
    private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(str == null) {
			marshaller.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
            marshaller.writeInt(byteArray.length);
            marshaller.write(byteArray);
    	}
    }

	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}
	
	private java.util.Date readDate(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = unmarshaller.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(unmarshaller.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }
    
    private void writeDate(java.util.Date date1, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(date1 == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeLong(date1.getTime());
    	}
    }
	private Integer readInteger(ObjectInputStream dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}
	
	private Integer readInteger(org.jboss.marshalling.Unmarshaller dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}

	private void writeInteger(Integer intNum, ObjectOutputStream dos) throws IOException{
		if(intNum == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeInt(intNum);
    	}
	}
	
	private void writeInteger(Integer intNum, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(intNum == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeInt(intNum);
    	}
	}

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.marque = readString(dis);
					
					this.modele = readString(dis);
					
					this.date_premiere_mise_circulation = readDate(dis);
					
					this.immatriculation = readString(dis);
					
					this.chassis = readString(dis);
					
					this.usage = readString(dis);
					
					this.charge_utile = readString(dis);
					
						this.puissance_fiscale = readInteger(dis);
					
					this.remorque = readString(dis);
					
						this.nombre_portes = readInteger(dis);
					
					this.immatriculation_remorque = readString(dis);
					
					this.source_energie = readString(dis);
					
					this.nombre_de_places = readString(dis);
					
						this.cylindree = readInteger(dis);
					
						this.double_commande = readInteger(dis);
					
						this.responsabilite_civile = readInteger(dis);
					
						this.utilitaire = readInteger(dis);
					
						this.type_engin = readInteger(dis);
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.poids_total_autorise_en_charge = null;
           				} else {
           			    	this.poids_total_autorise_en_charge = dis.readFloat();
           				}
					
					this.date_extraction = readDate(dis);
					
					this.date_depot = readDate(dis);
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }
    
    public void readData(org.jboss.marshalling.Unmarshaller dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.marque = readString(dis);
					
					this.modele = readString(dis);
					
					this.date_premiere_mise_circulation = readDate(dis);
					
					this.immatriculation = readString(dis);
					
					this.chassis = readString(dis);
					
					this.usage = readString(dis);
					
					this.charge_utile = readString(dis);
					
						this.puissance_fiscale = readInteger(dis);
					
					this.remorque = readString(dis);
					
						this.nombre_portes = readInteger(dis);
					
					this.immatriculation_remorque = readString(dis);
					
					this.source_energie = readString(dis);
					
					this.nombre_de_places = readString(dis);
					
						this.cylindree = readInteger(dis);
					
						this.double_commande = readInteger(dis);
					
						this.responsabilite_civile = readInteger(dis);
					
						this.utilitaire = readInteger(dis);
					
						this.type_engin = readInteger(dis);
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.poids_total_autorise_en_charge = null;
           				} else {
           			    	this.poids_total_autorise_en_charge = dis.readFloat();
           				}
					
					this.date_extraction = readDate(dis);
					
					this.date_depot = readDate(dis);
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.marque,dos);
					
					// String
				
						writeString(this.modele,dos);
					
					// java.util.Date
				
						writeDate(this.date_premiere_mise_circulation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.chassis,dos);
					
					// String
				
						writeString(this.usage,dos);
					
					// String
				
						writeString(this.charge_utile,dos);
					
					// Integer
				
						writeInteger(this.puissance_fiscale,dos);
					
					// String
				
						writeString(this.remorque,dos);
					
					// Integer
				
						writeInteger(this.nombre_portes,dos);
					
					// String
				
						writeString(this.immatriculation_remorque,dos);
					
					// String
				
						writeString(this.source_energie,dos);
					
					// String
				
						writeString(this.nombre_de_places,dos);
					
					// Integer
				
						writeInteger(this.cylindree,dos);
					
					// Integer
				
						writeInteger(this.double_commande,dos);
					
					// Integer
				
						writeInteger(this.responsabilite_civile,dos);
					
					// Integer
				
						writeInteger(this.utilitaire,dos);
					
					// Integer
				
						writeInteger(this.type_engin,dos);
					
					// Float
				
						if(this.poids_total_autorise_en_charge == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeFloat(this.poids_total_autorise_en_charge);
		            	}
					
					// java.util.Date
				
						writeDate(this.date_extraction,dos);
					
					// java.util.Date
				
						writeDate(this.date_depot,dos);
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }
    
    public void writeData(org.jboss.marshalling.Marshaller dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.marque,dos);
					
					// String
				
						writeString(this.modele,dos);
					
					// java.util.Date
				
						writeDate(this.date_premiere_mise_circulation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.chassis,dos);
					
					// String
				
						writeString(this.usage,dos);
					
					// String
				
						writeString(this.charge_utile,dos);
					
					// Integer
				
						writeInteger(this.puissance_fiscale,dos);
					
					// String
				
						writeString(this.remorque,dos);
					
					// Integer
				
						writeInteger(this.nombre_portes,dos);
					
					// String
				
						writeString(this.immatriculation_remorque,dos);
					
					// String
				
						writeString(this.source_energie,dos);
					
					// String
				
						writeString(this.nombre_de_places,dos);
					
					// Integer
				
						writeInteger(this.cylindree,dos);
					
					// Integer
				
						writeInteger(this.double_commande,dos);
					
					// Integer
				
						writeInteger(this.responsabilite_civile,dos);
					
					// Integer
				
						writeInteger(this.utilitaire,dos);
					
					// Integer
				
						writeInteger(this.type_engin,dos);
					
					// Float
				
						if(this.poids_total_autorise_en_charge == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeFloat(this.poids_total_autorise_en_charge);
		            	}
					
					// java.util.Date
				
						writeDate(this.date_extraction,dos);
					
					// java.util.Date
				
						writeDate(this.date_depot,dos);
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("code_assure="+code_assure);
		sb.append(",code_assureur="+code_assureur);
		sb.append(",marque="+marque);
		sb.append(",modele="+modele);
		sb.append(",date_premiere_mise_circulation="+String.valueOf(date_premiere_mise_circulation));
		sb.append(",immatriculation="+immatriculation);
		sb.append(",chassis="+chassis);
		sb.append(",usage="+usage);
		sb.append(",charge_utile="+charge_utile);
		sb.append(",puissance_fiscale="+String.valueOf(puissance_fiscale));
		sb.append(",remorque="+remorque);
		sb.append(",nombre_portes="+String.valueOf(nombre_portes));
		sb.append(",immatriculation_remorque="+immatriculation_remorque);
		sb.append(",source_energie="+source_energie);
		sb.append(",nombre_de_places="+nombre_de_places);
		sb.append(",cylindree="+String.valueOf(cylindree));
		sb.append(",double_commande="+String.valueOf(double_commande));
		sb.append(",responsabilite_civile="+String.valueOf(responsabilite_civile));
		sb.append(",utilitaire="+String.valueOf(utilitaire));
		sb.append(",type_engin="+String.valueOf(type_engin));
		sb.append(",poids_total_autorise_en_charge="+String.valueOf(poids_total_autorise_en_charge));
		sb.append(",date_extraction="+String.valueOf(date_extraction));
		sb.append(",date_depot="+String.valueOf(date_depot));
		sb.append(",errorCode="+errorCode);
		sb.append(",errorMessage="+errorMessage);
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row1Struct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class vehicule_rejet_structureStruct implements routines.system.IPersistableRow<vehicule_rejet_structureStruct> {
    final static byte[] commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[0];
    static byte[] commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[0];

	
			    public String code_assure;

				public String getCode_assure () {
					return this.code_assure;
				}
				
			    public String code_assureur;

				public String getCode_assureur () {
					return this.code_assureur;
				}
				
			    public String marque;

				public String getMarque () {
					return this.marque;
				}
				
			    public String modele;

				public String getModele () {
					return this.modele;
				}
				
			    public java.util.Date date_premiere_mise_circulation;

				public java.util.Date getDate_premiere_mise_circulation () {
					return this.date_premiere_mise_circulation;
				}
				
			    public String immatriculation;

				public String getImmatriculation () {
					return this.immatriculation;
				}
				
			    public String chassis;

				public String getChassis () {
					return this.chassis;
				}
				
			    public String usage;

				public String getUsage () {
					return this.usage;
				}
				
			    public String charge_utile;

				public String getCharge_utile () {
					return this.charge_utile;
				}
				
			    public Integer puissance_fiscale;

				public Integer getPuissance_fiscale () {
					return this.puissance_fiscale;
				}
				
			    public String remorque;

				public String getRemorque () {
					return this.remorque;
				}
				
			    public Integer nombre_portes;

				public Integer getNombre_portes () {
					return this.nombre_portes;
				}
				
			    public String immatriculation_remorque;

				public String getImmatriculation_remorque () {
					return this.immatriculation_remorque;
				}
				
			    public String source_energie;

				public String getSource_energie () {
					return this.source_energie;
				}
				
			    public String nombre_de_places;

				public String getNombre_de_places () {
					return this.nombre_de_places;
				}
				
			    public Integer cylindree;

				public Integer getCylindree () {
					return this.cylindree;
				}
				
			    public Integer double_commande;

				public Integer getDouble_commande () {
					return this.double_commande;
				}
				
			    public Integer responsabilite_civile;

				public Integer getResponsabilite_civile () {
					return this.responsabilite_civile;
				}
				
			    public Integer utilitaire;

				public Integer getUtilitaire () {
					return this.utilitaire;
				}
				
			    public Integer type_engin;

				public Integer getType_engin () {
					return this.type_engin;
				}
				
			    public Float poids_total_autorise_en_charge;

				public Float getPoids_total_autorise_en_charge () {
					return this.poids_total_autorise_en_charge;
				}
				
			    public java.util.Date date_extraction;

				public java.util.Date getDate_extraction () {
					return this.date_extraction;
				}
				
			    public java.util.Date date_depot;

				public java.util.Date getDate_depot () {
					return this.date_depot;
				}
				
			    public String errorCode;

				public String getErrorCode () {
					return this.errorCode;
				}
				
			    public String errorMessage;

				public String getErrorMessage () {
					return this.errorMessage;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length, utf8Charset);
		}
		return strReturn;
	}
	
	private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		String strReturn = null;
		int length = 0;
        length = unmarshaller.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[2 * length];
   				}
			}
			unmarshaller.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
    
    private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(str == null) {
			marshaller.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
            marshaller.writeInt(byteArray.length);
            marshaller.write(byteArray);
    	}
    }

	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}
	
	private java.util.Date readDate(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = unmarshaller.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(unmarshaller.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }
    
    private void writeDate(java.util.Date date1, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(date1 == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeLong(date1.getTime());
    	}
    }
	private Integer readInteger(ObjectInputStream dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}
	
	private Integer readInteger(org.jboss.marshalling.Unmarshaller dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}

	private void writeInteger(Integer intNum, ObjectOutputStream dos) throws IOException{
		if(intNum == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeInt(intNum);
    	}
	}
	
	private void writeInteger(Integer intNum, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(intNum == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeInt(intNum);
    	}
	}

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.marque = readString(dis);
					
					this.modele = readString(dis);
					
					this.date_premiere_mise_circulation = readDate(dis);
					
					this.immatriculation = readString(dis);
					
					this.chassis = readString(dis);
					
					this.usage = readString(dis);
					
					this.charge_utile = readString(dis);
					
						this.puissance_fiscale = readInteger(dis);
					
					this.remorque = readString(dis);
					
						this.nombre_portes = readInteger(dis);
					
					this.immatriculation_remorque = readString(dis);
					
					this.source_energie = readString(dis);
					
					this.nombre_de_places = readString(dis);
					
						this.cylindree = readInteger(dis);
					
						this.double_commande = readInteger(dis);
					
						this.responsabilite_civile = readInteger(dis);
					
						this.utilitaire = readInteger(dis);
					
						this.type_engin = readInteger(dis);
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.poids_total_autorise_en_charge = null;
           				} else {
           			    	this.poids_total_autorise_en_charge = dis.readFloat();
           				}
					
					this.date_extraction = readDate(dis);
					
					this.date_depot = readDate(dis);
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }
    
    public void readData(org.jboss.marshalling.Unmarshaller dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.marque = readString(dis);
					
					this.modele = readString(dis);
					
					this.date_premiere_mise_circulation = readDate(dis);
					
					this.immatriculation = readString(dis);
					
					this.chassis = readString(dis);
					
					this.usage = readString(dis);
					
					this.charge_utile = readString(dis);
					
						this.puissance_fiscale = readInteger(dis);
					
					this.remorque = readString(dis);
					
						this.nombre_portes = readInteger(dis);
					
					this.immatriculation_remorque = readString(dis);
					
					this.source_energie = readString(dis);
					
					this.nombre_de_places = readString(dis);
					
						this.cylindree = readInteger(dis);
					
						this.double_commande = readInteger(dis);
					
						this.responsabilite_civile = readInteger(dis);
					
						this.utilitaire = readInteger(dis);
					
						this.type_engin = readInteger(dis);
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.poids_total_autorise_en_charge = null;
           				} else {
           			    	this.poids_total_autorise_en_charge = dis.readFloat();
           				}
					
					this.date_extraction = readDate(dis);
					
					this.date_depot = readDate(dis);
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.marque,dos);
					
					// String
				
						writeString(this.modele,dos);
					
					// java.util.Date
				
						writeDate(this.date_premiere_mise_circulation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.chassis,dos);
					
					// String
				
						writeString(this.usage,dos);
					
					// String
				
						writeString(this.charge_utile,dos);
					
					// Integer
				
						writeInteger(this.puissance_fiscale,dos);
					
					// String
				
						writeString(this.remorque,dos);
					
					// Integer
				
						writeInteger(this.nombre_portes,dos);
					
					// String
				
						writeString(this.immatriculation_remorque,dos);
					
					// String
				
						writeString(this.source_energie,dos);
					
					// String
				
						writeString(this.nombre_de_places,dos);
					
					// Integer
				
						writeInteger(this.cylindree,dos);
					
					// Integer
				
						writeInteger(this.double_commande,dos);
					
					// Integer
				
						writeInteger(this.responsabilite_civile,dos);
					
					// Integer
				
						writeInteger(this.utilitaire,dos);
					
					// Integer
				
						writeInteger(this.type_engin,dos);
					
					// Float
				
						if(this.poids_total_autorise_en_charge == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeFloat(this.poids_total_autorise_en_charge);
		            	}
					
					// java.util.Date
				
						writeDate(this.date_extraction,dos);
					
					// java.util.Date
				
						writeDate(this.date_depot,dos);
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }
    
    public void writeData(org.jboss.marshalling.Marshaller dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.marque,dos);
					
					// String
				
						writeString(this.modele,dos);
					
					// java.util.Date
				
						writeDate(this.date_premiere_mise_circulation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.chassis,dos);
					
					// String
				
						writeString(this.usage,dos);
					
					// String
				
						writeString(this.charge_utile,dos);
					
					// Integer
				
						writeInteger(this.puissance_fiscale,dos);
					
					// String
				
						writeString(this.remorque,dos);
					
					// Integer
				
						writeInteger(this.nombre_portes,dos);
					
					// String
				
						writeString(this.immatriculation_remorque,dos);
					
					// String
				
						writeString(this.source_energie,dos);
					
					// String
				
						writeString(this.nombre_de_places,dos);
					
					// Integer
				
						writeInteger(this.cylindree,dos);
					
					// Integer
				
						writeInteger(this.double_commande,dos);
					
					// Integer
				
						writeInteger(this.responsabilite_civile,dos);
					
					// Integer
				
						writeInteger(this.utilitaire,dos);
					
					// Integer
				
						writeInteger(this.type_engin,dos);
					
					// Float
				
						if(this.poids_total_autorise_en_charge == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeFloat(this.poids_total_autorise_en_charge);
		            	}
					
					// java.util.Date
				
						writeDate(this.date_extraction,dos);
					
					// java.util.Date
				
						writeDate(this.date_depot,dos);
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("code_assure="+code_assure);
		sb.append(",code_assureur="+code_assureur);
		sb.append(",marque="+marque);
		sb.append(",modele="+modele);
		sb.append(",date_premiere_mise_circulation="+String.valueOf(date_premiere_mise_circulation));
		sb.append(",immatriculation="+immatriculation);
		sb.append(",chassis="+chassis);
		sb.append(",usage="+usage);
		sb.append(",charge_utile="+charge_utile);
		sb.append(",puissance_fiscale="+String.valueOf(puissance_fiscale));
		sb.append(",remorque="+remorque);
		sb.append(",nombre_portes="+String.valueOf(nombre_portes));
		sb.append(",immatriculation_remorque="+immatriculation_remorque);
		sb.append(",source_energie="+source_energie);
		sb.append(",nombre_de_places="+nombre_de_places);
		sb.append(",cylindree="+String.valueOf(cylindree));
		sb.append(",double_commande="+String.valueOf(double_commande));
		sb.append(",responsabilite_civile="+String.valueOf(responsabilite_civile));
		sb.append(",utilitaire="+String.valueOf(utilitaire));
		sb.append(",type_engin="+String.valueOf(type_engin));
		sb.append(",poids_total_autorise_en_charge="+String.valueOf(poids_total_autorise_en_charge));
		sb.append(",date_extraction="+String.valueOf(date_extraction));
		sb.append(",date_depot="+String.valueOf(date_depot));
		sb.append(",errorCode="+errorCode);
		sb.append(",errorMessage="+errorMessage);
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(vehicule_rejet_structureStruct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class row18Struct implements routines.system.IPersistableRow<row18Struct> {
    final static byte[] commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[0];
    static byte[] commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[0];

	
			    public String code_assure;

				public String getCode_assure () {
					return this.code_assure;
				}
				
			    public String code_assureur;

				public String getCode_assureur () {
					return this.code_assureur;
				}
				
			    public String marque;

				public String getMarque () {
					return this.marque;
				}
				
			    public String modele;

				public String getModele () {
					return this.modele;
				}
				
			    public java.util.Date date_premiere_mise_circulation;

				public java.util.Date getDate_premiere_mise_circulation () {
					return this.date_premiere_mise_circulation;
				}
				
			    public String immatriculation;

				public String getImmatriculation () {
					return this.immatriculation;
				}
				
			    public String chassis;

				public String getChassis () {
					return this.chassis;
				}
				
			    public String usage;

				public String getUsage () {
					return this.usage;
				}
				
			    public String charge_utile;

				public String getCharge_utile () {
					return this.charge_utile;
				}
				
			    public int puissance_fiscale;

				public int getPuissance_fiscale () {
					return this.puissance_fiscale;
				}
				
			    public String remorque;

				public String getRemorque () {
					return this.remorque;
				}
				
			    public Integer nombre_portes;

				public Integer getNombre_portes () {
					return this.nombre_portes;
				}
				
			    public String immatriculation_remorque;

				public String getImmatriculation_remorque () {
					return this.immatriculation_remorque;
				}
				
			    public String source_energie;

				public String getSource_energie () {
					return this.source_energie;
				}
				
			    public Integer nombre_de_places;

				public Integer getNombre_de_places () {
					return this.nombre_de_places;
				}
				
			    public Integer cylindree;

				public Integer getCylindree () {
					return this.cylindree;
				}
				
			    public Integer double_commande;

				public Integer getDouble_commande () {
					return this.double_commande;
				}
				
			    public Integer responsabilite_civile;

				public Integer getResponsabilite_civile () {
					return this.responsabilite_civile;
				}
				
			    public Integer utilitaire;

				public Integer getUtilitaire () {
					return this.utilitaire;
				}
				
			    public Integer type_engin;

				public Integer getType_engin () {
					return this.type_engin;
				}
				
			    public Float poids_total_autorise_en_charge;

				public Float getPoids_total_autorise_en_charge () {
					return this.poids_total_autorise_en_charge;
				}
				
			    public String errorCode;

				public String getErrorCode () {
					return this.errorCode;
				}
				
			    public String errorMessage;

				public String getErrorMessage () {
					return this.errorMessage;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length, utf8Charset);
		}
		return strReturn;
	}
	
	private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		String strReturn = null;
		int length = 0;
        length = unmarshaller.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[2 * length];
   				}
			}
			unmarshaller.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
    
    private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(str == null) {
			marshaller.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
            marshaller.writeInt(byteArray.length);
            marshaller.write(byteArray);
    	}
    }

	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}
	
	private java.util.Date readDate(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = unmarshaller.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(unmarshaller.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }
    
    private void writeDate(java.util.Date date1, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(date1 == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeLong(date1.getTime());
    	}
    }
	private Integer readInteger(ObjectInputStream dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}
	
	private Integer readInteger(org.jboss.marshalling.Unmarshaller dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}

	private void writeInteger(Integer intNum, ObjectOutputStream dos) throws IOException{
		if(intNum == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeInt(intNum);
    	}
	}
	
	private void writeInteger(Integer intNum, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(intNum == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeInt(intNum);
    	}
	}

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.marque = readString(dis);
					
					this.modele = readString(dis);
					
					this.date_premiere_mise_circulation = readDate(dis);
					
					this.immatriculation = readString(dis);
					
					this.chassis = readString(dis);
					
					this.usage = readString(dis);
					
					this.charge_utile = readString(dis);
					
			        this.puissance_fiscale = dis.readInt();
					
					this.remorque = readString(dis);
					
						this.nombre_portes = readInteger(dis);
					
					this.immatriculation_remorque = readString(dis);
					
					this.source_energie = readString(dis);
					
						this.nombre_de_places = readInteger(dis);
					
						this.cylindree = readInteger(dis);
					
						this.double_commande = readInteger(dis);
					
						this.responsabilite_civile = readInteger(dis);
					
						this.utilitaire = readInteger(dis);
					
						this.type_engin = readInteger(dis);
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.poids_total_autorise_en_charge = null;
           				} else {
           			    	this.poids_total_autorise_en_charge = dis.readFloat();
           				}
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }
    
    public void readData(org.jboss.marshalling.Unmarshaller dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.marque = readString(dis);
					
					this.modele = readString(dis);
					
					this.date_premiere_mise_circulation = readDate(dis);
					
					this.immatriculation = readString(dis);
					
					this.chassis = readString(dis);
					
					this.usage = readString(dis);
					
					this.charge_utile = readString(dis);
					
			        this.puissance_fiscale = dis.readInt();
					
					this.remorque = readString(dis);
					
						this.nombre_portes = readInteger(dis);
					
					this.immatriculation_remorque = readString(dis);
					
					this.source_energie = readString(dis);
					
						this.nombre_de_places = readInteger(dis);
					
						this.cylindree = readInteger(dis);
					
						this.double_commande = readInteger(dis);
					
						this.responsabilite_civile = readInteger(dis);
					
						this.utilitaire = readInteger(dis);
					
						this.type_engin = readInteger(dis);
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.poids_total_autorise_en_charge = null;
           				} else {
           			    	this.poids_total_autorise_en_charge = dis.readFloat();
           				}
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.marque,dos);
					
					// String
				
						writeString(this.modele,dos);
					
					// java.util.Date
				
						writeDate(this.date_premiere_mise_circulation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.chassis,dos);
					
					// String
				
						writeString(this.usage,dos);
					
					// String
				
						writeString(this.charge_utile,dos);
					
					// int
				
		            	dos.writeInt(this.puissance_fiscale);
					
					// String
				
						writeString(this.remorque,dos);
					
					// Integer
				
						writeInteger(this.nombre_portes,dos);
					
					// String
				
						writeString(this.immatriculation_remorque,dos);
					
					// String
				
						writeString(this.source_energie,dos);
					
					// Integer
				
						writeInteger(this.nombre_de_places,dos);
					
					// Integer
				
						writeInteger(this.cylindree,dos);
					
					// Integer
				
						writeInteger(this.double_commande,dos);
					
					// Integer
				
						writeInteger(this.responsabilite_civile,dos);
					
					// Integer
				
						writeInteger(this.utilitaire,dos);
					
					// Integer
				
						writeInteger(this.type_engin,dos);
					
					// Float
				
						if(this.poids_total_autorise_en_charge == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeFloat(this.poids_total_autorise_en_charge);
		            	}
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }
    
    public void writeData(org.jboss.marshalling.Marshaller dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.marque,dos);
					
					// String
				
						writeString(this.modele,dos);
					
					// java.util.Date
				
						writeDate(this.date_premiere_mise_circulation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.chassis,dos);
					
					// String
				
						writeString(this.usage,dos);
					
					// String
				
						writeString(this.charge_utile,dos);
					
					// int
				
		            	dos.writeInt(this.puissance_fiscale);
					
					// String
				
						writeString(this.remorque,dos);
					
					// Integer
				
						writeInteger(this.nombre_portes,dos);
					
					// String
				
						writeString(this.immatriculation_remorque,dos);
					
					// String
				
						writeString(this.source_energie,dos);
					
					// Integer
				
						writeInteger(this.nombre_de_places,dos);
					
					// Integer
				
						writeInteger(this.cylindree,dos);
					
					// Integer
				
						writeInteger(this.double_commande,dos);
					
					// Integer
				
						writeInteger(this.responsabilite_civile,dos);
					
					// Integer
				
						writeInteger(this.utilitaire,dos);
					
					// Integer
				
						writeInteger(this.type_engin,dos);
					
					// Float
				
						if(this.poids_total_autorise_en_charge == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeFloat(this.poids_total_autorise_en_charge);
		            	}
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("code_assure="+code_assure);
		sb.append(",code_assureur="+code_assureur);
		sb.append(",marque="+marque);
		sb.append(",modele="+modele);
		sb.append(",date_premiere_mise_circulation="+String.valueOf(date_premiere_mise_circulation));
		sb.append(",immatriculation="+immatriculation);
		sb.append(",chassis="+chassis);
		sb.append(",usage="+usage);
		sb.append(",charge_utile="+charge_utile);
		sb.append(",puissance_fiscale="+String.valueOf(puissance_fiscale));
		sb.append(",remorque="+remorque);
		sb.append(",nombre_portes="+String.valueOf(nombre_portes));
		sb.append(",immatriculation_remorque="+immatriculation_remorque);
		sb.append(",source_energie="+source_energie);
		sb.append(",nombre_de_places="+String.valueOf(nombre_de_places));
		sb.append(",cylindree="+String.valueOf(cylindree));
		sb.append(",double_commande="+String.valueOf(double_commande));
		sb.append(",responsabilite_civile="+String.valueOf(responsabilite_civile));
		sb.append(",utilitaire="+String.valueOf(utilitaire));
		sb.append(",type_engin="+String.valueOf(type_engin));
		sb.append(",poids_total_autorise_en_charge="+String.valueOf(poids_total_autorise_en_charge));
		sb.append(",errorCode="+errorCode);
		sb.append(",errorMessage="+errorMessage);
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row18Struct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class row10Struct implements routines.system.IPersistableRow<row10Struct> {
    final static byte[] commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[0];
    static byte[] commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[0];

	
			    public String code_assure;

				public String getCode_assure () {
					return this.code_assure;
				}
				
			    public String code_assureur;

				public String getCode_assureur () {
					return this.code_assureur;
				}
				
			    public String marque;

				public String getMarque () {
					return this.marque;
				}
				
			    public String modele;

				public String getModele () {
					return this.modele;
				}
				
			    public java.util.Date date_premiere_mise_circulation;

				public java.util.Date getDate_premiere_mise_circulation () {
					return this.date_premiere_mise_circulation;
				}
				
			    public String immatriculation;

				public String getImmatriculation () {
					return this.immatriculation;
				}
				
			    public String chassis;

				public String getChassis () {
					return this.chassis;
				}
				
			    public String usage;

				public String getUsage () {
					return this.usage;
				}
				
			    public String charge_utile;

				public String getCharge_utile () {
					return this.charge_utile;
				}
				
			    public int puissance_fiscale;

				public int getPuissance_fiscale () {
					return this.puissance_fiscale;
				}
				
			    public String remorque;

				public String getRemorque () {
					return this.remorque;
				}
				
			    public Integer nombre_portes;

				public Integer getNombre_portes () {
					return this.nombre_portes;
				}
				
			    public String immatriculation_remorque;

				public String getImmatriculation_remorque () {
					return this.immatriculation_remorque;
				}
				
			    public String source_energie;

				public String getSource_energie () {
					return this.source_energie;
				}
				
			    public Integer nombre_de_places;

				public Integer getNombre_de_places () {
					return this.nombre_de_places;
				}
				
			    public Integer cylindree;

				public Integer getCylindree () {
					return this.cylindree;
				}
				
			    public Integer double_commande;

				public Integer getDouble_commande () {
					return this.double_commande;
				}
				
			    public Integer responsabilite_civile;

				public Integer getResponsabilite_civile () {
					return this.responsabilite_civile;
				}
				
			    public Integer utilitaire;

				public Integer getUtilitaire () {
					return this.utilitaire;
				}
				
			    public Integer type_engin;

				public Integer getType_engin () {
					return this.type_engin;
				}
				
			    public Float poids_total_autorise_en_charge;

				public Float getPoids_total_autorise_en_charge () {
					return this.poids_total_autorise_en_charge;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length, utf8Charset);
		}
		return strReturn;
	}
	
	private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		String strReturn = null;
		int length = 0;
        length = unmarshaller.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[2 * length];
   				}
			}
			unmarshaller.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
    
    private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(str == null) {
			marshaller.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
            marshaller.writeInt(byteArray.length);
            marshaller.write(byteArray);
    	}
    }

	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}
	
	private java.util.Date readDate(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = unmarshaller.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(unmarshaller.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }
    
    private void writeDate(java.util.Date date1, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(date1 == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeLong(date1.getTime());
    	}
    }
	private Integer readInteger(ObjectInputStream dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}
	
	private Integer readInteger(org.jboss.marshalling.Unmarshaller dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}

	private void writeInteger(Integer intNum, ObjectOutputStream dos) throws IOException{
		if(intNum == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeInt(intNum);
    	}
	}
	
	private void writeInteger(Integer intNum, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(intNum == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeInt(intNum);
    	}
	}

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.marque = readString(dis);
					
					this.modele = readString(dis);
					
					this.date_premiere_mise_circulation = readDate(dis);
					
					this.immatriculation = readString(dis);
					
					this.chassis = readString(dis);
					
					this.usage = readString(dis);
					
					this.charge_utile = readString(dis);
					
			        this.puissance_fiscale = dis.readInt();
					
					this.remorque = readString(dis);
					
						this.nombre_portes = readInteger(dis);
					
					this.immatriculation_remorque = readString(dis);
					
					this.source_energie = readString(dis);
					
						this.nombre_de_places = readInteger(dis);
					
						this.cylindree = readInteger(dis);
					
						this.double_commande = readInteger(dis);
					
						this.responsabilite_civile = readInteger(dis);
					
						this.utilitaire = readInteger(dis);
					
						this.type_engin = readInteger(dis);
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.poids_total_autorise_en_charge = null;
           				} else {
           			    	this.poids_total_autorise_en_charge = dis.readFloat();
           				}
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }
    
    public void readData(org.jboss.marshalling.Unmarshaller dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.marque = readString(dis);
					
					this.modele = readString(dis);
					
					this.date_premiere_mise_circulation = readDate(dis);
					
					this.immatriculation = readString(dis);
					
					this.chassis = readString(dis);
					
					this.usage = readString(dis);
					
					this.charge_utile = readString(dis);
					
			        this.puissance_fiscale = dis.readInt();
					
					this.remorque = readString(dis);
					
						this.nombre_portes = readInteger(dis);
					
					this.immatriculation_remorque = readString(dis);
					
					this.source_energie = readString(dis);
					
						this.nombre_de_places = readInteger(dis);
					
						this.cylindree = readInteger(dis);
					
						this.double_commande = readInteger(dis);
					
						this.responsabilite_civile = readInteger(dis);
					
						this.utilitaire = readInteger(dis);
					
						this.type_engin = readInteger(dis);
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.poids_total_autorise_en_charge = null;
           				} else {
           			    	this.poids_total_autorise_en_charge = dis.readFloat();
           				}
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.marque,dos);
					
					// String
				
						writeString(this.modele,dos);
					
					// java.util.Date
				
						writeDate(this.date_premiere_mise_circulation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.chassis,dos);
					
					// String
				
						writeString(this.usage,dos);
					
					// String
				
						writeString(this.charge_utile,dos);
					
					// int
				
		            	dos.writeInt(this.puissance_fiscale);
					
					// String
				
						writeString(this.remorque,dos);
					
					// Integer
				
						writeInteger(this.nombre_portes,dos);
					
					// String
				
						writeString(this.immatriculation_remorque,dos);
					
					// String
				
						writeString(this.source_energie,dos);
					
					// Integer
				
						writeInteger(this.nombre_de_places,dos);
					
					// Integer
				
						writeInteger(this.cylindree,dos);
					
					// Integer
				
						writeInteger(this.double_commande,dos);
					
					// Integer
				
						writeInteger(this.responsabilite_civile,dos);
					
					// Integer
				
						writeInteger(this.utilitaire,dos);
					
					// Integer
				
						writeInteger(this.type_engin,dos);
					
					// Float
				
						if(this.poids_total_autorise_en_charge == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeFloat(this.poids_total_autorise_en_charge);
		            	}
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }
    
    public void writeData(org.jboss.marshalling.Marshaller dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.marque,dos);
					
					// String
				
						writeString(this.modele,dos);
					
					// java.util.Date
				
						writeDate(this.date_premiere_mise_circulation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.chassis,dos);
					
					// String
				
						writeString(this.usage,dos);
					
					// String
				
						writeString(this.charge_utile,dos);
					
					// int
				
		            	dos.writeInt(this.puissance_fiscale);
					
					// String
				
						writeString(this.remorque,dos);
					
					// Integer
				
						writeInteger(this.nombre_portes,dos);
					
					// String
				
						writeString(this.immatriculation_remorque,dos);
					
					// String
				
						writeString(this.source_energie,dos);
					
					// Integer
				
						writeInteger(this.nombre_de_places,dos);
					
					// Integer
				
						writeInteger(this.cylindree,dos);
					
					// Integer
				
						writeInteger(this.double_commande,dos);
					
					// Integer
				
						writeInteger(this.responsabilite_civile,dos);
					
					// Integer
				
						writeInteger(this.utilitaire,dos);
					
					// Integer
				
						writeInteger(this.type_engin,dos);
					
					// Float
				
						if(this.poids_total_autorise_en_charge == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeFloat(this.poids_total_autorise_en_charge);
		            	}
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("code_assure="+code_assure);
		sb.append(",code_assureur="+code_assureur);
		sb.append(",marque="+marque);
		sb.append(",modele="+modele);
		sb.append(",date_premiere_mise_circulation="+String.valueOf(date_premiere_mise_circulation));
		sb.append(",immatriculation="+immatriculation);
		sb.append(",chassis="+chassis);
		sb.append(",usage="+usage);
		sb.append(",charge_utile="+charge_utile);
		sb.append(",puissance_fiscale="+String.valueOf(puissance_fiscale));
		sb.append(",remorque="+remorque);
		sb.append(",nombre_portes="+String.valueOf(nombre_portes));
		sb.append(",immatriculation_remorque="+immatriculation_remorque);
		sb.append(",source_energie="+source_energie);
		sb.append(",nombre_de_places="+String.valueOf(nombre_de_places));
		sb.append(",cylindree="+String.valueOf(cylindree));
		sb.append(",double_commande="+String.valueOf(double_commande));
		sb.append(",responsabilite_civile="+String.valueOf(responsabilite_civile));
		sb.append(",utilitaire="+String.valueOf(utilitaire));
		sb.append(",type_engin="+String.valueOf(type_engin));
		sb.append(",poids_total_autorise_en_charge="+String.valueOf(poids_total_autorise_en_charge));
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row10Struct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class row11Struct implements routines.system.IPersistableRow<row11Struct> {
    final static byte[] commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[0];
    static byte[] commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[0];

	
			    public String code_assure;

				public String getCode_assure () {
					return this.code_assure;
				}
				
			    public String code_assureur;

				public String getCode_assureur () {
					return this.code_assureur;
				}
				
			    public String marque;

				public String getMarque () {
					return this.marque;
				}
				
			    public String modele;

				public String getModele () {
					return this.modele;
				}
				
			    public java.util.Date date_premiere_mise_circulation;

				public java.util.Date getDate_premiere_mise_circulation () {
					return this.date_premiere_mise_circulation;
				}
				
			    public String immatriculation;

				public String getImmatriculation () {
					return this.immatriculation;
				}
				
			    public String chassis;

				public String getChassis () {
					return this.chassis;
				}
				
			    public String usage;

				public String getUsage () {
					return this.usage;
				}
				
			    public String charge_utile;

				public String getCharge_utile () {
					return this.charge_utile;
				}
				
			    public int puissance_fiscale;

				public int getPuissance_fiscale () {
					return this.puissance_fiscale;
				}
				
			    public String remorque;

				public String getRemorque () {
					return this.remorque;
				}
				
			    public Integer nombre_portes;

				public Integer getNombre_portes () {
					return this.nombre_portes;
				}
				
			    public String immatriculation_remorque;

				public String getImmatriculation_remorque () {
					return this.immatriculation_remorque;
				}
				
			    public String source_energie;

				public String getSource_energie () {
					return this.source_energie;
				}
				
			    public Integer nombre_de_places;

				public Integer getNombre_de_places () {
					return this.nombre_de_places;
				}
				
			    public Integer cylindree;

				public Integer getCylindree () {
					return this.cylindree;
				}
				
			    public Integer double_commande;

				public Integer getDouble_commande () {
					return this.double_commande;
				}
				
			    public Integer responsabilite_civile;

				public Integer getResponsabilite_civile () {
					return this.responsabilite_civile;
				}
				
			    public Integer utilitaire;

				public Integer getUtilitaire () {
					return this.utilitaire;
				}
				
			    public Integer type_engin;

				public Integer getType_engin () {
					return this.type_engin;
				}
				
			    public Float poids_total_autorise_en_charge;

				public Float getPoids_total_autorise_en_charge () {
					return this.poids_total_autorise_en_charge;
				}
				
			    public String errorCode;

				public String getErrorCode () {
					return this.errorCode;
				}
				
			    public String errorMessage;

				public String getErrorMessage () {
					return this.errorMessage;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length, utf8Charset);
		}
		return strReturn;
	}
	
	private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		String strReturn = null;
		int length = 0;
        length = unmarshaller.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule = new byte[2 * length];
   				}
			}
			unmarshaller.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
    
    private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(str == null) {
			marshaller.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
            marshaller.writeInt(byteArray.length);
            marshaller.write(byteArray);
    	}
    }

	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}
	
	private java.util.Date readDate(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = unmarshaller.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(unmarshaller.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }
    
    private void writeDate(java.util.Date date1, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(date1 == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeLong(date1.getTime());
    	}
    }
	private Integer readInteger(ObjectInputStream dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}
	
	private Integer readInteger(org.jboss.marshalling.Unmarshaller dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}

	private void writeInteger(Integer intNum, ObjectOutputStream dos) throws IOException{
		if(intNum == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeInt(intNum);
    	}
	}
	
	private void writeInteger(Integer intNum, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(intNum == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeInt(intNum);
    	}
	}

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.marque = readString(dis);
					
					this.modele = readString(dis);
					
					this.date_premiere_mise_circulation = readDate(dis);
					
					this.immatriculation = readString(dis);
					
					this.chassis = readString(dis);
					
					this.usage = readString(dis);
					
					this.charge_utile = readString(dis);
					
			        this.puissance_fiscale = dis.readInt();
					
					this.remorque = readString(dis);
					
						this.nombre_portes = readInteger(dis);
					
					this.immatriculation_remorque = readString(dis);
					
					this.source_energie = readString(dis);
					
						this.nombre_de_places = readInteger(dis);
					
						this.cylindree = readInteger(dis);
					
						this.double_commande = readInteger(dis);
					
						this.responsabilite_civile = readInteger(dis);
					
						this.utilitaire = readInteger(dis);
					
						this.type_engin = readInteger(dis);
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.poids_total_autorise_en_charge = null;
           				} else {
           			    	this.poids_total_autorise_en_charge = dis.readFloat();
           				}
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }
    
    public void readData(org.jboss.marshalling.Unmarshaller dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vehicule) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.marque = readString(dis);
					
					this.modele = readString(dis);
					
					this.date_premiere_mise_circulation = readDate(dis);
					
					this.immatriculation = readString(dis);
					
					this.chassis = readString(dis);
					
					this.usage = readString(dis);
					
					this.charge_utile = readString(dis);
					
			        this.puissance_fiscale = dis.readInt();
					
					this.remorque = readString(dis);
					
						this.nombre_portes = readInteger(dis);
					
					this.immatriculation_remorque = readString(dis);
					
					this.source_energie = readString(dis);
					
						this.nombre_de_places = readInteger(dis);
					
						this.cylindree = readInteger(dis);
					
						this.double_commande = readInteger(dis);
					
						this.responsabilite_civile = readInteger(dis);
					
						this.utilitaire = readInteger(dis);
					
						this.type_engin = readInteger(dis);
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.poids_total_autorise_en_charge = null;
           				} else {
           			    	this.poids_total_autorise_en_charge = dis.readFloat();
           				}
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.marque,dos);
					
					// String
				
						writeString(this.modele,dos);
					
					// java.util.Date
				
						writeDate(this.date_premiere_mise_circulation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.chassis,dos);
					
					// String
				
						writeString(this.usage,dos);
					
					// String
				
						writeString(this.charge_utile,dos);
					
					// int
				
		            	dos.writeInt(this.puissance_fiscale);
					
					// String
				
						writeString(this.remorque,dos);
					
					// Integer
				
						writeInteger(this.nombre_portes,dos);
					
					// String
				
						writeString(this.immatriculation_remorque,dos);
					
					// String
				
						writeString(this.source_energie,dos);
					
					// Integer
				
						writeInteger(this.nombre_de_places,dos);
					
					// Integer
				
						writeInteger(this.cylindree,dos);
					
					// Integer
				
						writeInteger(this.double_commande,dos);
					
					// Integer
				
						writeInteger(this.responsabilite_civile,dos);
					
					// Integer
				
						writeInteger(this.utilitaire,dos);
					
					// Integer
				
						writeInteger(this.type_engin,dos);
					
					// Float
				
						if(this.poids_total_autorise_en_charge == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeFloat(this.poids_total_autorise_en_charge);
		            	}
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }
    
    public void writeData(org.jboss.marshalling.Marshaller dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.marque,dos);
					
					// String
				
						writeString(this.modele,dos);
					
					// java.util.Date
				
						writeDate(this.date_premiere_mise_circulation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.chassis,dos);
					
					// String
				
						writeString(this.usage,dos);
					
					// String
				
						writeString(this.charge_utile,dos);
					
					// int
				
		            	dos.writeInt(this.puissance_fiscale);
					
					// String
				
						writeString(this.remorque,dos);
					
					// Integer
				
						writeInteger(this.nombre_portes,dos);
					
					// String
				
						writeString(this.immatriculation_remorque,dos);
					
					// String
				
						writeString(this.source_energie,dos);
					
					// Integer
				
						writeInteger(this.nombre_de_places,dos);
					
					// Integer
				
						writeInteger(this.cylindree,dos);
					
					// Integer
				
						writeInteger(this.double_commande,dos);
					
					// Integer
				
						writeInteger(this.responsabilite_civile,dos);
					
					// Integer
				
						writeInteger(this.utilitaire,dos);
					
					// Integer
				
						writeInteger(this.type_engin,dos);
					
					// Float
				
						if(this.poids_total_autorise_en_charge == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeFloat(this.poids_total_autorise_en_charge);
		            	}
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("code_assure="+code_assure);
		sb.append(",code_assureur="+code_assureur);
		sb.append(",marque="+marque);
		sb.append(",modele="+modele);
		sb.append(",date_premiere_mise_circulation="+String.valueOf(date_premiere_mise_circulation));
		sb.append(",immatriculation="+immatriculation);
		sb.append(",chassis="+chassis);
		sb.append(",usage="+usage);
		sb.append(",charge_utile="+charge_utile);
		sb.append(",puissance_fiscale="+String.valueOf(puissance_fiscale));
		sb.append(",remorque="+remorque);
		sb.append(",nombre_portes="+String.valueOf(nombre_portes));
		sb.append(",immatriculation_remorque="+immatriculation_remorque);
		sb.append(",source_energie="+source_energie);
		sb.append(",nombre_de_places="+String.valueOf(nombre_de_places));
		sb.append(",cylindree="+String.valueOf(cylindree));
		sb.append(",double_commande="+String.valueOf(double_commande));
		sb.append(",responsabilite_civile="+String.valueOf(responsabilite_civile));
		sb.append(",utilitaire="+String.valueOf(utilitaire));
		sb.append(",type_engin="+String.valueOf(type_engin));
		sb.append(",poids_total_autorise_en_charge="+String.valueOf(poids_total_autorise_en_charge));
		sb.append(",errorCode="+errorCode);
		sb.append(",errorMessage="+errorMessage);
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row11Struct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}
public void tFileList_1Process(final java.util.Map<String, Object> globalMap) throws TalendException {
	globalMap.put("tFileList_1_SUBPROCESS_STATE", 0);

 final boolean execStat = this.execStat;
	
		String iterateId = "";
	
	
	String currentComponent = "";
	java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

	try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { //start the resume
				globalResumeTicket = true;



		row10Struct row10 = new row10Struct();
row12Struct row12 = new row12Struct();
row12Struct row14 = row12;
vehicule_valideStruct vehicule_valide = new vehicule_valideStruct();
vehicule_valideStruct row15 = vehicule_valide;
vehicule_rejetStruct vehicule_rejet = new vehicule_rejetStruct();
vehicule_rejetStruct row16 = vehicule_rejet;
row3Struct row3 = new row3Struct();
row13Struct row13 = new row13Struct();
row13Struct row17 = row13;
vehicule_rejet_SchemaStruct vehicule_rejet_Schema = new vehicule_rejet_SchemaStruct();
row2Struct row2 = new row2Struct();
row11Struct row11 = new row11Struct();
row11Struct row18 = row11;
vehicule_rejet_structureStruct vehicule_rejet_structure = new vehicule_rejet_structureStruct();
row1Struct row1 = new row1Struct();



	
	/**
	 * [tFileList_1 begin ] start
	 */

				
			int NB_ITERATE_tFileInputDelimited_1 = 0; //for statistics
			

	
		
		ok_Hash.put("tFileList_1", false);
		start_Hash.put("tFileList_1", System.currentTimeMillis());
		
	
	currentComponent="tFileList_1";

	
		int tos_count_tFileList_1 = 0;
		
	
 
     
    
  String directory_tFileList_1 = "/var/hubasac/inputfile";
  final java.util.List<String> maskList_tFileList_1 = new java.util.ArrayList<String>();
  final java.util.List<java.util.regex.Pattern> patternList_tFileList_1 = new java.util.ArrayList<java.util.regex.Pattern>(); 
    maskList_tFileList_1.add("vehicule*.txt");  
  for (final String filemask_tFileList_1 : maskList_tFileList_1) {
	String filemask_compile_tFileList_1 = filemask_tFileList_1;
	
		filemask_compile_tFileList_1 = org.apache.oro.text.GlobCompiler.globToPerl5(filemask_tFileList_1.toCharArray(), org.apache.oro.text.GlobCompiler.DEFAULT_MASK);
	
		java.util.regex.Pattern fileNamePattern_tFileList_1 = java.util.regex.Pattern.compile(filemask_compile_tFileList_1, java.util.regex.Pattern.CASE_INSENSITIVE);
	
	patternList_tFileList_1.add(fileNamePattern_tFileList_1);
  }
  int NB_FILEtFileList_1 = 0;

  final boolean case_sensitive_tFileList_1 = false;
	
	
	
    final java.util.List<java.io.File> list_tFileList_1 = new java.util.ArrayList<java.io.File>();
    final java.util.Set<String> filePath_tFileList_1 = new java.util.HashSet<String>();
	java.io.File file_tFileList_1 = new java.io.File(directory_tFileList_1);
    
		file_tFileList_1.listFiles(new java.io.FilenameFilter() {
			public boolean accept(java.io.File dir, String name) {
				java.io.File file = new java.io.File(dir, name);
				
	                if (!file.isDirectory()) {
						
    	String fileName_tFileList_1 = file.getName();
		for (final java.util.regex.Pattern fileNamePattern_tFileList_1 : patternList_tFileList_1) {
          	if (fileNamePattern_tFileList_1.matcher(fileName_tFileList_1).matches()){
					if(!filePath_tFileList_1.contains(file.getAbsolutePath())) {
			          list_tFileList_1.add(file);
			          filePath_tFileList_1.add(file.getAbsolutePath());
			        }
			}
		}
	                	return true;
	                } else {
	                  file.listFiles(this);
	                }
				
				return false;
			}
		}
		); 
      java.util.Collections.sort(list_tFileList_1);
    
    for (int i_tFileList_1 = 0; i_tFileList_1 < list_tFileList_1.size(); i_tFileList_1++){
      java.io.File files_tFileList_1 = list_tFileList_1.get(i_tFileList_1);
      String fileName_tFileList_1 = files_tFileList_1.getName();
      
      String currentFileName_tFileList_1 = files_tFileList_1.getName(); 
      String currentFilePath_tFileList_1 = files_tFileList_1.getAbsolutePath();
      String currentFileDirectory_tFileList_1 = files_tFileList_1.getParent();
      String currentFileExtension_tFileList_1 = null;
      
      if (files_tFileList_1.getName().contains(".") && files_tFileList_1.isFile()){
        currentFileExtension_tFileList_1 = files_tFileList_1.getName().substring(files_tFileList_1.getName().lastIndexOf(".") + 1);
      } else{
        currentFileExtension_tFileList_1 = "";
      }
      
      NB_FILEtFileList_1 ++;
      globalMap.put("tFileList_1_CURRENT_FILE", currentFileName_tFileList_1);
      globalMap.put("tFileList_1_CURRENT_FILEPATH", currentFilePath_tFileList_1);
      globalMap.put("tFileList_1_CURRENT_FILEDIRECTORY", currentFileDirectory_tFileList_1);
      globalMap.put("tFileList_1_CURRENT_FILEEXTENSION", currentFileExtension_tFileList_1);
      globalMap.put("tFileList_1_NB_FILE", NB_FILEtFileList_1);
      
 



/**
 * [tFileList_1 begin ] stop
 */
	
	/**
	 * [tFileList_1 main ] start
	 */

	

	
	
	currentComponent="tFileList_1";

	

 


	tos_count_tFileList_1++;

/**
 * [tFileList_1 main ] stop
 */
	
	/**
	 * [tFileList_1 process_data_begin ] start
	 */

	

	
	
	currentComponent="tFileList_1";

	

 



/**
 * [tFileList_1 process_data_begin ] stop
 */
	NB_ITERATE_tFileInputDelimited_1++;
	
	
					if(execStat){				
	       				runStat.updateStatOnConnection("row11", 3, 0);
					}           			
				
					if(execStat){				
	       				runStat.updateStatOnConnection("row15", 3, 0);
					}           			
				
					if(execStat){				
	       				runStat.updateStatOnConnection("row10", 3, 0);
					}           			
				
					if(execStat){				
	       				runStat.updateStatOnConnection("vehicule_rejet_structure", 3, 0);
					}           			
				
					if(execStat){				
	       				runStat.updateStatOnConnection("row17", 3, 0);
					}           			
				
					if(execStat){				
	       				runStat.updateStatOnConnection("row16", 3, 0);
					}           			
				
					if(execStat){				
	       				runStat.updateStatOnConnection("vehicule_rejet_Schema", 3, 0);
					}           			
				
					if(execStat){				
	       				runStat.updateStatOnConnection("row1", 3, 0);
					}           			
				
					if(execStat){				
	       				runStat.updateStatOnConnection("row13", 3, 0);
					}           			
				
					if(execStat){				
	       				runStat.updateStatOnConnection("row14", 3, 0);
					}           			
				
					if(execStat){				
	       				runStat.updateStatOnConnection("row18", 3, 0);
					}           			
				
					if(execStat){				
	       				runStat.updateStatOnConnection("row3", 3, 0);
					}           			
				
					if(execStat){				
	       				runStat.updateStatOnConnection("vehicule_rejet", 3, 0);
					}           			
				
					if(execStat){				
	       				runStat.updateStatOnConnection("vehicule_valide", 3, 0);
					}           			
				
					if(execStat){				
	       				runStat.updateStatOnConnection("row2", 3, 0);
					}           			
				
					if(execStat){				
	       				runStat.updateStatOnConnection("row12", 3, 0);
					}           			
				
					if(execStat){				
	       				runStat.updateStatOnConnection("OnComponentOk1", 3, 0);
					}           			
				
				if(execStat){
					runStat.updateStatOnConnection("iterate1", 1, "exec" + NB_ITERATE_tFileInputDelimited_1);
					//Thread.sleep(1000);
				}				
			






	
	/**
	 * [tDBOutput_1 begin ] start
	 */

	

	
		
		ok_Hash.put("tDBOutput_1", false);
		start_Hash.put("tDBOutput_1", System.currentTimeMillis());
		
	
	currentComponent="tDBOutput_1";

	
					if(execStat) {
						runStat.updateStatOnConnection(resourceMap,iterateId,0,0,"row15");
					}
				
		int tos_count_tDBOutput_1 = 0;
		





String dbschema_tDBOutput_1 = null;
	dbschema_tDBOutput_1 = context.pg_connexion_Schema;
	

String tableName_tDBOutput_1 = null;
if(dbschema_tDBOutput_1 == null || dbschema_tDBOutput_1.trim().length() == 0) {
	tableName_tDBOutput_1 = ("vehicule");
} else {
	tableName_tDBOutput_1 = dbschema_tDBOutput_1 + "\".\"" + ("vehicule");
}

        int updateKeyCount_tDBOutput_1 = 1;
        if(updateKeyCount_tDBOutput_1 < 1) {
            throw new RuntimeException("For update, Schema must have a key");
        } else if (updateKeyCount_tDBOutput_1 == 25 && true) {
                    System.err.println("For update, every Schema column can not be a key");
        }

int nb_line_tDBOutput_1 = 0;
int nb_line_update_tDBOutput_1 = 0;
int nb_line_inserted_tDBOutput_1 = 0;
int nb_line_deleted_tDBOutput_1 = 0;
int nb_line_rejected_tDBOutput_1 = 0;

int deletedCount_tDBOutput_1=0;
int updatedCount_tDBOutput_1=0;
int insertedCount_tDBOutput_1=0;
int rowsToCommitCount_tDBOutput_1=0;
int rejectedCount_tDBOutput_1=0;

boolean whetherReject_tDBOutput_1 = false;

java.sql.Connection conn_tDBOutput_1 = null;
String dbUser_tDBOutput_1 = null;

	
    java.lang.Class.forName("org.postgresql.Driver");
    
        String url_tDBOutput_1 = "jdbc:postgresql://"+context.pg_connexion_Server+":"+context.pg_connexion_Port+"/"+context.pg_connexion_Database + "?" + context.pg_connexion_AdditionalParams;
    dbUser_tDBOutput_1 = context.pg_connexion_Login;

	final String decryptedPassword_tDBOutput_1 = context.pg_connexion_Password; 

    String dbPwd_tDBOutput_1 = decryptedPassword_tDBOutput_1;

    conn_tDBOutput_1 = java.sql.DriverManager.getConnection(url_tDBOutput_1,dbUser_tDBOutput_1,dbPwd_tDBOutput_1);
	
	resourceMap.put("conn_tDBOutput_1", conn_tDBOutput_1);
        conn_tDBOutput_1.setAutoCommit(false);
        int commitEvery_tDBOutput_1 = 10000;
        int commitCounter_tDBOutput_1 = 0;



int count_tDBOutput_1=0;
                                java.sql.DatabaseMetaData dbMetaData_tDBOutput_1 = conn_tDBOutput_1.getMetaData();
                                boolean whetherExist_tDBOutput_1 = false;
                                try (java.sql.ResultSet rsTable_tDBOutput_1 = dbMetaData_tDBOutput_1.getTables(null, null, null, new String[]{"TABLE"})) {
                                    String defaultSchema_tDBOutput_1 = "public";
                                    if(dbschema_tDBOutput_1 == null || dbschema_tDBOutput_1.trim().length() == 0) {
                                        try(java.sql.Statement stmtSchema_tDBOutput_1 = conn_tDBOutput_1.createStatement();
                                            java.sql.ResultSet rsSchema_tDBOutput_1 = stmtSchema_tDBOutput_1.executeQuery("select current_schema() ")) {
                                            while(rsSchema_tDBOutput_1.next()){
                                                defaultSchema_tDBOutput_1 = rsSchema_tDBOutput_1.getString("current_schema");
                                            }
                                        }
                                    }
                                    while(rsTable_tDBOutput_1.next()) {
                                        String table_tDBOutput_1 = rsTable_tDBOutput_1.getString("TABLE_NAME");
                                        String schema_tDBOutput_1 = rsTable_tDBOutput_1.getString("TABLE_SCHEM");
                                        if(table_tDBOutput_1.equals(("vehicule"))
                                            && (schema_tDBOutput_1.equals(dbschema_tDBOutput_1) || ((dbschema_tDBOutput_1 ==null || dbschema_tDBOutput_1.trim().length() ==0) && defaultSchema_tDBOutput_1.equals(schema_tDBOutput_1)))) {
                                            whetherExist_tDBOutput_1 = true;
                                            break;
                                        }
                                    }
                                }
                                if(!whetherExist_tDBOutput_1) {
                                    try (java.sql.Statement stmtCreate_tDBOutput_1 = conn_tDBOutput_1.createStatement()) {
                                        stmtCreate_tDBOutput_1.execute("CREATE TABLE \"" + tableName_tDBOutput_1 + "\"(\"code_assure\" VARCHAR(128)   not null ,\"code_assureur\" VARCHAR(128)   not null ,\"marque\" VARCHAR(128)  ,\"modele\" VARCHAR(128)  ,\"date_premiere_mise_circulation\" TIMESTAMP ,\"immatriculation\" VARCHAR(128)   not null ,\"chassis\" VARCHAR(128)  ,\"usage\" VARCHAR(128)   not null ,\"charge_utile\" VARCHAR(128)  ,\"puissance_fiscale\" INT4  not null ,\"remorque\" VARCHAR(128)   not null ,\"nombre_portes\" INT4 ,\"immatriculation_remorque\" VARCHAR(128)  ,\"source_energie\" VARCHAR(128)  ,\"nombre_de_places\" INT4 ,\"cylindree\" INT4 ,\"double_commande\" INT4 ,\"responsabilite_civile\" INT4 ,\"utilitaire\" INT4 ,\"type_engin\" INT4 ,\"poids_total_autorise_en_charge\" FLOAT4 ,\"c_status\" INT4 default 1  not null ,\"c_date_mis_a_jour\" TIMESTAMP ,\"c_date_transfer\" TIMESTAMP ,\"commentaires\" VARCHAR(128)  ,primary key(\"immatriculation\"))");
                                    }
                                }
	    java.sql.PreparedStatement pstmt_tDBOutput_1 = conn_tDBOutput_1.prepareStatement("SELECT COUNT(1) FROM \"" + tableName_tDBOutput_1 + "\" WHERE \"immatriculation\" = ?");
	    resourceMap.put("pstmt_tDBOutput_1", pstmt_tDBOutput_1);
	    String insert_tDBOutput_1 = "INSERT INTO \"" + tableName_tDBOutput_1 + "\" (\"code_assure\",\"code_assureur\",\"marque\",\"modele\",\"date_premiere_mise_circulation\",\"immatriculation\",\"chassis\",\"usage\",\"charge_utile\",\"puissance_fiscale\",\"remorque\",\"nombre_portes\",\"immatriculation_remorque\",\"source_energie\",\"nombre_de_places\",\"cylindree\",\"double_commande\",\"responsabilite_civile\",\"utilitaire\",\"type_engin\",\"poids_total_autorise_en_charge\",\"c_status\",\"c_date_mis_a_jour\",\"c_date_transfer\",\"commentaires\") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	    java.sql.PreparedStatement pstmtInsert_tDBOutput_1 = conn_tDBOutput_1.prepareStatement(insert_tDBOutput_1);
	    resourceMap.put("pstmtInsert_tDBOutput_1", pstmtInsert_tDBOutput_1);
	    String update_tDBOutput_1 = "UPDATE \"" + tableName_tDBOutput_1 + "\" SET \"code_assure\" = ?,\"code_assureur\" = ?,\"marque\" = ?,\"modele\" = ?,\"date_premiere_mise_circulation\" = ?,\"chassis\" = ?,\"usage\" = ?,\"charge_utile\" = ?,\"puissance_fiscale\" = ?,\"remorque\" = ?,\"nombre_portes\" = ?,\"immatriculation_remorque\" = ?,\"source_energie\" = ?,\"nombre_de_places\" = ?,\"cylindree\" = ?,\"double_commande\" = ?,\"responsabilite_civile\" = ?,\"utilitaire\" = ?,\"type_engin\" = ?,\"poids_total_autorise_en_charge\" = ?,\"c_status\" = ?,\"c_date_mis_a_jour\" = ?,\"c_date_transfer\" = ?,\"commentaires\" = ? WHERE \"immatriculation\" = ?";
	    java.sql.PreparedStatement pstmtUpdate_tDBOutput_1 = conn_tDBOutput_1.prepareStatement(update_tDBOutput_1);
	    resourceMap.put("pstmtUpdate_tDBOutput_1", pstmtUpdate_tDBOutput_1);
	    

 



/**
 * [tDBOutput_1 begin ] stop
 */



	
	/**
	 * [tLogRow_4 begin ] start
	 */

	

	
		
		ok_Hash.put("tLogRow_4", false);
		start_Hash.put("tLogRow_4", System.currentTimeMillis());
		
	
	currentComponent="tLogRow_4";

	
					if(execStat) {
						runStat.updateStatOnConnection(resourceMap,iterateId,0,0,"vehicule_valide");
					}
				
		int tos_count_tLogRow_4 = 0;
		

	///////////////////////
	
         class Util_tLogRow_4 {

        String[] des_top = { ".", ".", "-", "+" };

        String[] des_head = { "|=", "=|", "-", "+" };

        String[] des_bottom = { "'", "'", "-", "+" };

        String name="";

        java.util.List<String[]> list = new java.util.ArrayList<String[]>();

        int[] colLengths = new int[25];

        public void addRow(String[] row) {

            for (int i = 0; i < 25; i++) {
                if (row[i]!=null) {
                  colLengths[i] = Math.max(colLengths[i], row[i].length());
                }
            }
            list.add(row);
        }

        public void setTableName(String name) {

            this.name = name;
        }

            public StringBuilder format() {
            
                StringBuilder sb = new StringBuilder();
  
            
                    sb.append(print(des_top));
    
                    int totals = 0;
                    for (int i = 0; i < colLengths.length; i++) {
                        totals = totals + colLengths[i];
                    }
    
                    // name
                    sb.append("|");
                    int k = 0;
                    for (k = 0; k < (totals + 24 - name.length()) / 2; k++) {
                        sb.append(' ');
                    }
                    sb.append(name);
                    for (int i = 0; i < totals + 24 - name.length() - k; i++) {
                        sb.append(' ');
                    }
                    sb.append("|\n");

                    // head and rows
                    sb.append(print(des_head));
                    for (int i = 0; i < list.size(); i++) {
    
                        String[] row = list.get(i);
    
                        java.util.Formatter formatter = new java.util.Formatter(new StringBuilder());
                        
                        StringBuilder sbformat = new StringBuilder();                                             
        			        sbformat.append("|%1$-");
        			        sbformat.append(colLengths[0]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%2$-");
        			        sbformat.append(colLengths[1]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%3$-");
        			        sbformat.append(colLengths[2]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%4$-");
        			        sbformat.append(colLengths[3]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%5$-");
        			        sbformat.append(colLengths[4]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%6$-");
        			        sbformat.append(colLengths[5]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%7$-");
        			        sbformat.append(colLengths[6]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%8$-");
        			        sbformat.append(colLengths[7]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%9$-");
        			        sbformat.append(colLengths[8]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%10$-");
        			        sbformat.append(colLengths[9]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%11$-");
        			        sbformat.append(colLengths[10]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%12$-");
        			        sbformat.append(colLengths[11]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%13$-");
        			        sbformat.append(colLengths[12]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%14$-");
        			        sbformat.append(colLengths[13]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%15$-");
        			        sbformat.append(colLengths[14]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%16$-");
        			        sbformat.append(colLengths[15]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%17$-");
        			        sbformat.append(colLengths[16]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%18$-");
        			        sbformat.append(colLengths[17]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%19$-");
        			        sbformat.append(colLengths[18]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%20$-");
        			        sbformat.append(colLengths[19]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%21$-");
        			        sbformat.append(colLengths[20]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%22$-");
        			        sbformat.append(colLengths[21]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%23$-");
        			        sbformat.append(colLengths[22]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%24$-");
        			        sbformat.append(colLengths[23]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%25$-");
        			        sbformat.append(colLengths[24]);
        			        sbformat.append("s");
        			                      
                        sbformat.append("|\n");                    
       
                        formatter.format(sbformat.toString(), (Object[])row);	
                                
                        sb.append(formatter.toString());
                        if (i == 0)
                            sb.append(print(des_head)); // print the head
                    }
    
                    // end
                    sb.append(print(des_bottom));
                    return sb;
                }
            

            private StringBuilder print(String[] fillChars) {
                StringBuilder sb = new StringBuilder();
                //first column
                sb.append(fillChars[0]);                
                    for (int i = 0; i < colLengths[0] - fillChars[0].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);	                

                    for (int i = 0; i < colLengths[1] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[2] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[3] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[4] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[5] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[6] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[7] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[8] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[9] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[10] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[11] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[12] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[13] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[14] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[15] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[16] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[17] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[18] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[19] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[20] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[21] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[22] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[23] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                
                    //last column
                    for (int i = 0; i < colLengths[24] - fillChars[1].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }         
                sb.append(fillChars[1]);
                sb.append("\n");               
                return sb;
            }
            
            public boolean isTableEmpty(){
            	if (list.size() > 1)
            		return false;
            	return true;
            }
        }
        Util_tLogRow_4 util_tLogRow_4 = new Util_tLogRow_4();
        util_tLogRow_4.setTableName("tLogRow_4");
        util_tLogRow_4.addRow(new String[]{"code_assure","code_assureur","marque","modele","date_premiere_mise_circulation","immatriculation","chassis","usage","charge_utile","puissance_fiscale","remorque","nombre_portes","immatriculation_remorque","source_energie","nombre_de_places","cylindree","double_commande","responsabilite_civile","utilitaire","type_engin","poids_total_autorise_en_charge","c_status","c_date_mis_a_jour","c_date_transfer","commentaires",});        
 		StringBuilder strBuffer_tLogRow_4 = null;
		int nb_line_tLogRow_4 = 0;
///////////////////////    			



 



/**
 * [tLogRow_4 begin ] stop
 */






	
	/**
	 * [tFileOutputDelimited_3 begin ] start
	 */

	

	
		
		ok_Hash.put("tFileOutputDelimited_3", false);
		start_Hash.put("tFileOutputDelimited_3", System.currentTimeMillis());
		
	
	currentComponent="tFileOutputDelimited_3";

	
					if(execStat) {
						runStat.updateStatOnConnection(resourceMap,iterateId,0,0,"row3");
					}
				
		int tos_count_tFileOutputDelimited_3 = 0;
		

String fileName_tFileOutputDelimited_3 = "";
    fileName_tFileOutputDelimited_3 = (new java.io.File("D:/Projet MOE/Tests/Data/rejets/vehicule_rejet_rg.txt")).getAbsolutePath().replace("\\","/");
    String fullName_tFileOutputDelimited_3 = null;
    String extension_tFileOutputDelimited_3 = null;
    String directory_tFileOutputDelimited_3 = null;
    if((fileName_tFileOutputDelimited_3.indexOf("/") != -1)) {
        if(fileName_tFileOutputDelimited_3.lastIndexOf(".") < fileName_tFileOutputDelimited_3.lastIndexOf("/")) {
            fullName_tFileOutputDelimited_3 = fileName_tFileOutputDelimited_3;
            extension_tFileOutputDelimited_3 = "";
        } else {
            fullName_tFileOutputDelimited_3 = fileName_tFileOutputDelimited_3.substring(0, fileName_tFileOutputDelimited_3.lastIndexOf("."));
            extension_tFileOutputDelimited_3 = fileName_tFileOutputDelimited_3.substring(fileName_tFileOutputDelimited_3.lastIndexOf("."));
        }
        directory_tFileOutputDelimited_3 = fileName_tFileOutputDelimited_3.substring(0, fileName_tFileOutputDelimited_3.lastIndexOf("/"));
    } else {
        if(fileName_tFileOutputDelimited_3.lastIndexOf(".") != -1) {
            fullName_tFileOutputDelimited_3 = fileName_tFileOutputDelimited_3.substring(0, fileName_tFileOutputDelimited_3.lastIndexOf("."));
            extension_tFileOutputDelimited_3 = fileName_tFileOutputDelimited_3.substring(fileName_tFileOutputDelimited_3.lastIndexOf("."));
        } else {
            fullName_tFileOutputDelimited_3 = fileName_tFileOutputDelimited_3;
            extension_tFileOutputDelimited_3 = "";
        }
        directory_tFileOutputDelimited_3 = "";
    }
    boolean isFileGenerated_tFileOutputDelimited_3 = true;
    java.io.File filetFileOutputDelimited_3 = new java.io.File(fileName_tFileOutputDelimited_3);
    globalMap.put("tFileOutputDelimited_3_FILE_NAME",fileName_tFileOutputDelimited_3);
        if(filetFileOutputDelimited_3.exists()){
            isFileGenerated_tFileOutputDelimited_3 = false;
        }
            int nb_line_tFileOutputDelimited_3 = 0;
            int splitedFileNo_tFileOutputDelimited_3 = 0;
            int currentRow_tFileOutputDelimited_3 = 0;

            final String OUT_DELIM_tFileOutputDelimited_3 = /** Start field tFileOutputDelimited_3:FIELDSEPARATOR */";"/** End field tFileOutputDelimited_3:FIELDSEPARATOR */;

            final String OUT_DELIM_ROWSEP_tFileOutputDelimited_3 = /** Start field tFileOutputDelimited_3:ROWSEPARATOR */"\n"/** End field tFileOutputDelimited_3:ROWSEPARATOR */;

                    //create directory only if not exists
                    if(directory_tFileOutputDelimited_3 != null && directory_tFileOutputDelimited_3.trim().length() != 0) {
                        java.io.File dir_tFileOutputDelimited_3 = new java.io.File(directory_tFileOutputDelimited_3);
                        if(!dir_tFileOutputDelimited_3.exists()) {
                            dir_tFileOutputDelimited_3.mkdirs();
                        }
                    }

                        //routines.system.Row
                        java.io.Writer outtFileOutputDelimited_3 = null;

                        outtFileOutputDelimited_3 = new java.io.BufferedWriter(new java.io.OutputStreamWriter(
                        new java.io.FileOutputStream(fileName_tFileOutputDelimited_3, true),"ISO-8859-15"));
                                    if(filetFileOutputDelimited_3.length()==0){
                                        outtFileOutputDelimited_3.write("code_assure");
                                            outtFileOutputDelimited_3.write(OUT_DELIM_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.write("code_assureur");
                                            outtFileOutputDelimited_3.write(OUT_DELIM_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.write("marque");
                                            outtFileOutputDelimited_3.write(OUT_DELIM_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.write("modele");
                                            outtFileOutputDelimited_3.write(OUT_DELIM_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.write("date_premiere_mise_circulation");
                                            outtFileOutputDelimited_3.write(OUT_DELIM_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.write("immatriculation");
                                            outtFileOutputDelimited_3.write(OUT_DELIM_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.write("chassis");
                                            outtFileOutputDelimited_3.write(OUT_DELIM_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.write("usage");
                                            outtFileOutputDelimited_3.write(OUT_DELIM_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.write("charge_utile");
                                            outtFileOutputDelimited_3.write(OUT_DELIM_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.write("puissance_fiscale");
                                            outtFileOutputDelimited_3.write(OUT_DELIM_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.write("remorque");
                                            outtFileOutputDelimited_3.write(OUT_DELIM_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.write("nombre_portes");
                                            outtFileOutputDelimited_3.write(OUT_DELIM_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.write("immatriculation_remorque");
                                            outtFileOutputDelimited_3.write(OUT_DELIM_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.write("source_energie");
                                            outtFileOutputDelimited_3.write(OUT_DELIM_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.write("nombre_de_places");
                                            outtFileOutputDelimited_3.write(OUT_DELIM_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.write("cylindree");
                                            outtFileOutputDelimited_3.write(OUT_DELIM_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.write("double_commande");
                                            outtFileOutputDelimited_3.write(OUT_DELIM_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.write("responsabilite_civile");
                                            outtFileOutputDelimited_3.write(OUT_DELIM_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.write("utilitaire");
                                            outtFileOutputDelimited_3.write(OUT_DELIM_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.write("type_engin");
                                            outtFileOutputDelimited_3.write(OUT_DELIM_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.write("poids_total_autorise_en_charge");
                                            outtFileOutputDelimited_3.write(OUT_DELIM_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.write("date_extraction");
                                            outtFileOutputDelimited_3.write(OUT_DELIM_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.write("date_depot");
                                            outtFileOutputDelimited_3.write(OUT_DELIM_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.write("Error_Message");
                                        outtFileOutputDelimited_3.write(OUT_DELIM_ROWSEP_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.flush();
                                    }


        resourceMap.put("out_tFileOutputDelimited_3", outtFileOutputDelimited_3);
resourceMap.put("nb_line_tFileOutputDelimited_3", nb_line_tFileOutputDelimited_3);

 



/**
 * [tFileOutputDelimited_3 begin ] stop
 */



	
	/**
	 * [tDBOutput_2 begin ] start
	 */

	

	
		
		ok_Hash.put("tDBOutput_2", false);
		start_Hash.put("tDBOutput_2", System.currentTimeMillis());
		
	
	currentComponent="tDBOutput_2";

	
					if(execStat) {
						runStat.updateStatOnConnection(resourceMap,iterateId,0,0,"row16");
					}
				
		int tos_count_tDBOutput_2 = 0;
		





String dbschema_tDBOutput_2 = null;
	dbschema_tDBOutput_2 = context.pg_connexion_Schema;
	

String tableName_tDBOutput_2 = null;
if(dbschema_tDBOutput_2 == null || dbschema_tDBOutput_2.trim().length() == 0) {
	tableName_tDBOutput_2 = ("vehicule_rejets_RG_check");
} else {
	tableName_tDBOutput_2 = dbschema_tDBOutput_2 + "\".\"" + ("vehicule_rejets_RG_check");
}


int nb_line_tDBOutput_2 = 0;
int nb_line_update_tDBOutput_2 = 0;
int nb_line_inserted_tDBOutput_2 = 0;
int nb_line_deleted_tDBOutput_2 = 0;
int nb_line_rejected_tDBOutput_2 = 0;

int deletedCount_tDBOutput_2=0;
int updatedCount_tDBOutput_2=0;
int insertedCount_tDBOutput_2=0;
int rowsToCommitCount_tDBOutput_2=0;
int rejectedCount_tDBOutput_2=0;

boolean whetherReject_tDBOutput_2 = false;

java.sql.Connection conn_tDBOutput_2 = null;
String dbUser_tDBOutput_2 = null;

	
    java.lang.Class.forName("org.postgresql.Driver");
    
        String url_tDBOutput_2 = "jdbc:postgresql://"+context.pg_connexion_Server+":"+context.pg_connexion_Port+"/"+context.pg_connexion_Database + "?" + context.pg_connexion_AdditionalParams;
    dbUser_tDBOutput_2 = context.pg_connexion_Login;

	final String decryptedPassword_tDBOutput_2 = context.pg_connexion_Password; 

    String dbPwd_tDBOutput_2 = decryptedPassword_tDBOutput_2;

    conn_tDBOutput_2 = java.sql.DriverManager.getConnection(url_tDBOutput_2,dbUser_tDBOutput_2,dbPwd_tDBOutput_2);
	
	resourceMap.put("conn_tDBOutput_2", conn_tDBOutput_2);
        conn_tDBOutput_2.setAutoCommit(false);
        int commitEvery_tDBOutput_2 = 10000;
        int commitCounter_tDBOutput_2 = 0;


   int batchSize_tDBOutput_2 = 10000;
   int batchSizeCounter_tDBOutput_2=0;

int count_tDBOutput_2=0;
                                java.sql.DatabaseMetaData dbMetaData_tDBOutput_2 = conn_tDBOutput_2.getMetaData();
                                boolean whetherExist_tDBOutput_2 = false;
                                try (java.sql.ResultSet rsTable_tDBOutput_2 = dbMetaData_tDBOutput_2.getTables(null, null, null, new String[]{"TABLE"})) {
                                    String defaultSchema_tDBOutput_2 = "public";
                                    if(dbschema_tDBOutput_2 == null || dbschema_tDBOutput_2.trim().length() == 0) {
                                        try(java.sql.Statement stmtSchema_tDBOutput_2 = conn_tDBOutput_2.createStatement();
                                            java.sql.ResultSet rsSchema_tDBOutput_2 = stmtSchema_tDBOutput_2.executeQuery("select current_schema() ")) {
                                            while(rsSchema_tDBOutput_2.next()){
                                                defaultSchema_tDBOutput_2 = rsSchema_tDBOutput_2.getString("current_schema");
                                            }
                                        }
                                    }
                                    while(rsTable_tDBOutput_2.next()) {
                                        String table_tDBOutput_2 = rsTable_tDBOutput_2.getString("TABLE_NAME");
                                        String schema_tDBOutput_2 = rsTable_tDBOutput_2.getString("TABLE_SCHEM");
                                        if(table_tDBOutput_2.equals(("vehicule_rejets_RG_check"))
                                            && (schema_tDBOutput_2.equals(dbschema_tDBOutput_2) || ((dbschema_tDBOutput_2 ==null || dbschema_tDBOutput_2.trim().length() ==0) && defaultSchema_tDBOutput_2.equals(schema_tDBOutput_2)))) {
                                            whetherExist_tDBOutput_2 = true;
                                            break;
                                        }
                                    }
                                }
                                if(!whetherExist_tDBOutput_2) {
                                    try (java.sql.Statement stmtCreate_tDBOutput_2 = conn_tDBOutput_2.createStatement()) {
                                        stmtCreate_tDBOutput_2.execute("CREATE TABLE \"" + tableName_tDBOutput_2 + "\"(\"code_assure\" VARCHAR(128)   not null ,\"code_assureur\" VARCHAR(128)   not null ,\"marque\" VARCHAR(128)  ,\"modele\" VARCHAR(128)  ,\"date_premiere_mise_circulation\" TIMESTAMP ,\"immatriculation\" VARCHAR(128)   not null ,\"chassis\" VARCHAR(128)  ,\"usage\" VARCHAR(128)   not null ,\"charge_utile\" VARCHAR(128)  ,\"puissance_fiscale\" INT4  not null ,\"remorque\" VARCHAR(128)   not null ,\"nombre_portes\" INT4 ,\"immatriculation_remorque\" VARCHAR(128)  ,\"source_energie\" VARCHAR(128)  ,\"nombre_de_places\" INT4 ,\"cylindree\" INT4 ,\"double_commande\" INT4 ,\"responsabilite_civile\" INT4 ,\"utilitaire\" INT4 ,\"type_engin\" INT4 ,\"poids_total_autorise_en_charge\" FLOAT4 ,\"date_extraction\" TIMESTAMP ,\"date_depot\" TIMESTAMP ,\"Error_Message\" VARCHAR(255)  ,primary key(\"immatriculation\"))");
                                    }
                                }
	    String insert_tDBOutput_2 = "INSERT INTO \"" + tableName_tDBOutput_2 + "\" (\"code_assure\",\"code_assureur\",\"marque\",\"modele\",\"date_premiere_mise_circulation\",\"immatriculation\",\"chassis\",\"usage\",\"charge_utile\",\"puissance_fiscale\",\"remorque\",\"nombre_portes\",\"immatriculation_remorque\",\"source_energie\",\"nombre_de_places\",\"cylindree\",\"double_commande\",\"responsabilite_civile\",\"utilitaire\",\"type_engin\",\"poids_total_autorise_en_charge\",\"date_extraction\",\"date_depot\",\"Error_Message\") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	    
	    java.sql.PreparedStatement pstmt_tDBOutput_2 = conn_tDBOutput_2.prepareStatement(insert_tDBOutput_2);
	    resourceMap.put("pstmt_tDBOutput_2", pstmt_tDBOutput_2);
	    

 



/**
 * [tDBOutput_2 begin ] stop
 */



	
	/**
	 * [tLogRow_5 begin ] start
	 */

	

	
		
		ok_Hash.put("tLogRow_5", false);
		start_Hash.put("tLogRow_5", System.currentTimeMillis());
		
	
	currentComponent="tLogRow_5";

	
					if(execStat) {
						runStat.updateStatOnConnection(resourceMap,iterateId,0,0,"vehicule_rejet");
					}
				
		int tos_count_tLogRow_5 = 0;
		

	///////////////////////
	
         class Util_tLogRow_5 {

        String[] des_top = { ".", ".", "-", "+" };

        String[] des_head = { "|=", "=|", "-", "+" };

        String[] des_bottom = { "'", "'", "-", "+" };

        String name="";

        java.util.List<String[]> list = new java.util.ArrayList<String[]>();

        int[] colLengths = new int[24];

        public void addRow(String[] row) {

            for (int i = 0; i < 24; i++) {
                if (row[i]!=null) {
                  colLengths[i] = Math.max(colLengths[i], row[i].length());
                }
            }
            list.add(row);
        }

        public void setTableName(String name) {

            this.name = name;
        }

            public StringBuilder format() {
            
                StringBuilder sb = new StringBuilder();
  
            
                    sb.append(print(des_top));
    
                    int totals = 0;
                    for (int i = 0; i < colLengths.length; i++) {
                        totals = totals + colLengths[i];
                    }
    
                    // name
                    sb.append("|");
                    int k = 0;
                    for (k = 0; k < (totals + 23 - name.length()) / 2; k++) {
                        sb.append(' ');
                    }
                    sb.append(name);
                    for (int i = 0; i < totals + 23 - name.length() - k; i++) {
                        sb.append(' ');
                    }
                    sb.append("|\n");

                    // head and rows
                    sb.append(print(des_head));
                    for (int i = 0; i < list.size(); i++) {
    
                        String[] row = list.get(i);
    
                        java.util.Formatter formatter = new java.util.Formatter(new StringBuilder());
                        
                        StringBuilder sbformat = new StringBuilder();                                             
        			        sbformat.append("|%1$-");
        			        sbformat.append(colLengths[0]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%2$-");
        			        sbformat.append(colLengths[1]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%3$-");
        			        sbformat.append(colLengths[2]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%4$-");
        			        sbformat.append(colLengths[3]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%5$-");
        			        sbformat.append(colLengths[4]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%6$-");
        			        sbformat.append(colLengths[5]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%7$-");
        			        sbformat.append(colLengths[6]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%8$-");
        			        sbformat.append(colLengths[7]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%9$-");
        			        sbformat.append(colLengths[8]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%10$-");
        			        sbformat.append(colLengths[9]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%11$-");
        			        sbformat.append(colLengths[10]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%12$-");
        			        sbformat.append(colLengths[11]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%13$-");
        			        sbformat.append(colLengths[12]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%14$-");
        			        sbformat.append(colLengths[13]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%15$-");
        			        sbformat.append(colLengths[14]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%16$-");
        			        sbformat.append(colLengths[15]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%17$-");
        			        sbformat.append(colLengths[16]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%18$-");
        			        sbformat.append(colLengths[17]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%19$-");
        			        sbformat.append(colLengths[18]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%20$-");
        			        sbformat.append(colLengths[19]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%21$-");
        			        sbformat.append(colLengths[20]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%22$-");
        			        sbformat.append(colLengths[21]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%23$-");
        			        sbformat.append(colLengths[22]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%24$-");
        			        sbformat.append(colLengths[23]);
        			        sbformat.append("s");
        			                      
                        sbformat.append("|\n");                    
       
                        formatter.format(sbformat.toString(), (Object[])row);	
                                
                        sb.append(formatter.toString());
                        if (i == 0)
                            sb.append(print(des_head)); // print the head
                    }
    
                    // end
                    sb.append(print(des_bottom));
                    return sb;
                }
            

            private StringBuilder print(String[] fillChars) {
                StringBuilder sb = new StringBuilder();
                //first column
                sb.append(fillChars[0]);                
                    for (int i = 0; i < colLengths[0] - fillChars[0].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);	                

                    for (int i = 0; i < colLengths[1] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[2] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[3] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[4] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[5] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[6] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[7] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[8] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[9] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[10] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[11] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[12] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[13] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[14] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[15] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[16] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[17] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[18] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[19] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[20] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[21] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[22] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                
                    //last column
                    for (int i = 0; i < colLengths[23] - fillChars[1].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }         
                sb.append(fillChars[1]);
                sb.append("\n");               
                return sb;
            }
            
            public boolean isTableEmpty(){
            	if (list.size() > 1)
            		return false;
            	return true;
            }
        }
        Util_tLogRow_5 util_tLogRow_5 = new Util_tLogRow_5();
        util_tLogRow_5.setTableName("tLogRow_5");
        util_tLogRow_5.addRow(new String[]{"code_assure","code_assureur","marque","modele","date_premiere_mise_circulation","immatriculation","chassis","usage","charge_utile","puissance_fiscale","remorque","nombre_portes","immatriculation_remorque","source_energie","nombre_de_places","cylindree","double_commande","responsabilite_civile","utilitaire","type_engin","poids_total_autorise_en_charge","date_extraction","date_depot","Error_Message",});        
 		StringBuilder strBuffer_tLogRow_5 = null;
		int nb_line_tLogRow_5 = 0;
///////////////////////    			



 



/**
 * [tLogRow_5 begin ] stop
 */



	
	/**
	 * [tMap_1 begin ] start
	 */

	

	
		
		ok_Hash.put("tMap_1", false);
		start_Hash.put("tMap_1", System.currentTimeMillis());
		
	
	currentComponent="tMap_1";

	
					if(execStat) {
						runStat.updateStatOnConnection(resourceMap,iterateId,0,0,"row14");
					}
				
		int tos_count_tMap_1 = 0;
		




// ###############################
// # Lookup's keys initialization
// ###############################        

// ###############################
// # Vars initialization
class  Var__tMap_1__Struct  {
	String D_P_M_Circulation;
	String S_energie;
	String Chassis;
	String Imma_remorq;
	String N_places;
	String cylindree;
	String Double_commande;
	String Responsabilite_civile;
	String Utilitaire;
	String Type_engin;
	String poids_total_en_charge;
	String SourceEnergieVal;
}
Var__tMap_1__Struct Var__tMap_1 = new Var__tMap_1__Struct();
// ###############################

// ###############################
// # Outputs initialization
vehicule_valideStruct vehicule_valide_tmp = new vehicule_valideStruct();
vehicule_rejetStruct vehicule_rejet_tmp = new vehicule_rejetStruct();
// ###############################

        
        



        









 



/**
 * [tMap_1 begin ] stop
 */



	
	/**
	 * [tLogRow_2 begin ] start
	 */

	

	
		
		ok_Hash.put("tLogRow_2", false);
		start_Hash.put("tLogRow_2", System.currentTimeMillis());
		
	
	currentComponent="tLogRow_2";

	
					if(execStat) {
						runStat.updateStatOnConnection(resourceMap,iterateId,0,0,"row12");
					}
				
		int tos_count_tLogRow_2 = 0;
		

	///////////////////////
	
         class Util_tLogRow_2 {

        String[] des_top = { ".", ".", "-", "+" };

        String[] des_head = { "|=", "=|", "-", "+" };

        String[] des_bottom = { "'", "'", "-", "+" };

        String name="";

        java.util.List<String[]> list = new java.util.ArrayList<String[]>();

        int[] colLengths = new int[21];

        public void addRow(String[] row) {

            for (int i = 0; i < 21; i++) {
                if (row[i]!=null) {
                  colLengths[i] = Math.max(colLengths[i], row[i].length());
                }
            }
            list.add(row);
        }

        public void setTableName(String name) {

            this.name = name;
        }

            public StringBuilder format() {
            
                StringBuilder sb = new StringBuilder();
  
            
                    sb.append(print(des_top));
    
                    int totals = 0;
                    for (int i = 0; i < colLengths.length; i++) {
                        totals = totals + colLengths[i];
                    }
    
                    // name
                    sb.append("|");
                    int k = 0;
                    for (k = 0; k < (totals + 20 - name.length()) / 2; k++) {
                        sb.append(' ');
                    }
                    sb.append(name);
                    for (int i = 0; i < totals + 20 - name.length() - k; i++) {
                        sb.append(' ');
                    }
                    sb.append("|\n");

                    // head and rows
                    sb.append(print(des_head));
                    for (int i = 0; i < list.size(); i++) {
    
                        String[] row = list.get(i);
    
                        java.util.Formatter formatter = new java.util.Formatter(new StringBuilder());
                        
                        StringBuilder sbformat = new StringBuilder();                                             
        			        sbformat.append("|%1$-");
        			        sbformat.append(colLengths[0]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%2$-");
        			        sbformat.append(colLengths[1]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%3$-");
        			        sbformat.append(colLengths[2]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%4$-");
        			        sbformat.append(colLengths[3]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%5$-");
        			        sbformat.append(colLengths[4]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%6$-");
        			        sbformat.append(colLengths[5]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%7$-");
        			        sbformat.append(colLengths[6]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%8$-");
        			        sbformat.append(colLengths[7]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%9$-");
        			        sbformat.append(colLengths[8]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%10$-");
        			        sbformat.append(colLengths[9]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%11$-");
        			        sbformat.append(colLengths[10]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%12$-");
        			        sbformat.append(colLengths[11]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%13$-");
        			        sbformat.append(colLengths[12]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%14$-");
        			        sbformat.append(colLengths[13]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%15$-");
        			        sbformat.append(colLengths[14]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%16$-");
        			        sbformat.append(colLengths[15]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%17$-");
        			        sbformat.append(colLengths[16]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%18$-");
        			        sbformat.append(colLengths[17]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%19$-");
        			        sbformat.append(colLengths[18]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%20$-");
        			        sbformat.append(colLengths[19]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%21$-");
        			        sbformat.append(colLengths[20]);
        			        sbformat.append("s");
        			                      
                        sbformat.append("|\n");                    
       
                        formatter.format(sbformat.toString(), (Object[])row);	
                                
                        sb.append(formatter.toString());
                        if (i == 0)
                            sb.append(print(des_head)); // print the head
                    }
    
                    // end
                    sb.append(print(des_bottom));
                    return sb;
                }
            

            private StringBuilder print(String[] fillChars) {
                StringBuilder sb = new StringBuilder();
                //first column
                sb.append(fillChars[0]);                
                    for (int i = 0; i < colLengths[0] - fillChars[0].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);	                

                    for (int i = 0; i < colLengths[1] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[2] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[3] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[4] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[5] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[6] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[7] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[8] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[9] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[10] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[11] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[12] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[13] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[14] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[15] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[16] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[17] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[18] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[19] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                
                    //last column
                    for (int i = 0; i < colLengths[20] - fillChars[1].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }         
                sb.append(fillChars[1]);
                sb.append("\n");               
                return sb;
            }
            
            public boolean isTableEmpty(){
            	if (list.size() > 1)
            		return false;
            	return true;
            }
        }
        Util_tLogRow_2 util_tLogRow_2 = new Util_tLogRow_2();
        util_tLogRow_2.setTableName("tLogRow_2");
        util_tLogRow_2.addRow(new String[]{"code_assure","code_assureur","marque","modele","date_premiere_mise_circulation","immatriculation","chassis","usage","charge_utile","puissance_fiscale","remorque","nombre_portes","immatriculation_remorque","source_energie","nombre_de_places","cylindree","double_commande","responsabilite_civile","utilitaire","type_engin","poids_total_autorise_en_charge",});        
 		StringBuilder strBuffer_tLogRow_2 = null;
		int nb_line_tLogRow_2 = 0;
///////////////////////    			



 



/**
 * [tLogRow_2 begin ] stop
 */







	
	/**
	 * [tFileOutputDelimited_2 begin ] start
	 */

	

	
		
		ok_Hash.put("tFileOutputDelimited_2", false);
		start_Hash.put("tFileOutputDelimited_2", System.currentTimeMillis());
		
	
	currentComponent="tFileOutputDelimited_2";

	
					if(execStat) {
						runStat.updateStatOnConnection(resourceMap,iterateId,0,0,"row2");
					}
				
		int tos_count_tFileOutputDelimited_2 = 0;
		

String fileName_tFileOutputDelimited_2 = "";
    fileName_tFileOutputDelimited_2 = (new java.io.File("D:/Projet MOE/Tests/Data/rejets/vehicule_rejet_schema.txt")).getAbsolutePath().replace("\\","/");
    String fullName_tFileOutputDelimited_2 = null;
    String extension_tFileOutputDelimited_2 = null;
    String directory_tFileOutputDelimited_2 = null;
    if((fileName_tFileOutputDelimited_2.indexOf("/") != -1)) {
        if(fileName_tFileOutputDelimited_2.lastIndexOf(".") < fileName_tFileOutputDelimited_2.lastIndexOf("/")) {
            fullName_tFileOutputDelimited_2 = fileName_tFileOutputDelimited_2;
            extension_tFileOutputDelimited_2 = "";
        } else {
            fullName_tFileOutputDelimited_2 = fileName_tFileOutputDelimited_2.substring(0, fileName_tFileOutputDelimited_2.lastIndexOf("."));
            extension_tFileOutputDelimited_2 = fileName_tFileOutputDelimited_2.substring(fileName_tFileOutputDelimited_2.lastIndexOf("."));
        }
        directory_tFileOutputDelimited_2 = fileName_tFileOutputDelimited_2.substring(0, fileName_tFileOutputDelimited_2.lastIndexOf("/"));
    } else {
        if(fileName_tFileOutputDelimited_2.lastIndexOf(".") != -1) {
            fullName_tFileOutputDelimited_2 = fileName_tFileOutputDelimited_2.substring(0, fileName_tFileOutputDelimited_2.lastIndexOf("."));
            extension_tFileOutputDelimited_2 = fileName_tFileOutputDelimited_2.substring(fileName_tFileOutputDelimited_2.lastIndexOf("."));
        } else {
            fullName_tFileOutputDelimited_2 = fileName_tFileOutputDelimited_2;
            extension_tFileOutputDelimited_2 = "";
        }
        directory_tFileOutputDelimited_2 = "";
    }
    boolean isFileGenerated_tFileOutputDelimited_2 = true;
    java.io.File filetFileOutputDelimited_2 = new java.io.File(fileName_tFileOutputDelimited_2);
    globalMap.put("tFileOutputDelimited_2_FILE_NAME",fileName_tFileOutputDelimited_2);
        if(filetFileOutputDelimited_2.exists()){
            isFileGenerated_tFileOutputDelimited_2 = false;
        }
            int nb_line_tFileOutputDelimited_2 = 0;
            int splitedFileNo_tFileOutputDelimited_2 = 0;
            int currentRow_tFileOutputDelimited_2 = 0;

            final String OUT_DELIM_tFileOutputDelimited_2 = /** Start field tFileOutputDelimited_2:FIELDSEPARATOR */";"/** End field tFileOutputDelimited_2:FIELDSEPARATOR */;

            final String OUT_DELIM_ROWSEP_tFileOutputDelimited_2 = /** Start field tFileOutputDelimited_2:ROWSEPARATOR */"\n"/** End field tFileOutputDelimited_2:ROWSEPARATOR */;

                    //create directory only if not exists
                    if(directory_tFileOutputDelimited_2 != null && directory_tFileOutputDelimited_2.trim().length() != 0) {
                        java.io.File dir_tFileOutputDelimited_2 = new java.io.File(directory_tFileOutputDelimited_2);
                        if(!dir_tFileOutputDelimited_2.exists()) {
                            dir_tFileOutputDelimited_2.mkdirs();
                        }
                    }

                        //routines.system.Row
                        java.io.Writer outtFileOutputDelimited_2 = null;

                        outtFileOutputDelimited_2 = new java.io.BufferedWriter(new java.io.OutputStreamWriter(
                        new java.io.FileOutputStream(fileName_tFileOutputDelimited_2, true),"ISO-8859-15"));
                                    if(filetFileOutputDelimited_2.length()==0){
                                        outtFileOutputDelimited_2.write("code_assure");
                                            outtFileOutputDelimited_2.write(OUT_DELIM_tFileOutputDelimited_2);
                                        outtFileOutputDelimited_2.write("code_assureur");
                                            outtFileOutputDelimited_2.write(OUT_DELIM_tFileOutputDelimited_2);
                                        outtFileOutputDelimited_2.write("marque");
                                            outtFileOutputDelimited_2.write(OUT_DELIM_tFileOutputDelimited_2);
                                        outtFileOutputDelimited_2.write("modele");
                                            outtFileOutputDelimited_2.write(OUT_DELIM_tFileOutputDelimited_2);
                                        outtFileOutputDelimited_2.write("date_premiere_mise_circulation");
                                            outtFileOutputDelimited_2.write(OUT_DELIM_tFileOutputDelimited_2);
                                        outtFileOutputDelimited_2.write("immatriculation");
                                            outtFileOutputDelimited_2.write(OUT_DELIM_tFileOutputDelimited_2);
                                        outtFileOutputDelimited_2.write("chassis");
                                            outtFileOutputDelimited_2.write(OUT_DELIM_tFileOutputDelimited_2);
                                        outtFileOutputDelimited_2.write("usage");
                                            outtFileOutputDelimited_2.write(OUT_DELIM_tFileOutputDelimited_2);
                                        outtFileOutputDelimited_2.write("charge_utile");
                                            outtFileOutputDelimited_2.write(OUT_DELIM_tFileOutputDelimited_2);
                                        outtFileOutputDelimited_2.write("puissance_fiscale");
                                            outtFileOutputDelimited_2.write(OUT_DELIM_tFileOutputDelimited_2);
                                        outtFileOutputDelimited_2.write("remorque");
                                            outtFileOutputDelimited_2.write(OUT_DELIM_tFileOutputDelimited_2);
                                        outtFileOutputDelimited_2.write("nombre_portes");
                                            outtFileOutputDelimited_2.write(OUT_DELIM_tFileOutputDelimited_2);
                                        outtFileOutputDelimited_2.write("immatriculation_remorque");
                                            outtFileOutputDelimited_2.write(OUT_DELIM_tFileOutputDelimited_2);
                                        outtFileOutputDelimited_2.write("source_energie");
                                            outtFileOutputDelimited_2.write(OUT_DELIM_tFileOutputDelimited_2);
                                        outtFileOutputDelimited_2.write("nombre_de_places");
                                            outtFileOutputDelimited_2.write(OUT_DELIM_tFileOutputDelimited_2);
                                        outtFileOutputDelimited_2.write("cylindree");
                                            outtFileOutputDelimited_2.write(OUT_DELIM_tFileOutputDelimited_2);
                                        outtFileOutputDelimited_2.write("double_commande");
                                            outtFileOutputDelimited_2.write(OUT_DELIM_tFileOutputDelimited_2);
                                        outtFileOutputDelimited_2.write("responsabilite_civile");
                                            outtFileOutputDelimited_2.write(OUT_DELIM_tFileOutputDelimited_2);
                                        outtFileOutputDelimited_2.write("utilitaire");
                                            outtFileOutputDelimited_2.write(OUT_DELIM_tFileOutputDelimited_2);
                                        outtFileOutputDelimited_2.write("type_engin");
                                            outtFileOutputDelimited_2.write(OUT_DELIM_tFileOutputDelimited_2);
                                        outtFileOutputDelimited_2.write("poids_total_autorise_en_charge");
                                            outtFileOutputDelimited_2.write(OUT_DELIM_tFileOutputDelimited_2);
                                        outtFileOutputDelimited_2.write("date_extraction");
                                            outtFileOutputDelimited_2.write(OUT_DELIM_tFileOutputDelimited_2);
                                        outtFileOutputDelimited_2.write("date_depot");
                                            outtFileOutputDelimited_2.write(OUT_DELIM_tFileOutputDelimited_2);
                                        outtFileOutputDelimited_2.write("errorCode");
                                            outtFileOutputDelimited_2.write(OUT_DELIM_tFileOutputDelimited_2);
                                        outtFileOutputDelimited_2.write("errorMessage");
                                        outtFileOutputDelimited_2.write(OUT_DELIM_ROWSEP_tFileOutputDelimited_2);
                                        outtFileOutputDelimited_2.flush();
                                    }


        resourceMap.put("out_tFileOutputDelimited_2", outtFileOutputDelimited_2);
resourceMap.put("nb_line_tFileOutputDelimited_2", nb_line_tFileOutputDelimited_2);

 



/**
 * [tFileOutputDelimited_2 begin ] stop
 */



	
	/**
	 * [tDBOutput_3 begin ] start
	 */

	

	
		
		ok_Hash.put("tDBOutput_3", false);
		start_Hash.put("tDBOutput_3", System.currentTimeMillis());
		
	
	currentComponent="tDBOutput_3";

	
					if(execStat) {
						runStat.updateStatOnConnection(resourceMap,iterateId,0,0,"vehicule_rejet_Schema");
					}
				
		int tos_count_tDBOutput_3 = 0;
		





String dbschema_tDBOutput_3 = null;
	dbschema_tDBOutput_3 = context.pg_connexion_Schema;
	

String tableName_tDBOutput_3 = null;
if(dbschema_tDBOutput_3 == null || dbschema_tDBOutput_3.trim().length() == 0) {
	tableName_tDBOutput_3 = ("vehicule_rejets_schema_check");
} else {
	tableName_tDBOutput_3 = dbschema_tDBOutput_3 + "\".\"" + ("vehicule_rejets_schema_check");
}


int nb_line_tDBOutput_3 = 0;
int nb_line_update_tDBOutput_3 = 0;
int nb_line_inserted_tDBOutput_3 = 0;
int nb_line_deleted_tDBOutput_3 = 0;
int nb_line_rejected_tDBOutput_3 = 0;

int deletedCount_tDBOutput_3=0;
int updatedCount_tDBOutput_3=0;
int insertedCount_tDBOutput_3=0;
int rowsToCommitCount_tDBOutput_3=0;
int rejectedCount_tDBOutput_3=0;

boolean whetherReject_tDBOutput_3 = false;

java.sql.Connection conn_tDBOutput_3 = null;
String dbUser_tDBOutput_3 = null;

	
    java.lang.Class.forName("org.postgresql.Driver");
    
        String url_tDBOutput_3 = "jdbc:postgresql://"+context.pg_connexion_Server+":"+context.pg_connexion_Port+"/"+context.pg_connexion_Database + "?" + context.pg_connexion_AdditionalParams;
    dbUser_tDBOutput_3 = context.pg_connexion_Login;

	final String decryptedPassword_tDBOutput_3 = context.pg_connexion_Password; 

    String dbPwd_tDBOutput_3 = decryptedPassword_tDBOutput_3;

    conn_tDBOutput_3 = java.sql.DriverManager.getConnection(url_tDBOutput_3,dbUser_tDBOutput_3,dbPwd_tDBOutput_3);
	
	resourceMap.put("conn_tDBOutput_3", conn_tDBOutput_3);
        conn_tDBOutput_3.setAutoCommit(false);
        int commitEvery_tDBOutput_3 = 10000;
        int commitCounter_tDBOutput_3 = 0;


   int batchSize_tDBOutput_3 = 10000;
   int batchSizeCounter_tDBOutput_3=0;

int count_tDBOutput_3=0;
                                java.sql.DatabaseMetaData dbMetaData_tDBOutput_3 = conn_tDBOutput_3.getMetaData();
                                boolean whetherExist_tDBOutput_3 = false;
                                try (java.sql.ResultSet rsTable_tDBOutput_3 = dbMetaData_tDBOutput_3.getTables(null, null, null, new String[]{"TABLE"})) {
                                    String defaultSchema_tDBOutput_3 = "public";
                                    if(dbschema_tDBOutput_3 == null || dbschema_tDBOutput_3.trim().length() == 0) {
                                        try(java.sql.Statement stmtSchema_tDBOutput_3 = conn_tDBOutput_3.createStatement();
                                            java.sql.ResultSet rsSchema_tDBOutput_3 = stmtSchema_tDBOutput_3.executeQuery("select current_schema() ")) {
                                            while(rsSchema_tDBOutput_3.next()){
                                                defaultSchema_tDBOutput_3 = rsSchema_tDBOutput_3.getString("current_schema");
                                            }
                                        }
                                    }
                                    while(rsTable_tDBOutput_3.next()) {
                                        String table_tDBOutput_3 = rsTable_tDBOutput_3.getString("TABLE_NAME");
                                        String schema_tDBOutput_3 = rsTable_tDBOutput_3.getString("TABLE_SCHEM");
                                        if(table_tDBOutput_3.equals(("vehicule_rejets_schema_check"))
                                            && (schema_tDBOutput_3.equals(dbschema_tDBOutput_3) || ((dbschema_tDBOutput_3 ==null || dbschema_tDBOutput_3.trim().length() ==0) && defaultSchema_tDBOutput_3.equals(schema_tDBOutput_3)))) {
                                            whetherExist_tDBOutput_3 = true;
                                            break;
                                        }
                                    }
                                }
                                if(!whetherExist_tDBOutput_3) {
                                    try (java.sql.Statement stmtCreate_tDBOutput_3 = conn_tDBOutput_3.createStatement()) {
                                        stmtCreate_tDBOutput_3.execute("CREATE TABLE \"" + tableName_tDBOutput_3 + "\"(\"code_assure\" VARCHAR(128)  ,\"code_assureur\" VARCHAR(128)  ,\"marque\" VARCHAR(128)  ,\"modele\" VARCHAR(128)  ,\"date_premiere_mise_circulation\" TIMESTAMP(12)  ,\"immatriculation\" VARCHAR(128)  ,\"chassis\" VARCHAR(128)  ,\"usage\" VARCHAR(128)  ,\"charge_utile\" VARCHAR(128)  ,\"puissance_fiscale\" INT4 ,\"remorque\" VARCHAR(128)  ,\"nombre_portes\" INT4 ,\"immatriculation_remorque\" VARCHAR(128)  ,\"source_energie\" VARCHAR(128)  ,\"nombre_de_places\" INT4 ,\"cylindree\" INT4 ,\"double_commande\" INT4 ,\"responsabilite_civile\" INT4 ,\"utilitaire\" INT4 ,\"type_engin\" INT4 ,\"poids_total_autorise_en_charge\" FLOAT4 ,\"date_extraction\" TIMESTAMP ,\"date_depot\" TIMESTAMP ,\"errorCode\" VARCHAR(255)  ,\"errorMessage\" VARCHAR(255)  )");
                                    }
                                }
	    String insert_tDBOutput_3 = "INSERT INTO \"" + tableName_tDBOutput_3 + "\" (\"code_assure\",\"code_assureur\",\"marque\",\"modele\",\"date_premiere_mise_circulation\",\"immatriculation\",\"chassis\",\"usage\",\"charge_utile\",\"puissance_fiscale\",\"remorque\",\"nombre_portes\",\"immatriculation_remorque\",\"source_energie\",\"nombre_de_places\",\"cylindree\",\"double_commande\",\"responsabilite_civile\",\"utilitaire\",\"type_engin\",\"poids_total_autorise_en_charge\",\"date_extraction\",\"date_depot\",\"errorCode\",\"errorMessage\") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	    
	    java.sql.PreparedStatement pstmt_tDBOutput_3 = conn_tDBOutput_3.prepareStatement(insert_tDBOutput_3);
	    resourceMap.put("pstmt_tDBOutput_3", pstmt_tDBOutput_3);
	    

 



/**
 * [tDBOutput_3 begin ] stop
 */



	
	/**
	 * [tMap_3 begin ] start
	 */

	

	
		
		ok_Hash.put("tMap_3", false);
		start_Hash.put("tMap_3", System.currentTimeMillis());
		
	
	currentComponent="tMap_3";

	
					if(execStat) {
						runStat.updateStatOnConnection(resourceMap,iterateId,0,0,"row17");
					}
				
		int tos_count_tMap_3 = 0;
		




// ###############################
// # Lookup's keys initialization
// ###############################        

// ###############################
// # Vars initialization
class  Var__tMap_3__Struct  {
}
Var__tMap_3__Struct Var__tMap_3 = new Var__tMap_3__Struct();
// ###############################

// ###############################
// # Outputs initialization
vehicule_rejet_SchemaStruct vehicule_rejet_Schema_tmp = new vehicule_rejet_SchemaStruct();
// ###############################

        
        



        









 



/**
 * [tMap_3 begin ] stop
 */



	
	/**
	 * [tLogRow_3 begin ] start
	 */

	

	
		
		ok_Hash.put("tLogRow_3", false);
		start_Hash.put("tLogRow_3", System.currentTimeMillis());
		
	
	currentComponent="tLogRow_3";

	
					if(execStat) {
						runStat.updateStatOnConnection(resourceMap,iterateId,0,0,"row13");
					}
				
		int tos_count_tLogRow_3 = 0;
		

	///////////////////////
	
         class Util_tLogRow_3 {

        String[] des_top = { ".", ".", "-", "+" };

        String[] des_head = { "|=", "=|", "-", "+" };

        String[] des_bottom = { "'", "'", "-", "+" };

        String name="";

        java.util.List<String[]> list = new java.util.ArrayList<String[]>();

        int[] colLengths = new int[23];

        public void addRow(String[] row) {

            for (int i = 0; i < 23; i++) {
                if (row[i]!=null) {
                  colLengths[i] = Math.max(colLengths[i], row[i].length());
                }
            }
            list.add(row);
        }

        public void setTableName(String name) {

            this.name = name;
        }

            public StringBuilder format() {
            
                StringBuilder sb = new StringBuilder();
  
            
                    sb.append(print(des_top));
    
                    int totals = 0;
                    for (int i = 0; i < colLengths.length; i++) {
                        totals = totals + colLengths[i];
                    }
    
                    // name
                    sb.append("|");
                    int k = 0;
                    for (k = 0; k < (totals + 22 - name.length()) / 2; k++) {
                        sb.append(' ');
                    }
                    sb.append(name);
                    for (int i = 0; i < totals + 22 - name.length() - k; i++) {
                        sb.append(' ');
                    }
                    sb.append("|\n");

                    // head and rows
                    sb.append(print(des_head));
                    for (int i = 0; i < list.size(); i++) {
    
                        String[] row = list.get(i);
    
                        java.util.Formatter formatter = new java.util.Formatter(new StringBuilder());
                        
                        StringBuilder sbformat = new StringBuilder();                                             
        			        sbformat.append("|%1$-");
        			        sbformat.append(colLengths[0]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%2$-");
        			        sbformat.append(colLengths[1]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%3$-");
        			        sbformat.append(colLengths[2]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%4$-");
        			        sbformat.append(colLengths[3]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%5$-");
        			        sbformat.append(colLengths[4]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%6$-");
        			        sbformat.append(colLengths[5]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%7$-");
        			        sbformat.append(colLengths[6]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%8$-");
        			        sbformat.append(colLengths[7]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%9$-");
        			        sbformat.append(colLengths[8]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%10$-");
        			        sbformat.append(colLengths[9]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%11$-");
        			        sbformat.append(colLengths[10]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%12$-");
        			        sbformat.append(colLengths[11]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%13$-");
        			        sbformat.append(colLengths[12]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%14$-");
        			        sbformat.append(colLengths[13]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%15$-");
        			        sbformat.append(colLengths[14]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%16$-");
        			        sbformat.append(colLengths[15]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%17$-");
        			        sbformat.append(colLengths[16]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%18$-");
        			        sbformat.append(colLengths[17]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%19$-");
        			        sbformat.append(colLengths[18]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%20$-");
        			        sbformat.append(colLengths[19]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%21$-");
        			        sbformat.append(colLengths[20]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%22$-");
        			        sbformat.append(colLengths[21]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%23$-");
        			        sbformat.append(colLengths[22]);
        			        sbformat.append("s");
        			                      
                        sbformat.append("|\n");                    
       
                        formatter.format(sbformat.toString(), (Object[])row);	
                                
                        sb.append(formatter.toString());
                        if (i == 0)
                            sb.append(print(des_head)); // print the head
                    }
    
                    // end
                    sb.append(print(des_bottom));
                    return sb;
                }
            

            private StringBuilder print(String[] fillChars) {
                StringBuilder sb = new StringBuilder();
                //first column
                sb.append(fillChars[0]);                
                    for (int i = 0; i < colLengths[0] - fillChars[0].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);	                

                    for (int i = 0; i < colLengths[1] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[2] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[3] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[4] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[5] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[6] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[7] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[8] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[9] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[10] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[11] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[12] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[13] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[14] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[15] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[16] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[17] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[18] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[19] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[20] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[21] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                
                    //last column
                    for (int i = 0; i < colLengths[22] - fillChars[1].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }         
                sb.append(fillChars[1]);
                sb.append("\n");               
                return sb;
            }
            
            public boolean isTableEmpty(){
            	if (list.size() > 1)
            		return false;
            	return true;
            }
        }
        Util_tLogRow_3 util_tLogRow_3 = new Util_tLogRow_3();
        util_tLogRow_3.setTableName("tLogRow_3");
        util_tLogRow_3.addRow(new String[]{"code_assure","code_assureur","marque","modele","date_premiere_mise_circulation","immatriculation","chassis","usage","charge_utile","puissance_fiscale","remorque","nombre_portes","immatriculation_remorque","source_energie","nombre_de_places","cylindree","double_commande","responsabilite_civile","utilitaire","type_engin","poids_total_autorise_en_charge","errorCode","errorMessage",});        
 		StringBuilder strBuffer_tLogRow_3 = null;
		int nb_line_tLogRow_3 = 0;
///////////////////////    			



 



/**
 * [tLogRow_3 begin ] stop
 */



	
	/**
	 * [tSchemaComplianceCheck_1 begin ] start
	 */

	

	
		
		ok_Hash.put("tSchemaComplianceCheck_1", false);
		start_Hash.put("tSchemaComplianceCheck_1", System.currentTimeMillis());
		
	
	currentComponent="tSchemaComplianceCheck_1";

	
					if(execStat) {
						runStat.updateStatOnConnection(resourceMap,iterateId,0,0,"row10");
					}
				
		int tos_count_tSchemaComplianceCheck_1 = 0;
		

    class RowSetValueUtil_tSchemaComplianceCheck_1 {

        boolean ifPassedThrough = true;
        int errorCodeThrough = 0;
        String errorMessageThrough = "";
        int resultErrorCodeThrough = 0;
        String resultErrorMessageThrough = "";
        String tmpContentThrough = null;

        boolean ifPassed = true;
        int errorCode = 0;
        String errorMessage = "";

        void handleBigdecimalPrecision(String data, int iPrecision, int maxLength){
            //number of digits before the decimal point(ignoring frontend zeroes)
            int len1 = 0;
            int len2 = 0;
            ifPassed = true;
            errorCode = 0;
            errorMessage = "";
            if(data.startsWith("-")){
                data = data.substring(1);
            }
            data = org.apache.commons.lang.StringUtils.stripStart(data, "0");

            if(data.indexOf(".") >= 0){
                len1 = data.indexOf(".");
                data = org.apache.commons.lang.StringUtils.stripEnd(data, "0");
                len2 = data.length() - (len1 + 1);
            }else{
                len1 = data.length();
            }

            if (iPrecision < len2) {
                ifPassed = false;
                errorCode += 8;
                errorMessage += "|precision Non-matches";
            } else if (maxLength < len1 + iPrecision) {
                ifPassed = false;
                errorCode += 8;
                errorMessage += "|invalid Length setting is unsuitable for Precision";
            }
        }

        int handleErrorCode(int errorCode, int resultErrorCode){
            if (errorCode > 0) {
                if (resultErrorCode > 0) {
                    resultErrorCode = 16;
                } else {
                    resultErrorCode = errorCode;
                }
            }
            return resultErrorCode;
        }

        String handleErrorMessage(String errorMessage, String resultErrorMessage, String columnLabel){
            if (errorMessage.length() > 0) {
                if (resultErrorMessage.length() > 0) {
                    resultErrorMessage += ";"+ errorMessage.replaceFirst("\\|", columnLabel);
                } else {
                    resultErrorMessage = errorMessage.replaceFirst("\\|", columnLabel);
                }
            }
            return resultErrorMessage;
        }

        void reset(){
            ifPassedThrough = true;
            errorCodeThrough = 0;
            errorMessageThrough = "";
            resultErrorCodeThrough = 0;
            resultErrorMessageThrough = "";
            tmpContentThrough = null;

            ifPassed = true;
            errorCode = 0;
            errorMessage = "";
        }

        void setRowValue_0(row10Struct row10) {
    // validate nullable (empty as null)
    if ((row10.code_assure == null) || ("".equals(row10.code_assure))) {
        ifPassedThrough = false;
        errorCodeThrough += 4;
        errorMessageThrough += "|empty or null";
    }    try {
        if(
        row10.code_assure != null
        ) {
            String tester_tSchemaComplianceCheck_1 = String.valueOf(row10.code_assure);
        }
    } catch(java.lang.Exception e) {
globalMap.put("tSchemaComplianceCheck_1_ERROR_MESSAGE",e.getMessage());
        ifPassedThrough = false;
        errorCodeThrough += 2;
        errorMessageThrough += "|wrong type";
    }
    if (
    row10.code_assure != null
    ) {
        if (row10.code_assure.length() > 128) {
            ifPassedThrough = false;
            errorCodeThrough += 8;
            errorMessageThrough += "|exceed max length";
        }
    }
            resultErrorCodeThrough = handleErrorCode(errorCodeThrough,resultErrorCodeThrough);
            errorCodeThrough = 0;
            resultErrorMessageThrough = handleErrorMessage(errorMessageThrough,resultErrorMessageThrough,"code_assure:");
            errorMessageThrough = "";
    // validate nullable (empty as null)
    if ((row10.code_assureur == null) || ("".equals(row10.code_assureur))) {
        ifPassedThrough = false;
        errorCodeThrough += 4;
        errorMessageThrough += "|empty or null";
    }    try {
        if(
        row10.code_assureur != null
        ) {
            String tester_tSchemaComplianceCheck_1 = String.valueOf(row10.code_assureur);
        }
    } catch(java.lang.Exception e) {
globalMap.put("tSchemaComplianceCheck_1_ERROR_MESSAGE",e.getMessage());
        ifPassedThrough = false;
        errorCodeThrough += 2;
        errorMessageThrough += "|wrong type";
    }
    if (
    row10.code_assureur != null
    ) {
        if (row10.code_assureur.length() > 128) {
            ifPassedThrough = false;
            errorCodeThrough += 8;
            errorMessageThrough += "|exceed max length";
        }
    }
            resultErrorCodeThrough = handleErrorCode(errorCodeThrough,resultErrorCodeThrough);
            errorCodeThrough = 0;
            resultErrorMessageThrough = handleErrorMessage(errorMessageThrough,resultErrorMessageThrough,"code_assureur:");
            errorMessageThrough = "";    try {
        if(
        row10.marque != null
        && (!"".equals(row10.marque))
        ) {
            String tester_tSchemaComplianceCheck_1 = String.valueOf(row10.marque);
        }
    } catch(java.lang.Exception e) {
globalMap.put("tSchemaComplianceCheck_1_ERROR_MESSAGE",e.getMessage());
        ifPassedThrough = false;
        errorCodeThrough += 2;
        errorMessageThrough += "|wrong type";
    }
    if (
    row10.marque != null
    && (!"".equals(row10.marque))
    ) {
        if (row10.marque.length() > 128) {
            ifPassedThrough = false;
            errorCodeThrough += 8;
            errorMessageThrough += "|exceed max length";
        }
    }
            resultErrorCodeThrough = handleErrorCode(errorCodeThrough,resultErrorCodeThrough);
            errorCodeThrough = 0;
            resultErrorMessageThrough = handleErrorMessage(errorMessageThrough,resultErrorMessageThrough,"marque:");
            errorMessageThrough = "";    try {
        if(
        row10.modele != null
        && (!"".equals(row10.modele))
        ) {
            String tester_tSchemaComplianceCheck_1 = String.valueOf(row10.modele);
        }
    } catch(java.lang.Exception e) {
globalMap.put("tSchemaComplianceCheck_1_ERROR_MESSAGE",e.getMessage());
        ifPassedThrough = false;
        errorCodeThrough += 2;
        errorMessageThrough += "|wrong type";
    }
    if (
    row10.modele != null
    && (!"".equals(row10.modele))
    ) {
        if (row10.modele.length() > 128) {
            ifPassedThrough = false;
            errorCodeThrough += 8;
            errorMessageThrough += "|exceed max length";
        }
    }
            resultErrorCodeThrough = handleErrorCode(errorCodeThrough,resultErrorCodeThrough);
            errorCodeThrough = 0;
            resultErrorMessageThrough = handleErrorMessage(errorMessageThrough,resultErrorMessageThrough,"modele:");
            errorMessageThrough = "";
            resultErrorCodeThrough = handleErrorCode(errorCodeThrough,resultErrorCodeThrough);
            errorCodeThrough = 0;
            resultErrorMessageThrough = handleErrorMessage(errorMessageThrough,resultErrorMessageThrough,"date_premiere_mise_circulation:");
            errorMessageThrough = "";
    // validate nullable (empty as null)
    if ((row10.immatriculation == null) || ("".equals(row10.immatriculation))) {
        ifPassedThrough = false;
        errorCodeThrough += 4;
        errorMessageThrough += "|empty or null";
    }    try {
        if(
        row10.immatriculation != null
        ) {
            String tester_tSchemaComplianceCheck_1 = String.valueOf(row10.immatriculation);
        }
    } catch(java.lang.Exception e) {
globalMap.put("tSchemaComplianceCheck_1_ERROR_MESSAGE",e.getMessage());
        ifPassedThrough = false;
        errorCodeThrough += 2;
        errorMessageThrough += "|wrong type";
    }
    if (
    row10.immatriculation != null
    ) {
        if (row10.immatriculation.length() > 128) {
            ifPassedThrough = false;
            errorCodeThrough += 8;
            errorMessageThrough += "|exceed max length";
        }
    }
            resultErrorCodeThrough = handleErrorCode(errorCodeThrough,resultErrorCodeThrough);
            errorCodeThrough = 0;
            resultErrorMessageThrough = handleErrorMessage(errorMessageThrough,resultErrorMessageThrough,"immatriculation:");
            errorMessageThrough = "";    try {
        if(
        row10.chassis != null
        && (!"".equals(row10.chassis))
        ) {
            String tester_tSchemaComplianceCheck_1 = String.valueOf(row10.chassis);
        }
    } catch(java.lang.Exception e) {
globalMap.put("tSchemaComplianceCheck_1_ERROR_MESSAGE",e.getMessage());
        ifPassedThrough = false;
        errorCodeThrough += 2;
        errorMessageThrough += "|wrong type";
    }
    if (
    row10.chassis != null
    && (!"".equals(row10.chassis))
    ) {
        if (row10.chassis.length() > 128) {
            ifPassedThrough = false;
            errorCodeThrough += 8;
            errorMessageThrough += "|exceed max length";
        }
    }
            resultErrorCodeThrough = handleErrorCode(errorCodeThrough,resultErrorCodeThrough);
            errorCodeThrough = 0;
            resultErrorMessageThrough = handleErrorMessage(errorMessageThrough,resultErrorMessageThrough,"chassis:");
            errorMessageThrough = "";
    // validate nullable (empty as null)
    if ((row10.usage == null) || ("".equals(row10.usage))) {
        ifPassedThrough = false;
        errorCodeThrough += 4;
        errorMessageThrough += "|empty or null";
    }    try {
        if(
        row10.usage != null
        ) {
            String tester_tSchemaComplianceCheck_1 = String.valueOf(row10.usage);
        }
    } catch(java.lang.Exception e) {
globalMap.put("tSchemaComplianceCheck_1_ERROR_MESSAGE",e.getMessage());
        ifPassedThrough = false;
        errorCodeThrough += 2;
        errorMessageThrough += "|wrong type";
    }
    if (
    row10.usage != null
    ) {
        if (row10.usage.length() > 128) {
            ifPassedThrough = false;
            errorCodeThrough += 8;
            errorMessageThrough += "|exceed max length";
        }
    }
            resultErrorCodeThrough = handleErrorCode(errorCodeThrough,resultErrorCodeThrough);
            errorCodeThrough = 0;
            resultErrorMessageThrough = handleErrorMessage(errorMessageThrough,resultErrorMessageThrough,"usage:");
            errorMessageThrough = "";    try {
        if(
        row10.charge_utile != null
        && (!"".equals(row10.charge_utile))
        ) {
            String tester_tSchemaComplianceCheck_1 = String.valueOf(row10.charge_utile);
        }
    } catch(java.lang.Exception e) {
globalMap.put("tSchemaComplianceCheck_1_ERROR_MESSAGE",e.getMessage());
        ifPassedThrough = false;
        errorCodeThrough += 2;
        errorMessageThrough += "|wrong type";
    }
    if (
    row10.charge_utile != null
    && (!"".equals(row10.charge_utile))
    ) {
        if (row10.charge_utile.length() > 128) {
            ifPassedThrough = false;
            errorCodeThrough += 8;
            errorMessageThrough += "|exceed max length";
        }
    }
            resultErrorCodeThrough = handleErrorCode(errorCodeThrough,resultErrorCodeThrough);
            errorCodeThrough = 0;
            resultErrorMessageThrough = handleErrorMessage(errorMessageThrough,resultErrorMessageThrough,"charge_utile:");
            errorMessageThrough = "";
            resultErrorCodeThrough = handleErrorCode(errorCodeThrough,resultErrorCodeThrough);
            errorCodeThrough = 0;
            resultErrorMessageThrough = handleErrorMessage(errorMessageThrough,resultErrorMessageThrough,"puissance_fiscale:");
            errorMessageThrough = "";
    // validate nullable (empty as null)
    if ((row10.remorque == null) || ("".equals(row10.remorque))) {
        ifPassedThrough = false;
        errorCodeThrough += 4;
        errorMessageThrough += "|empty or null";
    }    try {
        if(
        row10.remorque != null
        ) {
            String tester_tSchemaComplianceCheck_1 = String.valueOf(row10.remorque);
        }
    } catch(java.lang.Exception e) {
globalMap.put("tSchemaComplianceCheck_1_ERROR_MESSAGE",e.getMessage());
        ifPassedThrough = false;
        errorCodeThrough += 2;
        errorMessageThrough += "|wrong type";
    }
    if (
    row10.remorque != null
    ) {
        if (row10.remorque.length() > 128) {
            ifPassedThrough = false;
            errorCodeThrough += 8;
            errorMessageThrough += "|exceed max length";
        }
    }
            resultErrorCodeThrough = handleErrorCode(errorCodeThrough,resultErrorCodeThrough);
            errorCodeThrough = 0;
            resultErrorMessageThrough = handleErrorMessage(errorMessageThrough,resultErrorMessageThrough,"remorque:");
            errorMessageThrough = "";
            resultErrorCodeThrough = handleErrorCode(errorCodeThrough,resultErrorCodeThrough);
            errorCodeThrough = 0;
            resultErrorMessageThrough = handleErrorMessage(errorMessageThrough,resultErrorMessageThrough,"nombre_portes:");
            errorMessageThrough = "";    try {
        if(
        row10.immatriculation_remorque != null
        && (!"".equals(row10.immatriculation_remorque))
        ) {
            String tester_tSchemaComplianceCheck_1 = String.valueOf(row10.immatriculation_remorque);
        }
    } catch(java.lang.Exception e) {
globalMap.put("tSchemaComplianceCheck_1_ERROR_MESSAGE",e.getMessage());
        ifPassedThrough = false;
        errorCodeThrough += 2;
        errorMessageThrough += "|wrong type";
    }
    if (
    row10.immatriculation_remorque != null
    && (!"".equals(row10.immatriculation_remorque))
    ) {
        if (row10.immatriculation_remorque.length() > 128) {
            ifPassedThrough = false;
            errorCodeThrough += 8;
            errorMessageThrough += "|exceed max length";
        }
    }
            resultErrorCodeThrough = handleErrorCode(errorCodeThrough,resultErrorCodeThrough);
            errorCodeThrough = 0;
            resultErrorMessageThrough = handleErrorMessage(errorMessageThrough,resultErrorMessageThrough,"immatriculation_remorque:");
            errorMessageThrough = "";    try {
        if(
        row10.source_energie != null
        && (!"".equals(row10.source_energie))
        ) {
            String tester_tSchemaComplianceCheck_1 = String.valueOf(row10.source_energie);
        }
    } catch(java.lang.Exception e) {
globalMap.put("tSchemaComplianceCheck_1_ERROR_MESSAGE",e.getMessage());
        ifPassedThrough = false;
        errorCodeThrough += 2;
        errorMessageThrough += "|wrong type";
    }
    if (
    row10.source_energie != null
    && (!"".equals(row10.source_energie))
    ) {
        if (row10.source_energie.length() > 128) {
            ifPassedThrough = false;
            errorCodeThrough += 8;
            errorMessageThrough += "|exceed max length";
        }
    }
            resultErrorCodeThrough = handleErrorCode(errorCodeThrough,resultErrorCodeThrough);
            errorCodeThrough = 0;
            resultErrorMessageThrough = handleErrorMessage(errorMessageThrough,resultErrorMessageThrough,"source_energie:");
            errorMessageThrough = "";
            resultErrorCodeThrough = handleErrorCode(errorCodeThrough,resultErrorCodeThrough);
            errorCodeThrough = 0;
            resultErrorMessageThrough = handleErrorMessage(errorMessageThrough,resultErrorMessageThrough,"nombre_de_places:");
            errorMessageThrough = "";
            resultErrorCodeThrough = handleErrorCode(errorCodeThrough,resultErrorCodeThrough);
            errorCodeThrough = 0;
            resultErrorMessageThrough = handleErrorMessage(errorMessageThrough,resultErrorMessageThrough,"cylindree:");
            errorMessageThrough = "";
            resultErrorCodeThrough = handleErrorCode(errorCodeThrough,resultErrorCodeThrough);
            errorCodeThrough = 0;
            resultErrorMessageThrough = handleErrorMessage(errorMessageThrough,resultErrorMessageThrough,"double_commande:");
            errorMessageThrough = "";
            resultErrorCodeThrough = handleErrorCode(errorCodeThrough,resultErrorCodeThrough);
            errorCodeThrough = 0;
            resultErrorMessageThrough = handleErrorMessage(errorMessageThrough,resultErrorMessageThrough,"responsabilite_civile:");
            errorMessageThrough = "";
            resultErrorCodeThrough = handleErrorCode(errorCodeThrough,resultErrorCodeThrough);
            errorCodeThrough = 0;
            resultErrorMessageThrough = handleErrorMessage(errorMessageThrough,resultErrorMessageThrough,"utilitaire:");
            errorMessageThrough = "";
            resultErrorCodeThrough = handleErrorCode(errorCodeThrough,resultErrorCodeThrough);
            errorCodeThrough = 0;
            resultErrorMessageThrough = handleErrorMessage(errorMessageThrough,resultErrorMessageThrough,"type_engin:");
            errorMessageThrough = "";
            resultErrorCodeThrough = handleErrorCode(errorCodeThrough,resultErrorCodeThrough);
            errorCodeThrough = 0;
            resultErrorMessageThrough = handleErrorMessage(errorMessageThrough,resultErrorMessageThrough,"poids_total_autorise_en_charge:");
            errorMessageThrough = "";
        }
    }
    RowSetValueUtil_tSchemaComplianceCheck_1 rsvUtil_tSchemaComplianceCheck_1 = new RowSetValueUtil_tSchemaComplianceCheck_1();

 



/**
 * [tSchemaComplianceCheck_1 begin ] stop
 */







	
	/**
	 * [tFileOutputDelimited_1 begin ] start
	 */

	

	
		
		ok_Hash.put("tFileOutputDelimited_1", false);
		start_Hash.put("tFileOutputDelimited_1", System.currentTimeMillis());
		
	
	currentComponent="tFileOutputDelimited_1";

	
					if(execStat) {
						runStat.updateStatOnConnection(resourceMap,iterateId,0,0,"row1");
					}
				
		int tos_count_tFileOutputDelimited_1 = 0;
		

String fileName_tFileOutputDelimited_1 = "";
    fileName_tFileOutputDelimited_1 = (new java.io.File("D:/Projet MOE/Tests/Data/rejets/vehicule_rejet_structure.txt")).getAbsolutePath().replace("\\","/");
    String fullName_tFileOutputDelimited_1 = null;
    String extension_tFileOutputDelimited_1 = null;
    String directory_tFileOutputDelimited_1 = null;
    if((fileName_tFileOutputDelimited_1.indexOf("/") != -1)) {
        if(fileName_tFileOutputDelimited_1.lastIndexOf(".") < fileName_tFileOutputDelimited_1.lastIndexOf("/")) {
            fullName_tFileOutputDelimited_1 = fileName_tFileOutputDelimited_1;
            extension_tFileOutputDelimited_1 = "";
        } else {
            fullName_tFileOutputDelimited_1 = fileName_tFileOutputDelimited_1.substring(0, fileName_tFileOutputDelimited_1.lastIndexOf("."));
            extension_tFileOutputDelimited_1 = fileName_tFileOutputDelimited_1.substring(fileName_tFileOutputDelimited_1.lastIndexOf("."));
        }
        directory_tFileOutputDelimited_1 = fileName_tFileOutputDelimited_1.substring(0, fileName_tFileOutputDelimited_1.lastIndexOf("/"));
    } else {
        if(fileName_tFileOutputDelimited_1.lastIndexOf(".") != -1) {
            fullName_tFileOutputDelimited_1 = fileName_tFileOutputDelimited_1.substring(0, fileName_tFileOutputDelimited_1.lastIndexOf("."));
            extension_tFileOutputDelimited_1 = fileName_tFileOutputDelimited_1.substring(fileName_tFileOutputDelimited_1.lastIndexOf("."));
        } else {
            fullName_tFileOutputDelimited_1 = fileName_tFileOutputDelimited_1;
            extension_tFileOutputDelimited_1 = "";
        }
        directory_tFileOutputDelimited_1 = "";
    }
    boolean isFileGenerated_tFileOutputDelimited_1 = true;
    java.io.File filetFileOutputDelimited_1 = new java.io.File(fileName_tFileOutputDelimited_1);
    globalMap.put("tFileOutputDelimited_1_FILE_NAME",fileName_tFileOutputDelimited_1);
        if(filetFileOutputDelimited_1.exists()){
            isFileGenerated_tFileOutputDelimited_1 = false;
        }
            int nb_line_tFileOutputDelimited_1 = 0;
            int splitedFileNo_tFileOutputDelimited_1 = 0;
            int currentRow_tFileOutputDelimited_1 = 0;

            final String OUT_DELIM_tFileOutputDelimited_1 = /** Start field tFileOutputDelimited_1:FIELDSEPARATOR */";"/** End field tFileOutputDelimited_1:FIELDSEPARATOR */;

            final String OUT_DELIM_ROWSEP_tFileOutputDelimited_1 = /** Start field tFileOutputDelimited_1:ROWSEPARATOR */"\n"/** End field tFileOutputDelimited_1:ROWSEPARATOR */;

                    //create directory only if not exists
                    if(directory_tFileOutputDelimited_1 != null && directory_tFileOutputDelimited_1.trim().length() != 0) {
                        java.io.File dir_tFileOutputDelimited_1 = new java.io.File(directory_tFileOutputDelimited_1);
                        if(!dir_tFileOutputDelimited_1.exists()) {
                            dir_tFileOutputDelimited_1.mkdirs();
                        }
                    }

                        //routines.system.Row
                        java.io.Writer outtFileOutputDelimited_1 = null;

                        outtFileOutputDelimited_1 = new java.io.BufferedWriter(new java.io.OutputStreamWriter(
                        new java.io.FileOutputStream(fileName_tFileOutputDelimited_1, true),"ISO-8859-15"));
                                    if(filetFileOutputDelimited_1.length()==0){
                                        outtFileOutputDelimited_1.write("code_assure");
                                            outtFileOutputDelimited_1.write(OUT_DELIM_tFileOutputDelimited_1);
                                        outtFileOutputDelimited_1.write("code_assureur");
                                            outtFileOutputDelimited_1.write(OUT_DELIM_tFileOutputDelimited_1);
                                        outtFileOutputDelimited_1.write("marque");
                                            outtFileOutputDelimited_1.write(OUT_DELIM_tFileOutputDelimited_1);
                                        outtFileOutputDelimited_1.write("modele");
                                            outtFileOutputDelimited_1.write(OUT_DELIM_tFileOutputDelimited_1);
                                        outtFileOutputDelimited_1.write("date_premiere_mise_circulation");
                                            outtFileOutputDelimited_1.write(OUT_DELIM_tFileOutputDelimited_1);
                                        outtFileOutputDelimited_1.write("immatriculation");
                                            outtFileOutputDelimited_1.write(OUT_DELIM_tFileOutputDelimited_1);
                                        outtFileOutputDelimited_1.write("chassis");
                                            outtFileOutputDelimited_1.write(OUT_DELIM_tFileOutputDelimited_1);
                                        outtFileOutputDelimited_1.write("usage");
                                            outtFileOutputDelimited_1.write(OUT_DELIM_tFileOutputDelimited_1);
                                        outtFileOutputDelimited_1.write("charge_utile");
                                            outtFileOutputDelimited_1.write(OUT_DELIM_tFileOutputDelimited_1);
                                        outtFileOutputDelimited_1.write("puissance_fiscale");
                                            outtFileOutputDelimited_1.write(OUT_DELIM_tFileOutputDelimited_1);
                                        outtFileOutputDelimited_1.write("remorque");
                                            outtFileOutputDelimited_1.write(OUT_DELIM_tFileOutputDelimited_1);
                                        outtFileOutputDelimited_1.write("nombre_portes");
                                            outtFileOutputDelimited_1.write(OUT_DELIM_tFileOutputDelimited_1);
                                        outtFileOutputDelimited_1.write("immatriculation_remorque");
                                            outtFileOutputDelimited_1.write(OUT_DELIM_tFileOutputDelimited_1);
                                        outtFileOutputDelimited_1.write("source_energie");
                                            outtFileOutputDelimited_1.write(OUT_DELIM_tFileOutputDelimited_1);
                                        outtFileOutputDelimited_1.write("nombre_de_places");
                                            outtFileOutputDelimited_1.write(OUT_DELIM_tFileOutputDelimited_1);
                                        outtFileOutputDelimited_1.write("cylindree");
                                            outtFileOutputDelimited_1.write(OUT_DELIM_tFileOutputDelimited_1);
                                        outtFileOutputDelimited_1.write("double_commande");
                                            outtFileOutputDelimited_1.write(OUT_DELIM_tFileOutputDelimited_1);
                                        outtFileOutputDelimited_1.write("responsabilite_civile");
                                            outtFileOutputDelimited_1.write(OUT_DELIM_tFileOutputDelimited_1);
                                        outtFileOutputDelimited_1.write("utilitaire");
                                            outtFileOutputDelimited_1.write(OUT_DELIM_tFileOutputDelimited_1);
                                        outtFileOutputDelimited_1.write("type_engin");
                                            outtFileOutputDelimited_1.write(OUT_DELIM_tFileOutputDelimited_1);
                                        outtFileOutputDelimited_1.write("poids_total_autorise_en_charge");
                                            outtFileOutputDelimited_1.write(OUT_DELIM_tFileOutputDelimited_1);
                                        outtFileOutputDelimited_1.write("date_extraction");
                                            outtFileOutputDelimited_1.write(OUT_DELIM_tFileOutputDelimited_1);
                                        outtFileOutputDelimited_1.write("date_depot");
                                            outtFileOutputDelimited_1.write(OUT_DELIM_tFileOutputDelimited_1);
                                        outtFileOutputDelimited_1.write("errorCode");
                                            outtFileOutputDelimited_1.write(OUT_DELIM_tFileOutputDelimited_1);
                                        outtFileOutputDelimited_1.write("errorMessage");
                                        outtFileOutputDelimited_1.write(OUT_DELIM_ROWSEP_tFileOutputDelimited_1);
                                        outtFileOutputDelimited_1.flush();
                                    }


        resourceMap.put("out_tFileOutputDelimited_1", outtFileOutputDelimited_1);
resourceMap.put("nb_line_tFileOutputDelimited_1", nb_line_tFileOutputDelimited_1);

 



/**
 * [tFileOutputDelimited_1 begin ] stop
 */



	
	/**
	 * [tDBOutput_4 begin ] start
	 */

	

	
		
		ok_Hash.put("tDBOutput_4", false);
		start_Hash.put("tDBOutput_4", System.currentTimeMillis());
		
	
	currentComponent="tDBOutput_4";

	
					if(execStat) {
						runStat.updateStatOnConnection(resourceMap,iterateId,0,0,"vehicule_rejet_structure");
					}
				
		int tos_count_tDBOutput_4 = 0;
		





String dbschema_tDBOutput_4 = null;
	dbschema_tDBOutput_4 = context.pg_connexion_Schema;
	

String tableName_tDBOutput_4 = null;
if(dbschema_tDBOutput_4 == null || dbschema_tDBOutput_4.trim().length() == 0) {
	tableName_tDBOutput_4 = ("vehicule_rejets_structure_check");
} else {
	tableName_tDBOutput_4 = dbschema_tDBOutput_4 + "\".\"" + ("vehicule_rejets_structure_check");
}


int nb_line_tDBOutput_4 = 0;
int nb_line_update_tDBOutput_4 = 0;
int nb_line_inserted_tDBOutput_4 = 0;
int nb_line_deleted_tDBOutput_4 = 0;
int nb_line_rejected_tDBOutput_4 = 0;

int deletedCount_tDBOutput_4=0;
int updatedCount_tDBOutput_4=0;
int insertedCount_tDBOutput_4=0;
int rowsToCommitCount_tDBOutput_4=0;
int rejectedCount_tDBOutput_4=0;

boolean whetherReject_tDBOutput_4 = false;

java.sql.Connection conn_tDBOutput_4 = null;
String dbUser_tDBOutput_4 = null;

	
    java.lang.Class.forName("org.postgresql.Driver");
    
        String url_tDBOutput_4 = "jdbc:postgresql://"+context.pg_connexion_Server+":"+context.pg_connexion_Port+"/"+context.pg_connexion_Database + "?" + context.pg_connexion_AdditionalParams;
    dbUser_tDBOutput_4 = context.pg_connexion_Login;

	final String decryptedPassword_tDBOutput_4 = context.pg_connexion_Password; 

    String dbPwd_tDBOutput_4 = decryptedPassword_tDBOutput_4;

    conn_tDBOutput_4 = java.sql.DriverManager.getConnection(url_tDBOutput_4,dbUser_tDBOutput_4,dbPwd_tDBOutput_4);
	
	resourceMap.put("conn_tDBOutput_4", conn_tDBOutput_4);
        conn_tDBOutput_4.setAutoCommit(false);
        int commitEvery_tDBOutput_4 = 10000;
        int commitCounter_tDBOutput_4 = 0;


   int batchSize_tDBOutput_4 = 10000;
   int batchSizeCounter_tDBOutput_4=0;

int count_tDBOutput_4=0;
            try (java.sql.Statement stmtClear_tDBOutput_4 = conn_tDBOutput_4.createStatement()) {
                stmtClear_tDBOutput_4.executeUpdate("DELETE FROM \"" + tableName_tDBOutput_4 + "\"");
            }
	    String insert_tDBOutput_4 = "INSERT INTO \"" + tableName_tDBOutput_4 + "\" (\"code_assure\",\"code_assureur\",\"marque\",\"modele\",\"date_premiere_mise_circulation\",\"immatriculation\",\"chassis\",\"usage\",\"charge_utile\",\"puissance_fiscale\",\"remorque\",\"nombre_portes\",\"immatriculation_remorque\",\"source_energie\",\"nombre_de_places\",\"cylindree\",\"double_commande\",\"responsabilite_civile\",\"utilitaire\",\"type_engin\",\"poids_total_autorise_en_charge\",\"date_extraction\",\"date_depot\",\"errorCode\",\"errorMessage\") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	    
	    java.sql.PreparedStatement pstmt_tDBOutput_4 = conn_tDBOutput_4.prepareStatement(insert_tDBOutput_4);
	    resourceMap.put("pstmt_tDBOutput_4", pstmt_tDBOutput_4);
	    

 



/**
 * [tDBOutput_4 begin ] stop
 */



	
	/**
	 * [tMap_2 begin ] start
	 */

	

	
		
		ok_Hash.put("tMap_2", false);
		start_Hash.put("tMap_2", System.currentTimeMillis());
		
	
	currentComponent="tMap_2";

	
					if(execStat) {
						runStat.updateStatOnConnection(resourceMap,iterateId,0,0,"row18");
					}
				
		int tos_count_tMap_2 = 0;
		




// ###############################
// # Lookup's keys initialization
// ###############################        

// ###############################
// # Vars initialization
class  Var__tMap_2__Struct  {
}
Var__tMap_2__Struct Var__tMap_2 = new Var__tMap_2__Struct();
// ###############################

// ###############################
// # Outputs initialization
vehicule_rejet_structureStruct vehicule_rejet_structure_tmp = new vehicule_rejet_structureStruct();
// ###############################

        
        



        









 



/**
 * [tMap_2 begin ] stop
 */



	
	/**
	 * [tLogRow_1 begin ] start
	 */

	

	
		
		ok_Hash.put("tLogRow_1", false);
		start_Hash.put("tLogRow_1", System.currentTimeMillis());
		
	
	currentComponent="tLogRow_1";

	
					if(execStat) {
						runStat.updateStatOnConnection(resourceMap,iterateId,0,0,"row11");
					}
				
		int tos_count_tLogRow_1 = 0;
		

	///////////////////////
	
         class Util_tLogRow_1 {

        String[] des_top = { ".", ".", "-", "+" };

        String[] des_head = { "|=", "=|", "-", "+" };

        String[] des_bottom = { "'", "'", "-", "+" };

        String name="";

        java.util.List<String[]> list = new java.util.ArrayList<String[]>();

        int[] colLengths = new int[23];

        public void addRow(String[] row) {

            for (int i = 0; i < 23; i++) {
                if (row[i]!=null) {
                  colLengths[i] = Math.max(colLengths[i], row[i].length());
                }
            }
            list.add(row);
        }

        public void setTableName(String name) {

            this.name = name;
        }

            public StringBuilder format() {
            
                StringBuilder sb = new StringBuilder();
  
            
                    sb.append(print(des_top));
    
                    int totals = 0;
                    for (int i = 0; i < colLengths.length; i++) {
                        totals = totals + colLengths[i];
                    }
    
                    // name
                    sb.append("|");
                    int k = 0;
                    for (k = 0; k < (totals + 22 - name.length()) / 2; k++) {
                        sb.append(' ');
                    }
                    sb.append(name);
                    for (int i = 0; i < totals + 22 - name.length() - k; i++) {
                        sb.append(' ');
                    }
                    sb.append("|\n");

                    // head and rows
                    sb.append(print(des_head));
                    for (int i = 0; i < list.size(); i++) {
    
                        String[] row = list.get(i);
    
                        java.util.Formatter formatter = new java.util.Formatter(new StringBuilder());
                        
                        StringBuilder sbformat = new StringBuilder();                                             
        			        sbformat.append("|%1$-");
        			        sbformat.append(colLengths[0]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%2$-");
        			        sbformat.append(colLengths[1]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%3$-");
        			        sbformat.append(colLengths[2]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%4$-");
        			        sbformat.append(colLengths[3]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%5$-");
        			        sbformat.append(colLengths[4]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%6$-");
        			        sbformat.append(colLengths[5]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%7$-");
        			        sbformat.append(colLengths[6]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%8$-");
        			        sbformat.append(colLengths[7]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%9$-");
        			        sbformat.append(colLengths[8]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%10$-");
        			        sbformat.append(colLengths[9]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%11$-");
        			        sbformat.append(colLengths[10]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%12$-");
        			        sbformat.append(colLengths[11]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%13$-");
        			        sbformat.append(colLengths[12]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%14$-");
        			        sbformat.append(colLengths[13]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%15$-");
        			        sbformat.append(colLengths[14]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%16$-");
        			        sbformat.append(colLengths[15]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%17$-");
        			        sbformat.append(colLengths[16]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%18$-");
        			        sbformat.append(colLengths[17]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%19$-");
        			        sbformat.append(colLengths[18]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%20$-");
        			        sbformat.append(colLengths[19]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%21$-");
        			        sbformat.append(colLengths[20]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%22$-");
        			        sbformat.append(colLengths[21]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%23$-");
        			        sbformat.append(colLengths[22]);
        			        sbformat.append("s");
        			                      
                        sbformat.append("|\n");                    
       
                        formatter.format(sbformat.toString(), (Object[])row);	
                                
                        sb.append(formatter.toString());
                        if (i == 0)
                            sb.append(print(des_head)); // print the head
                    }
    
                    // end
                    sb.append(print(des_bottom));
                    return sb;
                }
            

            private StringBuilder print(String[] fillChars) {
                StringBuilder sb = new StringBuilder();
                //first column
                sb.append(fillChars[0]);                
                    for (int i = 0; i < colLengths[0] - fillChars[0].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);	                

                    for (int i = 0; i < colLengths[1] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[2] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[3] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[4] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[5] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[6] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[7] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[8] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[9] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[10] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[11] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[12] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[13] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[14] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[15] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[16] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[17] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[18] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[19] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[20] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[21] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                
                    //last column
                    for (int i = 0; i < colLengths[22] - fillChars[1].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }         
                sb.append(fillChars[1]);
                sb.append("\n");               
                return sb;
            }
            
            public boolean isTableEmpty(){
            	if (list.size() > 1)
            		return false;
            	return true;
            }
        }
        Util_tLogRow_1 util_tLogRow_1 = new Util_tLogRow_1();
        util_tLogRow_1.setTableName("tLogRow_1");
        util_tLogRow_1.addRow(new String[]{"code_assure","code_assureur","marque","modele","date_premiere_mise_circulation","immatriculation","chassis","usage","charge_utile","puissance_fiscale","remorque","nombre_portes","immatriculation_remorque","source_energie","nombre_de_places","cylindree","double_commande","responsabilite_civile","utilitaire","type_engin","poids_total_autorise_en_charge","errorCode","errorMessage",});        
 		StringBuilder strBuffer_tLogRow_1 = null;
		int nb_line_tLogRow_1 = 0;
///////////////////////    			



 



/**
 * [tLogRow_1 begin ] stop
 */



	
	/**
	 * [tFileInputDelimited_1 begin ] start
	 */

	

	
		
		ok_Hash.put("tFileInputDelimited_1", false);
		start_Hash.put("tFileInputDelimited_1", System.currentTimeMillis());
		
	
	currentComponent="tFileInputDelimited_1";

	
		int tos_count_tFileInputDelimited_1 = 0;
		
	
	
	
 
	
	
	final routines.system.RowState rowstate_tFileInputDelimited_1 = new routines.system.RowState();
	
	
				int nb_line_tFileInputDelimited_1 = 0;
				int footer_tFileInputDelimited_1 = 0;
				int totalLinetFileInputDelimited_1 = 0;
				int limittFileInputDelimited_1 = -1;
				int lastLinetFileInputDelimited_1 = -1;	
				
				char fieldSeparator_tFileInputDelimited_1[] = null;
				
				//support passing value (property: Field Separator) by 'context.fs' or 'globalMap.get("fs")'. 
				if ( ((String)context.vehicule_FieldSeparator).length() > 0 ){
					fieldSeparator_tFileInputDelimited_1 = ((String)context.vehicule_FieldSeparator).toCharArray();
				}else {			
					throw new IllegalArgumentException("Field Separator must be assigned a char."); 
				}
			
				char rowSeparator_tFileInputDelimited_1[] = null;
			
				//support passing value (property: Row Separator) by 'context.rs' or 'globalMap.get("rs")'. 
				if ( ((String)context.vehicule_RowSeparator).length() > 0 ){
					rowSeparator_tFileInputDelimited_1 = ((String)context.vehicule_RowSeparator).toCharArray();
				}else {
					throw new IllegalArgumentException("Row Separator must be assigned a char."); 
				}
			
				Object filename_tFileInputDelimited_1 = /** Start field tFileInputDelimited_1:FILENAME */((String) globalMap.get("tFileList_1_CURRENT_FILEPATH"))/** End field tFileInputDelimited_1:FILENAME */;		
				com.talend.csv.CSVReader csvReadertFileInputDelimited_1 = null;
	
				try{
					
						String[] rowtFileInputDelimited_1=null;
						int currentLinetFileInputDelimited_1 = 0;
	        			int outputLinetFileInputDelimited_1 = 0;
						try {//TD110 begin
							if(filename_tFileInputDelimited_1 instanceof java.io.InputStream){
							
			int footer_value_tFileInputDelimited_1 = 0;
			if(footer_value_tFileInputDelimited_1 > 0){
				throw new java.lang.Exception("When the input source is a stream,footer shouldn't be bigger than 0.");
			}
		
								csvReadertFileInputDelimited_1=new com.talend.csv.CSVReader((java.io.InputStream)filename_tFileInputDelimited_1, fieldSeparator_tFileInputDelimited_1[0], context.vehicule_Encoding);
							}else{
								csvReadertFileInputDelimited_1=new com.talend.csv.CSVReader(String.valueOf(filename_tFileInputDelimited_1),fieldSeparator_tFileInputDelimited_1[0], context.vehicule_Encoding);
		        			}
					
					
					csvReadertFileInputDelimited_1.setTrimWhitespace(false);
					if ( (rowSeparator_tFileInputDelimited_1[0] != '\n') && (rowSeparator_tFileInputDelimited_1[0] != '\r') )
	        			csvReadertFileInputDelimited_1.setLineEnd(""+rowSeparator_tFileInputDelimited_1[0]);
						
	        				csvReadertFileInputDelimited_1.setQuoteChar('"');
						
	            				csvReadertFileInputDelimited_1.setEscapeChar(csvReadertFileInputDelimited_1.getQuoteChar());
							      
		
			
						if(footer_tFileInputDelimited_1 > 0){
						for(totalLinetFileInputDelimited_1=0;totalLinetFileInputDelimited_1 < context.vehicule_Header; totalLinetFileInputDelimited_1++){
							csvReadertFileInputDelimited_1.readNext();
						}
						csvReadertFileInputDelimited_1.setSkipEmptyRecords(false);
			            while (csvReadertFileInputDelimited_1.readNext()) {
							
	                
	                		totalLinetFileInputDelimited_1++;
	                
							
	                
			            }
	            		int lastLineTemptFileInputDelimited_1 = totalLinetFileInputDelimited_1 - footer_tFileInputDelimited_1   < 0? 0 : totalLinetFileInputDelimited_1 - footer_tFileInputDelimited_1 ;
	            		if(lastLinetFileInputDelimited_1 > 0){
	                		lastLinetFileInputDelimited_1 = lastLinetFileInputDelimited_1 < lastLineTemptFileInputDelimited_1 ? lastLinetFileInputDelimited_1 : lastLineTemptFileInputDelimited_1; 
	            		}else {
	                		lastLinetFileInputDelimited_1 = lastLineTemptFileInputDelimited_1;
	            		}
	         
			          	csvReadertFileInputDelimited_1.close();
				        if(filename_tFileInputDelimited_1 instanceof java.io.InputStream){
				 			csvReadertFileInputDelimited_1=new com.talend.csv.CSVReader((java.io.InputStream)filename_tFileInputDelimited_1, fieldSeparator_tFileInputDelimited_1[0], context.vehicule_Encoding);
		        		}else{
							csvReadertFileInputDelimited_1=new com.talend.csv.CSVReader(String.valueOf(filename_tFileInputDelimited_1),fieldSeparator_tFileInputDelimited_1[0], context.vehicule_Encoding);
						}
						csvReadertFileInputDelimited_1.setTrimWhitespace(false);
						if ( (rowSeparator_tFileInputDelimited_1[0] != '\n') && (rowSeparator_tFileInputDelimited_1[0] != '\r') )	
	        				csvReadertFileInputDelimited_1.setLineEnd(""+rowSeparator_tFileInputDelimited_1[0]);
						
							csvReadertFileInputDelimited_1.setQuoteChar('"');
						
	        				csvReadertFileInputDelimited_1.setEscapeChar(csvReadertFileInputDelimited_1.getQuoteChar());
							  
	        		}
	        
			        if(limittFileInputDelimited_1 != 0){
			        	for(currentLinetFileInputDelimited_1=0;currentLinetFileInputDelimited_1 < context.vehicule_Header;currentLinetFileInputDelimited_1++){
			        		csvReadertFileInputDelimited_1.readNext();
			        	}
			        }
			        csvReadertFileInputDelimited_1.setSkipEmptyRecords(false);
	        
	    		} catch(java.lang.Exception e) {
globalMap.put("tFileInputDelimited_1_ERROR_MESSAGE",e.getMessage());
					
						
						System.err.println(e.getMessage());
					
	    		}//TD110 end
	        
			    
	        	while ( limittFileInputDelimited_1 != 0 && csvReadertFileInputDelimited_1!=null && csvReadertFileInputDelimited_1.readNext() ) { 
	        		rowstate_tFileInputDelimited_1.reset();
	        
		        	rowtFileInputDelimited_1=csvReadertFileInputDelimited_1.getValues();
		        	
					
	        	
	        	
	        		currentLinetFileInputDelimited_1++;
	            
		            if(lastLinetFileInputDelimited_1 > -1 && currentLinetFileInputDelimited_1 > lastLinetFileInputDelimited_1) {
		                break;
	    	        }
	        	    outputLinetFileInputDelimited_1++;
	            	if (limittFileInputDelimited_1 > 0 && outputLinetFileInputDelimited_1 > limittFileInputDelimited_1) {
	                	break;
	            	}  
	                                                                      
					
	    							row10 = null;			
								
	    							row11 = null;			
								
								boolean whetherReject_tFileInputDelimited_1 = false;
								row10 = new row10Struct();
								try {			
									
				char fieldSeparator_tFileInputDelimited_1_ListType[] = null;
				//support passing value (property: Field Separator) by 'context.fs' or 'globalMap.get("fs")'. 
				if ( ((String)context.vehicule_FieldSeparator).length() > 0 ){
					fieldSeparator_tFileInputDelimited_1_ListType = ((String)context.vehicule_FieldSeparator).toCharArray();
				}else {			
					throw new IllegalArgumentException("Field Separator must be assigned a char."); 
				}
				if(rowtFileInputDelimited_1.length == 1 && ("\015").equals(rowtFileInputDelimited_1[0])){//empty line when row separator is '\n'
					
							row10.code_assure = null;
					
							row10.code_assureur = null;
					
							row10.marque = null;
					
							row10.modele = null;
					
							row10.date_premiere_mise_circulation = null;
					
							row10.immatriculation = null;
					
							row10.chassis = null;
					
							row10.usage = null;
					
							row10.charge_utile = null;
					
							row10.puissance_fiscale = 0;
					
							row10.remorque = null;
					
							row10.nombre_portes = null;
					
							row10.immatriculation_remorque = null;
					
							row10.source_energie = null;
					
							row10.nombre_de_places = null;
					
							row10.cylindree = null;
					
							row10.double_commande = null;
					
							row10.responsabilite_civile = null;
					
							row10.utilitaire = null;
					
							row10.type_engin = null;
					
							row10.poids_total_autorise_en_charge = null;
					
				}else{
					
					for(int i_tFileInputDelimited_1=0;i_tFileInputDelimited_1<rowtFileInputDelimited_1.length;i_tFileInputDelimited_1++){
						rowtFileInputDelimited_1[i_tFileInputDelimited_1]=rowtFileInputDelimited_1[i_tFileInputDelimited_1].trim();
					}
					
	                int columnIndexWithD_tFileInputDelimited_1 = 0; //Column Index 
	                
						columnIndexWithD_tFileInputDelimited_1 = 0;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row10.code_assure = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row10.code_assure = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 1;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row10.code_assureur = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row10.code_assureur = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 2;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row10.marque = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row10.marque = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 3;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row10.modele = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row10.modele = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 4;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
								
									if(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1].length() > 0) {
										try {
									
											row10.date_premiere_mise_circulation = ParserUtils.parseTo_Date(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1], "dd/MM/yyyy", false);
										
									
										} catch(java.lang.Exception ex_tFileInputDelimited_1) {
globalMap.put("tFileInputDelimited_1_ERROR_MESSAGE",ex_tFileInputDelimited_1.getMessage());
											rowstate_tFileInputDelimited_1.setException(new RuntimeException(String.format("Couldn't parse value for column '%s' in '%s', value is '%s'. Details: %s",
												"date_premiere_mise_circulation", "row10", rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1], ex_tFileInputDelimited_1), ex_tFileInputDelimited_1));
										}
    								}else{
    									
											
												row10.date_premiere_mise_circulation = null;
											
    									
    								}
									
									
							
						
						}else{
						
							
								row10.date_premiere_mise_circulation = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 5;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row10.immatriculation = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row10.immatriculation = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 6;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row10.chassis = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row10.chassis = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 7;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row10.usage = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row10.usage = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 8;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row10.charge_utile = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row10.charge_utile = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 9;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
								
									if(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1].length() > 0) {
										try {
									
										row10.puissance_fiscale = ParserUtils.parseTo_int(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1]);
									
									
										} catch(java.lang.Exception ex_tFileInputDelimited_1) {
globalMap.put("tFileInputDelimited_1_ERROR_MESSAGE",ex_tFileInputDelimited_1.getMessage());
											rowstate_tFileInputDelimited_1.setException(new RuntimeException(String.format("Couldn't parse value for column '%s' in '%s', value is '%s'. Details: %s",
												"puissance_fiscale", "row10", rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1], ex_tFileInputDelimited_1), ex_tFileInputDelimited_1));
										}
    								}else{
    									
    										rowstate_tFileInputDelimited_1.setException(new RuntimeException("Value is empty for column : 'puissance_fiscale' in 'row10' connection, value is invalid or this column should be nullable or have a default value."));
    									
    								}
									
									
							
						
						}else{
						
							rowstate_tFileInputDelimited_1.setException(new RuntimeException("Value is empty for column : 'puissance_fiscale' in 'row10' connection, value is invalid or this column should be nullable or have a default value."));
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 10;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row10.remorque = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row10.remorque = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 11;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
								
									if(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1].length() > 0) {
										try {
									
										row10.nombre_portes = ParserUtils.parseTo_Integer(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1]);
									
									
										} catch(java.lang.Exception ex_tFileInputDelimited_1) {
globalMap.put("tFileInputDelimited_1_ERROR_MESSAGE",ex_tFileInputDelimited_1.getMessage());
											rowstate_tFileInputDelimited_1.setException(new RuntimeException(String.format("Couldn't parse value for column '%s' in '%s', value is '%s'. Details: %s",
												"nombre_portes", "row10", rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1], ex_tFileInputDelimited_1), ex_tFileInputDelimited_1));
										}
    								}else{
    									
											
												row10.nombre_portes = null;
											
    									
    								}
									
									
							
						
						}else{
						
							
								row10.nombre_portes = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 12;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row10.immatriculation_remorque = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row10.immatriculation_remorque = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 13;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row10.source_energie = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row10.source_energie = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 14;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
								
									if(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1].length() > 0) {
										try {
									
										row10.nombre_de_places = ParserUtils.parseTo_Integer(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1]);
									
									
										} catch(java.lang.Exception ex_tFileInputDelimited_1) {
globalMap.put("tFileInputDelimited_1_ERROR_MESSAGE",ex_tFileInputDelimited_1.getMessage());
											rowstate_tFileInputDelimited_1.setException(new RuntimeException(String.format("Couldn't parse value for column '%s' in '%s', value is '%s'. Details: %s",
												"nombre_de_places", "row10", rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1], ex_tFileInputDelimited_1), ex_tFileInputDelimited_1));
										}
    								}else{
    									
											
												row10.nombre_de_places = null;
											
    									
    								}
									
									
							
						
						}else{
						
							
								row10.nombre_de_places = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 15;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
								
									if(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1].length() > 0) {
										try {
									
										row10.cylindree = ParserUtils.parseTo_Integer(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1]);
									
									
										} catch(java.lang.Exception ex_tFileInputDelimited_1) {
globalMap.put("tFileInputDelimited_1_ERROR_MESSAGE",ex_tFileInputDelimited_1.getMessage());
											rowstate_tFileInputDelimited_1.setException(new RuntimeException(String.format("Couldn't parse value for column '%s' in '%s', value is '%s'. Details: %s",
												"cylindree", "row10", rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1], ex_tFileInputDelimited_1), ex_tFileInputDelimited_1));
										}
    								}else{
    									
											
												row10.cylindree = null;
											
    									
    								}
									
									
							
						
						}else{
						
							
								row10.cylindree = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 16;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
								
									if(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1].length() > 0) {
										try {
									
										row10.double_commande = ParserUtils.parseTo_Integer(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1]);
									
									
										} catch(java.lang.Exception ex_tFileInputDelimited_1) {
globalMap.put("tFileInputDelimited_1_ERROR_MESSAGE",ex_tFileInputDelimited_1.getMessage());
											rowstate_tFileInputDelimited_1.setException(new RuntimeException(String.format("Couldn't parse value for column '%s' in '%s', value is '%s'. Details: %s",
												"double_commande", "row10", rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1], ex_tFileInputDelimited_1), ex_tFileInputDelimited_1));
										}
    								}else{
    									
											
												row10.double_commande = null;
											
    									
    								}
									
									
							
						
						}else{
						
							
								row10.double_commande = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 17;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
								
									if(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1].length() > 0) {
										try {
									
										row10.responsabilite_civile = ParserUtils.parseTo_Integer(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1]);
									
									
										} catch(java.lang.Exception ex_tFileInputDelimited_1) {
globalMap.put("tFileInputDelimited_1_ERROR_MESSAGE",ex_tFileInputDelimited_1.getMessage());
											rowstate_tFileInputDelimited_1.setException(new RuntimeException(String.format("Couldn't parse value for column '%s' in '%s', value is '%s'. Details: %s",
												"responsabilite_civile", "row10", rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1], ex_tFileInputDelimited_1), ex_tFileInputDelimited_1));
										}
    								}else{
    									
											
												row10.responsabilite_civile = null;
											
    									
    								}
									
									
							
						
						}else{
						
							
								row10.responsabilite_civile = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 18;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
								
									if(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1].length() > 0) {
										try {
									
										row10.utilitaire = ParserUtils.parseTo_Integer(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1]);
									
									
										} catch(java.lang.Exception ex_tFileInputDelimited_1) {
globalMap.put("tFileInputDelimited_1_ERROR_MESSAGE",ex_tFileInputDelimited_1.getMessage());
											rowstate_tFileInputDelimited_1.setException(new RuntimeException(String.format("Couldn't parse value for column '%s' in '%s', value is '%s'. Details: %s",
												"utilitaire", "row10", rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1], ex_tFileInputDelimited_1), ex_tFileInputDelimited_1));
										}
    								}else{
    									
											
												row10.utilitaire = null;
											
    									
    								}
									
									
							
						
						}else{
						
							
								row10.utilitaire = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 19;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
								
									if(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1].length() > 0) {
										try {
									
										row10.type_engin = ParserUtils.parseTo_Integer(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1]);
									
									
										} catch(java.lang.Exception ex_tFileInputDelimited_1) {
globalMap.put("tFileInputDelimited_1_ERROR_MESSAGE",ex_tFileInputDelimited_1.getMessage());
											rowstate_tFileInputDelimited_1.setException(new RuntimeException(String.format("Couldn't parse value for column '%s' in '%s', value is '%s'. Details: %s",
												"type_engin", "row10", rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1], ex_tFileInputDelimited_1), ex_tFileInputDelimited_1));
										}
    								}else{
    									
											
												row10.type_engin = null;
											
    									
    								}
									
									
							
						
						}else{
						
							
								row10.type_engin = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 20;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
								
									if(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1].length() > 0) {
										try {
									
										row10.poids_total_autorise_en_charge = ParserUtils.parseTo_Float(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1]);
									
									
										} catch(java.lang.Exception ex_tFileInputDelimited_1) {
globalMap.put("tFileInputDelimited_1_ERROR_MESSAGE",ex_tFileInputDelimited_1.getMessage());
											rowstate_tFileInputDelimited_1.setException(new RuntimeException(String.format("Couldn't parse value for column '%s' in '%s', value is '%s'. Details: %s",
												"poids_total_autorise_en_charge", "row10", rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1], ex_tFileInputDelimited_1), ex_tFileInputDelimited_1));
										}
    								}else{
    									
											
												row10.poids_total_autorise_en_charge = null;
											
    									
    								}
									
									
							
						
						}else{
						
							
								row10.poids_total_autorise_en_charge = null;
							
						
						}
						
						
					
				}
				
 					int filedsum = rowtFileInputDelimited_1.length;
 					if(filedsum < (21 )){
 						throw new java.lang.Exception("Column(s) missing");
 					} else if(filedsum > (21 )) {
 						throw new RuntimeException("Too many columns");
 					}     
				
									
									if(rowstate_tFileInputDelimited_1.getException()!=null) {
										throw rowstate_tFileInputDelimited_1.getException();
									}
									
									
	    						} catch (java.lang.Exception e) {
globalMap.put("tFileInputDelimited_1_ERROR_MESSAGE",e.getMessage());
							        whetherReject_tFileInputDelimited_1 = true;
        							
						                    row11 = new row11Struct();
                							
    				row11.code_assure = row10.code_assure;
				
    				row11.code_assureur = row10.code_assureur;
				
    				row11.marque = row10.marque;
				
    				row11.modele = row10.modele;
				
    				row11.date_premiere_mise_circulation = row10.date_premiere_mise_circulation;
				
    				row11.immatriculation = row10.immatriculation;
				
    				row11.chassis = row10.chassis;
				
    				row11.usage = row10.usage;
				
    				row11.charge_utile = row10.charge_utile;
				
    				row11.puissance_fiscale = row10.puissance_fiscale;
				
    				row11.remorque = row10.remorque;
				
    				row11.nombre_portes = row10.nombre_portes;
				
    				row11.immatriculation_remorque = row10.immatriculation_remorque;
				
    				row11.source_energie = row10.source_energie;
				
    				row11.nombre_de_places = row10.nombre_de_places;
				
    				row11.cylindree = row10.cylindree;
				
    				row11.double_commande = row10.double_commande;
				
    				row11.responsabilite_civile = row10.responsabilite_civile;
				
    				row11.utilitaire = row10.utilitaire;
				
    				row11.type_engin = row10.type_engin;
				
    				row11.poids_total_autorise_en_charge = row10.poids_total_autorise_en_charge;
				
			
                							row11.errorMessage = e.getMessage() + " - Line: " + tos_count_tFileInputDelimited_1;
                							row10 = null;
                						
            							globalMap.put("tFileInputDelimited_1_ERROR_MESSAGE", e.getMessage());
            							
	    						}
	
							

 



/**
 * [tFileInputDelimited_1 begin ] stop
 */
	
	/**
	 * [tFileInputDelimited_1 main ] start
	 */

	

	
	
	currentComponent="tFileInputDelimited_1";

	

 


	tos_count_tFileInputDelimited_1++;

/**
 * [tFileInputDelimited_1 main ] stop
 */
	
	/**
	 * [tFileInputDelimited_1 process_data_begin ] start
	 */

	

	
	
	currentComponent="tFileInputDelimited_1";

	

 



/**
 * [tFileInputDelimited_1 process_data_begin ] stop
 */
// Start of branch "row10"
if(row10 != null) { 



	
	/**
	 * [tSchemaComplianceCheck_1 main ] start
	 */

	

	
	
	currentComponent="tSchemaComplianceCheck_1";

	
					if(execStat){
						runStat.updateStatOnConnection(iterateId,1,1
						
							,"row10"
						
						);
					}
					
	row12 = null;	row13 = null;
	rsvUtil_tSchemaComplianceCheck_1.setRowValue_0(row10);
	if (rsvUtil_tSchemaComplianceCheck_1.ifPassedThrough) {
		row12 = new row12Struct();
		row12.code_assure = row10.code_assure;
		row12.code_assureur = row10.code_assureur;
		row12.marque = row10.marque;
		row12.modele = row10.modele;
		row12.date_premiere_mise_circulation = row10.date_premiere_mise_circulation;
		row12.immatriculation = row10.immatriculation;
		row12.chassis = row10.chassis;
		row12.usage = row10.usage;
		row12.charge_utile = row10.charge_utile;
		row12.puissance_fiscale = row10.puissance_fiscale;
		row12.remorque = row10.remorque;
		row12.nombre_portes = row10.nombre_portes;
		row12.immatriculation_remorque = row10.immatriculation_remorque;
		row12.source_energie = row10.source_energie;
		row12.nombre_de_places = row10.nombre_de_places;
		row12.cylindree = row10.cylindree;
		row12.double_commande = row10.double_commande;
		row12.responsabilite_civile = row10.responsabilite_civile;
		row12.utilitaire = row10.utilitaire;
		row12.type_engin = row10.type_engin;
		row12.poids_total_autorise_en_charge = row10.poids_total_autorise_en_charge;
	}
	if (!rsvUtil_tSchemaComplianceCheck_1.ifPassedThrough) {
		row13 = new row13Struct();
		row13.code_assure = row10.code_assure;
		row13.code_assureur = row10.code_assureur;
		row13.marque = row10.marque;
		row13.modele = row10.modele;
		row13.date_premiere_mise_circulation = row10.date_premiere_mise_circulation;
		row13.immatriculation = row10.immatriculation;
		row13.chassis = row10.chassis;
		row13.usage = row10.usage;
		row13.charge_utile = row10.charge_utile;
		row13.puissance_fiscale = row10.puissance_fiscale;
		row13.remorque = row10.remorque;
		row13.nombre_portes = row10.nombre_portes;
		row13.immatriculation_remorque = row10.immatriculation_remorque;
		row13.source_energie = row10.source_energie;
		row13.nombre_de_places = row10.nombre_de_places;
		row13.cylindree = row10.cylindree;
		row13.double_commande = row10.double_commande;
		row13.responsabilite_civile = row10.responsabilite_civile;
		row13.utilitaire = row10.utilitaire;
		row13.type_engin = row10.type_engin;
		row13.poids_total_autorise_en_charge = row10.poids_total_autorise_en_charge;
		row13.errorCode = String.valueOf(rsvUtil_tSchemaComplianceCheck_1.resultErrorCodeThrough);
		row13.errorMessage = rsvUtil_tSchemaComplianceCheck_1.resultErrorMessageThrough;
	}
	rsvUtil_tSchemaComplianceCheck_1.reset();

 


	tos_count_tSchemaComplianceCheck_1++;

/**
 * [tSchemaComplianceCheck_1 main ] stop
 */
	
	/**
	 * [tSchemaComplianceCheck_1 process_data_begin ] start
	 */

	

	
	
	currentComponent="tSchemaComplianceCheck_1";

	

 



/**
 * [tSchemaComplianceCheck_1 process_data_begin ] stop
 */
// Start of branch "row12"
if(row12 != null) { 



	
	/**
	 * [tLogRow_2 main ] start
	 */

	

	
	
	currentComponent="tLogRow_2";

	
					if(execStat){
						runStat.updateStatOnConnection(iterateId,1,1
						
							,"row12"
						
						);
					}
					
///////////////////////		
						

				
				String[] row_tLogRow_2 = new String[21];
   				
	    		if(row12.code_assure != null) { //              
                 row_tLogRow_2[0]=    						    
				                String.valueOf(row12.code_assure)			
					          ;	
							
	    		} //			
    			   				
	    		if(row12.code_assureur != null) { //              
                 row_tLogRow_2[1]=    						    
				                String.valueOf(row12.code_assureur)			
					          ;	
							
	    		} //			
    			   				
	    		if(row12.marque != null) { //              
                 row_tLogRow_2[2]=    						    
				                String.valueOf(row12.marque)			
					          ;	
							
	    		} //			
    			   				
	    		if(row12.modele != null) { //              
                 row_tLogRow_2[3]=    						    
				                String.valueOf(row12.modele)			
					          ;	
							
	    		} //			
    			   				
	    		if(row12.date_premiere_mise_circulation != null) { //              
                 row_tLogRow_2[4]=    						
								FormatterUtils.format_Date(row12.date_premiere_mise_circulation, "dd/MM/yyyy")
					          ;	
							
	    		} //			
    			   				
	    		if(row12.immatriculation != null) { //              
                 row_tLogRow_2[5]=    						    
				                String.valueOf(row12.immatriculation)			
					          ;	
							
	    		} //			
    			   				
	    		if(row12.chassis != null) { //              
                 row_tLogRow_2[6]=    						    
				                String.valueOf(row12.chassis)			
					          ;	
							
	    		} //			
    			   				
	    		if(row12.usage != null) { //              
                 row_tLogRow_2[7]=    						    
				                String.valueOf(row12.usage)			
					          ;	
							
	    		} //			
    			   				
	    		if(row12.charge_utile != null) { //              
                 row_tLogRow_2[8]=    						    
				                String.valueOf(row12.charge_utile)			
					          ;	
							
	    		} //			
    			              
                 row_tLogRow_2[9]=    						    
				                String.valueOf(row12.puissance_fiscale)			
					          ;	
										
    			   				
	    		if(row12.remorque != null) { //              
                 row_tLogRow_2[10]=    						    
				                String.valueOf(row12.remorque)			
					          ;	
							
	    		} //			
    			   				
	    		if(row12.nombre_portes != null) { //              
                 row_tLogRow_2[11]=    						    
				                String.valueOf(row12.nombre_portes)			
					          ;	
							
	    		} //			
    			   				
	    		if(row12.immatriculation_remorque != null) { //              
                 row_tLogRow_2[12]=    						    
				                String.valueOf(row12.immatriculation_remorque)			
					          ;	
							
	    		} //			
    			   				
	    		if(row12.source_energie != null) { //              
                 row_tLogRow_2[13]=    						    
				                String.valueOf(row12.source_energie)			
					          ;	
							
	    		} //			
    			   				
	    		if(row12.nombre_de_places != null) { //              
                 row_tLogRow_2[14]=    						    
				                String.valueOf(row12.nombre_de_places)			
					          ;	
							
	    		} //			
    			   				
	    		if(row12.cylindree != null) { //              
                 row_tLogRow_2[15]=    						    
				                String.valueOf(row12.cylindree)			
					          ;	
							
	    		} //			
    			   				
	    		if(row12.double_commande != null) { //              
                 row_tLogRow_2[16]=    						    
				                String.valueOf(row12.double_commande)			
					          ;	
							
	    		} //			
    			   				
	    		if(row12.responsabilite_civile != null) { //              
                 row_tLogRow_2[17]=    						    
				                String.valueOf(row12.responsabilite_civile)			
					          ;	
							
	    		} //			
    			   				
	    		if(row12.utilitaire != null) { //              
                 row_tLogRow_2[18]=    						    
				                String.valueOf(row12.utilitaire)			
					          ;	
							
	    		} //			
    			   				
	    		if(row12.type_engin != null) { //              
                 row_tLogRow_2[19]=    						    
				                String.valueOf(row12.type_engin)			
					          ;	
							
	    		} //			
    			   				
	    		if(row12.poids_total_autorise_en_charge != null) { //              
                 row_tLogRow_2[20]=    						
								FormatterUtils.formatUnwithE(row12.poids_total_autorise_en_charge)
					          ;	
							
	    		} //			
    			 

				util_tLogRow_2.addRow(row_tLogRow_2);	
				nb_line_tLogRow_2++;
//////

//////                    
                    
///////////////////////    			

 
     row14 = row12;


	tos_count_tLogRow_2++;

/**
 * [tLogRow_2 main ] stop
 */
	
	/**
	 * [tLogRow_2 process_data_begin ] start
	 */

	

	
	
	currentComponent="tLogRow_2";

	

 



/**
 * [tLogRow_2 process_data_begin ] stop
 */

	
	/**
	 * [tMap_1 main ] start
	 */

	

	
	
	currentComponent="tMap_1";

	
					if(execStat){
						runStat.updateStatOnConnection(iterateId,1,1
						
							,"row14"
						
						);
					}
					

		
		
		boolean hasCasePrimitiveKeyWithNull_tMap_1 = false;
		

        // ###############################
        // # Input tables (lookups)
		  boolean rejectedInnerJoin_tMap_1 = false;
		  boolean mainRowRejected_tMap_1 = false;
            				    								  
		// ###############################
        { // start of Var scope
        
	        // ###############################
        	// # Vars tables
        
Var__tMap_1__Struct Var = Var__tMap_1;
Var.D_P_M_Circulation = row14.date_premiere_mise_circulation==null && !row14.usage.equals("6") ?"date_premiere_mise_circulation_vide_catégorie_differente_de_6   ":!row14.usage.equals("6") && row14.date_premiere_mise_circulation!=null && TalendDate.compareDate(row14.date_premiere_mise_circulation,TalendDate.parseDate("dd/MM/yyyy", "01/01/1950"))==-1?"date_premiere_mise_circulation_erronee   ":"" ;
Var.S_energie = ("").equals(row14.source_energie) && !row14.usage.equals("6") ?"source_energie_vide_catégorie_differente_de_6    ":!row14.usage.equals("6") && !("").equals(row14.source_energie) && !"essence_diesel_".contains(StringHandling.DOWNCASE(row14.source_energie))?"source_energie_erronee   ":"" ;
Var.Chassis = ("").equals(row14.chassis) && !row14.usage.equals("6") ?"Chassis_vide_catégorie_differente_de_6   ":!("").equals(row14.chassis) && !row14.usage.equals("6") && StringHandling.LEN(row14.chassis)<4?"Chassis_errone   ":"" ;
Var.Imma_remorq = !row14.remorque.equals("0") && ("").equals(row14.immatriculation_remorque)?"immatriculation_remorque_vide_vehicule_avec_remorque ":"" ;
Var.N_places = (row14.usage.equals("4a") && row14.nombre_de_places==null || row14.usage.equals("4b") && row14.nombre_de_places==null)?"Nombre_de_places_vide_avec_categorie_4a_ou_4b ":"" ;
Var.cylindree = (row14.usage.equals("5") && row14.cylindree==null)?"cylindree_vide_avec_categorie_5 ":"" ;
Var.Double_commande = (row14.usage.equals("7b") && row14.double_commande==null )?"double_commande_vide_avec_categorie_7b ":"" ;
Var.Responsabilite_civile = (row14.usage.equals("6") && row14.responsabilite_civile==null)?"responsabilite_civile_vide_avec_categorie_6 ":"" ;
Var.Utilitaire = (row14.usage.equals("8") && row14.utilitaire==null)?"utilitaire_vide_avec_categorie_8 ":"" ;
Var.Type_engin = (row14.usage.equals("9") && row14.type_engin==null)?"type_engin_vide_avec_categorie_9 ":"" ;
Var.poids_total_en_charge = (row14.usage.equals("2") && row14.poids_total_autorise_en_charge==null || row14.usage.equals("3") && row14.poids_total_autorise_en_charge==null)?"poids_total_autorise_en_charge_vide_avec_categorie_2_ou_3 ":"" ;
Var.SourceEnergieVal = "essence_diesel_" ;// ###############################
        // ###############################
        // # Output tables

vehicule_valide = null;
vehicule_rejet = null;

boolean rejected_tMap_1 = true;

// # Output table : 'vehicule_valide'
// # Filter conditions 
if( 

!(!row14.usage.equals("6") && row14.date_premiere_mise_circulation==null || !row14.usage.equals("6") && ("").equals(row14.chassis) || !row14.remorque.equals("0") && ("").equals(row14.immatriculation_remorque) ||!row14.usage.equals("6") && ("").equals(row14.source_energie) || row14.usage.equals("4a") && row14.nombre_de_places==null || row14.usage.equals("4b") && row14.nombre_de_places==null || row14.usage.equals("5") && row14.cylindree==null || row14.usage.equals("7b") && row14.double_commande==null || row14.usage.equals("6") && row14.responsabilite_civile==null || row14.usage.equals("8") && row14.utilitaire==null || row14.usage.equals("9") && row14.type_engin==null || row14.usage.equals("2") && row14.poids_total_autorise_en_charge==null || row14.usage.equals("3") && row14.poids_total_autorise_en_charge==null || !row14.usage.equals("6") && row14.date_premiere_mise_circulation!=null && TalendDate.compareDate(row14.date_premiere_mise_circulation,TalendDate.parseDate("dd/MM/yyyy", "01/01/1950"))==-1 ||!row14.usage.equals("6") && !("").equals(row14.source_energie) && !Var.SourceEnergieVal.contains(StringHandling.DOWNCASE(row14.source_energie)) || !row14.usage.equals("6") && StringHandling.LEN(row14.chassis)<4)

 ) {
rejected_tMap_1 = false;
vehicule_valide_tmp.code_assure = row14.code_assure ;
vehicule_valide_tmp.code_assureur = row14.code_assureur ;
vehicule_valide_tmp.marque = row14.marque ;
vehicule_valide_tmp.modele = row14.modele ;
vehicule_valide_tmp.date_premiere_mise_circulation = row14.date_premiere_mise_circulation ;
vehicule_valide_tmp.immatriculation = row14.immatriculation ;
vehicule_valide_tmp.chassis = row14.chassis ;
vehicule_valide_tmp.usage = row14.usage ;
vehicule_valide_tmp.charge_utile = row14.charge_utile ;
vehicule_valide_tmp.puissance_fiscale = row14.puissance_fiscale ;
vehicule_valide_tmp.remorque = row14.remorque ;
vehicule_valide_tmp.nombre_portes = row14.nombre_portes ;
vehicule_valide_tmp.immatriculation_remorque = row14.immatriculation_remorque ;
vehicule_valide_tmp.source_energie = row14.source_energie ;
vehicule_valide_tmp.nombre_de_places = row14.nombre_de_places ;
vehicule_valide_tmp.cylindree = row14.cylindree ;
vehicule_valide_tmp.double_commande = row14.double_commande ;
vehicule_valide_tmp.responsabilite_civile = row14.responsabilite_civile ;
vehicule_valide_tmp.utilitaire = row14.utilitaire ;
vehicule_valide_tmp.type_engin = row14.type_engin ;
vehicule_valide_tmp.poids_total_autorise_en_charge = row14.poids_total_autorise_en_charge ;
vehicule_valide_tmp.c_status = 1;
vehicule_valide_tmp.c_date_mis_a_jour = null;
vehicule_valide_tmp.c_date_transfer = null;
vehicule_valide_tmp.commentaires = null;
vehicule_valide = vehicule_valide_tmp;
} // closing filter/reject
// ###### START REJECTS ##### 

// # Output reject table : 'vehicule_rejet'
// # Filter conditions 
if( rejected_tMap_1 ) {
vehicule_rejet_tmp.code_assure = row14.code_assure ;
vehicule_rejet_tmp.code_assureur = row14.code_assureur ;
vehicule_rejet_tmp.marque = row14.marque ;
vehicule_rejet_tmp.modele = row14.modele ;
vehicule_rejet_tmp.date_premiere_mise_circulation = row14.date_premiere_mise_circulation ;
vehicule_rejet_tmp.immatriculation = row14.immatriculation ;
vehicule_rejet_tmp.chassis = row14.chassis ;
vehicule_rejet_tmp.usage = row14.usage ;
vehicule_rejet_tmp.charge_utile = row14.charge_utile ;
vehicule_rejet_tmp.puissance_fiscale = row14.puissance_fiscale ;
vehicule_rejet_tmp.remorque = row14.remorque ;
vehicule_rejet_tmp.nombre_portes = row14.nombre_portes ;
vehicule_rejet_tmp.immatriculation_remorque = row14.immatriculation_remorque ;
vehicule_rejet_tmp.source_energie = row14.source_energie ;
vehicule_rejet_tmp.nombre_de_places = row14.nombre_de_places ;
vehicule_rejet_tmp.cylindree = row14.cylindree ;
vehicule_rejet_tmp.double_commande = row14.double_commande ;
vehicule_rejet_tmp.responsabilite_civile = row14.responsabilite_civile ;
vehicule_rejet_tmp.utilitaire = row14.utilitaire ;
vehicule_rejet_tmp.type_engin = row14.type_engin ;
vehicule_rejet_tmp.poids_total_autorise_en_charge = row14.poids_total_autorise_en_charge ;
vehicule_rejet_tmp.date_extraction = null;
vehicule_rejet_tmp.date_depot = null;
vehicule_rejet_tmp.Error_Message =  Var.D_P_M_Circulation+Var.S_energie+Var.Chassis+Var.Imma_remorq+Var.N_places+Var.cylindree+Var.Double_commande+Var.Responsabilite_civile+Var.Utilitaire+Var.Type_engin+Var.poids_total_en_charge;
vehicule_rejet = vehicule_rejet_tmp;
} // closing filter/reject
// ###############################

} // end of Var scope

rejectedInnerJoin_tMap_1 = false;










 


	tos_count_tMap_1++;

/**
 * [tMap_1 main ] stop
 */
	
	/**
	 * [tMap_1 process_data_begin ] start
	 */

	

	
	
	currentComponent="tMap_1";

	

 



/**
 * [tMap_1 process_data_begin ] stop
 */
// Start of branch "vehicule_valide"
if(vehicule_valide != null) { 



	
	/**
	 * [tLogRow_4 main ] start
	 */

	

	
	
	currentComponent="tLogRow_4";

	
					if(execStat){
						runStat.updateStatOnConnection(iterateId,1,1
						
							,"vehicule_valide"
						
						);
					}
					
///////////////////////		
						

				
				String[] row_tLogRow_4 = new String[25];
   				
	    		if(vehicule_valide.code_assure != null) { //              
                 row_tLogRow_4[0]=    						    
				                String.valueOf(vehicule_valide.code_assure)			
					          ;	
							
	    		} //			
    			   				
	    		if(vehicule_valide.code_assureur != null) { //              
                 row_tLogRow_4[1]=    						    
				                String.valueOf(vehicule_valide.code_assureur)			
					          ;	
							
	    		} //			
    			   				
	    		if(vehicule_valide.marque != null) { //              
                 row_tLogRow_4[2]=    						    
				                String.valueOf(vehicule_valide.marque)			
					          ;	
							
	    		} //			
    			   				
	    		if(vehicule_valide.modele != null) { //              
                 row_tLogRow_4[3]=    						    
				                String.valueOf(vehicule_valide.modele)			
					          ;	
							
	    		} //			
    			   				
	    		if(vehicule_valide.date_premiere_mise_circulation != null) { //              
                 row_tLogRow_4[4]=    						
								FormatterUtils.format_Date(vehicule_valide.date_premiere_mise_circulation, "dd/MM/yyyy")
					          ;	
							
	    		} //			
    			   				
	    		if(vehicule_valide.immatriculation != null) { //              
                 row_tLogRow_4[5]=    						    
				                String.valueOf(vehicule_valide.immatriculation)			
					          ;	
							
	    		} //			
    			   				
	    		if(vehicule_valide.chassis != null) { //              
                 row_tLogRow_4[6]=    						    
				                String.valueOf(vehicule_valide.chassis)			
					          ;	
							
	    		} //			
    			   				
	    		if(vehicule_valide.usage != null) { //              
                 row_tLogRow_4[7]=    						    
				                String.valueOf(vehicule_valide.usage)			
					          ;	
							
	    		} //			
    			   				
	    		if(vehicule_valide.charge_utile != null) { //              
                 row_tLogRow_4[8]=    						    
				                String.valueOf(vehicule_valide.charge_utile)			
					          ;	
							
	    		} //			
    			              
                 row_tLogRow_4[9]=    						    
				                String.valueOf(vehicule_valide.puissance_fiscale)			
					          ;	
										
    			   				
	    		if(vehicule_valide.remorque != null) { //              
                 row_tLogRow_4[10]=    						    
				                String.valueOf(vehicule_valide.remorque)			
					          ;	
							
	    		} //			
    			   				
	    		if(vehicule_valide.nombre_portes != null) { //              
                 row_tLogRow_4[11]=    						    
				                String.valueOf(vehicule_valide.nombre_portes)			
					          ;	
							
	    		} //			
    			   				
	    		if(vehicule_valide.immatriculation_remorque != null) { //              
                 row_tLogRow_4[12]=    						    
				                String.valueOf(vehicule_valide.immatriculation_remorque)			
					          ;	
							
	    		} //			
    			   				
	    		if(vehicule_valide.source_energie != null) { //              
                 row_tLogRow_4[13]=    						    
				                String.valueOf(vehicule_valide.source_energie)			
					          ;	
							
	    		} //			
    			   				
	    		if(vehicule_valide.nombre_de_places != null) { //              
                 row_tLogRow_4[14]=    						    
				                String.valueOf(vehicule_valide.nombre_de_places)			
					          ;	
							
	    		} //			
    			   				
	    		if(vehicule_valide.cylindree != null) { //              
                 row_tLogRow_4[15]=    						    
				                String.valueOf(vehicule_valide.cylindree)			
					          ;	
							
	    		} //			
    			   				
	    		if(vehicule_valide.double_commande != null) { //              
                 row_tLogRow_4[16]=    						    
				                String.valueOf(vehicule_valide.double_commande)			
					          ;	
							
	    		} //			
    			   				
	    		if(vehicule_valide.responsabilite_civile != null) { //              
                 row_tLogRow_4[17]=    						    
				                String.valueOf(vehicule_valide.responsabilite_civile)			
					          ;	
							
	    		} //			
    			   				
	    		if(vehicule_valide.utilitaire != null) { //              
                 row_tLogRow_4[18]=    						    
				                String.valueOf(vehicule_valide.utilitaire)			
					          ;	
							
	    		} //			
    			   				
	    		if(vehicule_valide.type_engin != null) { //              
                 row_tLogRow_4[19]=    						    
				                String.valueOf(vehicule_valide.type_engin)			
					          ;	
							
	    		} //			
    			   				
	    		if(vehicule_valide.poids_total_autorise_en_charge != null) { //              
                 row_tLogRow_4[20]=    						
								FormatterUtils.formatUnwithE(vehicule_valide.poids_total_autorise_en_charge)
					          ;	
							
	    		} //			
    			              
                 row_tLogRow_4[21]=    						    
				                String.valueOf(vehicule_valide.c_status)			
					          ;	
										
    			   				
	    		if(vehicule_valide.c_date_mis_a_jour != null) { //              
                 row_tLogRow_4[22]=    						
								FormatterUtils.format_Date(vehicule_valide.c_date_mis_a_jour, "dd/MM/yyyy")
					          ;	
							
	    		} //			
    			   				
	    		if(vehicule_valide.c_date_transfer != null) { //              
                 row_tLogRow_4[23]=    						
								FormatterUtils.format_Date(vehicule_valide.c_date_transfer, "dd/MM/yyyy")
					          ;	
							
	    		} //			
    			   				
	    		if(vehicule_valide.commentaires != null) { //              
                 row_tLogRow_4[24]=    						    
				                String.valueOf(vehicule_valide.commentaires)			
					          ;	
							
	    		} //			
    			 

				util_tLogRow_4.addRow(row_tLogRow_4);	
				nb_line_tLogRow_4++;
//////

//////                    
                    
///////////////////////    			

 
     row15 = vehicule_valide;


	tos_count_tLogRow_4++;

/**
 * [tLogRow_4 main ] stop
 */
	
	/**
	 * [tLogRow_4 process_data_begin ] start
	 */

	

	
	
	currentComponent="tLogRow_4";

	

 



/**
 * [tLogRow_4 process_data_begin ] stop
 */

	
	/**
	 * [tDBOutput_1 main ] start
	 */

	

	
	
	currentComponent="tDBOutput_1";

	
					if(execStat){
						runStat.updateStatOnConnection(iterateId,1,1
						
							,"row15"
						
						);
					}
					



        whetherReject_tDBOutput_1 = false;
                    if(row15.immatriculation == null) {
pstmt_tDBOutput_1.setNull(1, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_1.setString(1, row15.immatriculation);
}

            int checkCount_tDBOutput_1 = -1;
            try (java.sql.ResultSet rs_tDBOutput_1 = pstmt_tDBOutput_1.executeQuery()) {
                while(rs_tDBOutput_1.next()) {
                    checkCount_tDBOutput_1 = rs_tDBOutput_1.getInt(1);
                }
            }
            if(checkCount_tDBOutput_1 > 0) {
                        if(row15.code_assure == null) {
pstmtUpdate_tDBOutput_1.setNull(1, java.sql.Types.VARCHAR);
} else {pstmtUpdate_tDBOutput_1.setString(1, row15.code_assure);
}

                        if(row15.code_assureur == null) {
pstmtUpdate_tDBOutput_1.setNull(2, java.sql.Types.VARCHAR);
} else {pstmtUpdate_tDBOutput_1.setString(2, row15.code_assureur);
}

                        if(row15.marque == null) {
pstmtUpdate_tDBOutput_1.setNull(3, java.sql.Types.VARCHAR);
} else {pstmtUpdate_tDBOutput_1.setString(3, row15.marque);
}

                        if(row15.modele == null) {
pstmtUpdate_tDBOutput_1.setNull(4, java.sql.Types.VARCHAR);
} else {pstmtUpdate_tDBOutput_1.setString(4, row15.modele);
}

                        if(row15.date_premiere_mise_circulation != null) {
pstmtUpdate_tDBOutput_1.setTimestamp(5, new java.sql.Timestamp(row15.date_premiere_mise_circulation.getTime()));
} else {
pstmtUpdate_tDBOutput_1.setNull(5, java.sql.Types.TIMESTAMP);
}

                        if(row15.chassis == null) {
pstmtUpdate_tDBOutput_1.setNull(6, java.sql.Types.VARCHAR);
} else {pstmtUpdate_tDBOutput_1.setString(6, row15.chassis);
}

                        if(row15.usage == null) {
pstmtUpdate_tDBOutput_1.setNull(7, java.sql.Types.VARCHAR);
} else {pstmtUpdate_tDBOutput_1.setString(7, row15.usage);
}

                        if(row15.charge_utile == null) {
pstmtUpdate_tDBOutput_1.setNull(8, java.sql.Types.VARCHAR);
} else {pstmtUpdate_tDBOutput_1.setString(8, row15.charge_utile);
}

                        pstmtUpdate_tDBOutput_1.setInt(9, row15.puissance_fiscale);

                        if(row15.remorque == null) {
pstmtUpdate_tDBOutput_1.setNull(10, java.sql.Types.VARCHAR);
} else {pstmtUpdate_tDBOutput_1.setString(10, row15.remorque);
}

                        if(row15.nombre_portes == null) {
pstmtUpdate_tDBOutput_1.setNull(11, java.sql.Types.INTEGER);
} else {pstmtUpdate_tDBOutput_1.setInt(11, row15.nombre_portes);
}

                        if(row15.immatriculation_remorque == null) {
pstmtUpdate_tDBOutput_1.setNull(12, java.sql.Types.VARCHAR);
} else {pstmtUpdate_tDBOutput_1.setString(12, row15.immatriculation_remorque);
}

                        if(row15.source_energie == null) {
pstmtUpdate_tDBOutput_1.setNull(13, java.sql.Types.VARCHAR);
} else {pstmtUpdate_tDBOutput_1.setString(13, row15.source_energie);
}

                        if(row15.nombre_de_places == null) {
pstmtUpdate_tDBOutput_1.setNull(14, java.sql.Types.INTEGER);
} else {pstmtUpdate_tDBOutput_1.setInt(14, row15.nombre_de_places);
}

                        if(row15.cylindree == null) {
pstmtUpdate_tDBOutput_1.setNull(15, java.sql.Types.INTEGER);
} else {pstmtUpdate_tDBOutput_1.setInt(15, row15.cylindree);
}

                        if(row15.double_commande == null) {
pstmtUpdate_tDBOutput_1.setNull(16, java.sql.Types.INTEGER);
} else {pstmtUpdate_tDBOutput_1.setInt(16, row15.double_commande);
}

                        if(row15.responsabilite_civile == null) {
pstmtUpdate_tDBOutput_1.setNull(17, java.sql.Types.INTEGER);
} else {pstmtUpdate_tDBOutput_1.setInt(17, row15.responsabilite_civile);
}

                        if(row15.utilitaire == null) {
pstmtUpdate_tDBOutput_1.setNull(18, java.sql.Types.INTEGER);
} else {pstmtUpdate_tDBOutput_1.setInt(18, row15.utilitaire);
}

                        if(row15.type_engin == null) {
pstmtUpdate_tDBOutput_1.setNull(19, java.sql.Types.INTEGER);
} else {pstmtUpdate_tDBOutput_1.setInt(19, row15.type_engin);
}

                        if(row15.poids_total_autorise_en_charge == null) {
pstmtUpdate_tDBOutput_1.setNull(20, java.sql.Types.FLOAT);
} else {pstmtUpdate_tDBOutput_1.setFloat(20, row15.poids_total_autorise_en_charge);
}

                        pstmtUpdate_tDBOutput_1.setInt(21, row15.c_status);

                        if(row15.c_date_mis_a_jour != null) {
pstmtUpdate_tDBOutput_1.setTimestamp(22, new java.sql.Timestamp(row15.c_date_mis_a_jour.getTime()));
} else {
pstmtUpdate_tDBOutput_1.setNull(22, java.sql.Types.TIMESTAMP);
}

                        if(row15.c_date_transfer != null) {
pstmtUpdate_tDBOutput_1.setTimestamp(23, new java.sql.Timestamp(row15.c_date_transfer.getTime()));
} else {
pstmtUpdate_tDBOutput_1.setNull(23, java.sql.Types.TIMESTAMP);
}

                        if(row15.commentaires == null) {
pstmtUpdate_tDBOutput_1.setNull(24, java.sql.Types.VARCHAR);
} else {pstmtUpdate_tDBOutput_1.setString(24, row15.commentaires);
}

                        if(row15.immatriculation == null) {
pstmtUpdate_tDBOutput_1.setNull(25 + count_tDBOutput_1, java.sql.Types.VARCHAR);
} else {pstmtUpdate_tDBOutput_1.setString(25 + count_tDBOutput_1, row15.immatriculation);
}

                try {
					
                    int processedCount_tDBOutput_1 = pstmtUpdate_tDBOutput_1.executeUpdate();
                    updatedCount_tDBOutput_1 += processedCount_tDBOutput_1;
                    rowsToCommitCount_tDBOutput_1 += processedCount_tDBOutput_1;
                    nb_line_tDBOutput_1++;
					
                } catch(java.lang.Exception e) {
globalMap.put("tDBOutput_1_ERROR_MESSAGE",e.getMessage());
					
                    whetherReject_tDBOutput_1 = true;
                        nb_line_tDBOutput_1++;
                            System.err.print(e.getMessage());
                }
            } else {
                        if(row15.code_assure == null) {
pstmtInsert_tDBOutput_1.setNull(1, java.sql.Types.VARCHAR);
} else {pstmtInsert_tDBOutput_1.setString(1, row15.code_assure);
}

                        if(row15.code_assureur == null) {
pstmtInsert_tDBOutput_1.setNull(2, java.sql.Types.VARCHAR);
} else {pstmtInsert_tDBOutput_1.setString(2, row15.code_assureur);
}

                        if(row15.marque == null) {
pstmtInsert_tDBOutput_1.setNull(3, java.sql.Types.VARCHAR);
} else {pstmtInsert_tDBOutput_1.setString(3, row15.marque);
}

                        if(row15.modele == null) {
pstmtInsert_tDBOutput_1.setNull(4, java.sql.Types.VARCHAR);
} else {pstmtInsert_tDBOutput_1.setString(4, row15.modele);
}

                        if(row15.date_premiere_mise_circulation != null) {
pstmtInsert_tDBOutput_1.setTimestamp(5, new java.sql.Timestamp(row15.date_premiere_mise_circulation.getTime()));
} else {
pstmtInsert_tDBOutput_1.setNull(5, java.sql.Types.TIMESTAMP);
}

                        if(row15.immatriculation == null) {
pstmtInsert_tDBOutput_1.setNull(6, java.sql.Types.VARCHAR);
} else {pstmtInsert_tDBOutput_1.setString(6, row15.immatriculation);
}

                        if(row15.chassis == null) {
pstmtInsert_tDBOutput_1.setNull(7, java.sql.Types.VARCHAR);
} else {pstmtInsert_tDBOutput_1.setString(7, row15.chassis);
}

                        if(row15.usage == null) {
pstmtInsert_tDBOutput_1.setNull(8, java.sql.Types.VARCHAR);
} else {pstmtInsert_tDBOutput_1.setString(8, row15.usage);
}

                        if(row15.charge_utile == null) {
pstmtInsert_tDBOutput_1.setNull(9, java.sql.Types.VARCHAR);
} else {pstmtInsert_tDBOutput_1.setString(9, row15.charge_utile);
}

                        pstmtInsert_tDBOutput_1.setInt(10, row15.puissance_fiscale);

                        if(row15.remorque == null) {
pstmtInsert_tDBOutput_1.setNull(11, java.sql.Types.VARCHAR);
} else {pstmtInsert_tDBOutput_1.setString(11, row15.remorque);
}

                        if(row15.nombre_portes == null) {
pstmtInsert_tDBOutput_1.setNull(12, java.sql.Types.INTEGER);
} else {pstmtInsert_tDBOutput_1.setInt(12, row15.nombre_portes);
}

                        if(row15.immatriculation_remorque == null) {
pstmtInsert_tDBOutput_1.setNull(13, java.sql.Types.VARCHAR);
} else {pstmtInsert_tDBOutput_1.setString(13, row15.immatriculation_remorque);
}

                        if(row15.source_energie == null) {
pstmtInsert_tDBOutput_1.setNull(14, java.sql.Types.VARCHAR);
} else {pstmtInsert_tDBOutput_1.setString(14, row15.source_energie);
}

                        if(row15.nombre_de_places == null) {
pstmtInsert_tDBOutput_1.setNull(15, java.sql.Types.INTEGER);
} else {pstmtInsert_tDBOutput_1.setInt(15, row15.nombre_de_places);
}

                        if(row15.cylindree == null) {
pstmtInsert_tDBOutput_1.setNull(16, java.sql.Types.INTEGER);
} else {pstmtInsert_tDBOutput_1.setInt(16, row15.cylindree);
}

                        if(row15.double_commande == null) {
pstmtInsert_tDBOutput_1.setNull(17, java.sql.Types.INTEGER);
} else {pstmtInsert_tDBOutput_1.setInt(17, row15.double_commande);
}

                        if(row15.responsabilite_civile == null) {
pstmtInsert_tDBOutput_1.setNull(18, java.sql.Types.INTEGER);
} else {pstmtInsert_tDBOutput_1.setInt(18, row15.responsabilite_civile);
}

                        if(row15.utilitaire == null) {
pstmtInsert_tDBOutput_1.setNull(19, java.sql.Types.INTEGER);
} else {pstmtInsert_tDBOutput_1.setInt(19, row15.utilitaire);
}

                        if(row15.type_engin == null) {
pstmtInsert_tDBOutput_1.setNull(20, java.sql.Types.INTEGER);
} else {pstmtInsert_tDBOutput_1.setInt(20, row15.type_engin);
}

                        if(row15.poids_total_autorise_en_charge == null) {
pstmtInsert_tDBOutput_1.setNull(21, java.sql.Types.FLOAT);
} else {pstmtInsert_tDBOutput_1.setFloat(21, row15.poids_total_autorise_en_charge);
}

                        pstmtInsert_tDBOutput_1.setInt(22, row15.c_status);

                        if(row15.c_date_mis_a_jour != null) {
pstmtInsert_tDBOutput_1.setTimestamp(23, new java.sql.Timestamp(row15.c_date_mis_a_jour.getTime()));
} else {
pstmtInsert_tDBOutput_1.setNull(23, java.sql.Types.TIMESTAMP);
}

                        if(row15.c_date_transfer != null) {
pstmtInsert_tDBOutput_1.setTimestamp(24, new java.sql.Timestamp(row15.c_date_transfer.getTime()));
} else {
pstmtInsert_tDBOutput_1.setNull(24, java.sql.Types.TIMESTAMP);
}

                        if(row15.commentaires == null) {
pstmtInsert_tDBOutput_1.setNull(25, java.sql.Types.VARCHAR);
} else {pstmtInsert_tDBOutput_1.setString(25, row15.commentaires);
}

                try {
					
                    int processedCount_tDBOutput_1 = pstmtInsert_tDBOutput_1.executeUpdate();
                    insertedCount_tDBOutput_1 += processedCount_tDBOutput_1;
                    rowsToCommitCount_tDBOutput_1 += processedCount_tDBOutput_1;
                    nb_line_tDBOutput_1++;
					
                } catch(java.lang.Exception e) {
globalMap.put("tDBOutput_1_ERROR_MESSAGE",e.getMessage());
					
                    whetherReject_tDBOutput_1 = true;
                        nb_line_tDBOutput_1++;
                            System.err.print(e.getMessage());
                }
            }
            if(!whetherReject_tDBOutput_1) {
            }
    		    commitCounter_tDBOutput_1++;
                if(commitEvery_tDBOutput_1 <= commitCounter_tDBOutput_1) {
                    if(rowsToCommitCount_tDBOutput_1 != 0){
                    	
                    }
                    conn_tDBOutput_1.commit();
                    if(rowsToCommitCount_tDBOutput_1 != 0){
                    	
                    	rowsToCommitCount_tDBOutput_1 = 0;
                    }
                    commitCounter_tDBOutput_1=0;
                }

 


	tos_count_tDBOutput_1++;

/**
 * [tDBOutput_1 main ] stop
 */
	
	/**
	 * [tDBOutput_1 process_data_begin ] start
	 */

	

	
	
	currentComponent="tDBOutput_1";

	

 



/**
 * [tDBOutput_1 process_data_begin ] stop
 */
	
	/**
	 * [tDBOutput_1 process_data_end ] start
	 */

	

	
	
	currentComponent="tDBOutput_1";

	

 



/**
 * [tDBOutput_1 process_data_end ] stop
 */



	
	/**
	 * [tLogRow_4 process_data_end ] start
	 */

	

	
	
	currentComponent="tLogRow_4";

	

 



/**
 * [tLogRow_4 process_data_end ] stop
 */

} // End of branch "vehicule_valide"




// Start of branch "vehicule_rejet"
if(vehicule_rejet != null) { 



	
	/**
	 * [tLogRow_5 main ] start
	 */

	

	
	
	currentComponent="tLogRow_5";

	
					if(execStat){
						runStat.updateStatOnConnection(iterateId,1,1
						
							,"vehicule_rejet"
						
						);
					}
					
///////////////////////		
						

				
				String[] row_tLogRow_5 = new String[24];
   				
	    		if(vehicule_rejet.code_assure != null) { //              
                 row_tLogRow_5[0]=    						    
				                String.valueOf(vehicule_rejet.code_assure)			
					          ;	
							
	    		} //			
    			   				
	    		if(vehicule_rejet.code_assureur != null) { //              
                 row_tLogRow_5[1]=    						    
				                String.valueOf(vehicule_rejet.code_assureur)			
					          ;	
							
	    		} //			
    			   				
	    		if(vehicule_rejet.marque != null) { //              
                 row_tLogRow_5[2]=    						    
				                String.valueOf(vehicule_rejet.marque)			
					          ;	
							
	    		} //			
    			   				
	    		if(vehicule_rejet.modele != null) { //              
                 row_tLogRow_5[3]=    						    
				                String.valueOf(vehicule_rejet.modele)			
					          ;	
							
	    		} //			
    			   				
	    		if(vehicule_rejet.date_premiere_mise_circulation != null) { //              
                 row_tLogRow_5[4]=    						
								FormatterUtils.format_Date(vehicule_rejet.date_premiere_mise_circulation, "dd/MM/yyyy")
					          ;	
							
	    		} //			
    			   				
	    		if(vehicule_rejet.immatriculation != null) { //              
                 row_tLogRow_5[5]=    						    
				                String.valueOf(vehicule_rejet.immatriculation)			
					          ;	
							
	    		} //			
    			   				
	    		if(vehicule_rejet.chassis != null) { //              
                 row_tLogRow_5[6]=    						    
				                String.valueOf(vehicule_rejet.chassis)			
					          ;	
							
	    		} //			
    			   				
	    		if(vehicule_rejet.usage != null) { //              
                 row_tLogRow_5[7]=    						    
				                String.valueOf(vehicule_rejet.usage)			
					          ;	
							
	    		} //			
    			   				
	    		if(vehicule_rejet.charge_utile != null) { //              
                 row_tLogRow_5[8]=    						    
				                String.valueOf(vehicule_rejet.charge_utile)			
					          ;	
							
	    		} //			
    			              
                 row_tLogRow_5[9]=    						    
				                String.valueOf(vehicule_rejet.puissance_fiscale)			
					          ;	
										
    			   				
	    		if(vehicule_rejet.remorque != null) { //              
                 row_tLogRow_5[10]=    						    
				                String.valueOf(vehicule_rejet.remorque)			
					          ;	
							
	    		} //			
    			   				
	    		if(vehicule_rejet.nombre_portes != null) { //              
                 row_tLogRow_5[11]=    						    
				                String.valueOf(vehicule_rejet.nombre_portes)			
					          ;	
							
	    		} //			
    			   				
	    		if(vehicule_rejet.immatriculation_remorque != null) { //              
                 row_tLogRow_5[12]=    						    
				                String.valueOf(vehicule_rejet.immatriculation_remorque)			
					          ;	
							
	    		} //			
    			   				
	    		if(vehicule_rejet.source_energie != null) { //              
                 row_tLogRow_5[13]=    						    
				                String.valueOf(vehicule_rejet.source_energie)			
					          ;	
							
	    		} //			
    			   				
	    		if(vehicule_rejet.nombre_de_places != null) { //              
                 row_tLogRow_5[14]=    						    
				                String.valueOf(vehicule_rejet.nombre_de_places)			
					          ;	
							
	    		} //			
    			   				
	    		if(vehicule_rejet.cylindree != null) { //              
                 row_tLogRow_5[15]=    						    
				                String.valueOf(vehicule_rejet.cylindree)			
					          ;	
							
	    		} //			
    			   				
	    		if(vehicule_rejet.double_commande != null) { //              
                 row_tLogRow_5[16]=    						    
				                String.valueOf(vehicule_rejet.double_commande)			
					          ;	
							
	    		} //			
    			   				
	    		if(vehicule_rejet.responsabilite_civile != null) { //              
                 row_tLogRow_5[17]=    						    
				                String.valueOf(vehicule_rejet.responsabilite_civile)			
					          ;	
							
	    		} //			
    			   				
	    		if(vehicule_rejet.utilitaire != null) { //              
                 row_tLogRow_5[18]=    						    
				                String.valueOf(vehicule_rejet.utilitaire)			
					          ;	
							
	    		} //			
    			   				
	    		if(vehicule_rejet.type_engin != null) { //              
                 row_tLogRow_5[19]=    						    
				                String.valueOf(vehicule_rejet.type_engin)			
					          ;	
							
	    		} //			
    			   				
	    		if(vehicule_rejet.poids_total_autorise_en_charge != null) { //              
                 row_tLogRow_5[20]=    						
								FormatterUtils.formatUnwithE(vehicule_rejet.poids_total_autorise_en_charge)
					          ;	
							
	    		} //			
    			   				
	    		if(vehicule_rejet.date_extraction != null) { //              
                 row_tLogRow_5[21]=    						
								FormatterUtils.format_Date(vehicule_rejet.date_extraction, "dd/MM/yyyy")
					          ;	
							
	    		} //			
    			   				
	    		if(vehicule_rejet.date_depot != null) { //              
                 row_tLogRow_5[22]=    						
								FormatterUtils.format_Date(vehicule_rejet.date_depot, "dd/MM/yyyy")
					          ;	
							
	    		} //			
    			   				
	    		if(vehicule_rejet.Error_Message != null) { //              
                 row_tLogRow_5[23]=    						    
				                String.valueOf(vehicule_rejet.Error_Message)			
					          ;	
							
	    		} //			
    			 

				util_tLogRow_5.addRow(row_tLogRow_5);	
				nb_line_tLogRow_5++;
//////

//////                    
                    
///////////////////////    			

 
     row16 = vehicule_rejet;


	tos_count_tLogRow_5++;

/**
 * [tLogRow_5 main ] stop
 */
	
	/**
	 * [tLogRow_5 process_data_begin ] start
	 */

	

	
	
	currentComponent="tLogRow_5";

	

 



/**
 * [tLogRow_5 process_data_begin ] stop
 */

	
	/**
	 * [tDBOutput_2 main ] start
	 */

	

	
	
	currentComponent="tDBOutput_2";

	
					if(execStat){
						runStat.updateStatOnConnection(iterateId,1,1
						
							,"row16"
						
						);
					}
					



            row3 = null;
        whetherReject_tDBOutput_2 = false;
                    if(row16.code_assure == null) {
pstmt_tDBOutput_2.setNull(1, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_2.setString(1, row16.code_assure);
}

                    if(row16.code_assureur == null) {
pstmt_tDBOutput_2.setNull(2, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_2.setString(2, row16.code_assureur);
}

                    if(row16.marque == null) {
pstmt_tDBOutput_2.setNull(3, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_2.setString(3, row16.marque);
}

                    if(row16.modele == null) {
pstmt_tDBOutput_2.setNull(4, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_2.setString(4, row16.modele);
}

                    if(row16.date_premiere_mise_circulation != null) {
pstmt_tDBOutput_2.setTimestamp(5, new java.sql.Timestamp(row16.date_premiere_mise_circulation.getTime()));
} else {
pstmt_tDBOutput_2.setNull(5, java.sql.Types.TIMESTAMP);
}

                    if(row16.immatriculation == null) {
pstmt_tDBOutput_2.setNull(6, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_2.setString(6, row16.immatriculation);
}

                    if(row16.chassis == null) {
pstmt_tDBOutput_2.setNull(7, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_2.setString(7, row16.chassis);
}

                    if(row16.usage == null) {
pstmt_tDBOutput_2.setNull(8, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_2.setString(8, row16.usage);
}

                    if(row16.charge_utile == null) {
pstmt_tDBOutput_2.setNull(9, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_2.setString(9, row16.charge_utile);
}

                    pstmt_tDBOutput_2.setInt(10, row16.puissance_fiscale);

                    if(row16.remorque == null) {
pstmt_tDBOutput_2.setNull(11, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_2.setString(11, row16.remorque);
}

                    if(row16.nombre_portes == null) {
pstmt_tDBOutput_2.setNull(12, java.sql.Types.INTEGER);
} else {pstmt_tDBOutput_2.setInt(12, row16.nombre_portes);
}

                    if(row16.immatriculation_remorque == null) {
pstmt_tDBOutput_2.setNull(13, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_2.setString(13, row16.immatriculation_remorque);
}

                    if(row16.source_energie == null) {
pstmt_tDBOutput_2.setNull(14, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_2.setString(14, row16.source_energie);
}

                    if(row16.nombre_de_places == null) {
pstmt_tDBOutput_2.setNull(15, java.sql.Types.INTEGER);
} else {pstmt_tDBOutput_2.setInt(15, row16.nombre_de_places);
}

                    if(row16.cylindree == null) {
pstmt_tDBOutput_2.setNull(16, java.sql.Types.INTEGER);
} else {pstmt_tDBOutput_2.setInt(16, row16.cylindree);
}

                    if(row16.double_commande == null) {
pstmt_tDBOutput_2.setNull(17, java.sql.Types.INTEGER);
} else {pstmt_tDBOutput_2.setInt(17, row16.double_commande);
}

                    if(row16.responsabilite_civile == null) {
pstmt_tDBOutput_2.setNull(18, java.sql.Types.INTEGER);
} else {pstmt_tDBOutput_2.setInt(18, row16.responsabilite_civile);
}

                    if(row16.utilitaire == null) {
pstmt_tDBOutput_2.setNull(19, java.sql.Types.INTEGER);
} else {pstmt_tDBOutput_2.setInt(19, row16.utilitaire);
}

                    if(row16.type_engin == null) {
pstmt_tDBOutput_2.setNull(20, java.sql.Types.INTEGER);
} else {pstmt_tDBOutput_2.setInt(20, row16.type_engin);
}

                    if(row16.poids_total_autorise_en_charge == null) {
pstmt_tDBOutput_2.setNull(21, java.sql.Types.FLOAT);
} else {pstmt_tDBOutput_2.setFloat(21, row16.poids_total_autorise_en_charge);
}

                    if(row16.date_extraction != null) {
pstmt_tDBOutput_2.setTimestamp(22, new java.sql.Timestamp(row16.date_extraction.getTime()));
} else {
pstmt_tDBOutput_2.setNull(22, java.sql.Types.TIMESTAMP);
}

                    if(row16.date_depot != null) {
pstmt_tDBOutput_2.setTimestamp(23, new java.sql.Timestamp(row16.date_depot.getTime()));
} else {
pstmt_tDBOutput_2.setNull(23, java.sql.Types.TIMESTAMP);
}

                    if(row16.Error_Message == null) {
pstmt_tDBOutput_2.setNull(24, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_2.setString(24, row16.Error_Message);
}

			
    		pstmt_tDBOutput_2.addBatch();
    		nb_line_tDBOutput_2++;
    		  
    		  
    		  batchSizeCounter_tDBOutput_2++;
    		  
            if(!whetherReject_tDBOutput_2) {
                            row3 = new row3Struct();
                                row3.code_assure = row16.code_assure;
                                row3.code_assureur = row16.code_assureur;
                                row3.marque = row16.marque;
                                row3.modele = row16.modele;
                                row3.date_premiere_mise_circulation = row16.date_premiere_mise_circulation;
                                row3.immatriculation = row16.immatriculation;
                                row3.chassis = row16.chassis;
                                row3.usage = row16.usage;
                                row3.charge_utile = row16.charge_utile;
                                row3.puissance_fiscale = row16.puissance_fiscale;
                                row3.remorque = row16.remorque;
                                row3.nombre_portes = row16.nombre_portes;
                                row3.immatriculation_remorque = row16.immatriculation_remorque;
                                row3.source_energie = row16.source_energie;
                                row3.nombre_de_places = row16.nombre_de_places;
                                row3.cylindree = row16.cylindree;
                                row3.double_commande = row16.double_commande;
                                row3.responsabilite_civile = row16.responsabilite_civile;
                                row3.utilitaire = row16.utilitaire;
                                row3.type_engin = row16.type_engin;
                                row3.poids_total_autorise_en_charge = row16.poids_total_autorise_en_charge;
                                row3.date_extraction = row16.date_extraction;
                                row3.date_depot = row16.date_depot;
                                row3.Error_Message = row16.Error_Message;
            }
    			if ((batchSize_tDBOutput_2 > 0) && (batchSize_tDBOutput_2 <= batchSizeCounter_tDBOutput_2)) {
                try {
						int countSum_tDBOutput_2 = 0;
						    
						for(int countEach_tDBOutput_2: pstmt_tDBOutput_2.executeBatch()) {
							countSum_tDBOutput_2 += (countEach_tDBOutput_2 < 0 ? 0 : countEach_tDBOutput_2);
						}
				    	rowsToCommitCount_tDBOutput_2 += countSum_tDBOutput_2;
				    	
				    		insertedCount_tDBOutput_2 += countSum_tDBOutput_2;
				    	
            	    	batchSizeCounter_tDBOutput_2 = 0;
                }catch (java.sql.BatchUpdateException e_tDBOutput_2){
globalMap.put("tDBOutput_2_ERROR_MESSAGE",e_tDBOutput_2.getMessage());
				    	java.sql.SQLException ne_tDBOutput_2 = e_tDBOutput_2.getNextException(),sqle_tDBOutput_2=null;
				    	String errormessage_tDBOutput_2;
						if (ne_tDBOutput_2 != null) {
							// build new exception to provide the original cause
							sqle_tDBOutput_2 = new java.sql.SQLException(e_tDBOutput_2.getMessage() + "\ncaused by: " + ne_tDBOutput_2.getMessage(), ne_tDBOutput_2.getSQLState(), ne_tDBOutput_2.getErrorCode(), ne_tDBOutput_2);
							errormessage_tDBOutput_2 = sqle_tDBOutput_2.getMessage();
						}else{
							errormessage_tDBOutput_2 = e_tDBOutput_2.getMessage();
						}
				    	
				    	int countSum_tDBOutput_2 = 0;
						for(int countEach_tDBOutput_2: e_tDBOutput_2.getUpdateCounts()) {
							countSum_tDBOutput_2 += (countEach_tDBOutput_2 < 0 ? 0 : countEach_tDBOutput_2);
						}
						rowsToCommitCount_tDBOutput_2 += countSum_tDBOutput_2;
						
				    		insertedCount_tDBOutput_2 += countSum_tDBOutput_2;
				    	
				    	System.err.println(errormessage_tDBOutput_2);
				    	
					}
    			}
    		
    		    commitCounter_tDBOutput_2++;
                if(commitEvery_tDBOutput_2 <= commitCounter_tDBOutput_2) {
                if ((batchSize_tDBOutput_2 > 0) && (batchSizeCounter_tDBOutput_2 > 0)) {
                try {
                		int countSum_tDBOutput_2 = 0;
                		    
						for(int countEach_tDBOutput_2: pstmt_tDBOutput_2.executeBatch()) {
							countSum_tDBOutput_2 += (countEach_tDBOutput_2 < 0 ? 0 : countEach_tDBOutput_2);
						}
            	    	rowsToCommitCount_tDBOutput_2 += countSum_tDBOutput_2;
            	    	
            	    		insertedCount_tDBOutput_2 += countSum_tDBOutput_2;
            	    	
                batchSizeCounter_tDBOutput_2 = 0;
               }catch (java.sql.BatchUpdateException e_tDBOutput_2){
globalMap.put("tDBOutput_2_ERROR_MESSAGE",e_tDBOutput_2.getMessage());
			    	java.sql.SQLException ne_tDBOutput_2 = e_tDBOutput_2.getNextException(),sqle_tDBOutput_2=null;
			    	String errormessage_tDBOutput_2;
					if (ne_tDBOutput_2 != null) {
						// build new exception to provide the original cause
						sqle_tDBOutput_2 = new java.sql.SQLException(e_tDBOutput_2.getMessage() + "\ncaused by: " + ne_tDBOutput_2.getMessage(), ne_tDBOutput_2.getSQLState(), ne_tDBOutput_2.getErrorCode(), ne_tDBOutput_2);
						errormessage_tDBOutput_2 = sqle_tDBOutput_2.getMessage();
					}else{
						errormessage_tDBOutput_2 = e_tDBOutput_2.getMessage();
					}
			    	
			    	int countSum_tDBOutput_2 = 0;
					for(int countEach_tDBOutput_2: e_tDBOutput_2.getUpdateCounts()) {
						countSum_tDBOutput_2 += (countEach_tDBOutput_2 < 0 ? 0 : countEach_tDBOutput_2);
					}
					rowsToCommitCount_tDBOutput_2 += countSum_tDBOutput_2;
					
			    		insertedCount_tDBOutput_2 += countSum_tDBOutput_2;
			    	
			    	System.err.println(errormessage_tDBOutput_2);
			    	
				}
            }
                    if(rowsToCommitCount_tDBOutput_2 != 0){
                    	
                    }
                    conn_tDBOutput_2.commit();
                    if(rowsToCommitCount_tDBOutput_2 != 0){
                    	
                    	rowsToCommitCount_tDBOutput_2 = 0;
                    }
                    commitCounter_tDBOutput_2=0;
                }

 


	tos_count_tDBOutput_2++;

/**
 * [tDBOutput_2 main ] stop
 */
	
	/**
	 * [tDBOutput_2 process_data_begin ] start
	 */

	

	
	
	currentComponent="tDBOutput_2";

	

 



/**
 * [tDBOutput_2 process_data_begin ] stop
 */
// Start of branch "row3"
if(row3 != null) { 



	
	/**
	 * [tFileOutputDelimited_3 main ] start
	 */

	

	
	
	currentComponent="tFileOutputDelimited_3";

	
					if(execStat){
						runStat.updateStatOnConnection(iterateId,1,1
						
							,"row3"
						
						);
					}
					


                    StringBuilder sb_tFileOutputDelimited_3 = new StringBuilder();
                            if(row3.code_assure != null) {
                        sb_tFileOutputDelimited_3.append(
                            row3.code_assure
                        );
                            }
                            sb_tFileOutputDelimited_3.append(OUT_DELIM_tFileOutputDelimited_3);
                            if(row3.code_assureur != null) {
                        sb_tFileOutputDelimited_3.append(
                            row3.code_assureur
                        );
                            }
                            sb_tFileOutputDelimited_3.append(OUT_DELIM_tFileOutputDelimited_3);
                            if(row3.marque != null) {
                        sb_tFileOutputDelimited_3.append(
                            row3.marque
                        );
                            }
                            sb_tFileOutputDelimited_3.append(OUT_DELIM_tFileOutputDelimited_3);
                            if(row3.modele != null) {
                        sb_tFileOutputDelimited_3.append(
                            row3.modele
                        );
                            }
                            sb_tFileOutputDelimited_3.append(OUT_DELIM_tFileOutputDelimited_3);
                            if(row3.date_premiere_mise_circulation != null) {
                        sb_tFileOutputDelimited_3.append(
                            FormatterUtils.format_Date(row3.date_premiere_mise_circulation, "dd/MM/yyyy")
                        );
                            }
                            sb_tFileOutputDelimited_3.append(OUT_DELIM_tFileOutputDelimited_3);
                            if(row3.immatriculation != null) {
                        sb_tFileOutputDelimited_3.append(
                            row3.immatriculation
                        );
                            }
                            sb_tFileOutputDelimited_3.append(OUT_DELIM_tFileOutputDelimited_3);
                            if(row3.chassis != null) {
                        sb_tFileOutputDelimited_3.append(
                            row3.chassis
                        );
                            }
                            sb_tFileOutputDelimited_3.append(OUT_DELIM_tFileOutputDelimited_3);
                            if(row3.usage != null) {
                        sb_tFileOutputDelimited_3.append(
                            row3.usage
                        );
                            }
                            sb_tFileOutputDelimited_3.append(OUT_DELIM_tFileOutputDelimited_3);
                            if(row3.charge_utile != null) {
                        sb_tFileOutputDelimited_3.append(
                            row3.charge_utile
                        );
                            }
                            sb_tFileOutputDelimited_3.append(OUT_DELIM_tFileOutputDelimited_3);
                        sb_tFileOutputDelimited_3.append(
                            row3.puissance_fiscale
                        );
                            sb_tFileOutputDelimited_3.append(OUT_DELIM_tFileOutputDelimited_3);
                            if(row3.remorque != null) {
                        sb_tFileOutputDelimited_3.append(
                            row3.remorque
                        );
                            }
                            sb_tFileOutputDelimited_3.append(OUT_DELIM_tFileOutputDelimited_3);
                            if(row3.nombre_portes != null) {
                        sb_tFileOutputDelimited_3.append(
                            row3.nombre_portes
                        );
                            }
                            sb_tFileOutputDelimited_3.append(OUT_DELIM_tFileOutputDelimited_3);
                            if(row3.immatriculation_remorque != null) {
                        sb_tFileOutputDelimited_3.append(
                            row3.immatriculation_remorque
                        );
                            }
                            sb_tFileOutputDelimited_3.append(OUT_DELIM_tFileOutputDelimited_3);
                            if(row3.source_energie != null) {
                        sb_tFileOutputDelimited_3.append(
                            row3.source_energie
                        );
                            }
                            sb_tFileOutputDelimited_3.append(OUT_DELIM_tFileOutputDelimited_3);
                            if(row3.nombre_de_places != null) {
                        sb_tFileOutputDelimited_3.append(
                            row3.nombre_de_places
                        );
                            }
                            sb_tFileOutputDelimited_3.append(OUT_DELIM_tFileOutputDelimited_3);
                            if(row3.cylindree != null) {
                        sb_tFileOutputDelimited_3.append(
                            row3.cylindree
                        );
                            }
                            sb_tFileOutputDelimited_3.append(OUT_DELIM_tFileOutputDelimited_3);
                            if(row3.double_commande != null) {
                        sb_tFileOutputDelimited_3.append(
                            row3.double_commande
                        );
                            }
                            sb_tFileOutputDelimited_3.append(OUT_DELIM_tFileOutputDelimited_3);
                            if(row3.responsabilite_civile != null) {
                        sb_tFileOutputDelimited_3.append(
                            row3.responsabilite_civile
                        );
                            }
                            sb_tFileOutputDelimited_3.append(OUT_DELIM_tFileOutputDelimited_3);
                            if(row3.utilitaire != null) {
                        sb_tFileOutputDelimited_3.append(
                            row3.utilitaire
                        );
                            }
                            sb_tFileOutputDelimited_3.append(OUT_DELIM_tFileOutputDelimited_3);
                            if(row3.type_engin != null) {
                        sb_tFileOutputDelimited_3.append(
                            row3.type_engin
                        );
                            }
                            sb_tFileOutputDelimited_3.append(OUT_DELIM_tFileOutputDelimited_3);
                            if(row3.poids_total_autorise_en_charge != null) {
                        sb_tFileOutputDelimited_3.append(
                            row3.poids_total_autorise_en_charge
                        );
                            }
                            sb_tFileOutputDelimited_3.append(OUT_DELIM_tFileOutputDelimited_3);
                            if(row3.date_extraction != null) {
                        sb_tFileOutputDelimited_3.append(
                            FormatterUtils.format_Date(row3.date_extraction, "dd/MM/yyyy")
                        );
                            }
                            sb_tFileOutputDelimited_3.append(OUT_DELIM_tFileOutputDelimited_3);
                            if(row3.date_depot != null) {
                        sb_tFileOutputDelimited_3.append(
                            FormatterUtils.format_Date(row3.date_depot, "dd/MM/yyyy")
                        );
                            }
                            sb_tFileOutputDelimited_3.append(OUT_DELIM_tFileOutputDelimited_3);
                            if(row3.Error_Message != null) {
                        sb_tFileOutputDelimited_3.append(
                            row3.Error_Message
                        );
                            }
                    sb_tFileOutputDelimited_3.append(OUT_DELIM_ROWSEP_tFileOutputDelimited_3);


                    nb_line_tFileOutputDelimited_3++;
                    resourceMap.put("nb_line_tFileOutputDelimited_3", nb_line_tFileOutputDelimited_3);

                        outtFileOutputDelimited_3.write(sb_tFileOutputDelimited_3.toString());




 


	tos_count_tFileOutputDelimited_3++;

/**
 * [tFileOutputDelimited_3 main ] stop
 */
	
	/**
	 * [tFileOutputDelimited_3 process_data_begin ] start
	 */

	

	
	
	currentComponent="tFileOutputDelimited_3";

	

 



/**
 * [tFileOutputDelimited_3 process_data_begin ] stop
 */
	
	/**
	 * [tFileOutputDelimited_3 process_data_end ] start
	 */

	

	
	
	currentComponent="tFileOutputDelimited_3";

	

 



/**
 * [tFileOutputDelimited_3 process_data_end ] stop
 */

} // End of branch "row3"




	
	/**
	 * [tDBOutput_2 process_data_end ] start
	 */

	

	
	
	currentComponent="tDBOutput_2";

	

 



/**
 * [tDBOutput_2 process_data_end ] stop
 */



	
	/**
	 * [tLogRow_5 process_data_end ] start
	 */

	

	
	
	currentComponent="tLogRow_5";

	

 



/**
 * [tLogRow_5 process_data_end ] stop
 */

} // End of branch "vehicule_rejet"




	
	/**
	 * [tMap_1 process_data_end ] start
	 */

	

	
	
	currentComponent="tMap_1";

	

 



/**
 * [tMap_1 process_data_end ] stop
 */



	
	/**
	 * [tLogRow_2 process_data_end ] start
	 */

	

	
	
	currentComponent="tLogRow_2";

	

 



/**
 * [tLogRow_2 process_data_end ] stop
 */

} // End of branch "row12"




// Start of branch "row13"
if(row13 != null) { 



	
	/**
	 * [tLogRow_3 main ] start
	 */

	

	
	
	currentComponent="tLogRow_3";

	
					if(execStat){
						runStat.updateStatOnConnection(iterateId,1,1
						
							,"row13"
						
						);
					}
					
///////////////////////		
						

				
				String[] row_tLogRow_3 = new String[23];
   				
	    		if(row13.code_assure != null) { //              
                 row_tLogRow_3[0]=    						    
				                String.valueOf(row13.code_assure)			
					          ;	
							
	    		} //			
    			   				
	    		if(row13.code_assureur != null) { //              
                 row_tLogRow_3[1]=    						    
				                String.valueOf(row13.code_assureur)			
					          ;	
							
	    		} //			
    			   				
	    		if(row13.marque != null) { //              
                 row_tLogRow_3[2]=    						    
				                String.valueOf(row13.marque)			
					          ;	
							
	    		} //			
    			   				
	    		if(row13.modele != null) { //              
                 row_tLogRow_3[3]=    						    
				                String.valueOf(row13.modele)			
					          ;	
							
	    		} //			
    			   				
	    		if(row13.date_premiere_mise_circulation != null) { //              
                 row_tLogRow_3[4]=    						
								FormatterUtils.format_Date(row13.date_premiere_mise_circulation, "dd/MM/yyyy")
					          ;	
							
	    		} //			
    			   				
	    		if(row13.immatriculation != null) { //              
                 row_tLogRow_3[5]=    						    
				                String.valueOf(row13.immatriculation)			
					          ;	
							
	    		} //			
    			   				
	    		if(row13.chassis != null) { //              
                 row_tLogRow_3[6]=    						    
				                String.valueOf(row13.chassis)			
					          ;	
							
	    		} //			
    			   				
	    		if(row13.usage != null) { //              
                 row_tLogRow_3[7]=    						    
				                String.valueOf(row13.usage)			
					          ;	
							
	    		} //			
    			   				
	    		if(row13.charge_utile != null) { //              
                 row_tLogRow_3[8]=    						    
				                String.valueOf(row13.charge_utile)			
					          ;	
							
	    		} //			
    			              
                 row_tLogRow_3[9]=    						    
				                String.valueOf(row13.puissance_fiscale)			
					          ;	
										
    			   				
	    		if(row13.remorque != null) { //              
                 row_tLogRow_3[10]=    						    
				                String.valueOf(row13.remorque)			
					          ;	
							
	    		} //			
    			   				
	    		if(row13.nombre_portes != null) { //              
                 row_tLogRow_3[11]=    						    
				                String.valueOf(row13.nombre_portes)			
					          ;	
							
	    		} //			
    			   				
	    		if(row13.immatriculation_remorque != null) { //              
                 row_tLogRow_3[12]=    						    
				                String.valueOf(row13.immatriculation_remorque)			
					          ;	
							
	    		} //			
    			   				
	    		if(row13.source_energie != null) { //              
                 row_tLogRow_3[13]=    						    
				                String.valueOf(row13.source_energie)			
					          ;	
							
	    		} //			
    			   				
	    		if(row13.nombre_de_places != null) { //              
                 row_tLogRow_3[14]=    						    
				                String.valueOf(row13.nombre_de_places)			
					          ;	
							
	    		} //			
    			   				
	    		if(row13.cylindree != null) { //              
                 row_tLogRow_3[15]=    						    
				                String.valueOf(row13.cylindree)			
					          ;	
							
	    		} //			
    			   				
	    		if(row13.double_commande != null) { //              
                 row_tLogRow_3[16]=    						    
				                String.valueOf(row13.double_commande)			
					          ;	
							
	    		} //			
    			   				
	    		if(row13.responsabilite_civile != null) { //              
                 row_tLogRow_3[17]=    						    
				                String.valueOf(row13.responsabilite_civile)			
					          ;	
							
	    		} //			
    			   				
	    		if(row13.utilitaire != null) { //              
                 row_tLogRow_3[18]=    						    
				                String.valueOf(row13.utilitaire)			
					          ;	
							
	    		} //			
    			   				
	    		if(row13.type_engin != null) { //              
                 row_tLogRow_3[19]=    						    
				                String.valueOf(row13.type_engin)			
					          ;	
							
	    		} //			
    			   				
	    		if(row13.poids_total_autorise_en_charge != null) { //              
                 row_tLogRow_3[20]=    						
								FormatterUtils.formatUnwithE(row13.poids_total_autorise_en_charge)
					          ;	
							
	    		} //			
    			   				
	    		if(row13.errorCode != null) { //              
                 row_tLogRow_3[21]=    						    
				                String.valueOf(row13.errorCode)			
					          ;	
							
	    		} //			
    			   				
	    		if(row13.errorMessage != null) { //              
                 row_tLogRow_3[22]=    						    
				                String.valueOf(row13.errorMessage)			
					          ;	
							
	    		} //			
    			 

				util_tLogRow_3.addRow(row_tLogRow_3);	
				nb_line_tLogRow_3++;
//////

//////                    
                    
///////////////////////    			

 
     row17 = row13;


	tos_count_tLogRow_3++;

/**
 * [tLogRow_3 main ] stop
 */
	
	/**
	 * [tLogRow_3 process_data_begin ] start
	 */

	

	
	
	currentComponent="tLogRow_3";

	

 



/**
 * [tLogRow_3 process_data_begin ] stop
 */

	
	/**
	 * [tMap_3 main ] start
	 */

	

	
	
	currentComponent="tMap_3";

	
					if(execStat){
						runStat.updateStatOnConnection(iterateId,1,1
						
							,"row17"
						
						);
					}
					

		
		
		boolean hasCasePrimitiveKeyWithNull_tMap_3 = false;
		

        // ###############################
        // # Input tables (lookups)
		  boolean rejectedInnerJoin_tMap_3 = false;
		  boolean mainRowRejected_tMap_3 = false;
            				    								  
		// ###############################
        { // start of Var scope
        
	        // ###############################
        	// # Vars tables
        
Var__tMap_3__Struct Var = Var__tMap_3;// ###############################
        // ###############################
        // # Output tables

vehicule_rejet_Schema = null;


// # Output table : 'vehicule_rejet_Schema'
vehicule_rejet_Schema_tmp.code_assure = row17.code_assure ;
vehicule_rejet_Schema_tmp.code_assureur = row17.code_assureur ;
vehicule_rejet_Schema_tmp.marque = row17.marque ;
vehicule_rejet_Schema_tmp.modele = row17.modele ;
vehicule_rejet_Schema_tmp.date_premiere_mise_circulation = row17.date_premiere_mise_circulation ;
vehicule_rejet_Schema_tmp.immatriculation = row17.immatriculation ;
vehicule_rejet_Schema_tmp.chassis = row17.chassis ;
vehicule_rejet_Schema_tmp.usage = row17.usage ;
vehicule_rejet_Schema_tmp.charge_utile = row17.charge_utile ;
vehicule_rejet_Schema_tmp.puissance_fiscale = row17.puissance_fiscale ;
vehicule_rejet_Schema_tmp.remorque = row17.remorque ;
vehicule_rejet_Schema_tmp.nombre_portes = row17.nombre_portes ;
vehicule_rejet_Schema_tmp.immatriculation_remorque = row17.immatriculation_remorque ;
vehicule_rejet_Schema_tmp.source_energie = row17.source_energie ;
vehicule_rejet_Schema_tmp.nombre_de_places = row17.nombre_de_places ;
vehicule_rejet_Schema_tmp.cylindree = row17.cylindree ;
vehicule_rejet_Schema_tmp.double_commande = row17.double_commande ;
vehicule_rejet_Schema_tmp.responsabilite_civile = row17.responsabilite_civile ;
vehicule_rejet_Schema_tmp.utilitaire = row17.utilitaire ;
vehicule_rejet_Schema_tmp.type_engin = row17.type_engin ;
vehicule_rejet_Schema_tmp.poids_total_autorise_en_charge = row17.poids_total_autorise_en_charge ;
vehicule_rejet_Schema_tmp.date_extraction = TalendDate.getCurrentDate() ;
vehicule_rejet_Schema_tmp.date_depot = null;
vehicule_rejet_Schema_tmp.errorCode = row17.errorCode ;
vehicule_rejet_Schema_tmp.errorMessage = row17.errorMessage ;
vehicule_rejet_Schema = vehicule_rejet_Schema_tmp;
// ###############################

} // end of Var scope

rejectedInnerJoin_tMap_3 = false;










 


	tos_count_tMap_3++;

/**
 * [tMap_3 main ] stop
 */
	
	/**
	 * [tMap_3 process_data_begin ] start
	 */

	

	
	
	currentComponent="tMap_3";

	

 



/**
 * [tMap_3 process_data_begin ] stop
 */
// Start of branch "vehicule_rejet_Schema"
if(vehicule_rejet_Schema != null) { 



	
	/**
	 * [tDBOutput_3 main ] start
	 */

	

	
	
	currentComponent="tDBOutput_3";

	
					if(execStat){
						runStat.updateStatOnConnection(iterateId,1,1
						
							,"vehicule_rejet_Schema"
						
						);
					}
					



            row2 = null;
        whetherReject_tDBOutput_3 = false;
                    if(vehicule_rejet_Schema.code_assure == null) {
pstmt_tDBOutput_3.setNull(1, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_3.setString(1, vehicule_rejet_Schema.code_assure);
}

                    if(vehicule_rejet_Schema.code_assureur == null) {
pstmt_tDBOutput_3.setNull(2, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_3.setString(2, vehicule_rejet_Schema.code_assureur);
}

                    if(vehicule_rejet_Schema.marque == null) {
pstmt_tDBOutput_3.setNull(3, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_3.setString(3, vehicule_rejet_Schema.marque);
}

                    if(vehicule_rejet_Schema.modele == null) {
pstmt_tDBOutput_3.setNull(4, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_3.setString(4, vehicule_rejet_Schema.modele);
}

                    if(vehicule_rejet_Schema.date_premiere_mise_circulation != null) {
pstmt_tDBOutput_3.setTimestamp(5, new java.sql.Timestamp(vehicule_rejet_Schema.date_premiere_mise_circulation.getTime()));
} else {
pstmt_tDBOutput_3.setNull(5, java.sql.Types.TIMESTAMP);
}

                    if(vehicule_rejet_Schema.immatriculation == null) {
pstmt_tDBOutput_3.setNull(6, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_3.setString(6, vehicule_rejet_Schema.immatriculation);
}

                    if(vehicule_rejet_Schema.chassis == null) {
pstmt_tDBOutput_3.setNull(7, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_3.setString(7, vehicule_rejet_Schema.chassis);
}

                    if(vehicule_rejet_Schema.usage == null) {
pstmt_tDBOutput_3.setNull(8, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_3.setString(8, vehicule_rejet_Schema.usage);
}

                    if(vehicule_rejet_Schema.charge_utile == null) {
pstmt_tDBOutput_3.setNull(9, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_3.setString(9, vehicule_rejet_Schema.charge_utile);
}

                    if(vehicule_rejet_Schema.puissance_fiscale == null) {
pstmt_tDBOutput_3.setNull(10, java.sql.Types.INTEGER);
} else {pstmt_tDBOutput_3.setInt(10, vehicule_rejet_Schema.puissance_fiscale);
}

                    if(vehicule_rejet_Schema.remorque == null) {
pstmt_tDBOutput_3.setNull(11, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_3.setString(11, vehicule_rejet_Schema.remorque);
}

                    if(vehicule_rejet_Schema.nombre_portes == null) {
pstmt_tDBOutput_3.setNull(12, java.sql.Types.INTEGER);
} else {pstmt_tDBOutput_3.setInt(12, vehicule_rejet_Schema.nombre_portes);
}

                    if(vehicule_rejet_Schema.immatriculation_remorque == null) {
pstmt_tDBOutput_3.setNull(13, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_3.setString(13, vehicule_rejet_Schema.immatriculation_remorque);
}

                    if(vehicule_rejet_Schema.source_energie == null) {
pstmt_tDBOutput_3.setNull(14, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_3.setString(14, vehicule_rejet_Schema.source_energie);
}

                    if(vehicule_rejet_Schema.nombre_de_places == null) {
pstmt_tDBOutput_3.setNull(15, java.sql.Types.INTEGER);
} else {pstmt_tDBOutput_3.setInt(15, vehicule_rejet_Schema.nombre_de_places);
}

                    if(vehicule_rejet_Schema.cylindree == null) {
pstmt_tDBOutput_3.setNull(16, java.sql.Types.INTEGER);
} else {pstmt_tDBOutput_3.setInt(16, vehicule_rejet_Schema.cylindree);
}

                    if(vehicule_rejet_Schema.double_commande == null) {
pstmt_tDBOutput_3.setNull(17, java.sql.Types.INTEGER);
} else {pstmt_tDBOutput_3.setInt(17, vehicule_rejet_Schema.double_commande);
}

                    if(vehicule_rejet_Schema.responsabilite_civile == null) {
pstmt_tDBOutput_3.setNull(18, java.sql.Types.INTEGER);
} else {pstmt_tDBOutput_3.setInt(18, vehicule_rejet_Schema.responsabilite_civile);
}

                    if(vehicule_rejet_Schema.utilitaire == null) {
pstmt_tDBOutput_3.setNull(19, java.sql.Types.INTEGER);
} else {pstmt_tDBOutput_3.setInt(19, vehicule_rejet_Schema.utilitaire);
}

                    if(vehicule_rejet_Schema.type_engin == null) {
pstmt_tDBOutput_3.setNull(20, java.sql.Types.INTEGER);
} else {pstmt_tDBOutput_3.setInt(20, vehicule_rejet_Schema.type_engin);
}

                    if(vehicule_rejet_Schema.poids_total_autorise_en_charge == null) {
pstmt_tDBOutput_3.setNull(21, java.sql.Types.FLOAT);
} else {pstmt_tDBOutput_3.setFloat(21, vehicule_rejet_Schema.poids_total_autorise_en_charge);
}

                    if(vehicule_rejet_Schema.date_extraction != null) {
pstmt_tDBOutput_3.setTimestamp(22, new java.sql.Timestamp(vehicule_rejet_Schema.date_extraction.getTime()));
} else {
pstmt_tDBOutput_3.setNull(22, java.sql.Types.TIMESTAMP);
}

                    if(vehicule_rejet_Schema.date_depot != null) {
pstmt_tDBOutput_3.setTimestamp(23, new java.sql.Timestamp(vehicule_rejet_Schema.date_depot.getTime()));
} else {
pstmt_tDBOutput_3.setNull(23, java.sql.Types.TIMESTAMP);
}

                    if(vehicule_rejet_Schema.errorCode == null) {
pstmt_tDBOutput_3.setNull(24, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_3.setString(24, vehicule_rejet_Schema.errorCode);
}

                    if(vehicule_rejet_Schema.errorMessage == null) {
pstmt_tDBOutput_3.setNull(25, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_3.setString(25, vehicule_rejet_Schema.errorMessage);
}

			
    		pstmt_tDBOutput_3.addBatch();
    		nb_line_tDBOutput_3++;
    		  
    		  
    		  batchSizeCounter_tDBOutput_3++;
    		  
            if(!whetherReject_tDBOutput_3) {
                            row2 = new row2Struct();
                                row2.code_assure = vehicule_rejet_Schema.code_assure;
                                row2.code_assureur = vehicule_rejet_Schema.code_assureur;
                                row2.marque = vehicule_rejet_Schema.marque;
                                row2.modele = vehicule_rejet_Schema.modele;
                                row2.date_premiere_mise_circulation = vehicule_rejet_Schema.date_premiere_mise_circulation;
                                row2.immatriculation = vehicule_rejet_Schema.immatriculation;
                                row2.chassis = vehicule_rejet_Schema.chassis;
                                row2.usage = vehicule_rejet_Schema.usage;
                                row2.charge_utile = vehicule_rejet_Schema.charge_utile;
                                row2.puissance_fiscale = vehicule_rejet_Schema.puissance_fiscale;
                                row2.remorque = vehicule_rejet_Schema.remorque;
                                row2.nombre_portes = vehicule_rejet_Schema.nombre_portes;
                                row2.immatriculation_remorque = vehicule_rejet_Schema.immatriculation_remorque;
                                row2.source_energie = vehicule_rejet_Schema.source_energie;
                                row2.nombre_de_places = vehicule_rejet_Schema.nombre_de_places;
                                row2.cylindree = vehicule_rejet_Schema.cylindree;
                                row2.double_commande = vehicule_rejet_Schema.double_commande;
                                row2.responsabilite_civile = vehicule_rejet_Schema.responsabilite_civile;
                                row2.utilitaire = vehicule_rejet_Schema.utilitaire;
                                row2.type_engin = vehicule_rejet_Schema.type_engin;
                                row2.poids_total_autorise_en_charge = vehicule_rejet_Schema.poids_total_autorise_en_charge;
                                row2.date_extraction = vehicule_rejet_Schema.date_extraction;
                                row2.date_depot = vehicule_rejet_Schema.date_depot;
                                row2.errorCode = vehicule_rejet_Schema.errorCode;
                                row2.errorMessage = vehicule_rejet_Schema.errorMessage;
            }
    			if ((batchSize_tDBOutput_3 > 0) && (batchSize_tDBOutput_3 <= batchSizeCounter_tDBOutput_3)) {
                try {
						int countSum_tDBOutput_3 = 0;
						    
						for(int countEach_tDBOutput_3: pstmt_tDBOutput_3.executeBatch()) {
							countSum_tDBOutput_3 += (countEach_tDBOutput_3 < 0 ? 0 : countEach_tDBOutput_3);
						}
				    	rowsToCommitCount_tDBOutput_3 += countSum_tDBOutput_3;
				    	
				    		insertedCount_tDBOutput_3 += countSum_tDBOutput_3;
				    	
            	    	batchSizeCounter_tDBOutput_3 = 0;
                }catch (java.sql.BatchUpdateException e_tDBOutput_3){
globalMap.put("tDBOutput_3_ERROR_MESSAGE",e_tDBOutput_3.getMessage());
				    	java.sql.SQLException ne_tDBOutput_3 = e_tDBOutput_3.getNextException(),sqle_tDBOutput_3=null;
				    	String errormessage_tDBOutput_3;
						if (ne_tDBOutput_3 != null) {
							// build new exception to provide the original cause
							sqle_tDBOutput_3 = new java.sql.SQLException(e_tDBOutput_3.getMessage() + "\ncaused by: " + ne_tDBOutput_3.getMessage(), ne_tDBOutput_3.getSQLState(), ne_tDBOutput_3.getErrorCode(), ne_tDBOutput_3);
							errormessage_tDBOutput_3 = sqle_tDBOutput_3.getMessage();
						}else{
							errormessage_tDBOutput_3 = e_tDBOutput_3.getMessage();
						}
				    	
				    	int countSum_tDBOutput_3 = 0;
						for(int countEach_tDBOutput_3: e_tDBOutput_3.getUpdateCounts()) {
							countSum_tDBOutput_3 += (countEach_tDBOutput_3 < 0 ? 0 : countEach_tDBOutput_3);
						}
						rowsToCommitCount_tDBOutput_3 += countSum_tDBOutput_3;
						
				    		insertedCount_tDBOutput_3 += countSum_tDBOutput_3;
				    	
				    	System.err.println(errormessage_tDBOutput_3);
				    	
					}
    			}
    		
    		    commitCounter_tDBOutput_3++;
                if(commitEvery_tDBOutput_3 <= commitCounter_tDBOutput_3) {
                if ((batchSize_tDBOutput_3 > 0) && (batchSizeCounter_tDBOutput_3 > 0)) {
                try {
                		int countSum_tDBOutput_3 = 0;
                		    
						for(int countEach_tDBOutput_3: pstmt_tDBOutput_3.executeBatch()) {
							countSum_tDBOutput_3 += (countEach_tDBOutput_3 < 0 ? 0 : countEach_tDBOutput_3);
						}
            	    	rowsToCommitCount_tDBOutput_3 += countSum_tDBOutput_3;
            	    	
            	    		insertedCount_tDBOutput_3 += countSum_tDBOutput_3;
            	    	
                batchSizeCounter_tDBOutput_3 = 0;
               }catch (java.sql.BatchUpdateException e_tDBOutput_3){
globalMap.put("tDBOutput_3_ERROR_MESSAGE",e_tDBOutput_3.getMessage());
			    	java.sql.SQLException ne_tDBOutput_3 = e_tDBOutput_3.getNextException(),sqle_tDBOutput_3=null;
			    	String errormessage_tDBOutput_3;
					if (ne_tDBOutput_3 != null) {
						// build new exception to provide the original cause
						sqle_tDBOutput_3 = new java.sql.SQLException(e_tDBOutput_3.getMessage() + "\ncaused by: " + ne_tDBOutput_3.getMessage(), ne_tDBOutput_3.getSQLState(), ne_tDBOutput_3.getErrorCode(), ne_tDBOutput_3);
						errormessage_tDBOutput_3 = sqle_tDBOutput_3.getMessage();
					}else{
						errormessage_tDBOutput_3 = e_tDBOutput_3.getMessage();
					}
			    	
			    	int countSum_tDBOutput_3 = 0;
					for(int countEach_tDBOutput_3: e_tDBOutput_3.getUpdateCounts()) {
						countSum_tDBOutput_3 += (countEach_tDBOutput_3 < 0 ? 0 : countEach_tDBOutput_3);
					}
					rowsToCommitCount_tDBOutput_3 += countSum_tDBOutput_3;
					
			    		insertedCount_tDBOutput_3 += countSum_tDBOutput_3;
			    	
			    	System.err.println(errormessage_tDBOutput_3);
			    	
				}
            }
                    if(rowsToCommitCount_tDBOutput_3 != 0){
                    	
                    }
                    conn_tDBOutput_3.commit();
                    if(rowsToCommitCount_tDBOutput_3 != 0){
                    	
                    	rowsToCommitCount_tDBOutput_3 = 0;
                    }
                    commitCounter_tDBOutput_3=0;
                }

 


	tos_count_tDBOutput_3++;

/**
 * [tDBOutput_3 main ] stop
 */
	
	/**
	 * [tDBOutput_3 process_data_begin ] start
	 */

	

	
	
	currentComponent="tDBOutput_3";

	

 



/**
 * [tDBOutput_3 process_data_begin ] stop
 */
// Start of branch "row2"
if(row2 != null) { 



	
	/**
	 * [tFileOutputDelimited_2 main ] start
	 */

	

	
	
	currentComponent="tFileOutputDelimited_2";

	
					if(execStat){
						runStat.updateStatOnConnection(iterateId,1,1
						
							,"row2"
						
						);
					}
					


                    StringBuilder sb_tFileOutputDelimited_2 = new StringBuilder();
                            if(row2.code_assure != null) {
                        sb_tFileOutputDelimited_2.append(
                            row2.code_assure
                        );
                            }
                            sb_tFileOutputDelimited_2.append(OUT_DELIM_tFileOutputDelimited_2);
                            if(row2.code_assureur != null) {
                        sb_tFileOutputDelimited_2.append(
                            row2.code_assureur
                        );
                            }
                            sb_tFileOutputDelimited_2.append(OUT_DELIM_tFileOutputDelimited_2);
                            if(row2.marque != null) {
                        sb_tFileOutputDelimited_2.append(
                            row2.marque
                        );
                            }
                            sb_tFileOutputDelimited_2.append(OUT_DELIM_tFileOutputDelimited_2);
                            if(row2.modele != null) {
                        sb_tFileOutputDelimited_2.append(
                            row2.modele
                        );
                            }
                            sb_tFileOutputDelimited_2.append(OUT_DELIM_tFileOutputDelimited_2);
                            if(row2.date_premiere_mise_circulation != null) {
                        sb_tFileOutputDelimited_2.append(
                            FormatterUtils.format_Date(row2.date_premiere_mise_circulation, "dd/MM/yyyy")
                        );
                            }
                            sb_tFileOutputDelimited_2.append(OUT_DELIM_tFileOutputDelimited_2);
                            if(row2.immatriculation != null) {
                        sb_tFileOutputDelimited_2.append(
                            row2.immatriculation
                        );
                            }
                            sb_tFileOutputDelimited_2.append(OUT_DELIM_tFileOutputDelimited_2);
                            if(row2.chassis != null) {
                        sb_tFileOutputDelimited_2.append(
                            row2.chassis
                        );
                            }
                            sb_tFileOutputDelimited_2.append(OUT_DELIM_tFileOutputDelimited_2);
                            if(row2.usage != null) {
                        sb_tFileOutputDelimited_2.append(
                            row2.usage
                        );
                            }
                            sb_tFileOutputDelimited_2.append(OUT_DELIM_tFileOutputDelimited_2);
                            if(row2.charge_utile != null) {
                        sb_tFileOutputDelimited_2.append(
                            row2.charge_utile
                        );
                            }
                            sb_tFileOutputDelimited_2.append(OUT_DELIM_tFileOutputDelimited_2);
                            if(row2.puissance_fiscale != null) {
                        sb_tFileOutputDelimited_2.append(
                            row2.puissance_fiscale
                        );
                            }
                            sb_tFileOutputDelimited_2.append(OUT_DELIM_tFileOutputDelimited_2);
                            if(row2.remorque != null) {
                        sb_tFileOutputDelimited_2.append(
                            row2.remorque
                        );
                            }
                            sb_tFileOutputDelimited_2.append(OUT_DELIM_tFileOutputDelimited_2);
                            if(row2.nombre_portes != null) {
                        sb_tFileOutputDelimited_2.append(
                            row2.nombre_portes
                        );
                            }
                            sb_tFileOutputDelimited_2.append(OUT_DELIM_tFileOutputDelimited_2);
                            if(row2.immatriculation_remorque != null) {
                        sb_tFileOutputDelimited_2.append(
                            row2.immatriculation_remorque
                        );
                            }
                            sb_tFileOutputDelimited_2.append(OUT_DELIM_tFileOutputDelimited_2);
                            if(row2.source_energie != null) {
                        sb_tFileOutputDelimited_2.append(
                            row2.source_energie
                        );
                            }
                            sb_tFileOutputDelimited_2.append(OUT_DELIM_tFileOutputDelimited_2);
                            if(row2.nombre_de_places != null) {
                        sb_tFileOutputDelimited_2.append(
                            row2.nombre_de_places
                        );
                            }
                            sb_tFileOutputDelimited_2.append(OUT_DELIM_tFileOutputDelimited_2);
                            if(row2.cylindree != null) {
                        sb_tFileOutputDelimited_2.append(
                            row2.cylindree
                        );
                            }
                            sb_tFileOutputDelimited_2.append(OUT_DELIM_tFileOutputDelimited_2);
                            if(row2.double_commande != null) {
                        sb_tFileOutputDelimited_2.append(
                            row2.double_commande
                        );
                            }
                            sb_tFileOutputDelimited_2.append(OUT_DELIM_tFileOutputDelimited_2);
                            if(row2.responsabilite_civile != null) {
                        sb_tFileOutputDelimited_2.append(
                            row2.responsabilite_civile
                        );
                            }
                            sb_tFileOutputDelimited_2.append(OUT_DELIM_tFileOutputDelimited_2);
                            if(row2.utilitaire != null) {
                        sb_tFileOutputDelimited_2.append(
                            row2.utilitaire
                        );
                            }
                            sb_tFileOutputDelimited_2.append(OUT_DELIM_tFileOutputDelimited_2);
                            if(row2.type_engin != null) {
                        sb_tFileOutputDelimited_2.append(
                            row2.type_engin
                        );
                            }
                            sb_tFileOutputDelimited_2.append(OUT_DELIM_tFileOutputDelimited_2);
                            if(row2.poids_total_autorise_en_charge != null) {
                        sb_tFileOutputDelimited_2.append(
                            row2.poids_total_autorise_en_charge
                        );
                            }
                            sb_tFileOutputDelimited_2.append(OUT_DELIM_tFileOutputDelimited_2);
                            if(row2.date_extraction != null) {
                        sb_tFileOutputDelimited_2.append(
                            FormatterUtils.format_Date(row2.date_extraction, "dd/MM/yyyy")
                        );
                            }
                            sb_tFileOutputDelimited_2.append(OUT_DELIM_tFileOutputDelimited_2);
                            if(row2.date_depot != null) {
                        sb_tFileOutputDelimited_2.append(
                            FormatterUtils.format_Date(row2.date_depot, "dd/MM/yyyy")
                        );
                            }
                            sb_tFileOutputDelimited_2.append(OUT_DELIM_tFileOutputDelimited_2);
                            if(row2.errorCode != null) {
                        sb_tFileOutputDelimited_2.append(
                            row2.errorCode
                        );
                            }
                            sb_tFileOutputDelimited_2.append(OUT_DELIM_tFileOutputDelimited_2);
                            if(row2.errorMessage != null) {
                        sb_tFileOutputDelimited_2.append(
                            row2.errorMessage
                        );
                            }
                    sb_tFileOutputDelimited_2.append(OUT_DELIM_ROWSEP_tFileOutputDelimited_2);


                    nb_line_tFileOutputDelimited_2++;
                    resourceMap.put("nb_line_tFileOutputDelimited_2", nb_line_tFileOutputDelimited_2);

                        outtFileOutputDelimited_2.write(sb_tFileOutputDelimited_2.toString());




 


	tos_count_tFileOutputDelimited_2++;

/**
 * [tFileOutputDelimited_2 main ] stop
 */
	
	/**
	 * [tFileOutputDelimited_2 process_data_begin ] start
	 */

	

	
	
	currentComponent="tFileOutputDelimited_2";

	

 



/**
 * [tFileOutputDelimited_2 process_data_begin ] stop
 */
	
	/**
	 * [tFileOutputDelimited_2 process_data_end ] start
	 */

	

	
	
	currentComponent="tFileOutputDelimited_2";

	

 



/**
 * [tFileOutputDelimited_2 process_data_end ] stop
 */

} // End of branch "row2"




	
	/**
	 * [tDBOutput_3 process_data_end ] start
	 */

	

	
	
	currentComponent="tDBOutput_3";

	

 



/**
 * [tDBOutput_3 process_data_end ] stop
 */

} // End of branch "vehicule_rejet_Schema"




	
	/**
	 * [tMap_3 process_data_end ] start
	 */

	

	
	
	currentComponent="tMap_3";

	

 



/**
 * [tMap_3 process_data_end ] stop
 */



	
	/**
	 * [tLogRow_3 process_data_end ] start
	 */

	

	
	
	currentComponent="tLogRow_3";

	

 



/**
 * [tLogRow_3 process_data_end ] stop
 */

} // End of branch "row13"




	
	/**
	 * [tSchemaComplianceCheck_1 process_data_end ] start
	 */

	

	
	
	currentComponent="tSchemaComplianceCheck_1";

	

 



/**
 * [tSchemaComplianceCheck_1 process_data_end ] stop
 */

} // End of branch "row10"




// Start of branch "row11"
if(row11 != null) { 



	
	/**
	 * [tLogRow_1 main ] start
	 */

	

	
	
	currentComponent="tLogRow_1";

	
					if(execStat){
						runStat.updateStatOnConnection(iterateId,1,1
						
							,"row11"
						
						);
					}
					
///////////////////////		
						

				
				String[] row_tLogRow_1 = new String[23];
   				
	    		if(row11.code_assure != null) { //              
                 row_tLogRow_1[0]=    						    
				                String.valueOf(row11.code_assure)			
					          ;	
							
	    		} //			
    			   				
	    		if(row11.code_assureur != null) { //              
                 row_tLogRow_1[1]=    						    
				                String.valueOf(row11.code_assureur)			
					          ;	
							
	    		} //			
    			   				
	    		if(row11.marque != null) { //              
                 row_tLogRow_1[2]=    						    
				                String.valueOf(row11.marque)			
					          ;	
							
	    		} //			
    			   				
	    		if(row11.modele != null) { //              
                 row_tLogRow_1[3]=    						    
				                String.valueOf(row11.modele)			
					          ;	
							
	    		} //			
    			   				
	    		if(row11.date_premiere_mise_circulation != null) { //              
                 row_tLogRow_1[4]=    						
								FormatterUtils.format_Date(row11.date_premiere_mise_circulation, "dd/MM/yyyy")
					          ;	
							
	    		} //			
    			   				
	    		if(row11.immatriculation != null) { //              
                 row_tLogRow_1[5]=    						    
				                String.valueOf(row11.immatriculation)			
					          ;	
							
	    		} //			
    			   				
	    		if(row11.chassis != null) { //              
                 row_tLogRow_1[6]=    						    
				                String.valueOf(row11.chassis)			
					          ;	
							
	    		} //			
    			   				
	    		if(row11.usage != null) { //              
                 row_tLogRow_1[7]=    						    
				                String.valueOf(row11.usage)			
					          ;	
							
	    		} //			
    			   				
	    		if(row11.charge_utile != null) { //              
                 row_tLogRow_1[8]=    						    
				                String.valueOf(row11.charge_utile)			
					          ;	
							
	    		} //			
    			              
                 row_tLogRow_1[9]=    						    
				                String.valueOf(row11.puissance_fiscale)			
					          ;	
										
    			   				
	    		if(row11.remorque != null) { //              
                 row_tLogRow_1[10]=    						    
				                String.valueOf(row11.remorque)			
					          ;	
							
	    		} //			
    			   				
	    		if(row11.nombre_portes != null) { //              
                 row_tLogRow_1[11]=    						    
				                String.valueOf(row11.nombre_portes)			
					          ;	
							
	    		} //			
    			   				
	    		if(row11.immatriculation_remorque != null) { //              
                 row_tLogRow_1[12]=    						    
				                String.valueOf(row11.immatriculation_remorque)			
					          ;	
							
	    		} //			
    			   				
	    		if(row11.source_energie != null) { //              
                 row_tLogRow_1[13]=    						    
				                String.valueOf(row11.source_energie)			
					          ;	
							
	    		} //			
    			   				
	    		if(row11.nombre_de_places != null) { //              
                 row_tLogRow_1[14]=    						    
				                String.valueOf(row11.nombre_de_places)			
					          ;	
							
	    		} //			
    			   				
	    		if(row11.cylindree != null) { //              
                 row_tLogRow_1[15]=    						    
				                String.valueOf(row11.cylindree)			
					          ;	
							
	    		} //			
    			   				
	    		if(row11.double_commande != null) { //              
                 row_tLogRow_1[16]=    						    
				                String.valueOf(row11.double_commande)			
					          ;	
							
	    		} //			
    			   				
	    		if(row11.responsabilite_civile != null) { //              
                 row_tLogRow_1[17]=    						    
				                String.valueOf(row11.responsabilite_civile)			
					          ;	
							
	    		} //			
    			   				
	    		if(row11.utilitaire != null) { //              
                 row_tLogRow_1[18]=    						    
				                String.valueOf(row11.utilitaire)			
					          ;	
							
	    		} //			
    			   				
	    		if(row11.type_engin != null) { //              
                 row_tLogRow_1[19]=    						    
				                String.valueOf(row11.type_engin)			
					          ;	
							
	    		} //			
    			   				
	    		if(row11.poids_total_autorise_en_charge != null) { //              
                 row_tLogRow_1[20]=    						
								FormatterUtils.formatUnwithE(row11.poids_total_autorise_en_charge)
					          ;	
							
	    		} //			
    			   				
	    		if(row11.errorCode != null) { //              
                 row_tLogRow_1[21]=    						    
				                String.valueOf(row11.errorCode)			
					          ;	
							
	    		} //			
    			   				
	    		if(row11.errorMessage != null) { //              
                 row_tLogRow_1[22]=    						    
				                String.valueOf(row11.errorMessage)			
					          ;	
							
	    		} //			
    			 

				util_tLogRow_1.addRow(row_tLogRow_1);	
				nb_line_tLogRow_1++;
//////

//////                    
                    
///////////////////////    			

 
     row18 = row11;


	tos_count_tLogRow_1++;

/**
 * [tLogRow_1 main ] stop
 */
	
	/**
	 * [tLogRow_1 process_data_begin ] start
	 */

	

	
	
	currentComponent="tLogRow_1";

	

 



/**
 * [tLogRow_1 process_data_begin ] stop
 */

	
	/**
	 * [tMap_2 main ] start
	 */

	

	
	
	currentComponent="tMap_2";

	
					if(execStat){
						runStat.updateStatOnConnection(iterateId,1,1
						
							,"row18"
						
						);
					}
					

		
		
		boolean hasCasePrimitiveKeyWithNull_tMap_2 = false;
		

        // ###############################
        // # Input tables (lookups)
		  boolean rejectedInnerJoin_tMap_2 = false;
		  boolean mainRowRejected_tMap_2 = false;
            				    								  
		// ###############################
        { // start of Var scope
        
	        // ###############################
        	// # Vars tables
        
Var__tMap_2__Struct Var = Var__tMap_2;// ###############################
        // ###############################
        // # Output tables

vehicule_rejet_structure = null;


// # Output table : 'vehicule_rejet_structure'
vehicule_rejet_structure_tmp.code_assure = row18.code_assure ;
vehicule_rejet_structure_tmp.code_assureur = row18.code_assureur ;
vehicule_rejet_structure_tmp.marque = row18.marque ;
vehicule_rejet_structure_tmp.modele = row18.modele ;
vehicule_rejet_structure_tmp.date_premiere_mise_circulation = row18.date_premiere_mise_circulation ;
vehicule_rejet_structure_tmp.immatriculation = row18.immatriculation ;
vehicule_rejet_structure_tmp.chassis = row18.chassis ;
vehicule_rejet_structure_tmp.usage = row18.usage ;
vehicule_rejet_structure_tmp.charge_utile = row18.charge_utile ;
vehicule_rejet_structure_tmp.puissance_fiscale = row18.puissance_fiscale ;
vehicule_rejet_structure_tmp.remorque = row18.remorque ;
vehicule_rejet_structure_tmp.nombre_portes = row18.nombre_portes ;
vehicule_rejet_structure_tmp.immatriculation_remorque = row18.immatriculation_remorque ;
vehicule_rejet_structure_tmp.source_energie = row18.source_energie ;
vehicule_rejet_structure_tmp.nombre_de_places = row18.nombre_de_places.toString(0) ;
vehicule_rejet_structure_tmp.cylindree = row18.cylindree ;
vehicule_rejet_structure_tmp.double_commande = row18.double_commande ;
vehicule_rejet_structure_tmp.responsabilite_civile = row18.responsabilite_civile ;
vehicule_rejet_structure_tmp.utilitaire = row18.utilitaire ;
vehicule_rejet_structure_tmp.type_engin = row18.type_engin ;
vehicule_rejet_structure_tmp.poids_total_autorise_en_charge = row18.poids_total_autorise_en_charge ;
vehicule_rejet_structure_tmp.date_extraction = null;
vehicule_rejet_structure_tmp.date_depot = null;
vehicule_rejet_structure_tmp.errorCode = row18.errorCode ;
vehicule_rejet_structure_tmp.errorMessage = row18.errorMessage ;
vehicule_rejet_structure = vehicule_rejet_structure_tmp;
// ###############################

} // end of Var scope

rejectedInnerJoin_tMap_2 = false;










 


	tos_count_tMap_2++;

/**
 * [tMap_2 main ] stop
 */
	
	/**
	 * [tMap_2 process_data_begin ] start
	 */

	

	
	
	currentComponent="tMap_2";

	

 



/**
 * [tMap_2 process_data_begin ] stop
 */
// Start of branch "vehicule_rejet_structure"
if(vehicule_rejet_structure != null) { 



	
	/**
	 * [tDBOutput_4 main ] start
	 */

	

	
	
	currentComponent="tDBOutput_4";

	
					if(execStat){
						runStat.updateStatOnConnection(iterateId,1,1
						
							,"vehicule_rejet_structure"
						
						);
					}
					



            row1 = null;
        whetherReject_tDBOutput_4 = false;
                    if(vehicule_rejet_structure.code_assure == null) {
pstmt_tDBOutput_4.setNull(1, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_4.setString(1, vehicule_rejet_structure.code_assure);
}

                    if(vehicule_rejet_structure.code_assureur == null) {
pstmt_tDBOutput_4.setNull(2, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_4.setString(2, vehicule_rejet_structure.code_assureur);
}

                    if(vehicule_rejet_structure.marque == null) {
pstmt_tDBOutput_4.setNull(3, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_4.setString(3, vehicule_rejet_structure.marque);
}

                    if(vehicule_rejet_structure.modele == null) {
pstmt_tDBOutput_4.setNull(4, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_4.setString(4, vehicule_rejet_structure.modele);
}

                    if(vehicule_rejet_structure.date_premiere_mise_circulation != null) {
pstmt_tDBOutput_4.setTimestamp(5, new java.sql.Timestamp(vehicule_rejet_structure.date_premiere_mise_circulation.getTime()));
} else {
pstmt_tDBOutput_4.setNull(5, java.sql.Types.TIMESTAMP);
}

                    if(vehicule_rejet_structure.immatriculation == null) {
pstmt_tDBOutput_4.setNull(6, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_4.setString(6, vehicule_rejet_structure.immatriculation);
}

                    if(vehicule_rejet_structure.chassis == null) {
pstmt_tDBOutput_4.setNull(7, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_4.setString(7, vehicule_rejet_structure.chassis);
}

                    if(vehicule_rejet_structure.usage == null) {
pstmt_tDBOutput_4.setNull(8, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_4.setString(8, vehicule_rejet_structure.usage);
}

                    if(vehicule_rejet_structure.charge_utile == null) {
pstmt_tDBOutput_4.setNull(9, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_4.setString(9, vehicule_rejet_structure.charge_utile);
}

                    if(vehicule_rejet_structure.puissance_fiscale == null) {
pstmt_tDBOutput_4.setNull(10, java.sql.Types.INTEGER);
} else {pstmt_tDBOutput_4.setInt(10, vehicule_rejet_structure.puissance_fiscale);
}

                    if(vehicule_rejet_structure.remorque == null) {
pstmt_tDBOutput_4.setNull(11, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_4.setString(11, vehicule_rejet_structure.remorque);
}

                    if(vehicule_rejet_structure.nombre_portes == null) {
pstmt_tDBOutput_4.setNull(12, java.sql.Types.INTEGER);
} else {pstmt_tDBOutput_4.setInt(12, vehicule_rejet_structure.nombre_portes);
}

                    if(vehicule_rejet_structure.immatriculation_remorque == null) {
pstmt_tDBOutput_4.setNull(13, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_4.setString(13, vehicule_rejet_structure.immatriculation_remorque);
}

                    if(vehicule_rejet_structure.source_energie == null) {
pstmt_tDBOutput_4.setNull(14, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_4.setString(14, vehicule_rejet_structure.source_energie);
}

                    if(vehicule_rejet_structure.nombre_de_places == null) {
pstmt_tDBOutput_4.setNull(15, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_4.setString(15, vehicule_rejet_structure.nombre_de_places);
}

                    if(vehicule_rejet_structure.cylindree == null) {
pstmt_tDBOutput_4.setNull(16, java.sql.Types.INTEGER);
} else {pstmt_tDBOutput_4.setInt(16, vehicule_rejet_structure.cylindree);
}

                    if(vehicule_rejet_structure.double_commande == null) {
pstmt_tDBOutput_4.setNull(17, java.sql.Types.INTEGER);
} else {pstmt_tDBOutput_4.setInt(17, vehicule_rejet_structure.double_commande);
}

                    if(vehicule_rejet_structure.responsabilite_civile == null) {
pstmt_tDBOutput_4.setNull(18, java.sql.Types.INTEGER);
} else {pstmt_tDBOutput_4.setInt(18, vehicule_rejet_structure.responsabilite_civile);
}

                    if(vehicule_rejet_structure.utilitaire == null) {
pstmt_tDBOutput_4.setNull(19, java.sql.Types.INTEGER);
} else {pstmt_tDBOutput_4.setInt(19, vehicule_rejet_structure.utilitaire);
}

                    if(vehicule_rejet_structure.type_engin == null) {
pstmt_tDBOutput_4.setNull(20, java.sql.Types.INTEGER);
} else {pstmt_tDBOutput_4.setInt(20, vehicule_rejet_structure.type_engin);
}

                    if(vehicule_rejet_structure.poids_total_autorise_en_charge == null) {
pstmt_tDBOutput_4.setNull(21, java.sql.Types.FLOAT);
} else {pstmt_tDBOutput_4.setFloat(21, vehicule_rejet_structure.poids_total_autorise_en_charge);
}

                    if(vehicule_rejet_structure.date_extraction != null) {
pstmt_tDBOutput_4.setTimestamp(22, new java.sql.Timestamp(vehicule_rejet_structure.date_extraction.getTime()));
} else {
pstmt_tDBOutput_4.setNull(22, java.sql.Types.TIMESTAMP);
}

                    if(vehicule_rejet_structure.date_depot != null) {
pstmt_tDBOutput_4.setTimestamp(23, new java.sql.Timestamp(vehicule_rejet_structure.date_depot.getTime()));
} else {
pstmt_tDBOutput_4.setNull(23, java.sql.Types.TIMESTAMP);
}

                    if(vehicule_rejet_structure.errorCode == null) {
pstmt_tDBOutput_4.setNull(24, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_4.setString(24, vehicule_rejet_structure.errorCode);
}

                    if(vehicule_rejet_structure.errorMessage == null) {
pstmt_tDBOutput_4.setNull(25, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_4.setString(25, vehicule_rejet_structure.errorMessage);
}

			
    		pstmt_tDBOutput_4.addBatch();
    		nb_line_tDBOutput_4++;
    		  
    		  
    		  batchSizeCounter_tDBOutput_4++;
    		  
            if(!whetherReject_tDBOutput_4) {
                            row1 = new row1Struct();
                                row1.code_assure = vehicule_rejet_structure.code_assure;
                                row1.code_assureur = vehicule_rejet_structure.code_assureur;
                                row1.marque = vehicule_rejet_structure.marque;
                                row1.modele = vehicule_rejet_structure.modele;
                                row1.date_premiere_mise_circulation = vehicule_rejet_structure.date_premiere_mise_circulation;
                                row1.immatriculation = vehicule_rejet_structure.immatriculation;
                                row1.chassis = vehicule_rejet_structure.chassis;
                                row1.usage = vehicule_rejet_structure.usage;
                                row1.charge_utile = vehicule_rejet_structure.charge_utile;
                                row1.puissance_fiscale = vehicule_rejet_structure.puissance_fiscale;
                                row1.remorque = vehicule_rejet_structure.remorque;
                                row1.nombre_portes = vehicule_rejet_structure.nombre_portes;
                                row1.immatriculation_remorque = vehicule_rejet_structure.immatriculation_remorque;
                                row1.source_energie = vehicule_rejet_structure.source_energie;
                                row1.nombre_de_places = vehicule_rejet_structure.nombre_de_places;
                                row1.cylindree = vehicule_rejet_structure.cylindree;
                                row1.double_commande = vehicule_rejet_structure.double_commande;
                                row1.responsabilite_civile = vehicule_rejet_structure.responsabilite_civile;
                                row1.utilitaire = vehicule_rejet_structure.utilitaire;
                                row1.type_engin = vehicule_rejet_structure.type_engin;
                                row1.poids_total_autorise_en_charge = vehicule_rejet_structure.poids_total_autorise_en_charge;
                                row1.date_extraction = vehicule_rejet_structure.date_extraction;
                                row1.date_depot = vehicule_rejet_structure.date_depot;
                                row1.errorCode = vehicule_rejet_structure.errorCode;
                                row1.errorMessage = vehicule_rejet_structure.errorMessage;
            }
    			if ((batchSize_tDBOutput_4 > 0) && (batchSize_tDBOutput_4 <= batchSizeCounter_tDBOutput_4)) {
                try {
						int countSum_tDBOutput_4 = 0;
						    
						for(int countEach_tDBOutput_4: pstmt_tDBOutput_4.executeBatch()) {
							countSum_tDBOutput_4 += (countEach_tDBOutput_4 < 0 ? 0 : countEach_tDBOutput_4);
						}
				    	rowsToCommitCount_tDBOutput_4 += countSum_tDBOutput_4;
				    	
				    		insertedCount_tDBOutput_4 += countSum_tDBOutput_4;
				    	
            	    	batchSizeCounter_tDBOutput_4 = 0;
                }catch (java.sql.BatchUpdateException e_tDBOutput_4){
globalMap.put("tDBOutput_4_ERROR_MESSAGE",e_tDBOutput_4.getMessage());
				    	java.sql.SQLException ne_tDBOutput_4 = e_tDBOutput_4.getNextException(),sqle_tDBOutput_4=null;
				    	String errormessage_tDBOutput_4;
						if (ne_tDBOutput_4 != null) {
							// build new exception to provide the original cause
							sqle_tDBOutput_4 = new java.sql.SQLException(e_tDBOutput_4.getMessage() + "\ncaused by: " + ne_tDBOutput_4.getMessage(), ne_tDBOutput_4.getSQLState(), ne_tDBOutput_4.getErrorCode(), ne_tDBOutput_4);
							errormessage_tDBOutput_4 = sqle_tDBOutput_4.getMessage();
						}else{
							errormessage_tDBOutput_4 = e_tDBOutput_4.getMessage();
						}
				    	
				    	int countSum_tDBOutput_4 = 0;
						for(int countEach_tDBOutput_4: e_tDBOutput_4.getUpdateCounts()) {
							countSum_tDBOutput_4 += (countEach_tDBOutput_4 < 0 ? 0 : countEach_tDBOutput_4);
						}
						rowsToCommitCount_tDBOutput_4 += countSum_tDBOutput_4;
						
				    		insertedCount_tDBOutput_4 += countSum_tDBOutput_4;
				    	
				    	System.err.println(errormessage_tDBOutput_4);
				    	
					}
    			}
    		
    		    commitCounter_tDBOutput_4++;
                if(commitEvery_tDBOutput_4 <= commitCounter_tDBOutput_4) {
                if ((batchSize_tDBOutput_4 > 0) && (batchSizeCounter_tDBOutput_4 > 0)) {
                try {
                		int countSum_tDBOutput_4 = 0;
                		    
						for(int countEach_tDBOutput_4: pstmt_tDBOutput_4.executeBatch()) {
							countSum_tDBOutput_4 += (countEach_tDBOutput_4 < 0 ? 0 : countEach_tDBOutput_4);
						}
            	    	rowsToCommitCount_tDBOutput_4 += countSum_tDBOutput_4;
            	    	
            	    		insertedCount_tDBOutput_4 += countSum_tDBOutput_4;
            	    	
                batchSizeCounter_tDBOutput_4 = 0;
               }catch (java.sql.BatchUpdateException e_tDBOutput_4){
globalMap.put("tDBOutput_4_ERROR_MESSAGE",e_tDBOutput_4.getMessage());
			    	java.sql.SQLException ne_tDBOutput_4 = e_tDBOutput_4.getNextException(),sqle_tDBOutput_4=null;
			    	String errormessage_tDBOutput_4;
					if (ne_tDBOutput_4 != null) {
						// build new exception to provide the original cause
						sqle_tDBOutput_4 = new java.sql.SQLException(e_tDBOutput_4.getMessage() + "\ncaused by: " + ne_tDBOutput_4.getMessage(), ne_tDBOutput_4.getSQLState(), ne_tDBOutput_4.getErrorCode(), ne_tDBOutput_4);
						errormessage_tDBOutput_4 = sqle_tDBOutput_4.getMessage();
					}else{
						errormessage_tDBOutput_4 = e_tDBOutput_4.getMessage();
					}
			    	
			    	int countSum_tDBOutput_4 = 0;
					for(int countEach_tDBOutput_4: e_tDBOutput_4.getUpdateCounts()) {
						countSum_tDBOutput_4 += (countEach_tDBOutput_4 < 0 ? 0 : countEach_tDBOutput_4);
					}
					rowsToCommitCount_tDBOutput_4 += countSum_tDBOutput_4;
					
			    		insertedCount_tDBOutput_4 += countSum_tDBOutput_4;
			    	
			    	System.err.println(errormessage_tDBOutput_4);
			    	
				}
            }
                    if(rowsToCommitCount_tDBOutput_4 != 0){
                    	
                    }
                    conn_tDBOutput_4.commit();
                    if(rowsToCommitCount_tDBOutput_4 != 0){
                    	
                    	rowsToCommitCount_tDBOutput_4 = 0;
                    }
                    commitCounter_tDBOutput_4=0;
                }

 


	tos_count_tDBOutput_4++;

/**
 * [tDBOutput_4 main ] stop
 */
	
	/**
	 * [tDBOutput_4 process_data_begin ] start
	 */

	

	
	
	currentComponent="tDBOutput_4";

	

 



/**
 * [tDBOutput_4 process_data_begin ] stop
 */
// Start of branch "row1"
if(row1 != null) { 



	
	/**
	 * [tFileOutputDelimited_1 main ] start
	 */

	

	
	
	currentComponent="tFileOutputDelimited_1";

	
					if(execStat){
						runStat.updateStatOnConnection(iterateId,1,1
						
							,"row1"
						
						);
					}
					


                    StringBuilder sb_tFileOutputDelimited_1 = new StringBuilder();
                            if(row1.code_assure != null) {
                        sb_tFileOutputDelimited_1.append(
                            row1.code_assure
                        );
                            }
                            sb_tFileOutputDelimited_1.append(OUT_DELIM_tFileOutputDelimited_1);
                            if(row1.code_assureur != null) {
                        sb_tFileOutputDelimited_1.append(
                            row1.code_assureur
                        );
                            }
                            sb_tFileOutputDelimited_1.append(OUT_DELIM_tFileOutputDelimited_1);
                            if(row1.marque != null) {
                        sb_tFileOutputDelimited_1.append(
                            row1.marque
                        );
                            }
                            sb_tFileOutputDelimited_1.append(OUT_DELIM_tFileOutputDelimited_1);
                            if(row1.modele != null) {
                        sb_tFileOutputDelimited_1.append(
                            row1.modele
                        );
                            }
                            sb_tFileOutputDelimited_1.append(OUT_DELIM_tFileOutputDelimited_1);
                            if(row1.date_premiere_mise_circulation != null) {
                        sb_tFileOutputDelimited_1.append(
                            FormatterUtils.format_Date(row1.date_premiere_mise_circulation, "dd/MM/yyyy")
                        );
                            }
                            sb_tFileOutputDelimited_1.append(OUT_DELIM_tFileOutputDelimited_1);
                            if(row1.immatriculation != null) {
                        sb_tFileOutputDelimited_1.append(
                            row1.immatriculation
                        );
                            }
                            sb_tFileOutputDelimited_1.append(OUT_DELIM_tFileOutputDelimited_1);
                            if(row1.chassis != null) {
                        sb_tFileOutputDelimited_1.append(
                            row1.chassis
                        );
                            }
                            sb_tFileOutputDelimited_1.append(OUT_DELIM_tFileOutputDelimited_1);
                            if(row1.usage != null) {
                        sb_tFileOutputDelimited_1.append(
                            row1.usage
                        );
                            }
                            sb_tFileOutputDelimited_1.append(OUT_DELIM_tFileOutputDelimited_1);
                            if(row1.charge_utile != null) {
                        sb_tFileOutputDelimited_1.append(
                            row1.charge_utile
                        );
                            }
                            sb_tFileOutputDelimited_1.append(OUT_DELIM_tFileOutputDelimited_1);
                            if(row1.puissance_fiscale != null) {
                        sb_tFileOutputDelimited_1.append(
                            row1.puissance_fiscale
                        );
                            }
                            sb_tFileOutputDelimited_1.append(OUT_DELIM_tFileOutputDelimited_1);
                            if(row1.remorque != null) {
                        sb_tFileOutputDelimited_1.append(
                            row1.remorque
                        );
                            }
                            sb_tFileOutputDelimited_1.append(OUT_DELIM_tFileOutputDelimited_1);
                            if(row1.nombre_portes != null) {
                        sb_tFileOutputDelimited_1.append(
                            row1.nombre_portes
                        );
                            }
                            sb_tFileOutputDelimited_1.append(OUT_DELIM_tFileOutputDelimited_1);
                            if(row1.immatriculation_remorque != null) {
                        sb_tFileOutputDelimited_1.append(
                            row1.immatriculation_remorque
                        );
                            }
                            sb_tFileOutputDelimited_1.append(OUT_DELIM_tFileOutputDelimited_1);
                            if(row1.source_energie != null) {
                        sb_tFileOutputDelimited_1.append(
                            row1.source_energie
                        );
                            }
                            sb_tFileOutputDelimited_1.append(OUT_DELIM_tFileOutputDelimited_1);
                            if(row1.nombre_de_places != null) {
                        sb_tFileOutputDelimited_1.append(
                            row1.nombre_de_places
                        );
                            }
                            sb_tFileOutputDelimited_1.append(OUT_DELIM_tFileOutputDelimited_1);
                            if(row1.cylindree != null) {
                        sb_tFileOutputDelimited_1.append(
                            row1.cylindree
                        );
                            }
                            sb_tFileOutputDelimited_1.append(OUT_DELIM_tFileOutputDelimited_1);
                            if(row1.double_commande != null) {
                        sb_tFileOutputDelimited_1.append(
                            row1.double_commande
                        );
                            }
                            sb_tFileOutputDelimited_1.append(OUT_DELIM_tFileOutputDelimited_1);
                            if(row1.responsabilite_civile != null) {
                        sb_tFileOutputDelimited_1.append(
                            row1.responsabilite_civile
                        );
                            }
                            sb_tFileOutputDelimited_1.append(OUT_DELIM_tFileOutputDelimited_1);
                            if(row1.utilitaire != null) {
                        sb_tFileOutputDelimited_1.append(
                            row1.utilitaire
                        );
                            }
                            sb_tFileOutputDelimited_1.append(OUT_DELIM_tFileOutputDelimited_1);
                            if(row1.type_engin != null) {
                        sb_tFileOutputDelimited_1.append(
                            row1.type_engin
                        );
                            }
                            sb_tFileOutputDelimited_1.append(OUT_DELIM_tFileOutputDelimited_1);
                            if(row1.poids_total_autorise_en_charge != null) {
                        sb_tFileOutputDelimited_1.append(
                            row1.poids_total_autorise_en_charge
                        );
                            }
                            sb_tFileOutputDelimited_1.append(OUT_DELIM_tFileOutputDelimited_1);
                            if(row1.date_extraction != null) {
                        sb_tFileOutputDelimited_1.append(
                            FormatterUtils.format_Date(row1.date_extraction, "dd/MM/yyyy")
                        );
                            }
                            sb_tFileOutputDelimited_1.append(OUT_DELIM_tFileOutputDelimited_1);
                            if(row1.date_depot != null) {
                        sb_tFileOutputDelimited_1.append(
                            FormatterUtils.format_Date(row1.date_depot, "dd/MM/yyyy")
                        );
                            }
                            sb_tFileOutputDelimited_1.append(OUT_DELIM_tFileOutputDelimited_1);
                            if(row1.errorCode != null) {
                        sb_tFileOutputDelimited_1.append(
                            row1.errorCode
                        );
                            }
                            sb_tFileOutputDelimited_1.append(OUT_DELIM_tFileOutputDelimited_1);
                            if(row1.errorMessage != null) {
                        sb_tFileOutputDelimited_1.append(
                            row1.errorMessage
                        );
                            }
                    sb_tFileOutputDelimited_1.append(OUT_DELIM_ROWSEP_tFileOutputDelimited_1);


                    nb_line_tFileOutputDelimited_1++;
                    resourceMap.put("nb_line_tFileOutputDelimited_1", nb_line_tFileOutputDelimited_1);

                        outtFileOutputDelimited_1.write(sb_tFileOutputDelimited_1.toString());




 


	tos_count_tFileOutputDelimited_1++;

/**
 * [tFileOutputDelimited_1 main ] stop
 */
	
	/**
	 * [tFileOutputDelimited_1 process_data_begin ] start
	 */

	

	
	
	currentComponent="tFileOutputDelimited_1";

	

 



/**
 * [tFileOutputDelimited_1 process_data_begin ] stop
 */
	
	/**
	 * [tFileOutputDelimited_1 process_data_end ] start
	 */

	

	
	
	currentComponent="tFileOutputDelimited_1";

	

 



/**
 * [tFileOutputDelimited_1 process_data_end ] stop
 */

} // End of branch "row1"




	
	/**
	 * [tDBOutput_4 process_data_end ] start
	 */

	

	
	
	currentComponent="tDBOutput_4";

	

 



/**
 * [tDBOutput_4 process_data_end ] stop
 */

} // End of branch "vehicule_rejet_structure"




	
	/**
	 * [tMap_2 process_data_end ] start
	 */

	

	
	
	currentComponent="tMap_2";

	

 



/**
 * [tMap_2 process_data_end ] stop
 */



	
	/**
	 * [tLogRow_1 process_data_end ] start
	 */

	

	
	
	currentComponent="tLogRow_1";

	

 



/**
 * [tLogRow_1 process_data_end ] stop
 */

} // End of branch "row11"




	
	/**
	 * [tFileInputDelimited_1 process_data_end ] start
	 */

	

	
	
	currentComponent="tFileInputDelimited_1";

	

 



/**
 * [tFileInputDelimited_1 process_data_end ] stop
 */
	
	/**
	 * [tFileInputDelimited_1 end ] start
	 */

	

	
	
	currentComponent="tFileInputDelimited_1";

	


				nb_line_tFileInputDelimited_1++;
			}
			
			}finally{
    			if(!(filename_tFileInputDelimited_1 instanceof java.io.InputStream)){
    				if(csvReadertFileInputDelimited_1!=null){
    					csvReadertFileInputDelimited_1.close();
    				}
    			}
    			if(csvReadertFileInputDelimited_1!=null){
    				globalMap.put("tFileInputDelimited_1_NB_LINE",nb_line_tFileInputDelimited_1);
    			}
				
			}
						  

 

ok_Hash.put("tFileInputDelimited_1", true);
end_Hash.put("tFileInputDelimited_1", System.currentTimeMillis());




/**
 * [tFileInputDelimited_1 end ] stop
 */

	
	/**
	 * [tSchemaComplianceCheck_1 end ] start
	 */

	

	
	
	currentComponent="tSchemaComplianceCheck_1";

	

				if(execStat){
			  		runStat.updateStat(resourceMap,iterateId,2,0,"row10");
			  	}
			  	
 

ok_Hash.put("tSchemaComplianceCheck_1", true);
end_Hash.put("tSchemaComplianceCheck_1", System.currentTimeMillis());




/**
 * [tSchemaComplianceCheck_1 end ] stop
 */

	
	/**
	 * [tLogRow_2 end ] start
	 */

	

	
	
	currentComponent="tLogRow_2";

	


//////

                    
                    java.io.PrintStream consoleOut_tLogRow_2 = null;
                    if (globalMap.get("tLogRow_CONSOLE")!=null)
                    {
                    	consoleOut_tLogRow_2 = (java.io.PrintStream) globalMap.get("tLogRow_CONSOLE");
                    }
                    else
                    {
                    	consoleOut_tLogRow_2 = new java.io.PrintStream(new java.io.BufferedOutputStream(System.out));
                    	globalMap.put("tLogRow_CONSOLE",consoleOut_tLogRow_2);
                    }
                    
                    consoleOut_tLogRow_2.println(util_tLogRow_2.format().toString());
                    consoleOut_tLogRow_2.flush();
//////
globalMap.put("tLogRow_2_NB_LINE",nb_line_tLogRow_2);

///////////////////////    			

				if(execStat){
			  		runStat.updateStat(resourceMap,iterateId,2,0,"row12");
			  	}
			  	
 

ok_Hash.put("tLogRow_2", true);
end_Hash.put("tLogRow_2", System.currentTimeMillis());




/**
 * [tLogRow_2 end ] stop
 */

	
	/**
	 * [tMap_1 end ] start
	 */

	

	
	
	currentComponent="tMap_1";

	


// ###############################
// # Lookup hashes releasing
// ###############################      





				if(execStat){
			  		runStat.updateStat(resourceMap,iterateId,2,0,"row14");
			  	}
			  	
 

ok_Hash.put("tMap_1", true);
end_Hash.put("tMap_1", System.currentTimeMillis());




/**
 * [tMap_1 end ] stop
 */

	
	/**
	 * [tLogRow_4 end ] start
	 */

	

	
	
	currentComponent="tLogRow_4";

	


//////

                    
                    java.io.PrintStream consoleOut_tLogRow_4 = null;
                    if (globalMap.get("tLogRow_CONSOLE")!=null)
                    {
                    	consoleOut_tLogRow_4 = (java.io.PrintStream) globalMap.get("tLogRow_CONSOLE");
                    }
                    else
                    {
                    	consoleOut_tLogRow_4 = new java.io.PrintStream(new java.io.BufferedOutputStream(System.out));
                    	globalMap.put("tLogRow_CONSOLE",consoleOut_tLogRow_4);
                    }
                    
                    consoleOut_tLogRow_4.println(util_tLogRow_4.format().toString());
                    consoleOut_tLogRow_4.flush();
//////
globalMap.put("tLogRow_4_NB_LINE",nb_line_tLogRow_4);

///////////////////////    			

				if(execStat){
			  		runStat.updateStat(resourceMap,iterateId,2,0,"vehicule_valide");
			  	}
			  	
 

ok_Hash.put("tLogRow_4", true);
end_Hash.put("tLogRow_4", System.currentTimeMillis());




/**
 * [tLogRow_4 end ] stop
 */

	
	/**
	 * [tDBOutput_1 end ] start
	 */

	

	
	
	currentComponent="tDBOutput_1";

	



        if(pstmtUpdate_tDBOutput_1 != null){
            pstmtUpdate_tDBOutput_1.close();
            resourceMap.remove("pstmtUpdate_tDBOutput_1");
        }
        if(pstmtInsert_tDBOutput_1 != null){
            pstmtInsert_tDBOutput_1.close();
            resourceMap.remove("pstmtInsert_tDBOutput_1");
        }
        if(pstmt_tDBOutput_1 != null) {
            pstmt_tDBOutput_1.close();
            resourceMap.remove("pstmt_tDBOutput_1");
        }
    resourceMap.put("statementClosed_tDBOutput_1", true);
			if(rowsToCommitCount_tDBOutput_1 != 0){
				
			}
			conn_tDBOutput_1.commit();
			if(rowsToCommitCount_tDBOutput_1 != 0){
				
				rowsToCommitCount_tDBOutput_1 = 0;
			}
			commitCounter_tDBOutput_1 = 0;
		
    	conn_tDBOutput_1 .close();
    	
    	resourceMap.put("finish_tDBOutput_1", true);
    	

	nb_line_deleted_tDBOutput_1=nb_line_deleted_tDBOutput_1+ deletedCount_tDBOutput_1;
	nb_line_update_tDBOutput_1=nb_line_update_tDBOutput_1 + updatedCount_tDBOutput_1;
	nb_line_inserted_tDBOutput_1=nb_line_inserted_tDBOutput_1 + insertedCount_tDBOutput_1;
	nb_line_rejected_tDBOutput_1=nb_line_rejected_tDBOutput_1 + rejectedCount_tDBOutput_1;
	
        globalMap.put("tDBOutput_1_NB_LINE",nb_line_tDBOutput_1);
        globalMap.put("tDBOutput_1_NB_LINE_UPDATED",nb_line_update_tDBOutput_1);
        globalMap.put("tDBOutput_1_NB_LINE_INSERTED",nb_line_inserted_tDBOutput_1);
        globalMap.put("tDBOutput_1_NB_LINE_DELETED",nb_line_deleted_tDBOutput_1);
        globalMap.put("tDBOutput_1_NB_LINE_REJECTED", nb_line_rejected_tDBOutput_1);
    

	


				if(execStat){
			  		runStat.updateStat(resourceMap,iterateId,2,0,"row15");
			  	}
			  	
 

ok_Hash.put("tDBOutput_1", true);
end_Hash.put("tDBOutput_1", System.currentTimeMillis());

				if(execStat){   
   	 				runStat.updateStatOnConnection("OnComponentOk1", 0, "ok");
				}
				tFileCopy_1Process(globalMap);



/**
 * [tDBOutput_1 end ] stop
 */







	
	/**
	 * [tLogRow_5 end ] start
	 */

	

	
	
	currentComponent="tLogRow_5";

	


//////

                    
                    java.io.PrintStream consoleOut_tLogRow_5 = null;
                    if (globalMap.get("tLogRow_CONSOLE")!=null)
                    {
                    	consoleOut_tLogRow_5 = (java.io.PrintStream) globalMap.get("tLogRow_CONSOLE");
                    }
                    else
                    {
                    	consoleOut_tLogRow_5 = new java.io.PrintStream(new java.io.BufferedOutputStream(System.out));
                    	globalMap.put("tLogRow_CONSOLE",consoleOut_tLogRow_5);
                    }
                    
                    consoleOut_tLogRow_5.println(util_tLogRow_5.format().toString());
                    consoleOut_tLogRow_5.flush();
//////
globalMap.put("tLogRow_5_NB_LINE",nb_line_tLogRow_5);

///////////////////////    			

				if(execStat){
			  		runStat.updateStat(resourceMap,iterateId,2,0,"vehicule_rejet");
			  	}
			  	
 

ok_Hash.put("tLogRow_5", true);
end_Hash.put("tLogRow_5", System.currentTimeMillis());




/**
 * [tLogRow_5 end ] stop
 */

	
	/**
	 * [tDBOutput_2 end ] start
	 */

	

	
	
	currentComponent="tDBOutput_2";

	



	    try {
				int countSum_tDBOutput_2 = 0;
				if (pstmt_tDBOutput_2 != null && batchSizeCounter_tDBOutput_2 > 0) {
						
					for(int countEach_tDBOutput_2: pstmt_tDBOutput_2.executeBatch()) {
						countSum_tDBOutput_2 += (countEach_tDBOutput_2 < 0 ? 0 : countEach_tDBOutput_2);
					}
					rowsToCommitCount_tDBOutput_2 += countSum_tDBOutput_2;
						
				}
		    	
		    		insertedCount_tDBOutput_2 += countSum_tDBOutput_2;
		    	
	    }catch (java.sql.BatchUpdateException e_tDBOutput_2){
globalMap.put("tDBOutput_2_ERROR_MESSAGE",e_tDBOutput_2.getMessage());
	    	java.sql.SQLException ne_tDBOutput_2 = e_tDBOutput_2.getNextException(),sqle_tDBOutput_2=null;
	    	String errormessage_tDBOutput_2;
			if (ne_tDBOutput_2 != null) {
				// build new exception to provide the original cause
				sqle_tDBOutput_2 = new java.sql.SQLException(e_tDBOutput_2.getMessage() + "\ncaused by: " + ne_tDBOutput_2.getMessage(), ne_tDBOutput_2.getSQLState(), ne_tDBOutput_2.getErrorCode(), ne_tDBOutput_2);
				errormessage_tDBOutput_2 = sqle_tDBOutput_2.getMessage();
			}else{
				errormessage_tDBOutput_2 = e_tDBOutput_2.getMessage();
			}
	    	
	    	int countSum_tDBOutput_2 = 0;
			for(int countEach_tDBOutput_2: e_tDBOutput_2.getUpdateCounts()) {
				countSum_tDBOutput_2 += (countEach_tDBOutput_2 < 0 ? 0 : countEach_tDBOutput_2);
			}
			rowsToCommitCount_tDBOutput_2 += countSum_tDBOutput_2;
			
	    		insertedCount_tDBOutput_2 += countSum_tDBOutput_2;
	    	
	    	System.err.println(errormessage_tDBOutput_2);
	    	
		}
	    
        if(pstmt_tDBOutput_2 != null) {
        		
            pstmt_tDBOutput_2.close();
            resourceMap.remove("pstmt_tDBOutput_2");
        }
    resourceMap.put("statementClosed_tDBOutput_2", true);
			if(rowsToCommitCount_tDBOutput_2 != 0){
				
			}
			conn_tDBOutput_2.commit();
			if(rowsToCommitCount_tDBOutput_2 != 0){
				
				rowsToCommitCount_tDBOutput_2 = 0;
			}
			commitCounter_tDBOutput_2 = 0;
		
    	conn_tDBOutput_2 .close();
    	
    	resourceMap.put("finish_tDBOutput_2", true);
    	

	nb_line_deleted_tDBOutput_2=nb_line_deleted_tDBOutput_2+ deletedCount_tDBOutput_2;
	nb_line_update_tDBOutput_2=nb_line_update_tDBOutput_2 + updatedCount_tDBOutput_2;
	nb_line_inserted_tDBOutput_2=nb_line_inserted_tDBOutput_2 + insertedCount_tDBOutput_2;
	nb_line_rejected_tDBOutput_2=nb_line_rejected_tDBOutput_2 + rejectedCount_tDBOutput_2;
	
        globalMap.put("tDBOutput_2_NB_LINE",nb_line_tDBOutput_2);
        globalMap.put("tDBOutput_2_NB_LINE_UPDATED",nb_line_update_tDBOutput_2);
        globalMap.put("tDBOutput_2_NB_LINE_INSERTED",nb_line_inserted_tDBOutput_2);
        globalMap.put("tDBOutput_2_NB_LINE_DELETED",nb_line_deleted_tDBOutput_2);
        globalMap.put("tDBOutput_2_NB_LINE_REJECTED", nb_line_rejected_tDBOutput_2);
    

	


				if(execStat){
			  		runStat.updateStat(resourceMap,iterateId,2,0,"row16");
			  	}
			  	
 

ok_Hash.put("tDBOutput_2", true);
end_Hash.put("tDBOutput_2", System.currentTimeMillis());




/**
 * [tDBOutput_2 end ] stop
 */

	
	/**
	 * [tFileOutputDelimited_3 end ] start
	 */

	

	
	
	currentComponent="tFileOutputDelimited_3";

	



		
			
					if(outtFileOutputDelimited_3!=null) {
						outtFileOutputDelimited_3.flush();
						outtFileOutputDelimited_3.close();
					}
				
				globalMap.put("tFileOutputDelimited_3_NB_LINE",nb_line_tFileOutputDelimited_3);
				globalMap.put("tFileOutputDelimited_3_FILE_NAME",fileName_tFileOutputDelimited_3);
			
		
		
		resourceMap.put("finish_tFileOutputDelimited_3", true);
	

				if(execStat){
			  		runStat.updateStat(resourceMap,iterateId,2,0,"row3");
			  	}
			  	
 

ok_Hash.put("tFileOutputDelimited_3", true);
end_Hash.put("tFileOutputDelimited_3", System.currentTimeMillis());




/**
 * [tFileOutputDelimited_3 end ] stop
 */
















	
	/**
	 * [tLogRow_3 end ] start
	 */

	

	
	
	currentComponent="tLogRow_3";

	


//////

                    
                    java.io.PrintStream consoleOut_tLogRow_3 = null;
                    if (globalMap.get("tLogRow_CONSOLE")!=null)
                    {
                    	consoleOut_tLogRow_3 = (java.io.PrintStream) globalMap.get("tLogRow_CONSOLE");
                    }
                    else
                    {
                    	consoleOut_tLogRow_3 = new java.io.PrintStream(new java.io.BufferedOutputStream(System.out));
                    	globalMap.put("tLogRow_CONSOLE",consoleOut_tLogRow_3);
                    }
                    
                    consoleOut_tLogRow_3.println(util_tLogRow_3.format().toString());
                    consoleOut_tLogRow_3.flush();
//////
globalMap.put("tLogRow_3_NB_LINE",nb_line_tLogRow_3);

///////////////////////    			

				if(execStat){
			  		runStat.updateStat(resourceMap,iterateId,2,0,"row13");
			  	}
			  	
 

ok_Hash.put("tLogRow_3", true);
end_Hash.put("tLogRow_3", System.currentTimeMillis());




/**
 * [tLogRow_3 end ] stop
 */

	
	/**
	 * [tMap_3 end ] start
	 */

	

	
	
	currentComponent="tMap_3";

	


// ###############################
// # Lookup hashes releasing
// ###############################      





				if(execStat){
			  		runStat.updateStat(resourceMap,iterateId,2,0,"row17");
			  	}
			  	
 

ok_Hash.put("tMap_3", true);
end_Hash.put("tMap_3", System.currentTimeMillis());




/**
 * [tMap_3 end ] stop
 */

	
	/**
	 * [tDBOutput_3 end ] start
	 */

	

	
	
	currentComponent="tDBOutput_3";

	



	    try {
				int countSum_tDBOutput_3 = 0;
				if (pstmt_tDBOutput_3 != null && batchSizeCounter_tDBOutput_3 > 0) {
						
					for(int countEach_tDBOutput_3: pstmt_tDBOutput_3.executeBatch()) {
						countSum_tDBOutput_3 += (countEach_tDBOutput_3 < 0 ? 0 : countEach_tDBOutput_3);
					}
					rowsToCommitCount_tDBOutput_3 += countSum_tDBOutput_3;
						
				}
		    	
		    		insertedCount_tDBOutput_3 += countSum_tDBOutput_3;
		    	
	    }catch (java.sql.BatchUpdateException e_tDBOutput_3){
globalMap.put("tDBOutput_3_ERROR_MESSAGE",e_tDBOutput_3.getMessage());
	    	java.sql.SQLException ne_tDBOutput_3 = e_tDBOutput_3.getNextException(),sqle_tDBOutput_3=null;
	    	String errormessage_tDBOutput_3;
			if (ne_tDBOutput_3 != null) {
				// build new exception to provide the original cause
				sqle_tDBOutput_3 = new java.sql.SQLException(e_tDBOutput_3.getMessage() + "\ncaused by: " + ne_tDBOutput_3.getMessage(), ne_tDBOutput_3.getSQLState(), ne_tDBOutput_3.getErrorCode(), ne_tDBOutput_3);
				errormessage_tDBOutput_3 = sqle_tDBOutput_3.getMessage();
			}else{
				errormessage_tDBOutput_3 = e_tDBOutput_3.getMessage();
			}
	    	
	    	int countSum_tDBOutput_3 = 0;
			for(int countEach_tDBOutput_3: e_tDBOutput_3.getUpdateCounts()) {
				countSum_tDBOutput_3 += (countEach_tDBOutput_3 < 0 ? 0 : countEach_tDBOutput_3);
			}
			rowsToCommitCount_tDBOutput_3 += countSum_tDBOutput_3;
			
	    		insertedCount_tDBOutput_3 += countSum_tDBOutput_3;
	    	
	    	System.err.println(errormessage_tDBOutput_3);
	    	
		}
	    
        if(pstmt_tDBOutput_3 != null) {
        		
            pstmt_tDBOutput_3.close();
            resourceMap.remove("pstmt_tDBOutput_3");
        }
    resourceMap.put("statementClosed_tDBOutput_3", true);
			if(rowsToCommitCount_tDBOutput_3 != 0){
				
			}
			conn_tDBOutput_3.commit();
			if(rowsToCommitCount_tDBOutput_3 != 0){
				
				rowsToCommitCount_tDBOutput_3 = 0;
			}
			commitCounter_tDBOutput_3 = 0;
		
    	conn_tDBOutput_3 .close();
    	
    	resourceMap.put("finish_tDBOutput_3", true);
    	

	nb_line_deleted_tDBOutput_3=nb_line_deleted_tDBOutput_3+ deletedCount_tDBOutput_3;
	nb_line_update_tDBOutput_3=nb_line_update_tDBOutput_3 + updatedCount_tDBOutput_3;
	nb_line_inserted_tDBOutput_3=nb_line_inserted_tDBOutput_3 + insertedCount_tDBOutput_3;
	nb_line_rejected_tDBOutput_3=nb_line_rejected_tDBOutput_3 + rejectedCount_tDBOutput_3;
	
        globalMap.put("tDBOutput_3_NB_LINE",nb_line_tDBOutput_3);
        globalMap.put("tDBOutput_3_NB_LINE_UPDATED",nb_line_update_tDBOutput_3);
        globalMap.put("tDBOutput_3_NB_LINE_INSERTED",nb_line_inserted_tDBOutput_3);
        globalMap.put("tDBOutput_3_NB_LINE_DELETED",nb_line_deleted_tDBOutput_3);
        globalMap.put("tDBOutput_3_NB_LINE_REJECTED", nb_line_rejected_tDBOutput_3);
    

	


				if(execStat){
			  		runStat.updateStat(resourceMap,iterateId,2,0,"vehicule_rejet_Schema");
			  	}
			  	
 

ok_Hash.put("tDBOutput_3", true);
end_Hash.put("tDBOutput_3", System.currentTimeMillis());




/**
 * [tDBOutput_3 end ] stop
 */

	
	/**
	 * [tFileOutputDelimited_2 end ] start
	 */

	

	
	
	currentComponent="tFileOutputDelimited_2";

	



		
			
					if(outtFileOutputDelimited_2!=null) {
						outtFileOutputDelimited_2.flush();
						outtFileOutputDelimited_2.close();
					}
				
				globalMap.put("tFileOutputDelimited_2_NB_LINE",nb_line_tFileOutputDelimited_2);
				globalMap.put("tFileOutputDelimited_2_FILE_NAME",fileName_tFileOutputDelimited_2);
			
		
		
		resourceMap.put("finish_tFileOutputDelimited_2", true);
	

				if(execStat){
			  		runStat.updateStat(resourceMap,iterateId,2,0,"row2");
			  	}
			  	
 

ok_Hash.put("tFileOutputDelimited_2", true);
end_Hash.put("tFileOutputDelimited_2", System.currentTimeMillis());




/**
 * [tFileOutputDelimited_2 end ] stop
 */
















	
	/**
	 * [tLogRow_1 end ] start
	 */

	

	
	
	currentComponent="tLogRow_1";

	


//////

                    
                    java.io.PrintStream consoleOut_tLogRow_1 = null;
                    if (globalMap.get("tLogRow_CONSOLE")!=null)
                    {
                    	consoleOut_tLogRow_1 = (java.io.PrintStream) globalMap.get("tLogRow_CONSOLE");
                    }
                    else
                    {
                    	consoleOut_tLogRow_1 = new java.io.PrintStream(new java.io.BufferedOutputStream(System.out));
                    	globalMap.put("tLogRow_CONSOLE",consoleOut_tLogRow_1);
                    }
                    
                    consoleOut_tLogRow_1.println(util_tLogRow_1.format().toString());
                    consoleOut_tLogRow_1.flush();
//////
globalMap.put("tLogRow_1_NB_LINE",nb_line_tLogRow_1);

///////////////////////    			

				if(execStat){
			  		runStat.updateStat(resourceMap,iterateId,2,0,"row11");
			  	}
			  	
 

ok_Hash.put("tLogRow_1", true);
end_Hash.put("tLogRow_1", System.currentTimeMillis());




/**
 * [tLogRow_1 end ] stop
 */

	
	/**
	 * [tMap_2 end ] start
	 */

	

	
	
	currentComponent="tMap_2";

	


// ###############################
// # Lookup hashes releasing
// ###############################      





				if(execStat){
			  		runStat.updateStat(resourceMap,iterateId,2,0,"row18");
			  	}
			  	
 

ok_Hash.put("tMap_2", true);
end_Hash.put("tMap_2", System.currentTimeMillis());




/**
 * [tMap_2 end ] stop
 */

	
	/**
	 * [tDBOutput_4 end ] start
	 */

	

	
	
	currentComponent="tDBOutput_4";

	



	    try {
				int countSum_tDBOutput_4 = 0;
				if (pstmt_tDBOutput_4 != null && batchSizeCounter_tDBOutput_4 > 0) {
						
					for(int countEach_tDBOutput_4: pstmt_tDBOutput_4.executeBatch()) {
						countSum_tDBOutput_4 += (countEach_tDBOutput_4 < 0 ? 0 : countEach_tDBOutput_4);
					}
					rowsToCommitCount_tDBOutput_4 += countSum_tDBOutput_4;
						
				}
		    	
		    		insertedCount_tDBOutput_4 += countSum_tDBOutput_4;
		    	
	    }catch (java.sql.BatchUpdateException e_tDBOutput_4){
globalMap.put("tDBOutput_4_ERROR_MESSAGE",e_tDBOutput_4.getMessage());
	    	java.sql.SQLException ne_tDBOutput_4 = e_tDBOutput_4.getNextException(),sqle_tDBOutput_4=null;
	    	String errormessage_tDBOutput_4;
			if (ne_tDBOutput_4 != null) {
				// build new exception to provide the original cause
				sqle_tDBOutput_4 = new java.sql.SQLException(e_tDBOutput_4.getMessage() + "\ncaused by: " + ne_tDBOutput_4.getMessage(), ne_tDBOutput_4.getSQLState(), ne_tDBOutput_4.getErrorCode(), ne_tDBOutput_4);
				errormessage_tDBOutput_4 = sqle_tDBOutput_4.getMessage();
			}else{
				errormessage_tDBOutput_4 = e_tDBOutput_4.getMessage();
			}
	    	
	    	int countSum_tDBOutput_4 = 0;
			for(int countEach_tDBOutput_4: e_tDBOutput_4.getUpdateCounts()) {
				countSum_tDBOutput_4 += (countEach_tDBOutput_4 < 0 ? 0 : countEach_tDBOutput_4);
			}
			rowsToCommitCount_tDBOutput_4 += countSum_tDBOutput_4;
			
	    		insertedCount_tDBOutput_4 += countSum_tDBOutput_4;
	    	
	    	System.err.println(errormessage_tDBOutput_4);
	    	
		}
	    
        if(pstmt_tDBOutput_4 != null) {
        		
            pstmt_tDBOutput_4.close();
            resourceMap.remove("pstmt_tDBOutput_4");
        }
    resourceMap.put("statementClosed_tDBOutput_4", true);
			if(rowsToCommitCount_tDBOutput_4 != 0){
				
			}
			conn_tDBOutput_4.commit();
			if(rowsToCommitCount_tDBOutput_4 != 0){
				
				rowsToCommitCount_tDBOutput_4 = 0;
			}
			commitCounter_tDBOutput_4 = 0;
		
    	conn_tDBOutput_4 .close();
    	
    	resourceMap.put("finish_tDBOutput_4", true);
    	

	nb_line_deleted_tDBOutput_4=nb_line_deleted_tDBOutput_4+ deletedCount_tDBOutput_4;
	nb_line_update_tDBOutput_4=nb_line_update_tDBOutput_4 + updatedCount_tDBOutput_4;
	nb_line_inserted_tDBOutput_4=nb_line_inserted_tDBOutput_4 + insertedCount_tDBOutput_4;
	nb_line_rejected_tDBOutput_4=nb_line_rejected_tDBOutput_4 + rejectedCount_tDBOutput_4;
	
        globalMap.put("tDBOutput_4_NB_LINE",nb_line_tDBOutput_4);
        globalMap.put("tDBOutput_4_NB_LINE_UPDATED",nb_line_update_tDBOutput_4);
        globalMap.put("tDBOutput_4_NB_LINE_INSERTED",nb_line_inserted_tDBOutput_4);
        globalMap.put("tDBOutput_4_NB_LINE_DELETED",nb_line_deleted_tDBOutput_4);
        globalMap.put("tDBOutput_4_NB_LINE_REJECTED", nb_line_rejected_tDBOutput_4);
    

	


				if(execStat){
			  		runStat.updateStat(resourceMap,iterateId,2,0,"vehicule_rejet_structure");
			  	}
			  	
 

ok_Hash.put("tDBOutput_4", true);
end_Hash.put("tDBOutput_4", System.currentTimeMillis());




/**
 * [tDBOutput_4 end ] stop
 */

	
	/**
	 * [tFileOutputDelimited_1 end ] start
	 */

	

	
	
	currentComponent="tFileOutputDelimited_1";

	



		
			
					if(outtFileOutputDelimited_1!=null) {
						outtFileOutputDelimited_1.flush();
						outtFileOutputDelimited_1.close();
					}
				
				globalMap.put("tFileOutputDelimited_1_NB_LINE",nb_line_tFileOutputDelimited_1);
				globalMap.put("tFileOutputDelimited_1_FILE_NAME",fileName_tFileOutputDelimited_1);
			
		
		
		resourceMap.put("finish_tFileOutputDelimited_1", true);
	

				if(execStat){
			  		runStat.updateStat(resourceMap,iterateId,2,0,"row1");
			  	}
			  	
 

ok_Hash.put("tFileOutputDelimited_1", true);
end_Hash.put("tFileOutputDelimited_1", System.currentTimeMillis());




/**
 * [tFileOutputDelimited_1 end ] stop
 */












						if(execStat){
							runStat.updateStatOnConnection("iterate1", 2, "exec" + NB_ITERATE_tFileInputDelimited_1);
						}				
					




	
	/**
	 * [tFileList_1 process_data_end ] start
	 */

	

	
	
	currentComponent="tFileList_1";

	

 



/**
 * [tFileList_1 process_data_end ] stop
 */
	
	/**
	 * [tFileList_1 end ] start
	 */

	

	
	
	currentComponent="tFileList_1";

	

  
    }
  globalMap.put("tFileList_1_NB_FILE", NB_FILEtFileList_1);
  

  
 

 

ok_Hash.put("tFileList_1", true);
end_Hash.put("tFileList_1", System.currentTimeMillis());




/**
 * [tFileList_1 end ] stop
 */
				}//end the resume

				



	
			}catch(java.lang.Exception e){	
				
				TalendException te = new TalendException(e, currentComponent, globalMap);
				
				throw te;
			}catch(java.lang.Error error){	
				
					runStat.stopThreadStat();
				
				throw error;
			}finally{
				
				try{
					
	
	/**
	 * [tFileList_1 finally ] start
	 */

	

	
	
	currentComponent="tFileList_1";

	

 



/**
 * [tFileList_1 finally ] stop
 */

	
	/**
	 * [tFileInputDelimited_1 finally ] start
	 */

	

	
	
	currentComponent="tFileInputDelimited_1";

	

 



/**
 * [tFileInputDelimited_1 finally ] stop
 */

	
	/**
	 * [tSchemaComplianceCheck_1 finally ] start
	 */

	

	
	
	currentComponent="tSchemaComplianceCheck_1";

	

 



/**
 * [tSchemaComplianceCheck_1 finally ] stop
 */

	
	/**
	 * [tLogRow_2 finally ] start
	 */

	

	
	
	currentComponent="tLogRow_2";

	

 



/**
 * [tLogRow_2 finally ] stop
 */

	
	/**
	 * [tMap_1 finally ] start
	 */

	

	
	
	currentComponent="tMap_1";

	

 



/**
 * [tMap_1 finally ] stop
 */

	
	/**
	 * [tLogRow_4 finally ] start
	 */

	

	
	
	currentComponent="tLogRow_4";

	

 



/**
 * [tLogRow_4 finally ] stop
 */

	
	/**
	 * [tDBOutput_1 finally ] start
	 */

	

	
	
	currentComponent="tDBOutput_1";

	



    try {
    if (resourceMap.get("statementClosed_tDBOutput_1") == null) {
                java.sql.PreparedStatement pstmtUpdateToClose_tDBOutput_1 = null;
                if ((pstmtUpdateToClose_tDBOutput_1 = (java.sql.PreparedStatement) resourceMap.remove("pstmtUpdate_tDBOutput_1")) != null) {
                    pstmtUpdateToClose_tDBOutput_1.close();
                }
                java.sql.PreparedStatement pstmtInsertToClose_tDBOutput_1 = null;
                if ((pstmtInsertToClose_tDBOutput_1 = (java.sql.PreparedStatement) resourceMap.remove("pstmtInsert_tDBOutput_1")) != null) {
                    pstmtInsertToClose_tDBOutput_1.close();
                }
                java.sql.PreparedStatement pstmtToClose_tDBOutput_1 = null;
                if ((pstmtToClose_tDBOutput_1 = (java.sql.PreparedStatement) resourceMap.remove("pstmt_tDBOutput_1")) != null) {
                    pstmtToClose_tDBOutput_1.close();
                }
    }
    } finally {
        if(resourceMap.get("finish_tDBOutput_1") == null){
            java.sql.Connection ctn_tDBOutput_1 = null;
            if((ctn_tDBOutput_1 = (java.sql.Connection)resourceMap.get("conn_tDBOutput_1")) != null){
                try {
                    ctn_tDBOutput_1.close();
                } catch (java.sql.SQLException sqlEx_tDBOutput_1) {
                    String errorMessage_tDBOutput_1 = "failed to close the connection in tDBOutput_1 :" + sqlEx_tDBOutput_1.getMessage();
                    System.err.println(errorMessage_tDBOutput_1);
                }
            }
        }
    }
 



/**
 * [tDBOutput_1 finally ] stop
 */







	
	/**
	 * [tLogRow_5 finally ] start
	 */

	

	
	
	currentComponent="tLogRow_5";

	

 



/**
 * [tLogRow_5 finally ] stop
 */

	
	/**
	 * [tDBOutput_2 finally ] start
	 */

	

	
	
	currentComponent="tDBOutput_2";

	



    try {
    if (resourceMap.get("statementClosed_tDBOutput_2") == null) {
                java.sql.PreparedStatement pstmtToClose_tDBOutput_2 = null;
                if ((pstmtToClose_tDBOutput_2 = (java.sql.PreparedStatement) resourceMap.remove("pstmt_tDBOutput_2")) != null) {
                    pstmtToClose_tDBOutput_2.close();
                }
    }
    } finally {
        if(resourceMap.get("finish_tDBOutput_2") == null){
            java.sql.Connection ctn_tDBOutput_2 = null;
            if((ctn_tDBOutput_2 = (java.sql.Connection)resourceMap.get("conn_tDBOutput_2")) != null){
                try {
                    ctn_tDBOutput_2.close();
                } catch (java.sql.SQLException sqlEx_tDBOutput_2) {
                    String errorMessage_tDBOutput_2 = "failed to close the connection in tDBOutput_2 :" + sqlEx_tDBOutput_2.getMessage();
                    System.err.println(errorMessage_tDBOutput_2);
                }
            }
        }
    }
 



/**
 * [tDBOutput_2 finally ] stop
 */

	
	/**
	 * [tFileOutputDelimited_3 finally ] start
	 */

	

	
	
	currentComponent="tFileOutputDelimited_3";

	


		if(resourceMap.get("finish_tFileOutputDelimited_3") == null){ 
			
				
						java.io.Writer outtFileOutputDelimited_3 = (java.io.Writer)resourceMap.get("out_tFileOutputDelimited_3");
						if(outtFileOutputDelimited_3!=null) {
							outtFileOutputDelimited_3.flush();
							outtFileOutputDelimited_3.close();
						}
					
				
			
		}
	

 



/**
 * [tFileOutputDelimited_3 finally ] stop
 */
















	
	/**
	 * [tLogRow_3 finally ] start
	 */

	

	
	
	currentComponent="tLogRow_3";

	

 



/**
 * [tLogRow_3 finally ] stop
 */

	
	/**
	 * [tMap_3 finally ] start
	 */

	

	
	
	currentComponent="tMap_3";

	

 



/**
 * [tMap_3 finally ] stop
 */

	
	/**
	 * [tDBOutput_3 finally ] start
	 */

	

	
	
	currentComponent="tDBOutput_3";

	



    try {
    if (resourceMap.get("statementClosed_tDBOutput_3") == null) {
                java.sql.PreparedStatement pstmtToClose_tDBOutput_3 = null;
                if ((pstmtToClose_tDBOutput_3 = (java.sql.PreparedStatement) resourceMap.remove("pstmt_tDBOutput_3")) != null) {
                    pstmtToClose_tDBOutput_3.close();
                }
    }
    } finally {
        if(resourceMap.get("finish_tDBOutput_3") == null){
            java.sql.Connection ctn_tDBOutput_3 = null;
            if((ctn_tDBOutput_3 = (java.sql.Connection)resourceMap.get("conn_tDBOutput_3")) != null){
                try {
                    ctn_tDBOutput_3.close();
                } catch (java.sql.SQLException sqlEx_tDBOutput_3) {
                    String errorMessage_tDBOutput_3 = "failed to close the connection in tDBOutput_3 :" + sqlEx_tDBOutput_3.getMessage();
                    System.err.println(errorMessage_tDBOutput_3);
                }
            }
        }
    }
 



/**
 * [tDBOutput_3 finally ] stop
 */

	
	/**
	 * [tFileOutputDelimited_2 finally ] start
	 */

	

	
	
	currentComponent="tFileOutputDelimited_2";

	


		if(resourceMap.get("finish_tFileOutputDelimited_2") == null){ 
			
				
						java.io.Writer outtFileOutputDelimited_2 = (java.io.Writer)resourceMap.get("out_tFileOutputDelimited_2");
						if(outtFileOutputDelimited_2!=null) {
							outtFileOutputDelimited_2.flush();
							outtFileOutputDelimited_2.close();
						}
					
				
			
		}
	

 



/**
 * [tFileOutputDelimited_2 finally ] stop
 */
















	
	/**
	 * [tLogRow_1 finally ] start
	 */

	

	
	
	currentComponent="tLogRow_1";

	

 



/**
 * [tLogRow_1 finally ] stop
 */

	
	/**
	 * [tMap_2 finally ] start
	 */

	

	
	
	currentComponent="tMap_2";

	

 



/**
 * [tMap_2 finally ] stop
 */

	
	/**
	 * [tDBOutput_4 finally ] start
	 */

	

	
	
	currentComponent="tDBOutput_4";

	



    try {
    if (resourceMap.get("statementClosed_tDBOutput_4") == null) {
                java.sql.PreparedStatement pstmtToClose_tDBOutput_4 = null;
                if ((pstmtToClose_tDBOutput_4 = (java.sql.PreparedStatement) resourceMap.remove("pstmt_tDBOutput_4")) != null) {
                    pstmtToClose_tDBOutput_4.close();
                }
    }
    } finally {
        if(resourceMap.get("finish_tDBOutput_4") == null){
            java.sql.Connection ctn_tDBOutput_4 = null;
            if((ctn_tDBOutput_4 = (java.sql.Connection)resourceMap.get("conn_tDBOutput_4")) != null){
                try {
                    ctn_tDBOutput_4.close();
                } catch (java.sql.SQLException sqlEx_tDBOutput_4) {
                    String errorMessage_tDBOutput_4 = "failed to close the connection in tDBOutput_4 :" + sqlEx_tDBOutput_4.getMessage();
                    System.err.println(errorMessage_tDBOutput_4);
                }
            }
        }
    }
 



/**
 * [tDBOutput_4 finally ] stop
 */

	
	/**
	 * [tFileOutputDelimited_1 finally ] start
	 */

	

	
	
	currentComponent="tFileOutputDelimited_1";

	


		if(resourceMap.get("finish_tFileOutputDelimited_1") == null){ 
			
				
						java.io.Writer outtFileOutputDelimited_1 = (java.io.Writer)resourceMap.get("out_tFileOutputDelimited_1");
						if(outtFileOutputDelimited_1!=null) {
							outtFileOutputDelimited_1.flush();
							outtFileOutputDelimited_1.close();
						}
					
				
			
		}
	

 



/**
 * [tFileOutputDelimited_1 finally ] stop
 */















				}catch(java.lang.Exception e){	
					//ignore
				}catch(java.lang.Error error){
					//ignore
				}
				resourceMap = null;
			}
		

		globalMap.put("tFileList_1_SUBPROCESS_STATE", 1);
	}
	

public void tFileCopy_1Process(final java.util.Map<String, Object> globalMap) throws TalendException {
	globalMap.put("tFileCopy_1_SUBPROCESS_STATE", 0);

 final boolean execStat = this.execStat;
	
		String iterateId = "";
	
	
	String currentComponent = "";
	java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

	try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { //start the resume
				globalResumeTicket = true;





	
	/**
	 * [tFileCopy_1 begin ] start
	 */

	

	
		
		ok_Hash.put("tFileCopy_1", false);
		start_Hash.put("tFileCopy_1", System.currentTimeMillis());
		
	
	currentComponent="tFileCopy_1";

	
		int tos_count_tFileCopy_1 = 0;
		

 



/**
 * [tFileCopy_1 begin ] stop
 */
	
	/**
	 * [tFileCopy_1 main ] start
	 */

	

	
	
	currentComponent="tFileCopy_1";

	

 


        String srcFileName_tFileCopy_1 = ((String) globalMap.get("tFileList_1_CURRENT_FILEPATH"));

		java.io.File srcFile_tFileCopy_1 = new java.io.File(srcFileName_tFileCopy_1);

		// here need check first, before mkdirs().
		if (!srcFile_tFileCopy_1.exists() || !srcFile_tFileCopy_1.isFile()) {
			String errorMessageFileDoesnotExistsOrIsNotAFile_tFileCopy_1 = String.format("The source File \"%s\" does not exist or is not a file.", srcFileName_tFileCopy_1);
				System.err.println(errorMessageFileDoesnotExistsOrIsNotAFile_tFileCopy_1);
				globalMap.put("tFileCopy_1_ERROR_MESSAGE", errorMessageFileDoesnotExistsOrIsNotAFile_tFileCopy_1);
		}
        String desDirName_tFileCopy_1 = "/var/hubasac/archives/vehicule";

		String desFileName_tFileCopy_1 =  srcFile_tFileCopy_1.getName() ;

		if (desFileName_tFileCopy_1 != null && ("").equals(desFileName_tFileCopy_1.trim())){
			desFileName_tFileCopy_1 = "NewName.temp";
		}

		java.io.File desFile_tFileCopy_1 = new java.io.File(desDirName_tFileCopy_1, desFileName_tFileCopy_1);

		if (!srcFile_tFileCopy_1.getPath().equals(desFile_tFileCopy_1.getPath())  ) {
				java.io.File parentFile_tFileCopy_1 = desFile_tFileCopy_1.getParentFile();

				if (parentFile_tFileCopy_1 != null && !parentFile_tFileCopy_1.exists()) {
					parentFile_tFileCopy_1.mkdirs();
				}           
				try {
					org.talend.FileCopy.copyFile(srcFile_tFileCopy_1.getPath(), desFile_tFileCopy_1.getPath(), true, false);
				} catch (Exception e) {
globalMap.put("tFileCopy_1_ERROR_MESSAGE",e.getMessage());
						System.err.println("tFileCopy_1 " + e.getMessage());
				}
				java.io.File isRemoved_tFileCopy_1 = new java.io.File(((String) globalMap.get("tFileList_1_CURRENT_FILEPATH")));
				if(isRemoved_tFileCopy_1.exists()) {
					String errorMessageCouldNotRemoveFile_tFileCopy_1 = String.format("tFileCopy_1 - The source file \"%s\" could not be removed from the folder because it is open or you only have read-only rights.", srcFileName_tFileCopy_1);
						System.err.println(errorMessageCouldNotRemoveFile_tFileCopy_1 + "\n");
						globalMap.put("tFileCopy_1_ERROR_MESSAGE", errorMessageCouldNotRemoveFile_tFileCopy_1);
				} 

		}
		globalMap.put("tFileCopy_1_DESTINATION_FILEPATH",desFile_tFileCopy_1.getPath()); 
		globalMap.put("tFileCopy_1_DESTINATION_FILENAME",desFile_tFileCopy_1.getName()); 

		globalMap.put("tFileCopy_1_SOURCE_DIRECTORY", srcFile_tFileCopy_1.getParent());
		globalMap.put("tFileCopy_1_DESTINATION_DIRECTORY", desFile_tFileCopy_1.getParent());

 


	tos_count_tFileCopy_1++;

/**
 * [tFileCopy_1 main ] stop
 */
	
	/**
	 * [tFileCopy_1 process_data_begin ] start
	 */

	

	
	
	currentComponent="tFileCopy_1";

	

 



/**
 * [tFileCopy_1 process_data_begin ] stop
 */
	
	/**
	 * [tFileCopy_1 process_data_end ] start
	 */

	

	
	
	currentComponent="tFileCopy_1";

	

 



/**
 * [tFileCopy_1 process_data_end ] stop
 */
	
	/**
	 * [tFileCopy_1 end ] start
	 */

	

	
	
	currentComponent="tFileCopy_1";

	

 

ok_Hash.put("tFileCopy_1", true);
end_Hash.put("tFileCopy_1", System.currentTimeMillis());




/**
 * [tFileCopy_1 end ] stop
 */
				}//end the resume

				



	
			}catch(java.lang.Exception e){	
				
				TalendException te = new TalendException(e, currentComponent, globalMap);
				
				throw te;
			}catch(java.lang.Error error){	
				
					runStat.stopThreadStat();
				
				throw error;
			}finally{
				
				try{
					
	
	/**
	 * [tFileCopy_1 finally ] start
	 */

	

	
	
	currentComponent="tFileCopy_1";

	

 



/**
 * [tFileCopy_1 finally ] stop
 */
				}catch(java.lang.Exception e){	
					//ignore
				}catch(java.lang.Error error){
					//ignore
				}
				resourceMap = null;
			}
		

		globalMap.put("tFileCopy_1_SUBPROCESS_STATE", 1);
	}
	
    public String resuming_logs_dir_path = null;
    public String resuming_checkpoint_path = null;
    public String parent_part_launcher = null;
    private String resumeEntryMethodName = null;
    private boolean globalResumeTicket = false;

    public boolean watch = false;
    // portStats is null, it means don't execute the statistics
    public Integer portStats = null;
    public int portTraces = 4334;
    public String clientHost;
    public String defaultClientHost = "localhost";
    public String contextStr = "Default";
    public boolean isDefaultContext = true;
    public String pid = "0";
    public String rootPid = null;
    public String fatherPid = null;
    public String fatherNode = null;
    public long startTime = 0;
    public boolean isChildJob = false;
    public String log4jLevel = "";
    
    private boolean enableLogStash;

    private boolean execStat = true;

    private ThreadLocal<java.util.Map<String, String>> threadLocal = new ThreadLocal<java.util.Map<String, String>>() {
        protected java.util.Map<String, String> initialValue() {
            java.util.Map<String,String> threadRunResultMap = new java.util.HashMap<String, String>();
            threadRunResultMap.put("errorCode", null);
            threadRunResultMap.put("status", "");
            return threadRunResultMap;
        };
    };


    protected PropertiesWithType context_param = new PropertiesWithType();
    public java.util.Map<String, Object> parentContextMap = new java.util.HashMap<String, Object>();

    public String status= "";
    

    public static void main(String[] args){
        final extract_vehicule extract_vehiculeClass = new extract_vehicule();

        int exitCode = extract_vehiculeClass.runJobInTOS(args);

        System.exit(exitCode);
    }


    public String[][] runJob(String[] args) {

        int exitCode = runJobInTOS(args);
        String[][] bufferValue = new String[][] { { Integer.toString(exitCode) } };

        return bufferValue;
    }

    public boolean hastBufferOutputComponent() {
		boolean hastBufferOutput = false;
    	
        return hastBufferOutput;
    }

    public int runJobInTOS(String[] args) {
	   	// reset status
	   	status = "";
	   	
        String lastStr = "";
        for (String arg : args) {
            if (arg.equalsIgnoreCase("--context_param")) {
                lastStr = arg;
            } else if (lastStr.equals("")) {
                evalParam(arg);
            } else {
                evalParam(lastStr + " " + arg);
                lastStr = "";
            }
        }
        enableLogStash = "true".equalsIgnoreCase(System.getProperty("audit.enabled"));

    	
    	

        if(clientHost == null) {
            clientHost = defaultClientHost;
        }

        if(pid == null || "0".equals(pid)) {
            pid = TalendString.getAsciiRandomString(6);
        }

        if (rootPid==null) {
            rootPid = pid;
        }
        if (fatherPid==null) {
            fatherPid = pid;
        }else{
            isChildJob = true;
        }

        if (portStats != null) {
            // portStats = -1; //for testing
            if (portStats < 0 || portStats > 65535) {
                // issue:10869, the portStats is invalid, so this client socket can't open
                System.err.println("The statistics socket port " + portStats + " is invalid.");
                execStat = false;
            }
        } else {
            execStat = false;
        }
        boolean inOSGi = routines.system.BundleUtils.inOSGi();

        if (inOSGi) {
            java.util.Dictionary<String, Object> jobProperties = routines.system.BundleUtils.getJobProperties(jobName);

            if (jobProperties != null && jobProperties.get("context") != null) {
                contextStr = (String)jobProperties.get("context");
            }
        }

        try {
            //call job/subjob with an existing context, like: --context=production. if without this parameter, there will use the default context instead.
            java.io.InputStream inContext = extract_vehicule.class.getClassLoader().getResourceAsStream("extracteur_hubasac_backup/extract_vehicule_1_5/contexts/" + contextStr + ".properties");
            if (inContext == null) {
                inContext = extract_vehicule.class.getClassLoader().getResourceAsStream("config/contexts/" + contextStr + ".properties");
            }
            if (inContext != null) {
                try {
                    //defaultProps is in order to keep the original context value
                    if(context != null && context.isEmpty()) {
	                defaultProps.load(inContext);
	                context = new ContextProperties(defaultProps);
                    }
                } finally {
                    inContext.close();
                }
            } else if (!isDefaultContext) {
                //print info and job continue to run, for case: context_param is not empty.
                System.err.println("Could not find the context " + contextStr);
            }

            if(!context_param.isEmpty()) {
                context.putAll(context_param);
				//set types for params from parentJobs
				for (Object key: context_param.keySet()){
					String context_key = key.toString();
					String context_type = context_param.getContextType(context_key);
					context.setContextType(context_key, context_type);

				}
            }
            class ContextProcessing {
                private void processContext_0() {
                        context.setContextType("vehicule_RowSeparator", "id_String");
                        if(context.getStringValue("vehicule_RowSeparator") == null) {
                            context.vehicule_RowSeparator = null;
                        } else {
                            context.vehicule_RowSeparator=(String) context.getProperty("vehicule_RowSeparator");
                        }
                        context.setContextType("vehicule_Header", "id_Integer");
                        if(context.getStringValue("vehicule_Header") == null) {
                            context.vehicule_Header = null;
                        } else {
                            try{
                                context.vehicule_Header=routines.system.ParserUtils.parseTo_Integer (context.getProperty("vehicule_Header"));
                            } catch(NumberFormatException e){
                                System.err.println(String.format("Null value will be used for context parameter %s: %s", "vehicule_Header", e.getMessage()));
                                context.vehicule_Header=null;
                            }
                        }
                        context.setContextType("vehicule_File", "id_File");
                        if(context.getStringValue("vehicule_File") == null) {
                            context.vehicule_File = null;
                        } else {
                            context.vehicule_File=(String) context.getProperty("vehicule_File");
                        }
                        context.setContextType("vehicule_FieldSeparator", "id_String");
                        if(context.getStringValue("vehicule_FieldSeparator") == null) {
                            context.vehicule_FieldSeparator = null;
                        } else {
                            context.vehicule_FieldSeparator=(String) context.getProperty("vehicule_FieldSeparator");
                        }
                        context.setContextType("vehicule_Encoding", "id_String");
                        if(context.getStringValue("vehicule_Encoding") == null) {
                            context.vehicule_Encoding = null;
                        } else {
                            context.vehicule_Encoding=(String) context.getProperty("vehicule_Encoding");
                        }
                        context.setContextType("pg_connexion_Port", "id_String");
                        if(context.getStringValue("pg_connexion_Port") == null) {
                            context.pg_connexion_Port = null;
                        } else {
                            context.pg_connexion_Port=(String) context.getProperty("pg_connexion_Port");
                        }
                        context.setContextType("pg_connexion_Login", "id_String");
                        if(context.getStringValue("pg_connexion_Login") == null) {
                            context.pg_connexion_Login = null;
                        } else {
                            context.pg_connexion_Login=(String) context.getProperty("pg_connexion_Login");
                        }
                        context.setContextType("pg_connexion_AdditionalParams", "id_String");
                        if(context.getStringValue("pg_connexion_AdditionalParams") == null) {
                            context.pg_connexion_AdditionalParams = null;
                        } else {
                            context.pg_connexion_AdditionalParams=(String) context.getProperty("pg_connexion_AdditionalParams");
                        }
                        context.setContextType("pg_connexion_Schema", "id_String");
                        if(context.getStringValue("pg_connexion_Schema") == null) {
                            context.pg_connexion_Schema = null;
                        } else {
                            context.pg_connexion_Schema=(String) context.getProperty("pg_connexion_Schema");
                        }
                        context.setContextType("pg_connexion_Server", "id_String");
                        if(context.getStringValue("pg_connexion_Server") == null) {
                            context.pg_connexion_Server = null;
                        } else {
                            context.pg_connexion_Server=(String) context.getProperty("pg_connexion_Server");
                        }
                        context.setContextType("pg_connexion_Password", "id_Password");
                        if(context.getStringValue("pg_connexion_Password") == null) {
                            context.pg_connexion_Password = null;
                        } else {
                            String pwd_pg_connexion_Password_value = context.getProperty("pg_connexion_Password");
                            context.pg_connexion_Password = null;
                            if(pwd_pg_connexion_Password_value!=null) {
                                if(context_param.containsKey("pg_connexion_Password")) {//no need to decrypt if it come from program argument or parent job runtime
                                    context.pg_connexion_Password = pwd_pg_connexion_Password_value;
                                } else if (!pwd_pg_connexion_Password_value.isEmpty()) {
                                    try {
                                        context.pg_connexion_Password = routines.system.PasswordEncryptUtil.decryptPassword(pwd_pg_connexion_Password_value);
                                        context.put("pg_connexion_Password",context.pg_connexion_Password);
                                    } catch (java.lang.RuntimeException e) {
                                        //do nothing
                                    }
                                }
                            }
                        }
                        context.setContextType("pg_connexion_Database", "id_String");
                        if(context.getStringValue("pg_connexion_Database") == null) {
                            context.pg_connexion_Database = null;
                        } else {
                            context.pg_connexion_Database=(String) context.getProperty("pg_connexion_Database");
                        }
                } 
                public void processAllContext() {
                        processContext_0();
                }
            }

            new ContextProcessing().processAllContext();
        } catch (java.io.IOException ie) {
            System.err.println("Could not load context "+contextStr);
            ie.printStackTrace();
        }

        // get context value from parent directly
        if (parentContextMap != null && !parentContextMap.isEmpty()) {if (parentContextMap.containsKey("vehicule_RowSeparator")) {
                context.vehicule_RowSeparator = (String) parentContextMap.get("vehicule_RowSeparator");
            }if (parentContextMap.containsKey("vehicule_Header")) {
                context.vehicule_Header = (Integer) parentContextMap.get("vehicule_Header");
            }if (parentContextMap.containsKey("vehicule_File")) {
                context.vehicule_File = (String) parentContextMap.get("vehicule_File");
            }if (parentContextMap.containsKey("vehicule_FieldSeparator")) {
                context.vehicule_FieldSeparator = (String) parentContextMap.get("vehicule_FieldSeparator");
            }if (parentContextMap.containsKey("vehicule_Encoding")) {
                context.vehicule_Encoding = (String) parentContextMap.get("vehicule_Encoding");
            }if (parentContextMap.containsKey("pg_connexion_Port")) {
                context.pg_connexion_Port = (String) parentContextMap.get("pg_connexion_Port");
            }if (parentContextMap.containsKey("pg_connexion_Login")) {
                context.pg_connexion_Login = (String) parentContextMap.get("pg_connexion_Login");
            }if (parentContextMap.containsKey("pg_connexion_AdditionalParams")) {
                context.pg_connexion_AdditionalParams = (String) parentContextMap.get("pg_connexion_AdditionalParams");
            }if (parentContextMap.containsKey("pg_connexion_Schema")) {
                context.pg_connexion_Schema = (String) parentContextMap.get("pg_connexion_Schema");
            }if (parentContextMap.containsKey("pg_connexion_Server")) {
                context.pg_connexion_Server = (String) parentContextMap.get("pg_connexion_Server");
            }if (parentContextMap.containsKey("pg_connexion_Password")) {
                context.pg_connexion_Password = (java.lang.String) parentContextMap.get("pg_connexion_Password");
            }if (parentContextMap.containsKey("pg_connexion_Database")) {
                context.pg_connexion_Database = (String) parentContextMap.get("pg_connexion_Database");
            }
        }

        //Resume: init the resumeUtil
        resumeEntryMethodName = ResumeUtil.getResumeEntryMethodName(resuming_checkpoint_path);
        resumeUtil = new ResumeUtil(resuming_logs_dir_path, isChildJob, rootPid);
        resumeUtil.initCommonInfo(pid, rootPid, fatherPid, projectName, jobName, contextStr, jobVersion);

		List<String> parametersToEncrypt = new java.util.ArrayList<String>();
			parametersToEncrypt.add("pg_connexion_Password");
        //Resume: jobStart
        resumeUtil.addLog("JOB_STARTED", "JOB:" + jobName, parent_part_launcher, Thread.currentThread().getId() + "", "","","","",resumeUtil.convertToJsonText(context,parametersToEncrypt));

if(execStat) {
    try {
        runStat.openSocket(!isChildJob);
        runStat.setAllPID(rootPid, fatherPid, pid, jobName);
        runStat.startThreadStat(clientHost, portStats);
        runStat.updateStatOnJob(RunStat.JOBSTART, fatherNode);
    } catch (java.io.IOException ioException) {
        ioException.printStackTrace();
    }
}



	
	    java.util.concurrent.ConcurrentHashMap<Object, Object> concurrentHashMap = new java.util.concurrent.ConcurrentHashMap<Object, Object>();
	    globalMap.put("concurrentHashMap", concurrentHashMap);
	

    long startUsedMemory = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
    long endUsedMemory = 0;
    long end = 0;

    startTime = System.currentTimeMillis();


this.globalResumeTicket = true;//to run tPreJob





this.globalResumeTicket = false;//to run others jobs

try {
errorCode = null;tFileList_1Process(globalMap);
if(!"failure".equals(status)) { status = "end"; }
}catch (TalendException e_tFileList_1) {
globalMap.put("tFileList_1_SUBPROCESS_STATE", -1);

e_tFileList_1.printStackTrace();

}

this.globalResumeTicket = true;//to run tPostJob




        end = System.currentTimeMillis();

        if (watch) {
            System.out.println((end-startTime)+" milliseconds");
        }

        endUsedMemory = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
        if (false) {
            System.out.println((endUsedMemory - startUsedMemory) + " bytes memory increase when running : extract_vehicule");
        }



if (execStat) {
    runStat.updateStatOnJob(RunStat.JOBEND, fatherNode);
    runStat.stopThreadStat();
}
    int returnCode = 0;


    if(errorCode == null) {
         returnCode = status != null && status.equals("failure") ? 1 : 0;
    } else {
         returnCode = errorCode.intValue();
    }
    resumeUtil.addLog("JOB_ENDED", "JOB:" + jobName, parent_part_launcher, Thread.currentThread().getId() + "", "","" + returnCode,"","","");

    return returnCode;

  }

    // only for OSGi env
    public void destroy() {


    }














    private java.util.Map<String, Object> getSharedConnections4REST() {
        java.util.Map<String, Object> connections = new java.util.HashMap<String, Object>();






        return connections;
    }

    private void evalParam(String arg) {
        if (arg.startsWith("--resuming_logs_dir_path")) {
            resuming_logs_dir_path = arg.substring(25);
        } else if (arg.startsWith("--resuming_checkpoint_path")) {
            resuming_checkpoint_path = arg.substring(27);
        } else if (arg.startsWith("--parent_part_launcher")) {
            parent_part_launcher = arg.substring(23);
        } else if (arg.startsWith("--watch")) {
            watch = true;
        } else if (arg.startsWith("--stat_port=")) {
            String portStatsStr = arg.substring(12);
            if (portStatsStr != null && !portStatsStr.equals("null")) {
                portStats = Integer.parseInt(portStatsStr);
            }
        } else if (arg.startsWith("--trace_port=")) {
            portTraces = Integer.parseInt(arg.substring(13));
        } else if (arg.startsWith("--client_host=")) {
            clientHost = arg.substring(14);
        } else if (arg.startsWith("--context=")) {
            contextStr = arg.substring(10);
            isDefaultContext = false;
        } else if (arg.startsWith("--father_pid=")) {
            fatherPid = arg.substring(13);
        } else if (arg.startsWith("--root_pid=")) {
            rootPid = arg.substring(11);
        } else if (arg.startsWith("--father_node=")) {
            fatherNode = arg.substring(14);
        } else if (arg.startsWith("--pid=")) {
            pid = arg.substring(6);
        } else if (arg.startsWith("--context_type")) {
            String keyValue = arg.substring(15);
			int index = -1;
            if (keyValue != null && (index = keyValue.indexOf('=')) > -1) {
                if (fatherPid==null) {
                    context_param.setContextType(keyValue.substring(0, index), replaceEscapeChars(keyValue.substring(index + 1)));
                } else { // the subjob won't escape the especial chars
                    context_param.setContextType(keyValue.substring(0, index), keyValue.substring(index + 1) );
                }

            }

		} else if (arg.startsWith("--context_param")) {
            String keyValue = arg.substring(16);
            int index = -1;
            if (keyValue != null && (index = keyValue.indexOf('=')) > -1) {
                if (fatherPid==null) {
                    context_param.put(keyValue.substring(0, index), replaceEscapeChars(keyValue.substring(index + 1)));
                } else { // the subjob won't escape the especial chars
                    context_param.put(keyValue.substring(0, index), keyValue.substring(index + 1) );
                }
            }
        } else if (arg.startsWith("--log4jLevel=")) {
            log4jLevel = arg.substring(13);
		} else if (arg.startsWith("--audit.enabled") && arg.contains("=")) {//for trunjob call
		    final int equal = arg.indexOf('=');
			final String key = arg.substring("--".length(), equal);
			System.setProperty(key, arg.substring(equal + 1));
		}
    }
    
    private static final String NULL_VALUE_EXPRESSION_IN_COMMAND_STRING_FOR_CHILD_JOB_ONLY = "<TALEND_NULL>";

    private final String[][] escapeChars = {
        {"\\\\","\\"},{"\\n","\n"},{"\\'","\'"},{"\\r","\r"},
        {"\\f","\f"},{"\\b","\b"},{"\\t","\t"}
        };
    private String replaceEscapeChars (String keyValue) {

		if (keyValue == null || ("").equals(keyValue.trim())) {
			return keyValue;
		}

		StringBuilder result = new StringBuilder();
		int currIndex = 0;
		while (currIndex < keyValue.length()) {
			int index = -1;
			// judege if the left string includes escape chars
			for (String[] strArray : escapeChars) {
				index = keyValue.indexOf(strArray[0],currIndex);
				if (index>=0) {

					result.append(keyValue.substring(currIndex, index + strArray[0].length()).replace(strArray[0], strArray[1]));
					currIndex = index + strArray[0].length();
					break;
				}
			}
			// if the left string doesn't include escape chars, append the left into the result
			if (index < 0) {
				result.append(keyValue.substring(currIndex));
				currIndex = currIndex + keyValue.length();
			}
		}

		return result.toString();
    }

    public Integer getErrorCode() {
        return errorCode;
    }


    public String getStatus() {
        return status;
    }

    ResumeUtil resumeUtil = null;
}
/************************************************************************************************
 *     678897 characters generated by Talend Open Studio for ESB 
 *     on the 9 avril 2023 à 07:34:47 CEST
 ************************************************************************************************/